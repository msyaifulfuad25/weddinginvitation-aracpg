<?php
	include "data-sap.php";
	include "data-client.php";

    $url_save_sap = $base_url.$uri_couple.'/save-sap.php';
    $url_get_sap = $base_url.$uri_couple.'/get-sap.php';
	// Untuk Development Product
	$asset_theme = $base_url.'product/diamond-detail/diamond1/preview/assets/';
	// Untuk Production
	// $asset_theme = $base_url.'satria-erika/assets/';
	$theme_name = 'Ara Diamond 1';
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
  <head>
    <meta charset="UTF-8">
    <style type="text/css">
      .wdp-comment-text img {
        max-width: 100% !important;
      }
    </style>
    <!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
    <title>Unity Diamond 1</title>
    <meta name="description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="Unity Diamond 1" />
    <meta property="og:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta property="og:url" content="https://unityinvitation.com/unity-diamond1/" />
    <meta property="og:site_name" content="Unity Invitation" />
    <meta property="article:section" content="Undangan" />
    <meta property="og:updated_time" content="2022-05-19T22:43:58+07:00" />
    <meta property="og:image" content="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-cover-invitation.jpg" />
    <meta property="og:image:secure_url" content="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-cover-invitation.jpg" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="500" />
    <meta property="og:image:alt" content="Unity Diamond 1" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Unity Diamond 1" />
    <meta name="twitter:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="twitter:image" content="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-cover-invitation.jpg" />
    <meta name="twitter:label1" content="Written by" />
    <meta name="twitter:data1" content="admin" />
    <meta name="twitter:label2" content="Time to read" />
    <meta name="twitter:data2" content="2 minutes" />
    <script type="application/ld+json" class="rank-math-schema">
      {
        "@context": "https://schema.org",
        "@graph": [{
          "@type": "BreadcrumbList",
          "@id": "https://unityinvitation.com/unity-diamond1/#breadcrumb",
          "itemListElement": [{
            "@type": "ListItem",
            "position": "1",
            "item": {
              "@id": "https://unityinvitation.com",
              "name": "Home"
            }
          }, {
            "@type": "ListItem",
            "position": "2",
            "item": {
              "@id": "https://unityinvitation.com/unity-diamond1/",
              "name": "Unity Diamond 1"
            }
          }]
        }]
      }
    </script>
    <!-- /Rank Math WordPress SEO plugin -->
    <link rel='dns-prefetch' href='//use.fontawesome.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Feed" href="https://unityinvitation.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Comments Feed" href="https://unityinvitation.com/comments/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Unity Diamond 1 Comments Feed" href="https://unityinvitation.com/unity-diamond1/feed/" />
    <script type="text/javascript">
      window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/",
        "svgExt": ".svg",
        "source": {
          "concatemoji": "https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.0"
        }
      };
      /*! This file is auto-generated */
      ! function(e, a, t) {
        var n, r, o, i = a.createElement("canvas"),
          p = i.getContext && i.getContext("2d");

        function s(e, t) {
          var a = String.fromCharCode,
            e = (p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0), i.toDataURL());
          return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
        }

        function c(e) {
          var t = a.createElement("script");
          t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
        }
        for (o = Array("flag", "emoji"), t.supports = {
            everything: !0,
            everythingExceptFlag: !0
          }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
          if (!p || !p.fillText) return !1;
          switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
            case "flag":
              return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
            case "emoji":
              return !s([129777, 127995, 8205, 129778, 127999], [129777, 127995, 8203, 129778, 127999])
          }
          return !1
        }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
        t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
          t.DOMReady = !0
        }, t.supports.everything || (n = function() {
          t.readyCallback()
        }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
          "complete" === a.readyState && t.readyCallback()
        })), (e = t.source || {}).concatemoji ? c(e.concatemoji) : e.wpemoji && e.twemoji && (c(e.twemoji), c(e.wpemoji)))
      }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 0.07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
    </style>
    <link rel='stylesheet' id='bdt-uikit-css' href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
    <link rel='stylesheet' id='ep-helper-css' href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css' href='https://unityinvitation.com/wp-includes/css/dist/block-library/style.min.css?ver=6.0' type='text/css' media='all' />
    <style id='global-styles-inline-css' type='text/css'>
      body {
        --wp--preset--color--black: #000000;
        --wp--preset--color--cyan-bluish-gray: #abb8c3;
        --wp--preset--color--white: #ffffff;
        --wp--preset--color--pale-pink: #f78da7;
        --wp--preset--color--vivid-red: #cf2e2e;
        --wp--preset--color--luminous-vivid-orange: #ff6900;
        --wp--preset--color--luminous-vivid-amber: #fcb900;
        --wp--preset--color--light-green-cyan: #7bdcb5;
        --wp--preset--color--vivid-green-cyan: #00d084;
        --wp--preset--color--pale-cyan-blue: #8ed1fc;
        --wp--preset--color--vivid-cyan-blue: #0693e3;
        --wp--preset--color--vivid-purple: #9b51e0;
        --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
        --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
        --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
        --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
        --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
        --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
        --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
        --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
        --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
        --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
        --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
        --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
        --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
        --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
        --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
        --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
        --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
        --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
        --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
        --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
        --wp--preset--font-size--small: 13px;
        --wp--preset--font-size--medium: 20px;
        --wp--preset--font-size--large: 36px;
        --wp--preset--font-size--x-large: 42px;
      }

      .has-black-color {
        color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-color {
        color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-color {
        color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-color {
        color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-color {
        color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-color {
        color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-color {
        color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-color {
        color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-color {
        color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-color {
        color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-color {
        color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-color {
        color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-background-color {
        background-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-background-color {
        background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-background-color {
        background-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-background-color {
        background-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-background-color {
        background-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-background-color {
        background-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-background-color {
        background-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-background-color {
        background-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-background-color {
        background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-background-color {
        background-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-border-color {
        border-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-border-color {
        border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-border-color {
        border-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-border-color {
        border-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-border-color {
        border-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-border-color {
        border-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-border-color {
        border-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-border-color {
        border-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-border-color {
        border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-border-color {
        border-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
        background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
      }

      .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
        background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
      }

      .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-orange-to-vivid-red-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
      }

      .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
        background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
      }

      .has-cool-to-warm-spectrum-gradient-background {
        background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
      }

      .has-blush-light-purple-gradient-background {
        background: var(--wp--preset--gradient--blush-light-purple) !important;
      }

      .has-blush-bordeaux-gradient-background {
        background: var(--wp--preset--gradient--blush-bordeaux) !important;
      }

      .has-luminous-dusk-gradient-background {
        background: var(--wp--preset--gradient--luminous-dusk) !important;
      }

      .has-pale-ocean-gradient-background {
        background: var(--wp--preset--gradient--pale-ocean) !important;
      }

      .has-electric-grass-gradient-background {
        background: var(--wp--preset--gradient--electric-grass) !important;
      }

      .has-midnight-gradient-background {
        background: var(--wp--preset--gradient--midnight) !important;
      }

      .has-small-font-size {
        font-size: var(--wp--preset--font-size--small) !important;
      }

      .has-medium-font-size {
        font-size: var(--wp--preset--font-size--medium) !important;
      }

      .has-large-font-size {
        font-size: var(--wp--preset--font-size--large) !important;
      }

      .has-x-large-font-size {
        font-size: var(--wp--preset--font-size--x-large) !important;
      }
    </style>
    <link rel='stylesheet' id='WEDKU_STYLE-css' href='https://unityinvitation.com/wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.18' type='text/css' media='all' />
    <link rel='stylesheet' id='pafe-extension-style-css' href='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp_style-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
    <style id='wdp_style-inline-css' type='text/css'>
      .wdp-wrapper {
        font-size: 14px
      }

      .wdp-wrapper {
        background: #ffffff;
      }

      .wdp-wrapper.wdp-border {
        border: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-comments a:link,
      .wdp-wrapper .wdp-wrap-comments a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-form {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {
        border-color: #64B6EC;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {
        color: #FFFFFF;
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {
        background: #a3a3a3;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {
        color: #44525F;
      }

      .wdp-wrapper .wdp-media-btns a>span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-media-btns a>span:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-comment-status {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {
        color: #319342;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {
        color: #C85951;
      }

      .wdp-wrapper .wdp-comment-status.wdp-loading>span {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border-bottom: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {
        color: #54595f !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {
        color: #2a5782 !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {
        color: #2a5782;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span:hover {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {
        color: #2C9E48;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {
        color: #D13D3D;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-counter-info {
        color: #9DA8B7;
      }

      .wdp-wrapper .wdp-holder span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a,
      .wdp-wrapper .wdp-holder a:link,
      .wdp-wrapper .wdp-holder a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a:hover,
      .wdp-wrapper .wdp-holder a:link:hover,
      .wdp-wrapper .wdp-holder a:visited:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {
        max-width: 28px;
        max-height: 28px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {
        margin-left: 38px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {
        max-width: 24px;
        max-height: 24px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {
        max-width: 21px;
        max-height: 21px;
      }
    </style>
    <link rel='stylesheet' id='wdp-centered-css-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-horizontal-css-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-fontello-css-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='exad-main-style-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-css' href='https://unityinvitation.com/wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-theme-style-css' href='https://unityinvitation.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-css' href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-skin-css' href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1399-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-1399.css?ver=1655470780' type='text/css' media='all' />
    <link rel='stylesheet' id='powerpack-frontend-css' href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='weddingpress-wdp-css' href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css' href='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='uael-frontend-css' href='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-tabs-frontend-css' href='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
    <!-- <link rel='stylesheet' id='elementor-post-29221-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-29221.css?ver=1655472402' type='text/css' media='all' /> -->
	<style>
		.elementor-29221
		.elementor-element.elementor-element-7cf4f909
		.elementor-background-slideshow__slide__image {
		background-position: center center;
		}
		.elementor-29221
		.elementor-element.elementor-element-7cf4f909
		> .elementor-background-overlay {
		background-color: transparent;
		background-image: linear-gradient(180deg, #010101a6 0%, #01010133 70%);
		opacity: 1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-7cf4f909 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 0px 0px 40px 0px;
		z-index: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-728c9a7e
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 350px 0px 300px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-728c9a7e
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-4b31d14d {
		text-align: center;
		z-index: 2;
		}
		.elementor-29221
		.elementor-element.elementor-element-4b31d14d
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 28px;
		font-weight: 100;
		letter-spacing: 6px;
		}
		.elementor-29221 .elementor-element.elementor-element-7e4a891f {
		text-align: center;
		z-index: 2;
		}
		.elementor-29221
		.elementor-element.elementor-element-7e4a891f
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		line-height: 0.8em;
		}
		.elementor-29221 .elementor-element.elementor-element-8e8ef10 {
		--divider-border-style: solid;
		--divider-color: #ffffff6e;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-8e8ef10
		.elementor-divider-separator {
		width: 35%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-8e8ef10
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-29221 .elementor-element.elementor-element-102383d3 {
		text-align: center;
		z-index: 2;
		}
		.elementor-29221
		.elementor-element.elementor-element-102383d3
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 40px;
		font-weight: 100;
		letter-spacing: 3px;
		}
		.elementor-29221
		.elementor-element.elementor-element-102383d3
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-d5f07cc.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff57;
		}
		.elementor-29221
		.elementor-element.elementor-element-d5f07cc.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-d5f07cc.elementor-view-default
		.elementor-icon {
		fill: #ffffff57;
		color: #ffffff57;
		border-color: #ffffff57;
		}
		.elementor-29221 .elementor-element.elementor-element-d5f07cc {
		--icon-box-icon-margin: 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-d5f07cc .elementor-icon {
		font-size: 25px;
		}
		.elementor-29221
		.elementor-element.elementor-element-d5f07cc
		.elementor-icon-box-title {
		color: #ffffff57;
		}
		.elementor-29221
		.elementor-element.elementor-element-d5f07cc
		.elementor-icon-box-title,
		.elementor-29221
		.elementor-element.elementor-element-d5f07cc
		.elementor-icon-box-title
		a {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: 400;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1a367b:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-1a1a367b
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #ffffff;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1a367b
		> .elementor-background-overlay {
		background-color: #ffffff00;
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Flower-Grey-Patterns.png");
		background-size: 50% auto;
		opacity: 0.1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-1a1a367b {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 0px 200px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-fe624d {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-fe624d
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 44px;
		font-weight: 500;
		}
		.elementor-29221 .elementor-element.elementor-element-3a7179cb {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-3a7179cb
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		line-height: 1em;
		}
		.elementor-29221
		.elementor-element.elementor-element-3a7179cb
		> .elementor-widget-container {
		margin: -20px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-427e4066 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-427e4066
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-427e4066
		> .elementor-widget-container {
		margin: 0px 200px 0px 200px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-591a2dee
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5fd7e859
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-29221 .elementor-element.elementor-element-5fd7e859 {
		padding: 0px 0px 0px 0px;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-6a801380.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a801380.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a801380
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a801380
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-6a801380
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-6a801380
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a801380
		> .elementor-element-populated {
		margin: 0px 50px 0px 50px;
		--e-column-margin-right: 50px;
		--e-column-margin-left: 50px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-21bb3e65
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-3a678b55:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-29221
		.elementor-element.elementor-element-3a678b55
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-3a678b55
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-3a678b55
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-3a678b55
		> .elementor-background-slideshow {
		border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-3a678b55
		> .elementor-element-populated {
		box-shadow: -2px 10px 20px -10px rgba(0, 0, 0, 0.57);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 30px 80px 50px 80px;
		--e-column-margin-right: 80px;
		--e-column-margin-left: 80px;
		padding: 10px 10px 20px 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-3a678b55
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-236d9d56 {
		text-align: center;
		}
		.elementor-29221 .elementor-element.elementor-element-236d9d56 img {
		border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221 .elementor-element.elementor-element-37037945 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-37037945
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		line-height: 0.8em;
		}
		.elementor-29221
		.elementor-element.elementor-element-37037945
		> .elementor-widget-container {
		margin: -150px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-644cb4f {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-644cb4f
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 500;
		}
		.elementor-29221 .elementor-element.elementor-element-50487b2d {
		--divider-border-style: solid;
		--divider-color: #6e6e6e;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-50487b2d
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-50487b2d
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-29221 .elementor-element.elementor-element-6a818b23 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a818b23
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: bold;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a818b23
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1b737c4d {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1b737c4d
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 100;
		line-height: 1.2em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1b737c4d
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-605b6ae8 {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 35px;
		--grid-column-gap: 15px;
		--grid-row-gap: 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-605b6ae8
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-605b6ae8
		.elementor-social-icon {
		background-color: #02010100;
		}
		.elementor-29221
		.elementor-element.elementor-element-605b6ae8
		.elementor-social-icon
		i {
		color: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221
		.elementor-element.elementor-element-605b6ae8
		.elementor-social-icon
		svg {
		fill: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221 .elementor-element.elementor-element-605b6ae8 .elementor-icon {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4b70fd63
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 0px 0px 200px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-46754259 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-46754259
		.elementor-heading-title {
		color: #6e6e6e85;
		font-family: "Playfair Display", Sans-serif;
		font-size: 72px;
		font-weight: 100;
		}
		.elementor-29221
		.elementor-element.elementor-element-46754259
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-555a6a47.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-555a6a47.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-555a6a47
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-555a6a47
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-555a6a47
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-555a6a47
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-555a6a47
		> .elementor-element-populated {
		margin: 0px 50px 0px 50px;
		--e-column-margin-right: 50px;
		--e-column-margin-left: 50px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-435ce5c4
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-64c67a:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-29221
		.elementor-element.elementor-element-64c67a
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-64c67a
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-64c67a
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-64c67a
		> .elementor-background-slideshow {
		border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-64c67a
		> .elementor-element-populated {
		box-shadow: 2px 10px 20px -10px rgba(0, 0, 0, 0.57);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin: 30px 80px 50px 80px;
		--e-column-margin-right: 80px;
		--e-column-margin-left: 80px;
		padding: 10px 10px 30px 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-64c67a
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-15ff036 {
		text-align: center;
		}
		.elementor-29221 .elementor-element.elementor-element-15ff036 img {
		border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221 .elementor-element.elementor-element-35b5887b {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-35b5887b
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		line-height: 0.8em;
		}
		.elementor-29221
		.elementor-element.elementor-element-35b5887b
		> .elementor-widget-container {
		margin: -150px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1d57a933 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d57a933
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 500;
		}
		.elementor-29221 .elementor-element.elementor-element-6456bdb9 {
		--divider-border-style: solid;
		--divider-color: #6e6e6e;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6456bdb9
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-6456bdb9
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-29221 .elementor-element.elementor-element-3505686e {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-3505686e
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: bold;
		}
		.elementor-29221
		.elementor-element.elementor-element-3505686e
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-8f8fb6d {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-8f8fb6d
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 100;
		line-height: 1.2em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-8f8fb6d
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-17f49bae {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 35px;
		--grid-column-gap: 15px;
		--grid-row-gap: 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-17f49bae
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-17f49bae
		.elementor-social-icon {
		background-color: #02010100;
		}
		.elementor-29221
		.elementor-element.elementor-element-17f49bae
		.elementor-social-icon
		i {
		color: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221
		.elementor-element.elementor-element-17f49bae
		.elementor-social-icon
		svg {
		fill: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221 .elementor-element.elementor-element-17f49bae .elementor-icon {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4f80d344
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-466bd400:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-466bd400
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-3.jpg");
		background-position: bottom center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		.elementor-29221
		.elementor-element.elementor-element-466bd400
		> .elementor-background-overlay {
		background-color: #232323;
		opacity: 0.77;
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-466bd400 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 200px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5ea45641
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-5ea45641
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-5ea45641
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5ea45641
		> .elementor-element-populated {
		padding: 100px 50px 50px 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6858d65a:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-6858d65a
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #ffffff12;
		}
		.elementor-29221 .elementor-element.elementor-element-6858d65a {
		border-style: solid;
		border-width: 1.5px 0px 0px 1px;
		border-color: #ffffff2e;
		box-shadow: 0px 0px 40px -20px rgba(0, 0, 0, 0.9);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-6858d65a,
		.elementor-29221
		.elementor-element.elementor-element-6858d65a
		> .elementor-background-overlay {
		border-radius: 800px 800px 150px 150px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6858d65a
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-40aaa8d5
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-40aaa8d5
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-40aaa8d5
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-40aaa8d5
		> .elementor-element-populated {
		padding: 150px 100px 150px 100px;
		}
		.elementor-29221 .elementor-element.elementor-element-1ed77922 img {
		width: 25%;
		}
		.elementor-29221 .elementor-element.elementor-element-6478e5e0 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6478e5e0
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-6478e5e0
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-43fcde86 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-43fcde86
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-43fcde86
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1d2a37.bdt-countdown--label-block
		.bdt-countdown-number {
		margin-bottom: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1d2a37.bdt-countdown--label-inline
		.bdt-countdown-number {
		margin-right: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1d2a37
		.bdt-countdown-wrapper {
		max-width: 90%;
		margin-left: auto;
		margin-right: auto;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1d2a37
		.bdt-countdown-item {
		border-radius: 0px 0px 0px 0px;
		padding: 30px 0px 30px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1d2a37
		.bdt-countdown-number {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 100;
		line-height: 1em;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a1d2a37
		.bdt-countdown-label {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 100;
		line-height: 1em;
		}
		.elementor-29221 .elementor-element.elementor-element-677b0c79 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-677b0c79
		.elementor-heading-title {
		color: #ffffff;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 100;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-677b0c79
		> .elementor-widget-container {
		margin: 0px 50px 0px 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-12cdca2e {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-12cdca2e
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: bold;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-12cdca2e
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-2d3a937c
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-24a218b6:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-24a218b6
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-slide3.jpg");
		background-position: top center;
		background-size: cover;
		}
		.elementor-29221
		.elementor-element.elementor-element-24a218b6
		> .elementor-background-overlay {
		background-color: #3b3a3a;
		opacity: 0.75;
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-24a218b6,
		.elementor-29221
		.elementor-element.elementor-element-24a218b6
		> .elementor-background-overlay {
		border-radius: 150px 150px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-24a218b6 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4508cd8f
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-4508cd8f
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-4508cd8f
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4508cd8f
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 300px 100px 70px 100px;
		}
		.elementor-29221 .elementor-element.elementor-element-3660a856 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-3660a856
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-3660a856
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-7a0bb1be {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-7a0bb1be
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		line-height: 1em;
		}
		.elementor-29221
		.elementor-element.elementor-element-7a0bb1be
		> .elementor-widget-container {
		margin: -10px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-444f26bc {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-444f26bc
		.elementor-heading-title {
		color: #ffffff;
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-444f26bc
		> .elementor-widget-container {
		margin: 0px 200px 0px 200px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-b3aa9bf
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-b3aa9bf:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-b3aa9bf
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221 .elementor-element.elementor-element-b3aa9bf,
		.elementor-29221
		.elementor-element.elementor-element-b3aa9bf
		> .elementor-background-overlay {
		border-radius: 0px 0px 150px 150px;
		}
		.elementor-29221 .elementor-element.elementor-element-b3aa9bf {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 0px 50px 100px 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-b3aa9bf
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-6d048667.elementor-column
		.elementor-widget-wrap {
		align-items: flex-start;
		}
		.elementor-29221
		.elementor-element.elementor-element-6d048667.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: flex-start;
		align-items: flex-start;
		}
		.elementor-29221
		.elementor-element.elementor-element-6d048667
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6d048667
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-6d048667
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-6d048667
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6d048667
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 70px 0px 20px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0.elementor-view-stacked
		.elementor-icon {
		background-color: #8d8d8d;
		}
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0.elementor-view-default
		.elementor-icon {
		color: #8d8d8d;
		border-color: #8d8d8d;
		}
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0.elementor-view-default
		.elementor-icon
		svg {
		fill: #8d8d8d;
		}
		.elementor-29221 .elementor-element.elementor-element-1c09a6b0 .elementor-icon {
		font-size: 100px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-29221
		.elementor-element.elementor-element-1c09a6b0
		> .elementor-widget-container {
		margin: 0px 0px 20px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1e91f4df {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1e91f4df
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		line-height: 0.7em;
		}
		.elementor-29221
		.elementor-element.elementor-element-1e91f4df
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5306891e {
		--divider-border-style: solid;
		--divider-color: #6e6e6e66;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5306891e
		.elementor-divider-separator {
		width: 75%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-5306891e
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-7f7cdcb9 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-7f7cdcb9
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-7f7cdcb9
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4d9f815b
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-29221 .elementor-element.elementor-element-4d9f815b {
		margin-top: 10px;
		margin-bottom: 0px;
		padding: 0px 60px 0px 60px;
		}
		.elementor-29221
		.elementor-element.elementor-element-602debfa
		> .elementor-element-populated {
		padding: 0px 10px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-77308cdf {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-77308cdf
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-77308cdf
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-356a564e
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-356a564e
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-44da5773
		.elementor-counter-number-wrapper {
		color: #6e6e6e;
		font-family: "Libre Baskerville", Sans-serif;
		font-size: 72px;
		font-weight: 100;
		}
		.elementor-29221
		.elementor-element.elementor-element-44da5773
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5900fb27
		> .elementor-element-populated {
		padding: 0px 0px 0px 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-1fc116a9 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1fc116a9
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-1fc116a9
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-772ae0 {
		--divider-border-style: solid;
		--divider-color: #6e6e6e66;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-772ae0
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-772ae0
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-29221 .elementor-element.elementor-element-1046017f {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1046017f
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-1046017f
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-19f8024f
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab.elementor-view-stacked
		.elementor-icon {
		background-color: #573c12;
		}
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab.elementor-view-default
		.elementor-icon {
		color: #573c12;
		border-color: #573c12;
		}
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab.elementor-view-default
		.elementor-icon
		svg {
		fill: #573c12;
		}
		.elementor-29221 .elementor-element.elementor-element-4ab09ab .elementor-icon {
		font-size: 30px;
		}
		.elementor-29221 .elementor-element.elementor-element-4ab09ab .elementor-icon i,
		.elementor-29221
		.elementor-element.elementor-element-4ab09ab
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-29221 .elementor-element.elementor-element-56cf1ac1 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-56cf1ac1
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 600;
		line-height: 1.3em;
		}
		.elementor-29221
		.elementor-element.elementor-element-56cf1ac1
		> .elementor-widget-container {
		margin: 10px 0px 20px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-677da5ec {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-677da5ec
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		}
		.elementor-29221
		.elementor-element.elementor-element-677da5ec
		> .elementor-widget-container {
		margin: 0px 50px 0px 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: normal;
		letter-spacing: 1px;
		background-color: #6e6e6e;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: #6e6e6e;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 10px -5px rgba(1, 1, 1, 0.32);
		padding: 10px 20px 10px 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button:focus {
		color: #6e6e6e;
		background-color: #127c9400;
		border-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		.elementor-button:focus
		svg {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-22c32e91
		> .elementor-widget-container {
		margin: 20px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-22c32e91 {
		z-index: 2;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84.elementor-column
		.elementor-widget-wrap {
		align-items: flex-start;
		}
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: flex-start;
		align-items: flex-start;
		}
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-3fae3d84
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 70px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-16cdd076
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-16cdd076.elementor-view-stacked
		.elementor-icon {
		background-color: #8d8d8d;
		}
		.elementor-29221
		.elementor-element.elementor-element-16cdd076.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-16cdd076.elementor-view-default
		.elementor-icon {
		color: #8d8d8d;
		border-color: #8d8d8d;
		}
		.elementor-29221
		.elementor-element.elementor-element-16cdd076.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-16cdd076.elementor-view-default
		.elementor-icon
		svg {
		fill: #8d8d8d;
		}
		.elementor-29221 .elementor-element.elementor-element-16cdd076 .elementor-icon {
		font-size: 110px;
		}
		.elementor-29221
		.elementor-element.elementor-element-16cdd076
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-16cdd076
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-29221
		.elementor-element.elementor-element-16cdd076
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5f6ee1f2 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-5f6ee1f2
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		line-height: 0.7em;
		}
		.elementor-29221
		.elementor-element.elementor-element-5f6ee1f2
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6f6be265 {
		--divider-border-style: solid;
		--divider-color: #6e6e6e66;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f6be265
		.elementor-divider-separator {
		width: 75%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f6be265
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-598c1545 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-598c1545
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-598c1545
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5f855133
		> .elementor-container
		> .elementor-column
		> .elementor-widget-wrap {
		align-content: center;
		align-items: center;
		}
		.elementor-29221 .elementor-element.elementor-element-5f855133 {
		margin-top: 10px;
		margin-bottom: 0px;
		padding: 0px 60px 0px 60px;
		}
		.elementor-29221
		.elementor-element.elementor-element-222c8a37
		> .elementor-element-populated {
		padding: 0px 10px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-722a84f1 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-722a84f1
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-722a84f1
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-30c1924f
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-30c1924f
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-762cd999
		.elementor-counter-number-wrapper {
		color: #6e6e6e;
		font-family: "Libre Baskerville", Sans-serif;
		font-size: 72px;
		font-weight: 100;
		}
		.elementor-29221
		.elementor-element.elementor-element-762cd999
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6b81e11a
		> .elementor-element-populated {
		padding: 0px 0px 0px 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-5dc7b685 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-5dc7b685
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-5dc7b685
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-51b13c5b {
		--divider-border-style: solid;
		--divider-color: #6e6e6e66;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-51b13c5b
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-51b13c5b
		.elementor-divider {
		text-align: center;
		padding-top: 15px;
		padding-bottom: 15px;
		}
		.elementor-29221 .elementor-element.elementor-element-56d3e677 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-56d3e677
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-56d3e677
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6b4b92bb
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-29221
		.elementor-element.elementor-element-f46ae10
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-f46ae10.elementor-view-stacked
		.elementor-icon {
		background-color: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221
		.elementor-element.elementor-element-f46ae10.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-f46ae10.elementor-view-default
		.elementor-icon {
		color: var(--e-global-color-1ac7aadb);
		border-color: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221
		.elementor-element.elementor-element-f46ae10.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-f46ae10.elementor-view-default
		.elementor-icon
		svg {
		fill: var(--e-global-color-1ac7aadb);
		}
		.elementor-29221 .elementor-element.elementor-element-f46ae10 .elementor-icon {
		font-size: 30px;
		}
		.elementor-29221 .elementor-element.elementor-element-f46ae10 .elementor-icon i,
		.elementor-29221
		.elementor-element.elementor-element-f46ae10
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-29221 .elementor-element.elementor-element-34d67635 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-34d67635
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 600;
		line-height: 1.3em;
		}
		.elementor-29221
		.elementor-element.elementor-element-34d67635
		> .elementor-widget-container {
		margin: 10px 0px 20px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-b4301f4 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4301f4
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4301f4
		> .elementor-widget-container {
		margin: 0px 50px 0px 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: normal;
		letter-spacing: 1px;
		background-color: #6e6e6e;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: #6e6e6e;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 10px -5px rgba(1, 1, 1, 0.32);
		padding: 10px 20px 10px 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button:focus {
		color: #6e6e6e;
		background-color: #127c9400;
		border-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		.elementor-button:focus
		svg {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-14aeb4e3
		> .elementor-widget-container {
		margin: 20px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-14aeb4e3 {
		z-index: 2;
		}
		.elementor-29221
		.elementor-element.elementor-element-7ccd1ab1:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-7ccd1ab1
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-7ccd1ab1
		> .elementor-background-overlay {
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Flower-Grey-Patterns.png");
		background-size: 50% auto;
		opacity: 0.1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-7ccd1ab1 {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 200px 300px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-7ccd1ab1
		> .elementor-shape-bottom
		.elementor-shape-fill {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-7ccd1ab1
		> .elementor-shape-bottom
		svg {
		width: calc(200% + 1.3px);
		height: 100px;
		}
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4
		> .elementor-background-slideshow {
		border-radius: 150px 150px 150px 150px;
		}
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4
		> .elementor-element-populated {
		box-shadow: 0px 0px 85px -28px rgba(0, 0, 0, 0.52);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-e7be9d4
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-428322bd {
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-38e95752
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-38e95752
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-38e95752
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-38e95752
		> .elementor-background-slideshow {
		border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-38e95752
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 100px 50px 75px 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-1dfcb620 img {
		border-radius: 100px 100px 100px 100px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1dfcb620
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-793f88bd
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-e97facb {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-e97facb
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 34px;
		font-weight: 600;
		}
		.elementor-29221
		.elementor-element.elementor-element-e97facb
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6e9f69b9 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6e9f69b9
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-6e9f69b9
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1a8da0b1 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a8da0b1
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a8da0b1
		> .elementor-widget-container {
		margin: 10px 300px 10px 300px;
		}
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: normal;
		letter-spacing: 1px;
		background-color: #6e6e6e;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: #6e6e6e;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 10px -5px rgba(1, 1, 1, 0.32);
		padding: 10px 20px 10px 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button:focus {
		color: #6e6e6e;
		background-color: #127c9400;
		border-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		.elementor-button:focus
		svg {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-7328789b
		> .elementor-widget-container {
		margin: -10px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-7328789b {
		z-index: 2;
		}
		.elementor-29221
		.elementor-element.elementor-element-218bb78f:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-218bb78f
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #6e6e6e;
		}
		.elementor-29221 .elementor-element.elementor-element-218bb78f {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 200px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-218bb78f
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-41a663a3
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5c6efbd7
		.pp-image-slider-thumb-item {
		border-radius: 40px 40px 40px 40px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5c6efbd7
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-189c3182
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-7c6b3cc9 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-7c6b3cc9
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 48px;
		font-weight: 100;
		}
		.elementor-29221
		.elementor-element.elementor-element-7c6b3cc9
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-64fd4d3f {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-64fd4d3f
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-64fd4d3f
		> .elementor-widget-container {
		margin: -10px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-efbf8f3 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-efbf8f3
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-efbf8f3
		> .elementor-widget-container {
		margin: 0px 300px 0px 300px;
		}
		.elementor-29221
		.elementor-element.elementor-element-44641f65
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-36805275
		> .elementor-element-populated {
		margin: 0px 150px 0px 150px;
		--e-column-margin-right: 150px;
		--e-column-margin-left: 150px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-vertical
		.pp-timeline-item:not(:last-child) {
		margin-bottom: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-horizontal
		.pp-timeline-item {
		padding-left: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-horizontal
		.slick-list {
		margin-left: -15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.pp-timeline-card {
		padding: 30px 50px 50px 50px;
		background-color: #ffffff73;
		border-radius: 50px 50px 50px 50px;
		color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.pp-timeline-card-content {
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-card {
		text-align: left;
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-weight: 500;
		line-height: 2em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.pp-timeline-arrow {
		color: #ffffff73;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-card-image {
		margin-bottom: 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-card-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-card-title-wrap {
		margin-bottom: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.pp-timeline-item-active
		.pp-timeline-card,
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.slick-current
		.pp-timeline-card {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.pp-timeline-item-active
		.pp-timeline-arrow,
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline
		.slick-current
		.pp-timeline-arrow {
		color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-marker {
		font-size: 13px;
		width: 30px;
		height: 30px;
		color: #ffffff00;
		background-color: #c8ac8100;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-marker
		img {
		width: 13px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-connector-wrap {
		width: 30px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-navigation:before,
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-navigation
		.pp-slider-arrow {
		bottom: calc(30px / 2);
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-marker
		svg {
		fill: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-item-active
		.pp-timeline-marker,
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.slick-current
		.pp-timeline-marker {
		color: var(--e-global-color-secondary);
		background-color: #b9b9b9;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-item-active
		.pp-timeline-marker
		svg,
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.slick-current
		.pp-timeline-marker
		svg {
		fill: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-card-date {
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 600;
		line-height: 2.5em;
		color: #6e6e6e63;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-vertical.pp-timeline-left
		.pp-timeline-marker-wrapper {
		margin-right: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-vertical.pp-timeline-right
		.pp-timeline-marker-wrapper {
		margin-left: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-vertical.pp-timeline-center
		.pp-timeline-marker-wrapper {
		margin-left: 15px;
		margin-right: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-horizontal {
		margin-top: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-navigation
		.pp-timeline-card-date-wrapper {
		margin-bottom: 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-vertical
		.pp-timeline-connector {
		width: 3px;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-navigation:before {
		height: 3px;
		transform: translateY(calc(3px / 2));
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-connector,
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-navigation:before {
		background-color: #c8a46c00;
		}
		.elementor-29221
		.elementor-element.elementor-element-23cbaee0
		.pp-timeline-connector-inner {
		background-color: #ffffff45;
		}
		.elementor-29221
		.elementor-element.elementor-element-c92d65e
		> .elementor-container {
		max-width: 1500px;
		}
		.elementor-29221
		.elementor-element.elementor-element-c92d65e:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-c92d65e
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-c92d65e
		> .elementor-background-overlay {
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Flower-Grey-Patterns.png");
		background-size: 50% auto;
		opacity: 0.1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-c92d65e {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 300px 200px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-c92d65e
		> .elementor-shape-top
		.elementor-shape-fill {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-c92d65e
		> .elementor-shape-top
		svg {
		width: calc(200% + 1.3px);
		height: 100px;
		}
		.elementor-29221 .elementor-element.elementor-element-ae8c293 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-ae8c293
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Averia Serif Libre", Sans-serif;
		font-size: 48px;
		font-weight: normal;
		}
		.elementor-29221 .elementor-element.elementor-element-5318aa6f {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-5318aa6f
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-5318aa6f
		> .elementor-widget-container {
		margin: -10px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6cbd54e
		> .elementor-widget-container {
		margin: -15px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5f021551 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-5f021551
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5f021551
		> .elementor-widget-container {
		margin: 0px 350px 0px 350px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-38e611f4
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-0439cb7 {
		--image-transition-duration: 800ms;
		--overlay-transition-duration: 800ms;
		--content-text-align: center;
		--content-padding: 20px;
		--content-transition-duration: 800ms;
		--content-transition-delay: 800ms;
		}
		.elementor-29221
		.elementor-element.elementor-element-0439cb7
		.e-gallery-item:hover
		.elementor-gallery-item__overlay {
		background-color: rgba(0, 0, 0, 0.5);
		}
		.elementor-29221
		.elementor-element.elementor-element-12c93c09
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-2b0b4c57 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b0b4c57
		.elementor-heading-title {
		color: var(--e-global-color-text);
		font-family: "Averia Serif Libre", Sans-serif;
		font-size: 20px;
		line-height: 1.2em;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b0b4c57
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-3052aa50 img {
		width: 70%;
		}
		.elementor-29221
		.elementor-element.elementor-element-3052aa50
		> .elementor-widget-container {
		margin: 10px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4633138d {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 30px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4633138d
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4633138d
		.elementor-social-icon {
		background-color: rgba(129, 134, 213, 0);
		}
		.elementor-29221
		.elementor-element.elementor-element-4633138d
		.elementor-social-icon
		i {
		color: var(--e-global-color-text);
		}
		.elementor-29221
		.elementor-element.elementor-element-4633138d
		.elementor-social-icon
		svg {
		fill: var(--e-global-color-text);
		}
		.elementor-29221
		.elementor-element.elementor-element-d9c435e:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-d9c435e
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #6e6e6e;
		}
		.elementor-29221 .elementor-element.elementor-element-d9c435e {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 200px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-d9c435e
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-8b04011 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-8b04011
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 56px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-8b04011
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-f16af6b {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-f16af6b
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-f16af6b
		> .elementor-widget-container {
		margin: 0px 250px 0px 250px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-61770a3 {
		--divider-border-style: solid;
		--divider-color: #ffffff73;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-61770a3
		.elementor-divider-separator {
		width: 55%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-61770a3
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-bd26f85 {
		margin-top: 0px;
		margin-bottom: 30px;
		padding: 0px 250px 0px 250px;
		}
		.elementor-29221
		.elementor-element.elementor-element-669e9bc
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-29221
		.elementor-element.elementor-element-669e9bc
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-669e9bc
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-669e9bc
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-29221
		.elementor-element.elementor-element-289b8da
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-29221
		.elementor-element.elementor-element-289b8da
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-289b8da
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-289b8da
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-29221 .elementor-element.elementor-element-81c0746 {
		margin-top: 30px;
		margin-bottom: 0px;
		padding: 0px 250px 0px 250px;
		}
		.elementor-29221
		.elementor-element.elementor-element-f1bde03
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-29221
		.elementor-element.elementor-element-f1bde03
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-f1bde03
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-f1bde03
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6ca5e8d
		.elementor-image-box-wrapper
		.elementor-image-box-img {
		width: 25%;
		}
		.elementor-29221
		.elementor-element.elementor-element-6ca5e8d
		.elementor-image-box-img
		img {
		transition-duration: 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-6ca5e8d
		.elementor-image-box-title {
		margin-bottom: 10px;
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 500;
		}
		.elementor-29221
		.elementor-element.elementor-element-6ca5e8d
		.elementor-image-box-description {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		line-height: 1.5em;
		letter-spacing: 0.8px;
		}
		.elementor-29221
		.elementor-element.elementor-element-396a7c4c:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-396a7c4c
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-396a7c4c
		> .elementor-background-overlay {
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Flower-Grey-Patterns.png");
		background-size: 50% auto;
		opacity: 0.1;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-396a7c4c {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 200px 200px 200px 200px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4bec872f
		> .elementor-element-populated {
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-7024c86d {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-7024c86d
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 42px;
		font-weight: 500;
		line-height: 1.2em;
		}
		.elementor-29221
		.elementor-element.elementor-element-7024c86d
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-7315716b {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-7315716b
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-7315716b
		> .elementor-widget-container {
		margin: -10px 0px 20px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6c5963f4 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6c5963f4
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6c5963f4
		> .elementor-widget-container {
		margin: 0px 200px 0px 200px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-42df8cab
		.elementor-spacer-inner {
		--spacer-size: 25px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6629be3f
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-6629be3f
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-6629be3f
		> .elementor-background-slideshow {
		border-radius: 30px 30px 30px 30px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6629be3f
		> .elementor-element-populated {
		margin: 0px 150px 0px 150px;
		--e-column-margin-right: 150px;
		--e-column-margin-left: 150px;
		padding: 50px 50px 50px 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-64232eaa .wdp-wrapper {
		background-color: #ffffff00;
		border-radius: 40px 40px 40px 40px;
		padding: 0px 0px 15px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-komentar-cswd
		input[type="text"],
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-komentar-cswd
		select,
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-komentar-cswd
		textarea {
		font-family: "Lora", Sans-serif;
		font-weight: 500;
		line-height: 1em;
		background-color: #ffffff54;
		border-radius: 13px 13px 13px 13px;
		padding: 13px 10px 13px 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.wdp-wrap-link
		a {
		font-family: "Lora", Sans-serif;
		font-weight: 600;
		line-height: 1em;
		color: var(--e-global-color-text);
		}
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-submit
		input[type="submit"],
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-submit
		button {
		color: var(--e-global-color-secondary);
		font-family: var(--e-global-typography-text-font-family), Sans-serif;
		font-weight: var(--e-global-typography-text-font-weight);
		line-height: var(--e-global-typography-text-line-height);
		background-color: var(--e-global-color-text);
		border-style: solid;
		border-color: var(--e-global-color-text);
		border-radius: 13px 13px 13px 13px;
		padding: 7px 25px 7px 25px;
		}
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-submit
		input[type="submit"]:hover,
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		.form-submit
		button:hover {
		color: var(--e-global-color-text);
		background-color: #a8742300;
		border-color: var(--e-global-color-text);
		}
		.elementor-29221
		.elementor-element.elementor-element-64232eaa
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-d2c9bff
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-40c7e3a2 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-40c7e3a2
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-40c7e3a2
		> .elementor-widget-container {
		margin: 0px 200px 0px 200px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4f98db44 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4f98db44
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 25px;
		font-weight: 600;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4f98db44
		> .elementor-widget-container {
		margin: 0px 250px 0px 250px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6ca099be
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-3bda5dcc img {
		width: 20%;
		}
		.elementor-29221
		.elementor-element.elementor-element-3bda5dcc
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-101550d:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-101550d
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #6e6e6e;
		}
		.elementor-29221 .elementor-element.elementor-element-101550d {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 400px 200px 400px;
		}
		.elementor-29221
		.elementor-element.elementor-element-101550d
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d
		> .elementor-background-slideshow {
		border-radius: 150px 150px 150px 150px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d
		> .elementor-element-populated {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 150px 50px 150px 50px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1cdd7b4d
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-677f329
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-677f329.elementor-view-stacked
		.elementor-icon {
		background-color: var(--e-global-color-text);
		}
		.elementor-29221
		.elementor-element.elementor-element-677f329.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-677f329.elementor-view-default
		.elementor-icon {
		color: var(--e-global-color-text);
		border-color: var(--e-global-color-text);
		}
		.elementor-29221
		.elementor-element.elementor-element-677f329.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-677f329.elementor-view-default
		.elementor-icon
		svg {
		fill: var(--e-global-color-text);
		}
		.elementor-29221 .elementor-element.elementor-element-677f329 .elementor-icon i,
		.elementor-29221
		.elementor-element.elementor-element-677f329
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-29221 .elementor-element.elementor-element-63f74300 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-63f74300
		.elementor-heading-title {
		color: #6e6e6e;
		font-family: "NewYork", Sans-serif;
		font-size: 60px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-63f74300
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-45d8d0db {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-45d8d0db
		.elementor-heading-title {
		color: #000000;
		font-family: "Lora", Sans-serif;
		font-size: 22px;
		font-weight: 500;
		font-style: italic;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-45d8d0db
		> .elementor-widget-container {
		margin: 0px 75px 0px 75px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button
		.elementor-align-icon-right {
		margin-left: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button
		.elementor-align-icon-left {
		margin-right: 10px;
		}
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: 500;
		line-height: 1.3em;
		fill: var(--e-global-color-secondary);
		color: var(--e-global-color-secondary);
		background-color: #6e6e6e;
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: #6e6e6e;
		border-radius: 13px 13px 13px 13px;
		box-shadow: 0px 10px 15px -5px rgba(0, 0, 0, 0.39);
		padding: 10px 20px 10px 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button:focus {
		color: #6e6e6e;
		background-color: #af8e4f00;
		border-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		.elementor-button:focus
		svg {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-2c0910fc
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-56120178
		> .elementor-element-populated {
		margin: 0px 0px 0px 0px;
		--e-column-margin-right: 0px;
		--e-column-margin-left: 0px;
		padding: 70px 30px 40px 30px;
		}
		.elementor-29221 .elementor-element.elementor-element-d21e409 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-d21e409
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 42px;
		font-weight: 500;
		line-height: 1.2em;
		}
		.elementor-29221
		.elementor-element.elementor-element-d21e409
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-fc7d887 {
		--divider-border-style: solid;
		--divider-color: #a99f9e63;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-fc7d887
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-fc7d887
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-5c9b00bc img {
		width: 70%;
		}
		.elementor-29221
		.elementor-element.elementor-element-5c9b00bc
		> .elementor-widget-container {
		margin: 0px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-26a9485 .copy-content {
		color: #6e6e6e;
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: 400;
		line-height: 2em;
		}
		.elementor-29221 .elementor-element.elementor-element-26a9485 .head-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		a.elementor-button,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: 500;
		line-height: 1.3em;
		fill: var(--e-global-color-secondary);
		color: var(--e-global-color-secondary);
		background-color: #6e6e6e;
		border-radius: 13px 13px 13px 13px;
		padding: 10px 20px 10px 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		a.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		a.elementor-button:focus,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		.elementor-button:focus {
		color: #6e6e6e;
		background-color: #af8e4f00;
		border-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		a.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		a.elementor-button:focus
		svg,
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		.elementor-button:focus
		svg {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-26a9485
		.elementor-button {
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: #6e6e6e;
		box-shadow: 0px 10px 15px -5px rgba(0, 0, 0, 0.39);
		}
		.elementor-29221
		.elementor-element.elementor-element-dd488f9
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-69d1247 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-69d1247
		.elementor-heading-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 42px;
		font-weight: 500;
		line-height: 1.2em;
		}
		.elementor-29221
		.elementor-element.elementor-element-69d1247
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-9599d1c {
		--divider-border-style: solid;
		--divider-color: #a99f9e63;
		--divider-border-width: 2px;
		}
		.elementor-29221
		.elementor-element.elementor-element-9599d1c
		.elementor-divider-separator {
		width: 50%;
		margin: 0 auto;
		margin-center: 0;
		}
		.elementor-29221
		.elementor-element.elementor-element-9599d1c
		.elementor-divider {
		text-align: center;
		padding-top: 25px;
		padding-bottom: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-5775fc48 .copy-content {
		color: #6e6e6e;
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: normal;
		font-style: italic;
		line-height: 1.5em;
		}
		.elementor-29221 .elementor-element.elementor-element-5775fc48 .head-title {
		color: var(--e-global-color-1ac7aadb);
		font-family: "Lora", Sans-serif;
		font-size: 30px;
		font-weight: normal;
		}
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		a.elementor-button,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		.elementor-button {
		font-family: "Poppins", Sans-serif;
		font-size: 14px;
		font-weight: 500;
		line-height: 1.3em;
		fill: var(--e-global-color-secondary);
		color: var(--e-global-color-secondary);
		background-color: #6e6e6e;
		border-radius: 13px 13px 13px 13px;
		padding: 10px 20px 10px 20px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		a.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		a.elementor-button:focus,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		.elementor-button:focus {
		color: #6e6e6e;
		background-color: #af8e4f00;
		border-color: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		a.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		.elementor-button:hover
		svg,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		a.elementor-button:focus
		svg,
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		.elementor-button:focus
		svg {
		fill: #6e6e6e;
		}
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		.elementor-button {
		border-style: solid;
		border-width: 2px 2px 2px 2px;
		border-color: #6e6e6e;
		box-shadow: 0px 10px 15px -5px rgba(0, 0, 0, 0.39);
		}
		.elementor-29221
		.elementor-element.elementor-element-5775fc48
		> .elementor-widget-container {
		margin: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-ca2bb6f:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
		.elementor-element.elementor-element-ca2bb6f
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: var(--e-global-color-secondary);
		background-image: url("https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-4.jpg");
		background-position: top center;
		background-repeat: no-repeat;
		background-size: cover;
		}
		.elementor-29221
		.elementor-element.elementor-element-ca2bb6f
		> .elementor-background-overlay {
		background-color: #111110;
		opacity: 0.8;
		mix-blend-mode: multiply;
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-ca2bb6f {
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 200px 0px 200px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-63ab2925 {
		margin-top: 0px;
		margin-bottom: 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-3b1c9e35 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-3b1c9e35
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 36px;
		font-weight: 500;
		}
		.elementor-29221 .elementor-element.elementor-element-3fca0f55 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-3fca0f55
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		font-weight: normal;
		}
		.elementor-29221 .elementor-element.elementor-element-6a9ffa3 {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a9ffa3
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 500;
		line-height: 1.3em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6a9ffa3
		> .elementor-widget-container {
		margin: 0px 150px 0px 150px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5fd6546a {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-5fd6546a
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 500;
		line-height: 1.5em;
		letter-spacing: 0.5px;
		}
		.elementor-29221
		.elementor-element.elementor-element-5fd6546a
		> .elementor-widget-container {
		margin: -15px 0px 0px 0px;
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-419f7d39
		.elementor-spacer-inner {
		--spacer-size: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-8a8769a {
		padding: 0px 0px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-cdfe0ce
		> .elementor-element-populated {
		border-style: solid;
		border-width: 0px 1px 0px 0px;
		border-color: var(--e-global-color-secondary);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-cdfe0ce
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-20f93bb img {
		width: 60%;
		}
		.elementor-29221
		.elementor-element.elementor-element-20f93bb
		> .elementor-widget-container {
		margin: 0px 0px -15px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-02b20fd {
		--grid-template-columns: repeat(0, auto);
		--icon-size: 18px;
		}
		.elementor-29221
		.elementor-element.elementor-element-02b20fd
		.elementor-widget-container {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-02b20fd
		.elementor-social-icon {
		background-color: rgba(129, 134, 213, 0);
		--icon-padding: 0.3em;
		}
		.elementor-29221
		.elementor-element.elementor-element-02b20fd
		.elementor-social-icon
		i {
		color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-02b20fd
		.elementor-social-icon
		svg {
		fill: var(--e-global-color-secondary);
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-4be6dad.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4be6dad.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4be6dad
		> .elementor-widget-wrap
		> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
		margin-bottom: 7px;
		}
		.elementor-29221
		.elementor-element.elementor-element-4be6dad
		> .elementor-element-populated {
		border-style: solid;
		border-width: 0px 0px 0px 1px;
		border-color: var(--e-global-color-secondary);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		}
		.elementor-29221
		.elementor-element.elementor-element-4be6dad
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-29221 .elementor-element.elementor-element-6354765 img {
		width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-ce61bfc {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-ce61bfc
		.elementor-heading-title {
		color: var(--e-global-color-secondary);
		font-family: "Poppins", Sans-serif;
		font-size: 7px;
		font-weight: normal;
		line-height: 1.3em;
		}
		.elementor-29221
		.elementor-element.elementor-element-78da72d3
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-78da72d3
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-78da72d3
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-29221 .elementor-element.elementor-element-78da72d3 {
		width: initial;
		max-width: initial;
		bottom: 150px;
		}
		body:not(.rtl) .elementor-29221 .elementor-element.elementor-element-78da72d3 {
		left: 50px;
		}
		body.rtl .elementor-29221 .elementor-element.elementor-element-78da72d3 {
		right: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-dear {
		text-align: center;
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 18px;
		font-weight: 500;
		letter-spacing: 0.5px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-text {
		text-align: center;
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 16px;
		font-style: italic;
		letter-spacing: 1px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .overlayy {
		background-color: #010101;
		opacity: 0.75;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-name {
		margin-top: 20px;
		font-family: "Lora", Sans-serif;
		font-size: 24px;
		font-weight: 100;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .dwp-sesi {
		margin-top: 20px;
		font-family: "Poppins", Sans-serif;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .text_tambahan {
		margin-top: 25px;
		font-family: "Lora", Sans-serif;
		font-size: 20px;
		font-weight: 400;
		letter-spacing: 6px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-mempelai {
		margin-top: 5px;
		margin-bottom: 5px;
		font-family: "NewYork", Sans-serif;
		font-size: 72px;
		color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		.wdp-button-wrapper {
		margin-top: 40px;
		}
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		.elementor-image
		img {
		width: 10%;
		}
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		a.elementor-button,
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		.elementor-button {
		fill: #000000;
		color: #000000;
		font-family: "Poppins", Sans-serif;
		font-size: 12px;
		font-weight: 500;
		line-height: 1.3em;
		background-color: var(--e-global-color-secondary);
		border-radius: 13px 13px 13px 13px;
		padding: 7px 15px 7px 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		.elementor-button {
		border-style: solid;
		border-width: 1px 1px 1px 1px;
		border-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		a.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		.elementor-button:hover,
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		a.elementor-button:focus,
		.elementor-29221
		.elementor-element.elementor-element-111b2886
		.elementor-button:focus {
		color: var(--e-global-color-secondary);
		background-color: #ffffff00;
		border-color: var(--e-global-color-secondary);
		}
		.elementor-29221
		.elementor-element.elementor-element-4d44ac51
		> .elementor-container {
		max-width: 1500px;
		}
		.elementor-29221 .elementor-element.elementor-element-4d44ac51 {
		margin-top: -130px;
		margin-bottom: 0px;
		padding: 30px 500px 0px 500px;
		z-index: 998;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc:not(.elementor-motion-effects-element-type-background)
		> .elementor-widget-wrap,
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-widget-wrap
		> .elementor-motion-effects-container
		> .elementor-motion-effects-layer {
		background-color: #ffffff;
		background-image: url("https://unityinvitation.com/wp-content/uploads/2022/02/white-texture-3.jpg");
		background-size: cover;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-element-populated
		> .elementor-background-overlay {
		background-color: var(--e-global-color-secondary);
		opacity: 0.85;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-element-populated,
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-element-populated
		> .elementor-background-overlay,
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-background-slideshow {
		border-radius: 50px 50px 0px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-element-populated {
		box-shadow: 0px -5px 20px -5px rgba(0, 0, 0, 0.24);
		transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
		padding: 15px 0px 10px 0px;
		}
		.elementor-29221
		.elementor-element.elementor-element-b4e5efc
		> .elementor-element-populated
		> .elementor-background-overlay {
		transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-1a68644e.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1a68644e.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221 .elementor-element.elementor-element-1cfee501 img {
		width: 40%;
		border-radius: 100px 100px 100px 100px;
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-4c7d8168.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4c7d8168.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff00;
		color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-default
		.elementor-icon {
		color: #ffffff00;
		border-color: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-default
		.elementor-icon
		svg {
		fill: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-framed
		.elementor-icon {
		background-color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-stacked
		.elementor-icon
		svg {
		fill: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-stacked
		.elementor-icon:hover {
		background-color: #eeeeef00;
		color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-default
		.elementor-icon:hover {
		color: #eeeeef00;
		border-color: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-default
		.elementor-icon:hover
		svg {
		fill: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-framed
		.elementor-icon:hover {
		background-color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234.elementor-view-stacked
		.elementor-icon:hover
		svg {
		fill: #474746;
		}
		.elementor-29221 .elementor-element.elementor-element-1d170234 .elementor-icon {
		font-size: 40px;
		padding: 0px;
		border-radius: 15px 15px 15px 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-1d170234
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-1d170234
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-2a2acf80.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-2a2acf80.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff00;
		color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-default
		.elementor-icon {
		color: #ffffff00;
		border-color: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-default
		.elementor-icon
		svg {
		fill: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-framed
		.elementor-icon {
		background-color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-stacked
		.elementor-icon
		svg {
		fill: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-stacked
		.elementor-icon:hover {
		background-color: #eeeeef00;
		color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-default
		.elementor-icon:hover {
		color: #eeeeef00;
		border-color: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-default
		.elementor-icon:hover
		svg {
		fill: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-framed
		.elementor-icon:hover {
		background-color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95.elementor-view-stacked
		.elementor-icon:hover
		svg {
		fill: #474746;
		}
		.elementor-29221 .elementor-element.elementor-element-10ceca95 .elementor-icon {
		font-size: 40px;
		padding: 0px;
		border-radius: 15px 15px 15px 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-10ceca95
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-10ceca95
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-6bebb4a.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6bebb4a.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff00;
		color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-default
		.elementor-icon {
		color: #ffffff00;
		border-color: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-default
		.elementor-icon
		svg {
		fill: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-framed
		.elementor-icon {
		background-color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-stacked
		.elementor-icon
		svg {
		fill: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-stacked
		.elementor-icon:hover {
		background-color: #eeeeef00;
		color: #18332c;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-default
		.elementor-icon:hover {
		color: #eeeeef00;
		border-color: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-default
		.elementor-icon:hover
		svg {
		fill: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-framed
		.elementor-icon:hover {
		background-color: #18332c;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792.elementor-view-stacked
		.elementor-icon:hover
		svg {
		fill: #18332c;
		}
		.elementor-29221 .elementor-element.elementor-element-6f1e2792 .elementor-icon {
		font-size: 35px;
		padding: 0px;
		border-radius: 15px 15px 15px 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-6f1e2792
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-4b6b46c7.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-4b6b46c7.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff00;
		color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-default
		.elementor-icon {
		color: #ffffff00;
		border-color: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-default
		.elementor-icon
		svg {
		fill: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-framed
		.elementor-icon {
		background-color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-stacked
		.elementor-icon
		svg {
		fill: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-stacked
		.elementor-icon:hover {
		background-color: #eeeeef00;
		color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-default
		.elementor-icon:hover {
		color: #eeeeef00;
		border-color: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-default
		.elementor-icon:hover
		svg {
		fill: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-framed
		.elementor-icon:hover {
		background-color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe.elementor-view-stacked
		.elementor-icon:hover
		svg {
		fill: #474746;
		}
		.elementor-29221 .elementor-element.elementor-element-12d8e4fe .elementor-icon {
		font-size: 40px;
		padding: 0px;
		border-radius: 15px 15px 15px 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-12d8e4fe
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-bc-flex-widget
		.elementor-29221
		.elementor-element.elementor-element-265976dd.elementor-column
		.elementor-widget-wrap {
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-265976dd.elementor-column.elementor-element[data-element_type="column"]
		> .elementor-widget-wrap.elementor-element-populated {
		align-content: center;
		align-items: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854
		.elementor-icon-wrapper {
		text-align: center;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-stacked
		.elementor-icon {
		background-color: #ffffff00;
		color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-default
		.elementor-icon {
		color: #ffffff00;
		border-color: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-framed
		.elementor-icon,
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-default
		.elementor-icon
		svg {
		fill: #ffffff00;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-framed
		.elementor-icon {
		background-color: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-stacked
		.elementor-icon
		svg {
		fill: #b1b1b1;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-stacked
		.elementor-icon:hover {
		background-color: #eeeeef00;
		color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-default
		.elementor-icon:hover {
		color: #eeeeef00;
		border-color: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-framed
		.elementor-icon:hover,
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-default
		.elementor-icon:hover
		svg {
		fill: #eeeeef00;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-framed
		.elementor-icon:hover {
		background-color: #474746;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854.elementor-view-stacked
		.elementor-icon:hover
		svg {
		fill: #474746;
		}
		.elementor-29221 .elementor-element.elementor-element-2b98a854 .elementor-icon {
		font-size: 40px;
		padding: 0px;
		border-radius: 15px 15px 15px 15px;
		}
		.elementor-29221
		.elementor-element.elementor-element-2b98a854
		.elementor-icon
		i,
		.elementor-29221
		.elementor-element.elementor-element-2b98a854
		.elementor-icon
		svg {
		transform: rotate(0deg);
		}
		.elementor-widget .tippy-tooltip .tippy-content {
		text-align: center;
		}
		@media (min-width: 768px) {
		.elementor-29221 .elementor-element.elementor-element-6a801380 {
			width: 45%;
		}
		.elementor-29221 .elementor-element.elementor-element-4b70fd63 {
			width: 9.332%;
		}
		.elementor-29221 .elementor-element.elementor-element-555a6a47 {
			width: 45%;
		}
		.elementor-29221 .elementor-element.elementor-element-602debfa {
			width: 35%;
		}
		.elementor-29221 .elementor-element.elementor-element-356a564e {
			width: 29.332%;
		}
		.elementor-29221 .elementor-element.elementor-element-5900fb27 {
			width: 35%;
		}
		.elementor-29221 .elementor-element.elementor-element-222c8a37 {
			width: 35%;
		}
		.elementor-29221 .elementor-element.elementor-element-30c1924f {
			width: 29.332%;
		}
		.elementor-29221 .elementor-element.elementor-element-6b81e11a {
			width: 35%;
		}
		.elementor-29221 .elementor-element.elementor-element-112325bd {
			width: 15%;
		}
		.elementor-29221 .elementor-element.elementor-element-56120178 {
			width: 69.33%;
		}
		.elementor-29221 .elementor-element.elementor-element-54be1392 {
			width: 15%;
		}
		}
		@media (max-width: 1024px) and (min-width: 768px) {
		.elementor-29221 .elementor-element.elementor-element-3a678b55 {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-64c67a {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-602debfa {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-356a564e {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-5900fb27 {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-222c8a37 {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-30c1924f {
			width: 50%;
		}
		.elementor-29221 .elementor-element.elementor-element-6b81e11a {
			width: 50%;
		}
		}
		@media (min-width: 1025px) {
		.elementor-29221
			.elementor-element.elementor-element-ca2bb6f:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
			.elementor-element.elementor-element-ca2bb6f
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-attachment: fixed;
		}
		}
		@media (max-width: 1024px) {
		.elementor-29221 .elementor-element.elementor-element-1a1a367b {
			margin-top: 0px;
			margin-bottom: 70px;
			padding: 0px 50px 0px 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-427e4066
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5fd7e859 {
			padding: 0px 10px 0px 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-21bb3e65 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-435ce5c4 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-444f26bc
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-b3aa9bf {
			padding: 0px 10px 0px 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-4d9f815b {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5f855133 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-7ccd1ab1 {
			margin-top: 0px;
			margin-bottom: 70px;
		}
		.elementor-29221 .elementor-element.elementor-element-218bb78f {
			margin-top: 0px;
			margin-bottom: 70px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-tablet-left
			.pp-timeline-marker-wrapper {
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-tablet-right
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-tablet-center
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
			margin-right: 5px;
		}
		.elementor-29221 .elementor-element.elementor-element-c92d65e {
			margin-top: 0px;
			margin-bottom: 70px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5f021551
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2b0b4c57
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23d56a0
			> .elementor-element-populated {
			padding: 75px 50px 70px 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f16af6b
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-bd26f85 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 250px 0px 250px;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-29221 .elementor-element.elementor-element-81c0746 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 250px 0px 250px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 70%;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-title {
			font-size: 14px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7024c86d
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6c5963f4
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-submit
			input[type="submit"],
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-submit
			button {
			line-height: var(--e-global-typography-text-line-height);
		}
		.elementor-29221
			.elementor-element.elementor-element-40c7e3a2
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4f98db44
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1cdd7b4d
			> .elementor-element-populated {
			padding: 75px 50px 70px 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-45d8d0db
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-d21e409
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-69d1247
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a9ffa3
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5fd6546a
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		}
		@media (max-width: 767px) {
		.elementor-29221 .elementor-element.elementor-element-7cf4f909 {
			padding: 0px 0px 30px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-728c9a7e
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-728c9a7e
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 100px 0px 750px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4b31d14d
			.elementor-heading-title {
			font-size: 14px;
			letter-spacing: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7e4a891f
			.elementor-heading-title {
			font-size: 40px;
			line-height: 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7e4a891f
			> .elementor-widget-container {
			margin: 5px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-8e8ef10
			.elementor-divider-separator {
			width: 50%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-29221
			.elementor-element.elementor-element-8e8ef10
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-102383d3
			.elementor-heading-title {
			font-size: 20px;
			letter-spacing: 2px;
		}
		.elementor-29221
			.elementor-element.elementor-element-102383d3
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-d5f07cc {
			--icon-box-icon-margin: -5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-d5f07cc
			.elementor-icon {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-d5f07cc
			.elementor-icon-box-title,
		.elementor-29221
			.elementor-element.elementor-element-d5f07cc
			.elementor-icon-box-title
			a {
			font-size: 8px;
			letter-spacing: 0.5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-d5f07cc
			> .elementor-widget-container {
			margin: 10px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1a367b
			> .elementor-background-overlay {
			background-size: 600px auto;
		}
		.elementor-29221 .elementor-element.elementor-element-1a1a367b {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 10px 0px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4116c59a
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-fe624d
			.elementor-heading-title {
			font-size: 24px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3a7179cb
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3a7179cb
			> .elementor-widget-container {
			margin: -20px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-427e4066
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-29221
			.elementor-element.elementor-element-427e4066
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-591a2dee
			.elementor-spacer-inner {
			--spacer-size: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-5fd7e859 {
			margin-top: -10px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6a801380 {
			width: 45%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-6a801380.elementor-column
			.elementor-widget-wrap {
			align-items: flex-start;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a801380.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-start;
			align-items: flex-start;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a801380
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-21bb3e65 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 10px 10px 25px 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-3a678b55 {
			width: 100%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-3a678b55.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-3a678b55.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-3a678b55.elementor-column
			> .elementor-widget-wrap {
			justify-content: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-3a678b55
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-3a678b55
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-3a678b55
			> .elementor-background-slideshow {
			border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3a678b55
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 5px 5px 7px 5px;
		}
		.elementor-29221 .elementor-element.elementor-element-236d9d56 img {
			border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221 .elementor-element.elementor-element-37037945 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-37037945
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-29221
			.elementor-element.elementor-element-37037945
			> .elementor-widget-container {
			margin: -80px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-644cb4f {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-644cb4f
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-644cb4f
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-50487b2d
			.elementor-divider-separator {
			width: 65%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-29221
			.elementor-element.elementor-element-50487b2d
			.elementor-divider {
			text-align: center;
			padding-top: 2px;
			padding-bottom: 2px;
		}
		.elementor-29221
			.elementor-element.elementor-element-50487b2d
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6a818b23 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a818b23
			.elementor-heading-title {
			font-size: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a818b23
			> .elementor-widget-container {
			margin: 0px 0px -5px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1b737c4d {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-1b737c4d
			.elementor-heading-title {
			font-size: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1b737c4d
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-605b6ae8
			.elementor-widget-container {
			text-align: center;
		}
		.elementor-29221 .elementor-element.elementor-element-605b6ae8 {
			--icon-size: 25px;
			--grid-column-gap: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-605b6ae8
			> .elementor-widget-container {
			margin: -9px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4b70fd63 {
			width: 10%;
		}
		.elementor-29221
			.elementor-element.elementor-element-4b70fd63
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 150px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-46754259 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-46754259
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-29221
			.elementor-element.elementor-element-46754259
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-555a6a47 {
			width: 45%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-555a6a47.elementor-column
			.elementor-widget-wrap {
			align-items: flex-start;
		}
		.elementor-29221
			.elementor-element.elementor-element-555a6a47.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-start;
			align-items: flex-start;
		}
		.elementor-29221
			.elementor-element.elementor-element-555a6a47
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-435ce5c4 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 10px 10px 25px 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-64c67a {
			width: 100%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-64c67a.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-64c67a.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-64c67a.elementor-column
			> .elementor-widget-wrap {
			justify-content: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-64c67a
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-64c67a
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-64c67a
			> .elementor-background-slideshow {
			border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64c67a
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 5px 5px 7px 5px;
		}
		.elementor-29221 .elementor-element.elementor-element-15ff036 img {
			border-radius: 500px 500px 200px 200px;
		}
		.elementor-29221 .elementor-element.elementor-element-35b5887b {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-35b5887b
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-29221
			.elementor-element.elementor-element-35b5887b
			> .elementor-widget-container {
			margin: -80px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1d57a933 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-1d57a933
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1d57a933
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6456bdb9
			.elementor-divider-separator {
			width: 65%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-29221
			.elementor-element.elementor-element-6456bdb9
			.elementor-divider {
			text-align: center;
			padding-top: 2px;
			padding-bottom: 2px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6456bdb9
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-3505686e {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-3505686e
			.elementor-heading-title {
			font-size: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3505686e
			> .elementor-widget-container {
			margin: 0px 0px -5px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-8f8fb6d {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-8f8fb6d
			.elementor-heading-title {
			font-size: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-8f8fb6d
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-17f49bae
			.elementor-widget-container {
			text-align: center;
		}
		.elementor-29221 .elementor-element.elementor-element-17f49bae {
			--icon-size: 25px;
			--grid-column-gap: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-17f49bae
			> .elementor-widget-container {
			margin: -9px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4f80d344
			.elementor-spacer-inner {
			--spacer-size: 100px;
		}
		.elementor-29221
			.elementor-element.elementor-element-466bd400:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
			.elementor-element.elementor-element-466bd400
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-position: center center;
		}
		.elementor-29221 .elementor-element.elementor-element-466bd400 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 150px 20px 100px 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5ea45641
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6858d65a,
		.elementor-29221
			.elementor-element.elementor-element-6858d65a
			> .elementor-background-overlay {
			border-radius: 500px 500px 150px 150px;
		}
		.elementor-29221
			.elementor-element.elementor-element-40aaa8d5
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-40aaa8d5
			> .elementor-element-populated {
			padding: 70px 20px 50px 20px;
		}
		.elementor-29221 .elementor-element.elementor-element-1ed77922 img {
			width: 35%;
		}
		.elementor-29221
			.elementor-element.elementor-element-1ed77922
			> .elementor-widget-container {
			margin: 0px 0px 20px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6478e5e0
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-43fcde86
			.elementor-heading-title {
			font-size: 30px;
			line-height: 1em;
		}
		.elementor-29221
			.elementor-element.elementor-element-43fcde86
			> .elementor-widget-container {
			margin: 5px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1d2a37.bdt-countdown--label-block
			.bdt-countdown-number {
			margin-bottom: 7px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1d2a37.bdt-countdown--label-inline
			.bdt-countdown-number {
			margin-right: 7px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1d2a37
			.bdt-countdown-wrapper {
			max-width: 150%;
			margin-left: auto;
			margin-right: auto;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1d2a37
			.bdt-countdown-item {
			border-radius: 15px 15px 15px 15px;
			padding: 15px 0px 15px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1d2a37
			.bdt-countdown-number {
			font-size: 24px;
			letter-spacing: -1px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a1d2a37
			.bdt-countdown-label {
			font-size: 10px;
		}
		.elementor-29221 .elementor-element.elementor-element-677b0c79 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-677b0c79
			.elementor-heading-title {
			font-size: 12px;
		}
		.elementor-29221
			.elementor-element.elementor-element-677b0c79
			> .elementor-widget-container {
			margin: 0px 10px 0px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-12cdca2e
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2d3a937c
			.elementor-spacer-inner {
			--spacer-size: 25px;
		}
		.elementor-29221 .elementor-element.elementor-element-24a218b6,
		.elementor-29221
			.elementor-element.elementor-element-24a218b6
			> .elementor-background-overlay {
			border-radius: 50px 50px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4508cd8f
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-4508cd8f
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-4508cd8f
			> .elementor-background-slideshow {
			border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4508cd8f
			> .elementor-element-populated {
			padding: 150px 0px 30px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3660a856
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3660a856
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7a0bb1be
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7a0bb1be
			> .elementor-widget-container {
			margin: -15px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-444f26bc
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-29221
			.elementor-element.elementor-element-444f26bc
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-b3aa9bf,
		.elementor-29221
			.elementor-element.elementor-element-b3aa9bf
			> .elementor-background-overlay {
			border-radius: 0px 0px 50px 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-b3aa9bf {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6d048667 {
			width: 100%;
		}
		.elementor-29221
			.elementor-element.elementor-element-6d048667
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6d048667
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-6d048667
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-6d048667
			> .elementor-background-slideshow {
			border-radius: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6d048667
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 50px 10px 40px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1c09a6b0
			.elementor-icon {
			font-size: 70px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1e91f4df
			.elementor-heading-title {
			font-size: 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1e91f4df
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7f7cdcb9
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7f7cdcb9
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4d9f815b {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 40px 0px 40px;
		}
		.elementor-29221 .elementor-element.elementor-element-602debfa {
			width: 35%;
		}
		.elementor-29221
			.elementor-element.elementor-element-602debfa
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-77308cdf {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-77308cdf
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-77308cdf
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-356a564e {
			width: 30%;
		}
		.elementor-29221
			.elementor-element.elementor-element-356a564e
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-44da5773
			.elementor-counter-number-wrapper {
			font-size: 52px;
		}
		.elementor-29221
			.elementor-element.elementor-element-44da5773
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5900fb27 {
			width: 35%;
		}
		.elementor-29221
			.elementor-element.elementor-element-5900fb27
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1fc116a9 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-1fc116a9
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1fc116a9
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1046017f
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1046017f
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4ab09ab
			.elementor-icon {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-56cf1ac1
			.elementor-heading-title {
			font-size: 18px;
			line-height: 1.3em;
		}
		.elementor-29221
			.elementor-element.elementor-element-56cf1ac1
			> .elementor-widget-container {
			margin: 5px 0px 5px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-677da5ec {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-677da5ec
			.elementor-heading-title {
			font-size: 11px;
			letter-spacing: 0.5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-677da5ec
			> .elementor-widget-container {
			margin: 0px 40px 0px 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-22c32e91
			.elementor-button {
			font-size: 12px;
			border-width: 2px 2px 2px 2px;
			padding: 7px 15px 7px 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-22c32e91
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3fae3d84
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3fae3d84
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-3fae3d84
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-3fae3d84
			> .elementor-background-slideshow {
			border-radius: 0px 0px 500px 500px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3fae3d84
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 40px 10px 60px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-16cdd076
			.elementor-icon {
			font-size: 75px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5f6ee1f2
			.elementor-heading-title {
			font-size: 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5f6ee1f2
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-598c1545
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-598c1545
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5f855133 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 0px 40px 0px 40px;
		}
		.elementor-29221 .elementor-element.elementor-element-222c8a37 {
			width: 35%;
		}
		.elementor-29221
			.elementor-element.elementor-element-222c8a37
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-722a84f1 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-722a84f1
			.elementor-heading-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-722a84f1
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-30c1924f {
			width: 30%;
		}
		.elementor-29221
			.elementor-element.elementor-element-30c1924f
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-762cd999
			.elementor-counter-number-wrapper {
			font-size: 52px;
		}
		.elementor-29221
			.elementor-element.elementor-element-762cd999
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6b81e11a {
			width: 35%;
		}
		.elementor-29221
			.elementor-element.elementor-element-6b81e11a
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5dc7b685 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-5dc7b685
			.elementor-heading-title {
			font-size: 18px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5dc7b685
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-56d3e677
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-56d3e677
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f46ae10
			.elementor-icon {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-34d67635
			.elementor-heading-title {
			font-size: 18px;
			line-height: 1.3em;
		}
		.elementor-29221
			.elementor-element.elementor-element-34d67635
			> .elementor-widget-container {
			margin: 5px 0px 5px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-b4301f4 {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-b4301f4
			.elementor-heading-title {
			font-size: 11px;
			letter-spacing: 0.5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-b4301f4
			> .elementor-widget-container {
			margin: 0px 40px 0px 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-14aeb4e3
			.elementor-button {
			font-size: 12px;
			border-width: 2px 2px 2px 2px;
			padding: 7px 15px 7px 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-14aeb4e3
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7ccd1ab1
			> .elementor-background-overlay {
			background-size: 600px auto;
		}
		.elementor-29221
			.elementor-element.elementor-element-7ccd1ab1
			> .elementor-shape-bottom
			svg {
			width: calc(100% + 1.3px);
			height: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-7ccd1ab1 {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 30px 150px 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-e7be9d4
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-e7be9d4
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-e7be9d4
			> .elementor-background-slideshow {
			border-radius: 50px 50px 50px 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-38e95752
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-38e95752
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-38e95752
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-38e95752
			> .elementor-background-slideshow {
			border-radius: 25px 25px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-38e95752
			> .elementor-element-populated {
			padding: 50px 20px 40px 20px;
		}
		.elementor-29221 .elementor-element.elementor-element-1dfcb620 img {
			border-radius: 30px 30px 30px 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-793f88bd
			.elementor-spacer-inner {
			--spacer-size: 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-e97facb
			.elementor-heading-title {
			font-size: 18px;
			line-height: 1.3em;
		}
		.elementor-29221
			.elementor-element.elementor-element-e97facb
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6e9f69b9
			.elementor-heading-title {
			font-size: 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6e9f69b9
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a8da0b1
			.elementor-heading-title {
			font-size: 12px;
			letter-spacing: 0.5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a8da0b1
			> .elementor-widget-container {
			margin: 10px 40px 10px 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7328789b
			.elementor-button {
			font-size: 12px;
			border-width: 2px 2px 2px 2px;
			padding: 10px 15px 10px 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7328789b
			> .elementor-widget-container {
			margin: -10px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-218bb78f {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 20px 100px 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-41a663a3
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-41a663a3
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5c6efbd7
			.pp-image-slider-thumb-pagination
			.pp-image-slider-thumb-item-wrap {
			padding-left: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5c6efbd7.pp-image-slider-align-top
			.pp-image-slider-thumb-pagination {
			width: calc(100% + 0px);
		}
		.elementor-29221
			.elementor-element.elementor-element-5c6efbd7
			.pp-image-slider-thumb-pagination {
			margin-left: -0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5c6efbd7
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-189c3182
			.elementor-spacer-inner {
			--spacer-size: 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7c6b3cc9
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64fd4d3f
			.elementor-heading-title {
			font-size: 34px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64fd4d3f
			> .elementor-widget-container {
			margin: -5px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-efbf8f3
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-29221
			.elementor-element.elementor-element-efbf8f3
			> .elementor-widget-container {
			margin: 0px 30px 10px 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-44641f65
			.elementor-spacer-inner {
			--spacer-size: 500px;
		}
		.elementor-29221 .elementor-element.elementor-element-7d08d2e8 {
			margin-top: -500px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-36805275
			> .elementor-element-populated {
			margin: 0px 5px 0px 0px;
			--e-column-margin-right: 5px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline
			.pp-timeline-card {
			padding: 30px 30px 30px 30px;
			border-radius: 35px 35px 35px 35px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-card {
			text-align: left;
			font-size: 11px;
			line-height: 1.5em;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-card-title {
			font-size: 18px;
			line-height: 1.3em;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-card-title-wrap {
			margin-bottom: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-marker {
			font-size: 8px;
			width: 20px;
			height: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-marker
			img {
			width: 8px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-connector-wrap {
			width: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-navigation:before,
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-navigation
			.pp-slider-arrow {
			bottom: calc(20px / 2);
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-card-date {
			font-size: 12px;
			line-height: 1.5em;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-mobile-left
			.pp-timeline-marker-wrapper {
			margin-right: 5px !important;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-mobile-right
			.pp-timeline-marker-wrapper {
			margin-left: 5px !important;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-mobile-center
			.pp-timeline-marker-wrapper {
			margin-left: 5px !important;
			margin-right: 5px !important;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-left
			.pp-timeline-marker-wrapper {
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-right
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-vertical.pp-timeline-center
			.pp-timeline-marker-wrapper {
			margin-left: 5px;
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-horizontal {
			margin-top: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23cbaee0
			.pp-timeline-navigation
			.pp-timeline-card-date-wrapper {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-c92d65e
			> .elementor-background-overlay {
			background-size: 600px auto;
		}
		.elementor-29221
			.elementor-element.elementor-element-c92d65e
			> .elementor-shape-top
			svg {
			width: calc(100% + 1.3px);
			height: 50px;
		}
		.elementor-29221 .elementor-element.elementor-element-c92d65e {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 150px 20px 100px 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-72de8a
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-72de8a
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-ae8c293
			.elementor-heading-title {
			font-size: 24px;
		}
		.elementor-29221 .elementor-element.elementor-element-ae8c293 {
			z-index: 1;
		}
		.elementor-29221
			.elementor-element.elementor-element-5318aa6f
			.elementor-heading-title {
			font-size: 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5318aa6f
			> .elementor-widget-container {
			margin: -8px 0px 15px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5318aa6f {
			z-index: 1;
		}
		.elementor-29221
			.elementor-element.elementor-element-6cbd54e
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5f021551
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5f021551
			> .elementor-widget-container {
			margin: 20px 20px 10px 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-38e611f4
			.elementor-spacer-inner {
			--spacer-size: 550px;
		}
		.elementor-29221
			.elementor-element.elementor-element-38e611f4
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-ed5b3eb {
			margin-top: -550px;
			margin-bottom: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-a239ba7
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 3px;
		}
		.elementor-29221
			.elementor-element.elementor-element-a239ba7
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2b0b4c57
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-2b0b4c57
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-3052aa50 img {
			width: 40%;
		}
		.elementor-29221
			.elementor-element.elementor-element-3052aa50
			> .elementor-widget-container {
			margin: 10px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4633138d {
			--icon-size: 18px;
		}
		.elementor-29221 .elementor-element.elementor-element-d9c435e {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 30px 100px 30px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23d56a0
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-23d56a0
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-8b04011
			.elementor-heading-title {
			font-size: 42px;
			letter-spacing: 3px;
		}
		.elementor-29221
			.elementor-element.elementor-element-8b04011
			> .elementor-widget-container {
			margin: 5px 0px 5px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f16af6b
			.elementor-heading-title {
			font-size: 13px;
			line-height: 1.3em;
		}
		.elementor-29221
			.elementor-element.elementor-element-f16af6b
			> .elementor-widget-container {
			margin: 0px 20px 0px 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-61770a3
			.elementor-divider-separator {
			width: 75%;
		}
		.elementor-29221
			.elementor-element.elementor-element-61770a3
			.elementor-divider {
			padding-top: 25px;
			padding-bottom: 25px;
		}
		.elementor-29221
			.elementor-element.elementor-element-61770a3
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-bd26f85 {
			margin-top: 0px;
			margin-bottom: 20px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-3fc455c {
			width: 50%;
		}
		.elementor-29221
			.elementor-element.elementor-element-3fc455c
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-669e9bc
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-9b02a18 {
			width: 50%;
		}
		.elementor-29221
			.elementor-element.elementor-element-9b02a18
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-289b8da
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-81c0746 {
			margin-top: 20px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6ce2bb8 {
			width: 50%;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ce2bb8
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-f1bde03
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-f14f6ed {
			width: 50%;
		}
		.elementor-29221
			.elementor-element.elementor-element-f14f6ed
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d.elementor-position-right
			.elementor-image-box-img {
			margin-left: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d.elementor-position-left
			.elementor-image-box-img {
			margin-right: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d.elementor-position-top
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-img {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-wrapper
			.elementor-image-box-img {
			width: 30%;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-wrapper {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-title {
			margin-bottom: 5px;
			font-size: 12px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-6ca5e8d
			.elementor-image-box-description {
			font-size: 8px;
			letter-spacing: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-396a7c4c
			> .elementor-background-overlay {
			background-size: 600px auto;
		}
		.elementor-29221 .elementor-element.elementor-element-396a7c4c {
			padding: 100px 20px 120px 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4bec872f
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4bec872f
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7024c86d
			.elementor-heading-title {
			font-size: 18px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-7024c86d
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-7315716b
			.elementor-heading-title {
			font-size: 30px;
			line-height: 1.5em;
		}
		.elementor-29221
			.elementor-element.elementor-element-7315716b
			> .elementor-widget-container {
			margin: -7px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6c5963f4
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6c5963f4
			> .elementor-widget-container {
			margin: 0px 0px -20px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-42df8cab
			.elementor-spacer-inner {
			--spacer-size: 750px;
		}
		.elementor-29221 .elementor-element.elementor-element-568157b9 {
			margin-top: -750px;
			margin-bottom: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6629be3f
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-6629be3f
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-6629be3f
			> .elementor-background-slideshow {
			border-radius: 50px 50px 50px 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6629be3f
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-komentar-cswd
			input[type="text"],
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-komentar-cswd
			select,
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-komentar-cswd
			textarea {
			font-size: 11px;
			line-height: 1.5em;
			letter-spacing: 0.3px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.wdp-wrap-link
			a {
			font-size: 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-submit
			input[type="submit"],
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			.form-submit
			button {
			line-height: var(--e-global-typography-text-line-height);
			border-width: 2px 2px 2px 2px;
		}
		.elementor-29221
			.elementor-element.elementor-element-64232eaa
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-d2c9bff
			.elementor-spacer-inner {
			--spacer-size: 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-40c7e3a2
			.elementor-heading-title {
			font-size: 12px;
		}
		.elementor-29221
			.elementor-element.elementor-element-40c7e3a2
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4f98db44
			.elementor-heading-title {
			font-size: 13px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4f98db44
			> .elementor-widget-container {
			margin: 10px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-3bda5dcc img {
			width: 40%;
		}
		.elementor-29221 .elementor-element.elementor-element-101550d {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 100px 20px 150px 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1cdd7b4d
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1cdd7b4d
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-1cdd7b4d
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-1cdd7b4d
			> .elementor-background-slideshow {
			border-radius: 50px 50px 50px 50px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1cdd7b4d
			> .elementor-element-populated {
			margin: 0px 20px 0px 20px;
			--e-column-margin-right: 20px;
			--e-column-margin-left: 20px;
			padding: 50px 10px 30px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-63f74300
			.elementor-heading-title {
			font-size: 28px;
			line-height: 0.8em;
		}
		.elementor-29221
			.elementor-element.elementor-element-63f74300
			> .elementor-widget-container {
			margin: 10px 0px 10px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-45d8d0db
			.elementor-heading-title {
			font-size: 12px;
		}
		.elementor-29221
			.elementor-element.elementor-element-45d8d0db
			> .elementor-widget-container {
			margin: 0px 15px 0px 15px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2c0910fc
			.elementor-button {
			font-size: 12px;
			letter-spacing: 0.5px;
			padding: 10px 15px 10px 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2c0910fc
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-415a7c1d {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-56120178
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-56120178
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-56120178
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-56120178
			> .elementor-background-slideshow {
			border-radius: 40px 40px 40px 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-56120178
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 30px 10px 20px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-d21e409
			.elementor-heading-title {
			font-size: 18px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-d21e409
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-fc7d887
			.elementor-divider-separator {
			width: 65%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-29221
			.elementor-element.elementor-element-fc7d887
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-fc7d887
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5c9b00bc img {
			width: 50%;
		}
		.elementor-29221
			.elementor-element.elementor-element-5c9b00bc
			> .elementor-widget-container {
			margin: 0px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-26a9485 .copy-content {
			font-size: 20px;
			line-height: 1em;
		}
		.elementor-29221 .elementor-element.elementor-element-26a9485 .head-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-26a9485
			a.elementor-button,
		.elementor-29221
			.elementor-element.elementor-element-26a9485
			.elementor-button {
			font-size: 12px;
			letter-spacing: 0.5px;
			padding: 5px 15px 5px 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-69d1247
			.elementor-heading-title {
			font-size: 18px;
			line-height: 1.2em;
		}
		.elementor-29221
			.elementor-element.elementor-element-69d1247
			> .elementor-widget-container {
			margin: 0px 30px 0px 30px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-9599d1c
			.elementor-divider-separator {
			width: 65%;
			margin: 0 auto;
			margin-center: 0;
		}
		.elementor-29221
			.elementor-element.elementor-element-9599d1c
			.elementor-divider {
			text-align: center;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		.elementor-29221
			.elementor-element.elementor-element-9599d1c
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-5775fc48 .copy-content {
			font-size: 11px;
			line-height: 1.5em;
			letter-spacing: 0.5px;
		}
		.elementor-29221 .elementor-element.elementor-element-5775fc48 .head-title {
			font-size: 16px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5775fc48
			a.elementor-button,
		.elementor-29221
			.elementor-element.elementor-element-5775fc48
			.elementor-button {
			font-size: 12px;
			letter-spacing: 0.5px;
			padding: 5px 15px 5px 15px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5775fc48
			> .elementor-widget-container {
			margin: 0px 10px 0px 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-ca2bb6f:not(.elementor-motion-effects-element-type-background),
		.elementor-29221
			.elementor-element.elementor-element-ca2bb6f
			> .elementor-motion-effects-container
			> .elementor-motion-effects-layer {
			background-position: center center;
			background-size: cover;
		}
		.elementor-29221 .elementor-element.elementor-element-ca2bb6f {
			margin-top: 0px;
			margin-bottom: 0px;
			padding: 200px 30px 100px 30px;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-2699623a.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-2699623a.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-2699623a.elementor-column
			> .elementor-widget-wrap {
			justify-content: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-2699623a
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2699623a
			> .elementor-element-populated {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-200bfa5f
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 10px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3b1c9e35
			.elementor-heading-title {
			font-size: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-3fca0f55
			.elementor-heading-title {
			font-size: 36px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a9ffa3
			.elementor-heading-title {
			font-size: 14px;
			line-height: 1.3em;
		}
		.elementor-29221
			.elementor-element.elementor-element-6a9ffa3
			> .elementor-widget-container {
			margin: 10px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5fd6546a
			.elementor-heading-title {
			font-size: 12px;
		}
		.elementor-29221
			.elementor-element.elementor-element-5fd6546a
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-335369e {
			width: 15%;
		}
		.elementor-29221 .elementor-element.elementor-element-cdfe0ce {
			width: 35%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-cdfe0ce.elementor-column
			.elementor-widget-wrap {
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-cdfe0ce.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: center;
			align-items: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-cdfe0ce
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 7px;
		}
		.elementor-29221
			.elementor-element.elementor-element-cdfe0ce
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 10px 0px 10px 15px;
		}
		.elementor-29221 .elementor-element.elementor-element-20f93bb img {
			width: 60%;
		}
		.elementor-29221
			.elementor-element.elementor-element-20f93bb
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-02b20fd {
			--icon-size: 18px;
		}
		.elementor-29221 .elementor-element.elementor-element-4be6dad {
			width: 35%;
		}
		.elementor-29221
			.elementor-element.elementor-element-4be6dad
			> .elementor-widget-wrap
			> .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
			margin-bottom: 4px;
		}
		.elementor-29221
			.elementor-element.elementor-element-4be6dad
			> .elementor-element-populated {
			padding: 10px 15px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6354765 img {
			width: 60%;
		}
		.elementor-29221 .elementor-element.elementor-element-ce61bfc {
			text-align: center;
		}
		.elementor-29221
			.elementor-element.elementor-element-ce61bfc
			.elementor-heading-title {
			font-size: 5px;
			line-height: 1.4em;
		}
		.elementor-29221 .elementor-element.elementor-element-a26b241 {
			width: 15%;
		}
		.elementor-29221
			.elementor-element.elementor-element-78da72d3
			.elementor-icon {
			font-size: 1px;
		}
		.elementor-29221 .elementor-element.elementor-element-78da72d3 {
			width: 35px;
			max-width: 35px;
			bottom: 70px;
			z-index: 999;
		}
		body:not(.rtl)
			.elementor-29221
			.elementor-element.elementor-element-78da72d3 {
			left: 5px;
		}
		body.rtl .elementor-29221 .elementor-element.elementor-element-78da72d3 {
			right: 5px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-text {
			margin-top: 10px;
			font-size: 11px;
			line-height: 1.5em;
			letter-spacing: 0.5px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-dear {
			margin-top: 10px;
			font-size: 13px;
			letter-spacing: 0.5px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-name {
			margin-top: 10px;
			font-size: 16px;
			letter-spacing: 0.5px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .dwp-sesi {
			margin-top: 10px;
			font-size: 12px;
		}
		.elementor-29221
			.elementor-element.elementor-element-111b2886
			.text_tambahan {
			margin-top: 20px;
			font-size: 14px;
			line-height: 1em;
			letter-spacing: 4px;
		}
		.elementor-29221 .elementor-element.elementor-element-111b2886 .wdp-mempelai {
			margin-top: 0px;
			margin-bottom: 0px;
			font-size: 40px;
		}
		.elementor-29221
			.elementor-element.elementor-element-111b2886
			.wdp-button-wrapper {
			margin-top: 20px;
		}
		.elementor-29221
			.elementor-element.elementor-element-111b2886
			.elementor-image
			img {
			width: 35%;
		}
		.elementor-29221 .elementor-element.elementor-element-4d44ac51 {
			margin-top: -84px;
			margin-bottom: 0px;
			padding: 30px 30px 0px 30px;
			z-index: 998;
		}
		.elementor-29221
			.elementor-element.elementor-element-b4e5efc
			> .elementor-element-populated,
		.elementor-29221
			.elementor-element.elementor-element-b4e5efc
			> .elementor-element-populated
			> .elementor-background-overlay,
		.elementor-29221
			.elementor-element.elementor-element-b4e5efc
			> .elementor-background-slideshow {
			border-radius: 25px 25px 0px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-b4e5efc
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 15px 0px 10px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4301bc67 {
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1a68644e {
			width: 16.666%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-1a68644e.elementor-column
			.elementor-widget-wrap {
			align-items: flex-start;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a68644e.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-start;
			align-items: flex-start;
		}
		.elementor-29221
			.elementor-element.elementor-element-1a68644e
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 1px 0px 1.5px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-1cfee501 img {
			width: 38%;
		}
		.elementor-29221
			.elementor-element.elementor-element-1cfee501
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4c7d8168 {
			width: 16.666%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-4c7d8168.elementor-column
			.elementor-widget-wrap {
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-4c7d8168.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-end;
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-4c7d8168
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 2.5px 0px 1.5px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1d170234
			.elementor-icon {
			font-size: 21px;
		}
		.elementor-29221
			.elementor-element.elementor-element-1d170234
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-2a2acf80 {
			width: 16.666%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-2a2acf80.elementor-column
			.elementor-widget-wrap {
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-2a2acf80.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-end;
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-2a2acf80
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 2.5px 0px 1.5px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-10ceca95
			.elementor-icon {
			font-size: 21px;
		}
		.elementor-29221
			.elementor-element.elementor-element-10ceca95
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-6bebb4a {
			width: 16.666%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-6bebb4a.elementor-column
			.elementor-widget-wrap {
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-6bebb4a.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-end;
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-6bebb4a
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 2.5px 0px 1.5px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6f1e2792
			.elementor-icon {
			font-size: 19px;
		}
		.elementor-29221
			.elementor-element.elementor-element-6f1e2792
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-4b6b46c7 {
			width: 16.666%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-4b6b46c7.elementor-column
			.elementor-widget-wrap {
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-4b6b46c7.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-end;
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-4b6b46c7
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 2.5px 0px 1.5px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-12d8e4fe
			.elementor-icon {
			font-size: 21px;
		}
		.elementor-29221
			.elementor-element.elementor-element-12d8e4fe
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		.elementor-29221 .elementor-element.elementor-element-265976dd {
			width: 16.666%;
		}
		.elementor-bc-flex-widget
			.elementor-29221
			.elementor-element.elementor-element-265976dd.elementor-column
			.elementor-widget-wrap {
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-265976dd.elementor-column.elementor-element[data-element_type="column"]
			> .elementor-widget-wrap.elementor-element-populated {
			align-content: flex-end;
			align-items: flex-end;
		}
		.elementor-29221
			.elementor-element.elementor-element-265976dd
			> .elementor-element-populated {
			margin: 0px 0px 0px 0px;
			--e-column-margin-right: 0px;
			--e-column-margin-left: 0px;
			padding: 2.5px 0px 1.5px 0px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2b98a854
			.elementor-icon {
			font-size: 21px;
		}
		.elementor-29221
			.elementor-element.elementor-element-2b98a854
			> .elementor-widget-container {
			margin: 0px 0px 0px 0px;
			padding: 0px 0px 0px 0px;
		}
		} /* Start custom CSS for section, class: .elementor-element-6858d65a */
		.elementor-29221 .elementor-element.elementor-element-6858d65a {
		backdrop-filter: blur(4px);
		} /* End custom CSS */
		/* Start Custom Fonts CSS */
		@font-face {
		font-family: "NewYork";
		font-style: normal;
		font-weight: normal;
		font-display: auto;
		src: url("<?= $asset_theme ?>fonts/NewYork.eot");
		src: url("<?= $asset_theme ?>fonts/NewYork.eot?#iefix")
			format("embedded-opentype"),
			url("<?= $asset_theme ?>fonts/NewYork.woff2")
			format("woff2"),
			url("<?= $asset_theme ?>fonts/NewYork.woff")
			format("woff"),
			url("<?= $asset_theme ?>fonts/NewYork.ttf")
			format("truetype"),
			url("<?= $asset_theme ?>fonts/NewYork.svg#NewYork")
			format("svg");
		}
		/* End Custom Fonts CSS */
	</style>

	
    <link rel='stylesheet' id='elementor-post-51207-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51207.css?ver=1655470781' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-31584-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-31584.css?ver=1655470781' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-css' href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
    <link rel='stylesheet' id='loftloader-lite-animation-css' href='https://unityinvitation.com/wp-content/plugins/loftloader/assets/css/loftloader.min.css?ver=2022022501' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-v4shim-css' href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
    <style id='font-awesome-official-v4shim-inline-css' type='text/css'>
      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
        unicode-range: U+F004-F005, U+F007, U+F017, U+F022, U+F024, U+F02E, U+F03E, U+F044, U+F057-F059, U+F06E, U+F070, U+F075, U+F07B-F07C, U+F080, U+F086, U+F089, U+F094, U+F09D, U+F0A0, U+F0A4-F0A7, U+F0C5, U+F0C7-F0C8, U+F0E0, U+F0EB, U+F0F3, U+F0F8, U+F0FE, U+F111, U+F118-F11A, U+F11C, U+F133, U+F144, U+F146, U+F14A, U+F14D-F14E, U+F150-F152, U+F15B-F15C, U+F164-F165, U+F185-F186, U+F191-F192, U+F1AD, U+F1C1-F1C9, U+F1CD, U+F1D8, U+F1E3, U+F1EA, U+F1F6, U+F1F9, U+F20A, U+F247-F249, U+F24D, U+F254-F25B, U+F25D, U+F267, U+F271-F274, U+F279, U+F28B, U+F28D, U+F2B5-F2B6, U+F2B9, U+F2BB, U+F2BD, U+F2C1-F2C2, U+F2D0, U+F2D2, U+F2DC, U+F2ED, U+F328, U+F358-F35B, U+F3A5, U+F3D1, U+F410, U+F4AD;
      }
    </style>
    <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLora%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLibre+Baskerville%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-shared-0-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-wedding-css' href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/wedding/css/wedding.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-unityinv-css' href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-regular-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
    <script type='text/javascript' id='jquery-core-js-extra'>
      /* 
					<![CDATA[ */
      var pp = {
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
    <link rel="https://api.w.org/" href="https://unityinvitation.com/wp-json/" />
    <link rel="alternate" type="application/json" href="https://unityinvitation.com/wp-json/wp/v2/posts/29221" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://unityinvitation.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://unityinvitation.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.0" />
    <link rel='shortlink' href='https://unityinvitation.com/?p=29221' />
    <link rel="alternate" type="application/json+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-diamond1%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-diamond1%2F&#038;format=xml" />
    <link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-180x180.png" />
    <meta name="msapplication-TileImage" content="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-270x270.png" />
    <style>
      .pswp.pafe-lightbox-modal {
        display: none;
      }
    </style>
    <style id="loftloader-lite-custom-bg-color">
      #loftloader-wrapper .loader-section {
        background: #ffffff;
      }
    </style>
    <style id="loftloader-lite-custom-bg-opacity">
      #loftloader-wrapper .loader-section {
        opacity: 1;
      }
    </style>
    <style id="loftloader-lite-custom-loader">
      #loftloader-wrapper.pl-imgloading #loader {
        width: 70px;
      }

      #loftloader-wrapper.pl-imgloading #loader span {
        background-size: cover;
        background-image: url(https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1.png);
      }
    </style>
    <style type="text/css" id="wp-custom-css">
      .elementor-section {
        overflow: hidden;
      }

      .justify-cstm .pp-timeline-card {
        text-align: justify !important;
      }
    </style>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
  </head>
  <body data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-29221 single-format-standard loftloader-lite-enabled elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-29221">
    <div id="loftloader-wrapper" class="pl-imgloading" data-show-close-time="15000" data-max-load-time="10000">
      <div class="loader-inner">
        <div id="loader">
          <div class="imgloading-container">
            <span style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1.png);"></span>
          </div>
          <img width="70" height="43" data-no-lazy="1" class="skip-lazy" alt="loader image" src="https://unityinvitation.com/wp-content/uploads/2021/05/Unity-Logos-1.png">
        </div>
      </div>
      <div class="loader-section section-fade"></div>
      <div class="loader-close-button" style="display: none;">
        <span class="screen-reader-text">Close</span>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-dark-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0.49803921568627" />
            <feFuncG type="table" tableValues="0 0.49803921568627" />
            <feFuncB type="table" tableValues="0 0.49803921568627" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-red">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 0.27843137254902" />
            <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-midnight">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0" />
            <feFuncG type="table" tableValues="0 0.64705882352941" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-magenta-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.78039215686275 1" />
            <feFuncG type="table" tableValues="0 0.94901960784314" />
            <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-green">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.44705882352941 0.4" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-orange">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.098039215686275 1" />
            <feFuncG type="table" tableValues="0 0.66274509803922" />
            <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <div data-elementor-type="wp-post" data-elementor-id="29221" class="elementor elementor-29221">
      <section class="pp-bg-effects pp-bg-effects-7cf4f909 elementor-section elementor-top-section elementor-element elementor-element-7cf4f909 elementor-section-stretched pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="7cf4f909" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="10" data-part-speed="" data-part-direction="none" data-hover-effect="grab" data-hover-size="" data-id="7cf4f909" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;slideshow&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:45768,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Diamond-1-home1-scaled.jpg&quot;},{&quot;id&quot;:45769,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Diamond-1-home2-scaled.jpg&quot;},{&quot;id&quot;:45770,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Diamond-1-home3-scaled.jpg&quot;}],&quot;background_slideshow_transition_duration&quot;:1500,&quot;background_slideshow_slide_duration&quot;:2500,&quot;sticky&quot;:&quot;top&quot;,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-728c9a7e wdp-sticky-section-no" data-id="728c9a7e" data-element_type="column" id="home" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-4b31d14d animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4b31d14d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h3 class="elementor-heading-title elementor-size-default">PERNIKAHAN</h3>
                </div>
              </div>
              <div class="elementor-element elementor-element-7e4a891f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7e4a891f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h1 class="elementor-heading-title elementor-size-default">Romi & Hazel</h1>
                </div>
              </div>
              <div class="elementor-element elementor-element-8e8ef10 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="8e8ef10" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-102383d3 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="102383d3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h3 class="elementor-heading-title elementor-size-default">31 . 12 . 2022</h3>
                </div>
              </div>
              <div class="elementor-element elementor-element-d5f07cc elementor-view-default elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="d5f07cc" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon-box.default">
                <div class="elementor-widget-container">
                  <div class="elementor-icon-box-wrapper">
                    <div class="elementor-icon-box-icon">
                      <span class="elementor-icon elementor-animation-">
                        <i aria-hidden="true" class="fas fa-angle-double-up"></i>
                      </span>
                    </div>
                    <div class="elementor-icon-box-content">
                      <h3 class="elementor-icon-box-title">
                        <span> Swipe Up </span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-1a1a367b animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="1a1a367b" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4116c59a wdp-sticky-section-no" data-id="4116c59a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-fe624d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="fe624d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-3a7179cb wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3a7179cb" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-427e4066 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="427e4066" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Maha Suci Allah Subhanahu wa Ta'ala yang telah menciptakan makhluk-Nya berpasang-pasangan. Ya Allah, perkenankanlah dan Ridhoilah Pernikahan Kami.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-591a2dee wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="591a2dee" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-5fd7e859 elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5fd7e859" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6a801380 animated-slow wdp-sticky-section-no" data-id="6a801380" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-21bb3e65 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="21bb3e65" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3a678b55 animated-slow wdp-sticky-section-no elementor-invisible" data-id="3a678b55" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-236d9d56 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="236d9d56" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                  <img width="683" height="1024" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPP-683x1024.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPP-683x1024.jpg 683w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPP-200x300.jpg 200w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPP-768x1152.jpg 768w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPP.jpg 1000w" sizes="(max-width: 683px) 100vw, 683px" />
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-37037945 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="37037945" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <h3 class="elementor-heading-title elementor-size-default">Romi</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-644cb4f wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="644cb4f" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">Romi Raffah</h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-50487b2d elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="50487b2d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6a818b23 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="6a818b23" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Putra Ketiga</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1b737c4d wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1b737c4d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Bapak Tony Raffah <br>& <br> Ibu Hasannah </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-605b6ae8 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="605b6ae8" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="#" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4b70fd63 animated-slow wdp-sticky-section-no" data-id="4b70fd63" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-46754259 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="46754259" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">&</h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-555a6a47 animated-slow wdp-sticky-section-no" data-id="555a6a47" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-435ce5c4 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="435ce5c4" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-64c67a animated-slow wdp-sticky-section-no elementor-invisible" data-id="64c67a" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-15ff036 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="15ff036" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                  <img width="683" height="1024" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPW-683x1024.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPW-683x1024.jpg 683w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPW-200x300.jpg 200w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPW-768x1152.jpg 768w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-CPW.jpg 1000w" sizes="(max-width: 683px) 100vw, 683px" />
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-35b5887b animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="35b5887b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <h3 class="elementor-heading-title elementor-size-default">Hazel</h3>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-1d57a933 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1d57a933" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">Hazel Hazian</h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6456bdb9 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="6456bdb9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3505686e wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3505686e" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Putri Kedua</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-8f8fb6d wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="8f8fb6d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Bapak Amri Hazian <br> & <br> Ibu Karmilla </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-17f49bae e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="17f49bae" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="#" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-4f80d344 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="4f80d344" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-188cd1ef wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="188cd1ef" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="couple" class="elementor-menu-anchor"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-466bd400 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="466bd400" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5ea45641 wdp-sticky-section-no" data-id="5ea45641" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-6858d65a elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="6858d65a" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-40aaa8d5 wdp-sticky-section-no" data-id="40aaa8d5" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1ed77922 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="1ed77922" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:63}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="499" height="433" src="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-white-1-1.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-white-1-1.png 499w, https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-white-1-1-300x260.png 300w" sizes="(max-width: 499px) 100vw, 499px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6478e5e0 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6478e5e0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Hitung Mundur</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-43fcde86 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="43fcde86" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Hari Bahagia Kami</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1a1d2a37 bdt-countdown--align-center bdt-countdown--label-block wdp-sticky-section-no elementor-widget elementor-widget-bdt-countdown" data-id="1a1d2a37" data-element_type="widget" data-widget_type="bdt-countdown.default">
                        <div class="elementor-widget-container">
                          <div class="bdt-countdown-wrapper bdt-countdown-skin-default" data-settings="{&quot;id&quot;:&quot;#bdt-countdown-1a1d2a37&quot;,&quot;msgId&quot;:&quot;#bdt-countdown-msg-1a1d2a37&quot;,&quot;adminAjaxUrl&quot;:&quot;https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php&quot;,&quot;endActionType&quot;:&quot;none&quot;,&quot;redirectUrl&quot;:null,&quot;redirectDelay&quot;:1000,&quot;finalTime&quot;:&quot;2022-12-30T17:00:00+00:00&quot;,&quot;wpCurrentTime&quot;:1655780340,&quot;endTime&quot;:1672419600,&quot;loopHours&quot;:false,&quot;isLogged&quot;:false,&quot;couponTrickyId&quot;:&quot;bdt-sf-1a1d2a37&quot;,&quot;triggerId&quot;:false}">
                            <div id="bdt-countdown-1a1d2a37-timer" class="bdt-grid bdt-grid-small bdt-child-width-1-4 bdt-child-width-1-2@s bdt-child-width-1-4@m" data-bdt-countdown="date: 2022-12-30T17:00:00+00:00" data-bdt-grid="" style="x">
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-days-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-days bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Hari</span>
                                </div>
                              </div>
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-hours-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-hours bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Jam</span>
                                </div>
                              </div>
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-minutes-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-minutes bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Menit</span>
                                </div>
                              </div>
                              <div class="bdt-countdown-item-wrapper">
                                <div class="bdt-countdown-item bdt-seconds-wrapper">
                                  <span class="bdt-countdown-number bdt-countdown-seconds bdt-text-center"></span>
                                  <span class="bdt-countdown-label bdt-text-center">Detik</span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-677b0c79 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="677b0c79" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri, agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. Sungguh, pada yang demikian itu benar-benar terdapat tanda-tanda (kebesaran Allah) bagi kaum yang berpikir.</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-12cdca2e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="12cdca2e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">- Q.S. Ar-Rum : 21 -</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-2d3a937c wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="2d3a937c" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-24a218b6 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="24a218b6" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-background-overlay"></div>
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4508cd8f wdp-sticky-section-no" data-id="4508cd8f" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-3660a856 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3660a856" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Waktu & Tempat</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7a0bb1be wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7a0bb1be" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Pernikahan</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-444f26bc wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="444f26bc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">“Cinta tidak mengenal hambatan. Ia melompati rintangan, melompati pagar, menembus tembok untuk sampai pada tujuannya dengan penuh harapan.” </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-29f8b399 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="29f8b399" data-element_type="widget" data-widget_type="menu-anchor.default">
                        <div class="elementor-widget-container">
                          <div id="event" class="elementor-menu-anchor"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-b3aa9bf elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="b3aa9bf" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-6d048667 animated-slow wdp-sticky-section-no" data-id="6d048667" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1c09a6b0 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="1c09a6b0" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fontelloicon fontello-iconwedding-photography"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1e91f4df animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1e91f4df" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Akad</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5306891e elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="5306891e" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7f7cdcb9 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7f7cdcb9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Desember</p>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-4d9f815b elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4d9f815b" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-602debfa wdp-sticky-section-no" data-id="602debfa" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-77308cdf wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="77308cdf" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Minggu</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-356a564e wdp-sticky-section-no" data-id="356a564e" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-44da5773 wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="44da5773" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-counter">
                                    <div class="elementor-counter-number-wrapper">
                                      <span class="elementor-counter-number-prefix"></span>
                                      <span class="elementor-counter-number" data-duration="2000" data-to-value="31" data-from-value="0">0</span>
                                      <span class="elementor-counter-number-suffix"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5900fb27 wdp-sticky-section-no" data-id="5900fb27" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-1fc116a9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1fc116a9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">2022 </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-772ae0 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="772ae0" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1046017f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1046017f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">08.00 WIB</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-19f8024f wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="19f8024f" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4ab09ab elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="4ab09ab" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-56cf1ac1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56cf1ac1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Auditorium <br> Graha Widyatama </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-677da5ec wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="677da5ec" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-22c32e91 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="22c32e91" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="https://goo.gl/maps/LhSNdxaXNEUPChSw7" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-3fae3d84 animated-slow wdp-sticky-section-no" data-id="3fae3d84" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-16cdd076 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="16cdd076" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fontelloicon fontello-iconwedding-dinner"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5f6ee1f2 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5f6ee1f2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Resepsi</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6f6be265 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="6f6be265" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-598c1545 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="598c1545" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Desember</p>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-5f855133 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5f855133" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-222c8a37 wdp-sticky-section-no" data-id="222c8a37" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-722a84f1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="722a84f1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Minggu</p>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-30c1924f wdp-sticky-section-no" data-id="30c1924f" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-762cd999 wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="762cd999" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-counter">
                                    <div class="elementor-counter-number-wrapper">
                                      <span class="elementor-counter-number-prefix"></span>
                                      <span class="elementor-counter-number" data-duration="2000" data-to-value="31" data-from-value="0">0</span>
                                      <span class="elementor-counter-number-suffix"></span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-6b81e11a wdp-sticky-section-no" data-id="6b81e11a" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-5dc7b685 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5dc7b685" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">2022 </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                      <div class="elementor-element elementor-element-51b13c5b elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="51b13c5b" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-56d3e677 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56d3e677" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">14.00 WIB</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6b4b92bb wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="6b4b92bb" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-f46ae10 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="f46ae10" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-34d67635 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="34d67635" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Auditorium <br> Graha Widyatama </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-b4301f4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="b4301f4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-14aeb4e3 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="14aeb4e3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="https://goo.gl/maps/LhSNdxaXNEUPChSw7" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-7ccd1ab1 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7ccd1ab1" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;triangle&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M500,98.9L0,6.1V0h1000v6.1L500,98.9z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e7be9d4 wdp-sticky-section-no" data-id="e7be9d4" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-428322bd elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="428322bd" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-38e95752 wdp-sticky-section-no" data-id="38e95752" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1dfcb620 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="1dfcb620" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:10,&quot;end&quot;:50}},&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500,&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img src="https://unityinvitation.com/wp-content/uploads/elementor/thumbs/Unity-Diamond-1-virtual-pi3fpj9v9120uv3tixhqcpn9a7sniawddxg66fifko.jpg" title="Unity Diamond 1 virtual" alt="Unity Diamond 1 virtual" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-793f88bd wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="793f88bd" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-e97facb wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e97facb" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Kami akan <br> Melangsungkan </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6e9f69b9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6e9f69b9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Live Streaming </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1a8da0b1 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1a8da0b1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Momen kebahagiaan prosesi Pernikahan akan kami tayangkan secara virtual melalui Youtube Live.</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7328789b elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="7328789b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="https://www.youtube.com/channel/UCC3HlQTFyY0fOkiBbyWqtiA" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fab fa-youtube"></i>
                                </span>
                                <span class="elementor-button-text">Join Streaming</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-218bb78f elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="218bb78f" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-41a663a3 wdp-sticky-section-no" data-id="41a663a3" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-5c6efbd7 pp-image-slider-carousel pp-ins-normal pp-ins-hover-normal wdp-sticky-section-no elementor-widget elementor-widget-pp-image-slider" data-id="5c6efbd7" data-element_type="widget" data-settings="{&quot;skin&quot;:&quot;carousel&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;infinite_loop&quot;:&quot;yes&quot;}" data-widget_type="pp-image-slider.default">
                <div class="elementor-widget-container">
                  <div class="pp-image-slider-container">
                    <div class="pp-image-slider-wrap swiper-container-wrap">
                      <div class="pp-image-slider-box">
                        <div class="pp-image-slider pp-swiper-slider swiper-container" id="pp-image-slider-5c6efbd7" data-slider-settings="{&quot;direction&quot;:&quot;horizontal&quot;,&quot;speed&quot;:1500,&quot;effect&quot;:&quot;fade&quot;,&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10,&quot;autoHeight&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:{&quot;delay&quot;:2000},&quot;breakpoints&quot;:{&quot;1025&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;768&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;320&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:0}}}">
                          <div class="swiper-wrapper">
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-slide1.jpg" alt="Unity Diamond 1 slide1" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-slide2.jpg" alt="Unity Diamond 1 slide2" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-slide3.jpg" alt="Unity Diamond 1 slide3" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                            <div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
                              <div class="pp-image-slider-thumb-item pp-ins-filter-hover">
                                <div class="pp-image-slider-thumb-image pp-ins-filter-target">
                                  <img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-slide4.jpg" alt="Unity Diamond 1 slide4" />
                                </div>
                                <div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-189c3182 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="189c3182" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-7c6b3cc9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7c6b3cc9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Sebuah Kisah</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-64fd4d3f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="64fd4d3f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Perjalanan Kami</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-efbf8f3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="efbf8f3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Aku tidak tahu dimana ujung perjalanan ini, aku tidak bisa menjanjikan apapun. Tapi, selama aku mampu, mimpi-mimpi kita adalah prioritas." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-44641f65 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="44641f65" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-3b06949 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="3b06949" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="story" class="elementor-menu-anchor"></div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-7d08d2e8 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7d08d2e8" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-36805275 animated-slow wdp-sticky-section-no" data-id="36805275" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-23cbaee0 animated-slow justify-cstm wdp-sticky-section-no elementor-widget elementor-widget-pp-timeline" data-id="23cbaee0" data-element_type="widget" data-settings="{&quot;direction&quot;:&quot;left&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation_delay&quot;:200,&quot;layout&quot;:&quot;vertical&quot;,&quot;dates&quot;:&quot;yes&quot;,&quot;card_arrow&quot;:&quot;yes&quot;,&quot;direction_tablet&quot;:&quot;left&quot;,&quot;direction_mobile&quot;:&quot;left&quot;}" data-widget_type="pp-timeline.default">
                        <div class="elementor-widget-container">
                          <div class="pp-timeline-wrapper">
                            <div class="pp-timeline pp-timeline-vertical pp-timeline-left pp-timeline-tablet-left pp-timeline-mobile-left pp-timeline-dates pp-timeline-arrows-middle" data-timeline-layout="vertical">
                              <div class="pp-timeline-connector-wrap">
                                <div class="pp-timeline-connector">
                                  <div class="pp-timeline-connector-inner"></div>
                                </div>
                              </div>
                              <div class="pp-timeline-items">
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-3ddf251">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 11 Januari 2016 </div>
                                        <h2 class="pp-timeline-card-title"> Pertama Bertemu </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Pertama kali kita bertemu di bangku SMA tanpa mengenal satu sama lain. Kita hanya saling sapa.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 11 Januari 2016 </div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-48bb742">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 08 Juni 2019 </div>
                                        <h2 class="pp-timeline-card-title"> Menjalin Hubungan </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Setelah pertemuan di bangku SMA dan masing-masing dari kita sudah lulus, barulah kita mulai menjalin hubungan yang berawal dari DM Instagram. Hingga kita memutuskan untuk sama-sama dan LDR.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 08 Juni 2019 </div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-9e7595d">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 17 Agustus 2021 </div>
                                        <h2 class="pp-timeline-card-title"> Bertunangan </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Komunikasi jarak jauh dan kesibukan kita masing-masing bukan penghalang. Pada Agustus 2020 kedua keluarga saling bertemu dan melaksanakan acara tunangan kami.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 17 Agustus 2021 </div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-255189e">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"> 01 Desember 2022 </div>
                                        <h2 class="pp-timeline-card-title"> Menikah </h2>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p>   Akhirnya momen spesial pernikahan kami dilaksanakan pada 01 Desember 2022. Ini menjadi tanggal yang kami pilih untuk saling mengikat janji menjadi sebuah keluarga yang setia &amp; saling menyayangi.</p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"> 01 Desember 2022 </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-c92d65e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="c92d65e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;triangle&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M500,98.9L0,6.1V0h1000v6.1L500,98.9z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-72de8a wdp-sticky-section-no" data-id="72de8a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-ae8c293 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ae8c293" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Momen</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-5318aa6f animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5318aa6f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Bahagia Kami</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-6cbd54e elementor-aspect-ratio-169 wdp-sticky-section-no elementor-widget elementor-widget-video" data-id="6cbd54e" data-element_type="widget" data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/youtu.be\/-hq2DNUItd0&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;play_on_mobile&quot;:&quot;yes&quot;,&quot;mute&quot;:&quot;yes&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;yt_privacy&quot;:&quot;yes&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}" data-widget_type="video.default">
                <div class="elementor-widget-container">
                  <div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
                    <div class="elementor-video"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-5f021551 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5f021551" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Kamu adalah sahabat dan kekasihku, dan aku tidak tahu sisi mana darimu yang paling aku nikmati. Aku menghargai setiap sisi, sama seperti aku telah menghargai hidup kita bersama." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-38e611f4 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="38e611f4" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-33290783 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="33290783" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="gallery" class="elementor-menu-anchor"></div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-ed5b3eb elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ed5b3eb" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-a239ba7 wdp-sticky-section-no" data-id="a239ba7" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-0439cb7 wdp-sticky-section-no elementor-widget elementor-widget-gallery" data-id="0439cb7" data-element_type="widget" data-settings="{&quot;gallery_layout&quot;:&quot;justified&quot;,&quot;ideal_row_height&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:350,&quot;sizes&quot;:[]},&quot;ideal_row_height_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:130,&quot;sizes&quot;:[]},&quot;gap&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:5,&quot;sizes&quot;:[]},&quot;gap_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:3,&quot;sizes&quot;:[]},&quot;ideal_row_height_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:150,&quot;sizes&quot;:[]},&quot;gap_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;link_to&quot;:&quot;file&quot;,&quot;overlay_background&quot;:&quot;yes&quot;,&quot;content_hover_animation&quot;:&quot;fade-in&quot;}" data-widget_type="gallery.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-gallery__container">
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-1.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3NzcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-1-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-10.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xMC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMDQzOWNiNyJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-10-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-9.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS05LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-9-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-8.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODQsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS04LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-8-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-7.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS03LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-7-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-6.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODIsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS02LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-6-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-5.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS01LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-5-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-4.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3ODAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS00LmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-4-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-3.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3NzksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0zLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-3-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-2.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDU3NzgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0yLmpwZyIsInNsaWRlc2hvdyI6ImFsbC0wNDM5Y2I3In0%3D">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-Galeri-2-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-11.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NjcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xMS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMDQzOWNiNyJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-11-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-12.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NjgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xMi5qcGciLCJzbGlkZXNob3ciOiJhbGwtMDQzOWNiNyJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-12-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-13.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NjksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xMy5qcGciLCJzbGlkZXNob3ciOiJhbGwtMDQzOWNiNyJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-13-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-14.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NzAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xNC5qcGciLCJzbGlkZXNob3ciOiJhbGwtMDQzOWNiNyJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-14-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                            <a class="e-gallery-item elementor-gallery-item elementor-animated-content" href="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-15.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="all-0439cb7" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NzE2NzEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIyXC8wNVwvVW5pdHktRGlhbW9uZC0xLUdhbGVyaS0xNS5qcGciLCJzbGlkZXNob3ciOiJhbGwtMDQzOWNiNyJ9">
                              <div class="e-gallery-image elementor-gallery-item__image" data-thumbnail="https://unityinvitation.com/wp-content/uploads/2022/05/Unity-Diamond-1-Galeri-15-819x1024.jpg" data-width="800" data-height="1000" alt=""></div>
                              <div class="elementor-gallery-item__overlay"></div>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-20ee93b5 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="20ee93b5" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2e849993 wdp-sticky-section-no" data-id="2e849993" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-12c93c09 wdp-sticky-section-no" data-id="12c93c09" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-2b0b4c57 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2b0b4c57" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Photo by</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3052aa50 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3052aa50" data-element_type="widget" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="800" height="220" src="https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-1024x281.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-1024x281.png 1024w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-300x82.png 300w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-768x211.png 768w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-1536x422.png 1536w, https://unityinvitation.com/wp-content/uploads/2022/01/Logo-Setangkai-Project-Black-2048x563.png 2048w" sizes="(max-width: 800px) 100vw, 800px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4633138d elementor-shape-circle elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="4633138d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/setangkaiproject/" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-e163715 wdp-sticky-section-no" data-id="e163715" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-d9c435e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d9c435e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-23d56a0 wdp-sticky-section-no" data-id="23d56a0" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-8b04011 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="8b04011" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">PROTOKOL KESEHATAN</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-f16af6b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f16af6b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Tanpa mengurangi rasa hormat, <br>acara ini menerapkan Protokol Kesehatan, sesuai dengan peraturan & rekomendasi pemerintah. </h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-61770a3 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="61770a3" data-element_type="widget" data-widget_type="divider.default">
                <div class="elementor-widget-container">
                  <div class="elementor-divider">
                    <span class="elementor-divider-separator"></span>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-bd26f85 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="bd26f85" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-3fc455c animated-slow wdp-sticky-section-no elementor-invisible" data-id="3fc455c" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-669e9bc elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="669e9bc" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/medical-mask.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Gunakan Masker</h3>
                              <p class="elementor-image-box-description">Wajib menggunakan <br> masker di dalam acara </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9b02a18 animated-slow wdp-sticky-section-no elementor-invisible" data-id="9b02a18" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-289b8da elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="289b8da" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/washing-hands.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Cuci Tangan</h3>
                              <p class="elementor-image-box-description">Mencuci tangan dan <br> menggunakan Handsanitizier </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-81c0746 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="81c0746" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-6ce2bb8 animated-slow wdp-sticky-section-no elementor-invisible" data-id="6ce2bb8" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:300,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-f1bde03 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="f1bde03" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/physical.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Jaga Jarak </h3>
                              <p class="elementor-image-box-description">Menjaga jarak 2M dengan <br> tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f14f6ed animated-slow wdp-sticky-section-no elementor-invisible" data-id="f14f6ed" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:600,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6ca5e8d elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="6ca5e8d" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/no-handshake.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Tidak Berjabat Tangan</h3>
                              <p class="elementor-image-box-description">Menghindari kontak fisik dengan <br>tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-396a7c4c elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="396a7c4c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4bec872f wdp-sticky-section-no" data-id="4bec872f" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-7024c86d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7024c86d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Doa & Ucapan untuk</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-7315716b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7315716b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-6c5963f4 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6c5963f4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Merupakan suatu kehormatan dan kebahagiaan bagi kami, apabila Bapak/ Ibu/ Saudara/ i berkenan hadir, untuk memberikan do'a restu kepada kedua mempelai.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-42df8cab wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="42df8cab" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-568157b9 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="568157b9" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6629be3f animated-slow wdp-sticky-section-no elementor-invisible" data-id="6629be3f" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-64232eaa wdp-sticky-section-no elementor-widget elementor-widget-cswdeding-komentar" data-id="64232eaa" data-element_type="widget" data-widget_type="cswdeding-komentar.default">
                        <div class="elementor-widget-container">
                          <style>
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text],
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea,
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select,
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=submit] {
                              height: auto !important;
                            }

                            .halooo {
                              background-color: red !important;
                              color: #fff !important;
                            }

                            .wdp-wrapper .wdp-wrap-link {
                              text-align: center;
                              padding: 18px;
                              margin-top: 15px;
                            }

                            .wdp-wrapper .wdp-wrap-form {
                              border-top: 0px;
                            }

                            .konfirmasi-kehadiran {
                              display: flex;
                              justify-content: center;
                              flex-direction: row;
                              padding: 10px;
                              margin-top: 10px;
                            }

                            .jenis-konfirmasi>i {
                              margin: 0px 5px;
                            }

                            .jenis-konfirmasi {
                              padding: 5px 10px;
                              background: red;
                              border-radius: 0.5em;
                              color: #fff;
                              margin: 7px;
                            }
                          </style>
                          <div class='wdp-wrapper wdp-custom' style='overflow: hidden;'>
                            <div class='wdp-wrap-link'>
                              <a id='wdp-link-29221' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=29221&amp;comments=4&amp;get=0&amp;order=DESC' title='4 Ucapan'></a>
                              <div class='konfirmasi-kehadiran elementor-screen-only'>
                                <div class='jenis-konfirmasi cswd-hadir'>
                                  <i class='fas fa-check'></i> 2 Hadir
                                </div>
                                <div class='jenis-konfirmasi cswd-tidak-hadir'>
                                  <i class='fas fa-times'></i> 2 Tidak Hadir
                                </div>
                              </div>
                            </div>
                            <div id='wdp-wrap-commnent-29221' class='wdp-wrap-comments' style='display:none;'>
                              <div id='wdp-wrap-form-29221' class='wdp-wrap-form wdp-clearfix'>
                                <div id='wdp-container-form-29221' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                                  <div id='respond-29221' class='respond wdp-clearfix'>
                                    <form action='https://unityinvitation.com/wp-comments-post.php' method='post' id='commentform-29221'>
                                      <p class="comment-form-author wdp-field-1">
                                        <input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" />
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span>
                                      </p>
                                      <div class="wdp-wrap-textarea">
                                        <textarea id="wdp-textarea-29221" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                      </div>
                                      <div class="wdp-wrap-select">
                                        <select class="waci_comment wdp-select" name="konfirmasi">
                                          <option value="" disabled selected>Konfirmasi Kehadiran</option>> </option>
                                          <option value="Hadir">Hadir</option>
                                          <option value="Tidak Hadir">Tidak Hadir</option>
                                        </select>
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                      </div>
                                      <div class='wdp-wrap-submit wdp-clearfix'>
                                        <p class='form-submit'>
                                          <span class="wdp-hide">Do not change these fields following</span>
                                          <input type="text" class="wdp-hide" name="name" value="username">
                                          <input type="text" class="wdp-hide" name="nombre" value="">
                                          <input type="text" class="wdp-hide" name="form-wdp" value="">
                                          <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                          <input name='submit' id='submit-29221' value='Kirim' type='submit' />
                                          <input type='hidden' name='commentpress' value='true' />
                                          <input type='hidden' name='comment_post_ID' value='29221' id='comment_post_ID' />
                                          <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                        </p>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <div id='wdp-comment-status-29221' class='wdp-comment-status'></div>
                              <ul id='wdp-container-comment-29221' class='wdp-container-comments wdp-order-DESC  wdp-has-4-comments wdp-multiple-comments' data-order='DESC'></ul>
                              <div class='wdp-holder-29221 wdp-holder'></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-6fd76194 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="6fd76194" data-element_type="widget" data-widget_type="menu-anchor.default">
                <div class="elementor-widget-container">
                  <div id="wishes" class="elementor-menu-anchor"></div>
                </div>
              </div>
              <div class="elementor-element elementor-element-d2c9bff wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="d2c9bff" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-40c7e3a2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="40c7e3a2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Semoga Allah memberkahimu dan memberkahi apa yang menjadi tanggung jawabmu, serta menyatukan kalian berdua dalam kebaikan." </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-4f98db44 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4f98db44" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">(HR. Ahmad, at-Tirmidzi, an-Nasa'i, <br>Abu Dawud, dan Ibnu Majah) </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-6ca099be wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="6ca099be" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-3bda5dcc wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3bda5dcc" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:50}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                <div class="elementor-widget-container">
                  <img width="440" height="383" src="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-1-1.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-1-1.png 440w, https://unityinvitation.com/wp-content/uploads/2021/10/Botanical-line-Art-1-1-300x261.png 300w" sizes="(max-width: 440px) 100vw, 440px" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-101550d elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="101550d" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1cdd7b4d wdp-sticky-section-no" data-id="1cdd7b4d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-31701175 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="31701175" data-element_type="widget" data-widget_type="html.default">
                <div class="elementor-widget-container">
                  <script>
                    document.addEventListener('DOMContentLoaded', function() {
                      jQuery(function($) {
                        $('.clicktoshow').each(function(i) {
                          $(this).click(function() {
                            $('.showclick').eq(i).toggle();
                            $('.clicktoshow');
                            $('.showclick2').eq(i).hide();
                          });
                        });
                      });
                    });
                  </script>
                  <style>
                    .clicktoshow {
                      cursor: pointer;
                    }

                    .showclick {
                      display: none;
                    }
                  </style>
                  <script>
                    document.addEventListener('DOMContentLoaded', function() {
                      jQuery(function($) {
                        $('.clicktoshow2').each(function(i) {
                          $(this).click(function() {
                            $('.showclick2').eq(i).toggle();
                            $('.clicktoshow2');
                          });
                        });
                      });
                    });
                  </script>
                  <style>
                    .clicktoshow2 {
                      cursor: pointer;
                    }

                    .showclick2 {
                      display: none;
                    }
                  </style>
                </div>
              </div>
              <div class="elementor-element elementor-element-677f329 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="677f329" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
                <div class="elementor-widget-container">
                  <div class="elementor-icon-wrapper">
                    <div class="elementor-icon">
                      <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-63f74300 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="63f74300" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Hadiah Pernikahan</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-45d8d0db wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="45d8d0db" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami. </h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-2c0910fc elementor-align-center clicktoshow wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="2c0910fc" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="button.default">
                <div class="elementor-widget-container">
                  <div class="elementor-button-wrapper">
                    <a class="elementor-button elementor-size-sm elementor-animation-grow" role="button">
                      <span class="elementor-button-content-wrapper">
                        <span class="elementor-button-icon elementor-align-icon-left">
                          <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                        </span>
                        <span class="elementor-button-text">Kirim Hadiah</span>
                      </span>
                    </a>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-415a7c1d showclick elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="415a7c1d" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-112325bd wdp-sticky-section-no" data-id="112325bd" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-56120178 wdp-sticky-section-no" data-id="56120178" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-d21e409 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d21e409" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Amplop</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-fc7d887 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="fc7d887" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5c9b00bc animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="5c9b00bc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <img width="768" height="300" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA-768x300.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA-768x300.png 768w, https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA-300x117.png 300w, https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA-1024x400.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA-1536x600.png 1536w, https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA.png 1600w" sizes="(max-width: 768px) 100vw, 768px" />
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-26a9485 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="26a9485" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image img"></div>
                          <div class="head-title">a/n Hazel Hazian</div>
                          <div class="elementor-button-wrapper">
                            <div class="copy-content spancontent">123456&zwj;7891011</div>
                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                              <div class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="far fa-copy"></i>
                                </span>
                                <span class="elementor-button-text">Salin</span>
                              </div>
                            </a>
                          </div>
                          <style type="text/css">
                            .spancontent {
                              padding-bottom: 20px;
                            }

                            .copy-content {
                              color: #6EC1E4;
                              text-align: center;
                            }

                            .head-title {
                              color: #6EC1E4;
                              text-align: center;
                            }
                          </style>
                          <script>
                            function copyText(el) {
                              var content = jQuery(el).siblings('div.copy-content').html()
                              var temp = jQuery(" < textarea > ");
                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                var text = jQuery(el).html()
                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                var interval = setInterval(function() {
                                  counter++;
                                  if (counter == 2) {
                                    jQuery(el).html(text)
                                    Interval(interval);
                                  }
                                }, 5000);
                              }
                          </script>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-dd488f9 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="dd488f9" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-69d1247 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="69d1247" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Kado</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-9599d1c elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="9599d1c" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5775fc48 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="5775fc48" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image img"></div>
                          <div class="head-title">a/n Hazel Hazian</div>
                          <div class="elementor-button-wrapper">
                            <div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, &zwj;Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                              <div class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="far fa-copy"></i>
                                </span>
                                <span class="elementor-button-text">Salin</span>
                              </div>
                            </a>
                          </div>
                          <style type="text/css">
                            .spancontent {
                              padding-bottom: 20px;
                            }

                            .copy-content {
                              color: #6EC1E4;
                              text-align: center;
                            }

                            .head-title {
                              color: #6EC1E4;
                              text-align: center;
                            }
                          </style>
                          <script>
                            function copyText(el) {
                              var content = jQuery(el).siblings('div.copy-content').html()
                              var temp = jQuery(" < textarea > ");
                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                var text = jQuery(el).html()
                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                var interval = setInterval(function() {
                                  counter++;
                                  if (counter == 2) {
                                    jQuery(el).html(text)
                                    Interval(interval);
                                  }
                                }, 5000);
                              }
                          </script>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-54be1392 wdp-sticky-section-no" data-id="54be1392" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="pp-bg-effects pp-bg-effects-ca2bb6f elementor-section elementor-top-section elementor-element elementor-element-ca2bb6f pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="ca2bb6f" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="10" data-part-speed="" data-part-direction="none" data-hover-effect="noeffect" data-hover-size="" data-id="ca2bb6f" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;}">
        <div class="elementor-background-overlay"></div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-2699623a wdp-sticky-section-no" data-id="2699623a" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-63ab2925 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="63ab2925" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-200bfa5f animated-fast wdp-sticky-section-no elementor-invisible" data-id="200bfa5f" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-3b1c9e35 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3b1c9e35" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Terima Kasih</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3fca0f55 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3fca0f55" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h1 class="elementor-heading-title elementor-size-default">Romi & Hazel</h1>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6a9ffa3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6a9ffa3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Keluarga Besar</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5fd6546a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5fd6546a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Bapak Tony Raffah & Ibu Hasannah <br> Bapak Amri Hazian & Ibu Karmilla </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-419f7d39 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="419f7d39" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-8a8769a animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="8a8769a" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-4856caa wdp-sticky-section-no" data-id="4856caa" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-8b3c469 wdp-sticky-section-no" data-id="8b3c469" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-335369e wdp-sticky-section-no" data-id="335369e" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-cdfe0ce animated-slow wdp-sticky-section-no elementor-invisible" data-id="cdfe0ce" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-20f93bb animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="20f93bb" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="https://unityinvitation.com/" target="_blank">
                            <img width="768" height="589" src="https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-768x589.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-768x589.png 768w, https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-300x230.png 300w, https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite.png 779w" sizes="(max-width: 768px) 100vw, 768px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-02b20fd elementor-shape-circle animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="02b20fd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-305f5e1" href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Halo%20Kak,%20saya%20mau%20tanya%20Soal%20Undangan%20Digital%20Unity%20nih.%20Bisa%20dibantu?" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Whatsapp</span>
                                <i class="fab fa-whatsapp"></i>
                              </a>
                            </span>
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/unity.invitation/" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-4be6dad animated-slow wdp-sticky-section-no elementor-invisible" data-id="4be6dad" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6354765 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="6354765" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="https://unityinvitation.com/" target="_blank">
                            <img width="1001" height="1001" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png 1001w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-150x150.png 150w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-768x768.png 768w" sizes="(max-width: 1001px) 100vw, 1001px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-ce61bfc wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ce61bfc" data-element_type="widget" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">© 2022 Unity Invitation, <br>All Rights Reserved </h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-a26b241 wdp-sticky-section-no" data-id="a26b241" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-1aff0ae wdp-sticky-section-no" data-id="1aff0ae" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-ad0ee02 wdp-sticky-section-no" data-id="ad0ee02" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-78da72d3 elementor-widget__width-initial elementor-fixed elementor-widget-mobile__width-initial elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-cswd-audio" data-id="78da72d3" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="cswd-audio.default">
                <div class="elementor-widget-container">
                  <script>
                    var settingAutoplay = 'yes';
                    window.settingAutoplay = settingAutoplay === 'disable' ? false : true;
                  </script>
                  <div id="audio-container" class="audio-box">
                    <style>
                      .play_cswd_audio {
                        animation-name: putar;
                        animation-duration: 5000ms;
                        animation-iteration-count: infinite;
                        animation-timing-function: linear;
                      }

                      @keyframes putar {
                        from {
                          transform: rotate(0deg);
                        }

                        to {
                          transform: rotate(360deg);
                        }
                      }
                    </style>
                    <audio id="song" loop>
                      <source src="https://unityinvitation.com/wp-content/uploads/2022/03/Kenny-G-The-Moment-Saxophone.mp3" type="audio/mp3">
                    </audio>
                    <div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
                      <div class="elementor-icon">
                        <img src="https://unityinvitation.com/wp-content/uploads/2022/02/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="pause_cswd_audio">
                      </div>
                    </div>
                    <div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
                      <div class="elementor-icon">
                        <img src="https://unityinvitation.com/wp-content/uploads/2022/02/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="play_cswd_audio">
                      </div>
                    </div>
                  </div>
                  <!-- <script>
			jQuery("document").ready(function (n) {
				var e = window.settingAutoplay;
				e ? (n("#mute-sound").show(), document.getElementById("song").play()) : n("#unmute-sound").show(),
					n("#audio-container").click(function (u) {
						e ? (n("#mute-sound").hide(), n("#unmute-sound").show(), document.getElementById("song").pause(), (e = !1)) : (n("#unmute-sound").hide(), n("#mute-sound").show(), document.getElementById("song").play(), (e = !0));
					});
			});
		</script> -->
                </div>
              </div>
              <div class="elementor-element elementor-element-111b2886 wdp-sticky-section-no elementor-widget elementor-widget-CSWeding_modal_popup" data-id="111b2886" data-element_type="widget" data-settings="{&quot;exit_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="CSWeding_modal_popup.default">
                <div class="elementor-widget-container">
                  <style>
                    .elementor-image>img {
                      display: initial !important;
                    }
                  </style>
                  <div class="modalx animated " data-sampul='https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-1-cover1-scaled.jpg'>
                    <div class="overlayy"></div>
                    <div class="content-modalx">
                      <div class="info_modalx">
                        <div class="elementor-image img">
                          <img src="https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Diamond-1-cover2.png" title="Unity Diamond 1 cover2" alt="Unity Diamond 1 cover2" />
                        </div>
                        <div class="text_tambahan">PERNIKAHAN</div>
                        <div class="wdp-mempelai">Romi & Hazel</div>
                        <div class="wdp-dear">Yth. Bapak/Ibu/Saudara/i</div>
                        <div class="wdp-name"> Nama Tamu </div>
                        <div class="wdp-text">Tanpa mengurangi rasa hormat, <br> Kami mengundang Bapak/Ibu/Saudara/i <br>untuk hadir di acara pernikahan kami. </div>
                        <div class="wdp-button-wrapper">
                          <button class="elementor-button"> Buka Undangan </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <script>
                    const sampul = jQuery('.modalx').data('sampul');
                    jQuery('.modalx').css('background-image', 'url(' + sampul + ')');
                    jQuery('body').css('overflow', 'hidden');
                    jQuery('.wdp-button-wrapper button').on('click', function() {
                      jQuery('.modalx').removeClass('');
                      jQuery('.modalx').addClass('fadeIn reverse');
                      setTimeout(function() {
                        jQuery('.modalx').addClass('removeModals');
                      }, 1600);
                      jQuery('body').css('overflow', 'auto');
                      document.getElementById("song").play();
                    });
                  </script>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer class="elementor-section elementor-top-section elementor-element elementor-element-4d44ac51 animated-slow elementor-hidden-desktop elementor-hidden-tablet elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="4d44ac51" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:2500,&quot;sticky&quot;:&quot;bottom&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-b4e5efc wdp-sticky-section-no" data-id="b4e5efc" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-background-overlay"></div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-4301bc67 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4301bc67" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-1a68644e animated-slow wdp-sticky-section-no elementor-invisible" data-id="1a68644e" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:2500,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1cfee501 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="1cfee501" data-element_type="widget" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="#home">
                            <img width="500" height="500" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-menu.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-menu.png 500w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-menu-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Diamond-1-menu-150x150.png 150w" sizes="(max-width: 500px) 100vw, 500px" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-4c7d8168 animated-slow wdp-sticky-section-no elementor-invisible" data-id="4c7d8168" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:3000,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1d170234 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="1d170234" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#couple">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-couple"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-2a2acf80 animated-slow wdp-sticky-section-no elementor-invisible" data-id="2a2acf80" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:3500,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-10ceca95 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="10ceca95" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#event">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-event"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-6bebb4a animated-slow wdp-sticky-section-no elementor-invisible" data-id="6bebb4a" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:4000,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6f1e2792 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="6f1e2792" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#story">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-love-story"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-4b6b46c7 animated-slow wdp-sticky-section-no elementor-invisible" data-id="4b6b46c7" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:4500,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-12d8e4fe elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="12d8e4fe" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#gallery">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gallery"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-265976dd animated-slow wdp-sticky-section-no elementor-invisible" data-id="265976dd" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:5000,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-2b98a854 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="2b98a854" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <a class="elementor-icon elementor-animation-grow" href="#wishes">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-wishes"></i>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </footer>
    </div>
    <div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
          <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-widget-wrap elementor-element-populated">
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-spacer">
                              <div class="elementor-spacer-inner"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                  <div class="elementor-widget-container">
                    <p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami. </p>
                  </div>
                </div>
                <div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
                  <div class="elementor-widget-container">
                    <div class="elementor-spacer">
                      <div class="elementor-spacer-inner"></div>
                    </div>
                  </div>
                </div>
                <div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
                  <div class="elementor-widget-container">
                    <div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
                      <div class="jet-tabs__control-wrapper">
                        <div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-Mandiri.png" alt="">
                          </div>
                        </div>
                      </div>
                      <div class="jet-tabs__content-wrapper">
                        <div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                          <div class="elementor-widget-container">
                            <h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-icon-wrapper">
                              <div class="elementor-icon">
                                <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-image img"></div>
                            <div class="head-title">a/n Bimo Bramantyo</div>
                            <div class="elementor-button-wrapper">
                              <div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
                              <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                <div class="elementor-button-content-wrapper">
                                  <span class="elementor-button-icon elementor-align-icon-left">
                                    <i aria-hidden="true" class="far fa-copy"></i>
                                  </span>
                                  <span class="elementor-button-text">Salin</span>
                                </div>
                              </a>
                            </div>
                            <style type="text/css">
                              .spancontent {
                                padding-bottom: 20px;
                              }

                              .copy-content {
                                color: #6EC1E4;
                                text-align: center;
                              }

                              .head-title {
                                color: #6EC1E4;
                                text-align: center;
                              }
                            </style>
                            <script>
                              function copyText(el) {
                                var content = jQuery(el).siblings('div.copy-content').html()
                                var temp = jQuery(" < textarea > ");
                                  jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                  var text = jQuery(el).html()
                                  jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                  var interval = setInterval(function() {
                                    counter++;
                                    if (counter == 2) {
                                      jQuery(el).html(text)
                                      Interval(interval);
                                    }
                                  }, 5000);
                                }
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
          <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
              <div class="elementor-widget-wrap elementor-element-populated">
                <div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
                  <div class="elementor-widget-container">
                    <div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
                      <nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                      <div class="pp-menu-toggle pp-menu-toggle-on-tablet">
                        <div class="pp-hamburger">
                          <div class="pp-hamburger-box">
                            <div class="pp-hamburger-inner"></div>
                          </div>
                        </div>
                      </div>
                      <nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <link rel='stylesheet' id='ep-countdown-css' href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-countdown.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='fancybox-css' href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.css?ver=2.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-gallery-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/e-gallery/css/e-gallery.min.css?ver=1.2.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-51208-css' href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51208.css?ver=1655470782' type='text/css' media='all' />
    <link rel='stylesheet' id='e-animations-css' href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
    <script type='text/javascript' id='WEDKU_SCRP-js-extra'>
      /* 
																																								<![CDATA[ */
      var ceper = {
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.18' id='WEDKU_SCRP-js'></script>
    <script type='text/javascript' id='wdp_js_script-js-extra'>
      /* 
																																								<![CDATA[ */
      var WDP_WP = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "wdpNonce": "3ae8897e57",
        "jpages": "true",
        "jPagesNum": "5",
        "textCounter": "true",
        "textCounterNum": "500",
        "widthWrap": "",
        "autoLoad": "true",
        "thanksComment": "Terima kasih atas ucapan & doanya!",
        "thanksReplyComment": "Terima kasih atas balasannya!",
        "duplicateComment": "You might have left one of the fields blank, or duplicate comments",
        "insertImage": "Insert image",
        "insertVideo": "Insert video",
        "insertLink": "Insert link",
        "checkVideo": "Check video",
        "accept": "Accept",
        "cancel": "Cancel",
        "reply": "Balas",
        "textWriteComment": "Tulis Ucapan & Doa",
        "classPopularComment": "wdp-popular-comment",
        "textUrlImage": "Url image",
        "textUrlVideo": "Url video youtube or vimeo",
        "textUrlLink": "Url link",
        "textToDisplay": "Text to display",
        "textCharacteresMin": "Minimal 2 karakter",
        "textNavNext": "Selanjutnya",
        "textNavPrev": "Sebelumnya",
        "textMsgDeleteComment": "Do you want delete this comment?",
        "textLoadMore": "Load more",
        "textLikes": "Likes"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/loftloader/assets/js/loftloader.min.js?ver=2022022501' id='loftloader-lite-front-main-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/pp-bg-effects.min.js?ver=1.0.0' id='pp-bg-effects-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/particles/particles.min.js?ver=2.0.0' id='particles-js'></script>
    <script type='text/javascript' id='bdt-uikit-js-extra'>
      /* 
																																								<![CDATA[ */
      var element_pack_ajax_login_config = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "language": "en",
        "loadingmessage": "Sending user info, please wait...",
        "unknownerror": "Unknown error, make sure access is correct!"
      };
      var ElementPackConfig = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "nonce": "ebe2274658",
        "data_table": {
          "language": {
            "lengthMenu": "Show _MENU_ Entries",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "search": "Search :",
            "paginate": {
              "previous": "Previous",
              "next": "Next"
            }
          }
        },
        "contact_form": {
          "sending_msg": "Sending message please wait...",
          "captcha_nd": "Invisible captcha not defined!",
          "captcha_nr": "Could not get invisible captcha response!"
        },
        "mailchimp": {
          "subscribing": "Subscribing you please wait..."
        },
        "elements_data": {
          "sections": [],
          "columns": [],
          "widgets": []
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
    <script type='text/javascript' id='elementor-frontend-js-before'>
      var elementorFrontendConfig = {
        "environmentMode": {
          "edit": false,
          "wpPreview": false,
          "isScriptDebug": false
        },
        "i18n": {
          "shareOnFacebook": "Share on Facebook",
          "shareOnTwitter": "Share on Twitter",
          "pinIt": "Pin it",
          "download": "Download",
          "downloadImage": "Download image",
          "fullscreen": "Fullscreen",
          "zoom": "Zoom",
          "share": "Share",
          "playVideo": "Play Video",
          "previous": "Previous",
          "next": "Next",
          "close": "Close"
        },
        "is_rtl": false,
        "breakpoints": {
          "xs": 0,
          "sm": 480,
          "md": 768,
          "lg": 1025,
          "xl": 1440,
          "xxl": 1600
        },
        "responsive": {
          "breakpoints": {
            "mobile": {
              "label": "Mobile",
              "value": 767,
              "default_value": 767,
              "direction": "max",
              "is_enabled": true
            },
            "mobile_extra": {
              "label": "Mobile Extra",
              "value": 880,
              "default_value": 880,
              "direction": "max",
              "is_enabled": false
            },
            "tablet": {
              "label": "Tablet",
              "value": 1024,
              "default_value": 1024,
              "direction": "max",
              "is_enabled": true
            },
            "tablet_extra": {
              "label": "Tablet Extra",
              "value": 1200,
              "default_value": 1200,
              "direction": "max",
              "is_enabled": false
            },
            "laptop": {
              "label": "Laptop",
              "value": 1366,
              "default_value": 1366,
              "direction": "max",
              "is_enabled": false
            },
            "widescreen": {
              "label": "Widescreen",
              "value": 2400,
              "default_value": 2400,
              "direction": "min",
              "is_enabled": false
            }
          }
        },
        "version": "3.6.4",
        "is_static": false,
        "experimentalFeatures": {
          "e_dom_optimization": true,
          "a11y_improvements": true,
          "e_import_export": true,
          "e_hidden_wordpress_widgets": true,
          "theme_builder_v2": true,
          "landing-pages": true,
          "elements-color-picker": true,
          "favorite-widgets": true,
          "admin-top-bar": true,
          "form-submissions": true
        },
        "urls": {
          "assets": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"
        },
        "settings": {
          "page": [],
          "editorPreferences": []
        },
        "kit": {
          "viewport_tablet": 1024,
          "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
          "global_image_lightbox": "yes"
        },
        "post": {
          "id": 29221,
          "title": "Unity%20Diamond%201",
          "excerpt": "",
          "featuredImage": "https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/10\/Unity-Diamond-1-cover-invitation.jpg"
        }
      };
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/modules/ep-countdown.min.js?ver=6.0.10' id='ep-countdown-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1' id='jquery-numerator-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.js?ver=2.8.2' id='jquery-fancybox-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/jquery-resize/jquery.resize.min.js?ver=0.5.3' id='jquery-resize-js'></script>
    <script type='text/javascript' id='powerpack-frontend-js-extra'>
      /* 
																																								<![CDATA[ */
      var ppLogin = {
        "empty_username": "Enter a username or email address.",
        "empty_password": "Enter password.",
        "empty_password_1": "Enter a password.",
        "empty_password_2": "Re-enter password.",
        "empty_recaptcha": "Please check the captcha to verify you are not a robot.",
        "email_sent": "A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.",
        "reset_success": "Your password has been reset successfully.",
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      var ppRegistration = {
        "invalid_username": "This username is invalid because it uses illegal characters. Please enter a valid username.",
        "username_exists": "This username is already registered. Please choose another one.",
        "empty_email": "Please type your email address.",
        "invalid_email": "The email address isn\u2019t correct!",
        "email_exists": "The email is already registered, please choose another one.",
        "password": "Password must not contain the character \"\\\\\"",
        "password_length": "Your password should be at least 8 characters long.",
        "password_mismatch": "Password does not match.",
        "invalid_url": "URL seems to be invalid.",
        "recaptcha_php_ver": "reCAPTCHA API requires PHP version 5.3 or above.",
        "recaptcha_missing_key": "Your reCAPTCHA Site or Secret Key is missing!",
        "show_password": "Show password",
        "hide_password": "Hide password",
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/slick/slick.min.js?ver=2.8.2' id='pp-slick-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-timeline.min.js?ver=2.8.2' id='pp-timeline-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/e-gallery/js/e-gallery.min.js?ver=1.2.0' id='elementor-gallery-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
    <script type='text/javascript' id='elementor-pro-frontend-js-before'>
      var ElementorProFrontendConfig = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "nonce": "1c6e428e33",
        "urls": {
          "assets": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/",
          "rest": "https:\/\/unityinvitation.com\/wp-json\/"
        },
        "i18n": {
          "toc_no_headings_found": "No headings were found on this page."
        },
        "shareButtonsNetworks": {
          "facebook": {
            "title": "Facebook",
            "has_counter": true
          },
          "twitter": {
            "title": "Twitter"
          },
          "linkedin": {
            "title": "LinkedIn",
            "has_counter": true
          },
          "pinterest": {
            "title": "Pinterest",
            "has_counter": true
          },
          "reddit": {
            "title": "Reddit",
            "has_counter": true
          },
          "vk": {
            "title": "VK",
            "has_counter": true
          },
          "odnoklassniki": {
            "title": "OK",
            "has_counter": true
          },
          "tumblr": {
            "title": "Tumblr"
          },
          "digg": {
            "title": "Digg"
          },
          "skype": {
            "title": "Skype"
          },
          "stumbleupon": {
            "title": "StumbleUpon",
            "has_counter": true
          },
          "mix": {
            "title": "Mix"
          },
          "telegram": {
            "title": "Telegram"
          },
          "pocket": {
            "title": "Pocket",
            "has_counter": true
          },
          "xing": {
            "title": "XING",
            "has_counter": true
          },
          "whatsapp": {
            "title": "WhatsApp"
          },
          "email": {
            "title": "Email"
          },
          "print": {
            "title": "Print"
          }
        },
        "facebook_sdk": {
          "lang": "en_US",
          "app_id": ""
        },
        "lottie": {
          "defaultAnimationUrl": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"
        }
      };
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
    <script type='text/javascript' id='jet-elements-js-extra'>
      /* 
																																								<![CDATA[ */
      var jetElements = {
        "ajaxUrl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "isMobile": "false",
        "templateApiUrl": "https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template",
        "devMode": "false",
        "messages": {
          "invalidMail": "Please specify a valid e-mail"
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
    <script type='text/javascript' id='jet-tabs-frontend-js-extra'>
      /* 
																																								<![CDATA[ */
      var JetTabsSettings = {
        "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "isMobile": "false",
        "templateApiUrl": "https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template",
        "devMode": "false"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
    <script type='text/javascript' id='weddingpress-wdp-js-extra'>
      /* 
																																								<![CDATA[ */
      var cevar = {
        "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "plugin_url": "https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
    <div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://unityinvitation.com/wp-admin/admin-ajax.php"></div>
    <div data-pafe-form-builder-tinymce-upload="https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>
  </body>
</html>
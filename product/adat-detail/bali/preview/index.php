
<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
		
        <style type="text/css">

            .wdp-comment-text img {

                max-width: 100% !important;

            }

        </style>

        
<!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
<title>Unity Adat Bali</title>
<meta name="description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami."/>
<meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet"/>
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Unity Adat Bali" />
<meta property="og:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
<meta property="og:url" content="https://unityinvitation.com/unity-adat-bali/" />
<meta property="og:site_name" content="Unity Invitation" />
<meta property="article:section" content="Undangan" />
<meta property="og:updated_time" content="2022-04-04T00:49:52+07:00" />
<meta property="og:image" content="https://unityinvitation.com/wp-content/uploads/2021/11/Adat-Bali-cover-invitation.jpg" />
<meta property="og:image:secure_url" content="https://unityinvitation.com/wp-content/uploads/2021/11/Adat-Bali-cover-invitation.jpg" />
<meta property="og:image:width" content="500" />
<meta property="og:image:height" content="500" />
<meta property="og:image:alt" content="Unity Adat Bali" />
<meta property="og:image:type" content="image/jpeg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Unity Adat Bali" />
<meta name="twitter:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
<meta name="twitter:image" content="https://unityinvitation.com/wp-content/uploads/2021/11/Adat-Bali-cover-invitation.jpg" />
<meta name="twitter:label1" content="Written by" />
<meta name="twitter:data1" content="admin" />
<meta name="twitter:label2" content="Time to read" />
<meta name="twitter:data2" content="3 minutes" />
<script type="application/ld+json" class="rank-math-schema">{"@context":"https://schema.org","@graph":[{"@type":"BreadcrumbList","@id":"https://unityinvitation.com/unity-adat-bali/#breadcrumb","itemListElement":[{"@type":"ListItem","position":"1","item":{"@id":"https://unityinvitation.com","name":"Home"}},{"@type":"ListItem","position":"2","item":{"@id":"https://unityinvitation.com/unity-adat-bali/","name":"Unity Adat Bali"}}]}]}</script>
<!-- /Rank Math WordPress SEO plugin -->

<link rel='dns-prefetch' href='//use.fontawesome.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Feed" href="https://unityinvitation.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Comments Feed" href="https://unityinvitation.com/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Unity Adat Bali Comments Feed" href="https://unityinvitation.com/unity-adat-bali/feed/" />
<script type="text/javascript">
window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.9.3"}};
/*! This file is auto-generated */
!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([10084,65039,8205,55357,56613],[10084,65039,8203,55357,56613])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
</script>
<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 0.07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='bdt-uikit-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
<link rel='stylesheet' id='ep-helper-css'  href='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://unityinvitation.com/wp-includes/css/dist/block-library/style.min.css?ver=5.9.3' type='text/css' media='all' />
<style id='global-styles-inline-css' type='text/css'>
body{--wp--preset--color--black: #000000;--wp--preset--color--cyan-bluish-gray: #abb8c3;--wp--preset--color--white: #ffffff;--wp--preset--color--pale-pink: #f78da7;--wp--preset--color--vivid-red: #cf2e2e;--wp--preset--color--luminous-vivid-orange: #ff6900;--wp--preset--color--luminous-vivid-amber: #fcb900;--wp--preset--color--light-green-cyan: #7bdcb5;--wp--preset--color--vivid-green-cyan: #00d084;--wp--preset--color--pale-cyan-blue: #8ed1fc;--wp--preset--color--vivid-cyan-blue: #0693e3;--wp--preset--color--vivid-purple: #9b51e0;--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg,rgba(6,147,227,1) 0%,rgb(155,81,224) 100%);--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg,rgb(122,220,180) 0%,rgb(0,208,130) 100%);--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg,rgba(252,185,0,1) 0%,rgba(255,105,0,1) 100%);--wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg,rgba(255,105,0,1) 0%,rgb(207,46,46) 100%);--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg,rgb(238,238,238) 0%,rgb(169,184,195) 100%);--wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg,rgb(74,234,220) 0%,rgb(151,120,209) 20%,rgb(207,42,186) 40%,rgb(238,44,130) 60%,rgb(251,105,98) 80%,rgb(254,248,76) 100%);--wp--preset--gradient--blush-light-purple: linear-gradient(135deg,rgb(255,206,236) 0%,rgb(152,150,240) 100%);--wp--preset--gradient--blush-bordeaux: linear-gradient(135deg,rgb(254,205,165) 0%,rgb(254,45,45) 50%,rgb(107,0,62) 100%);--wp--preset--gradient--luminous-dusk: linear-gradient(135deg,rgb(255,203,112) 0%,rgb(199,81,192) 50%,rgb(65,88,208) 100%);--wp--preset--gradient--pale-ocean: linear-gradient(135deg,rgb(255,245,203) 0%,rgb(182,227,212) 50%,rgb(51,167,181) 100%);--wp--preset--gradient--electric-grass: linear-gradient(135deg,rgb(202,248,128) 0%,rgb(113,206,126) 100%);--wp--preset--gradient--midnight: linear-gradient(135deg,rgb(2,3,129) 0%,rgb(40,116,252) 100%);--wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');--wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');--wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');--wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');--wp--preset--duotone--midnight: url('#wp-duotone-midnight');--wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');--wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');--wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');--wp--preset--font-size--small: 13px;--wp--preset--font-size--medium: 20px;--wp--preset--font-size--large: 36px;--wp--preset--font-size--x-large: 42px;}.has-black-color{color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-color{color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-color{color: var(--wp--preset--color--white) !important;}.has-pale-pink-color{color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-color{color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-color{color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-color{color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-color{color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-color{color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-color{color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-color{color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-color{color: var(--wp--preset--color--vivid-purple) !important;}.has-black-background-color{background-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-background-color{background-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-background-color{background-color: var(--wp--preset--color--white) !important;}.has-pale-pink-background-color{background-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-background-color{background-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-background-color{background-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-background-color{background-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-background-color{background-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-background-color{background-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-background-color{background-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-background-color{background-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-background-color{background-color: var(--wp--preset--color--vivid-purple) !important;}.has-black-border-color{border-color: var(--wp--preset--color--black) !important;}.has-cyan-bluish-gray-border-color{border-color: var(--wp--preset--color--cyan-bluish-gray) !important;}.has-white-border-color{border-color: var(--wp--preset--color--white) !important;}.has-pale-pink-border-color{border-color: var(--wp--preset--color--pale-pink) !important;}.has-vivid-red-border-color{border-color: var(--wp--preset--color--vivid-red) !important;}.has-luminous-vivid-orange-border-color{border-color: var(--wp--preset--color--luminous-vivid-orange) !important;}.has-luminous-vivid-amber-border-color{border-color: var(--wp--preset--color--luminous-vivid-amber) !important;}.has-light-green-cyan-border-color{border-color: var(--wp--preset--color--light-green-cyan) !important;}.has-vivid-green-cyan-border-color{border-color: var(--wp--preset--color--vivid-green-cyan) !important;}.has-pale-cyan-blue-border-color{border-color: var(--wp--preset--color--pale-cyan-blue) !important;}.has-vivid-cyan-blue-border-color{border-color: var(--wp--preset--color--vivid-cyan-blue) !important;}.has-vivid-purple-border-color{border-color: var(--wp--preset--color--vivid-purple) !important;}.has-vivid-cyan-blue-to-vivid-purple-gradient-background{background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;}.has-light-green-cyan-to-vivid-green-cyan-gradient-background{background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;}.has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;}.has-luminous-vivid-orange-to-vivid-red-gradient-background{background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;}.has-very-light-gray-to-cyan-bluish-gray-gradient-background{background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;}.has-cool-to-warm-spectrum-gradient-background{background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;}.has-blush-light-purple-gradient-background{background: var(--wp--preset--gradient--blush-light-purple) !important;}.has-blush-bordeaux-gradient-background{background: var(--wp--preset--gradient--blush-bordeaux) !important;}.has-luminous-dusk-gradient-background{background: var(--wp--preset--gradient--luminous-dusk) !important;}.has-pale-ocean-gradient-background{background: var(--wp--preset--gradient--pale-ocean) !important;}.has-electric-grass-gradient-background{background: var(--wp--preset--gradient--electric-grass) !important;}.has-midnight-gradient-background{background: var(--wp--preset--gradient--midnight) !important;}.has-small-font-size{font-size: var(--wp--preset--font-size--small) !important;}.has-medium-font-size{font-size: var(--wp--preset--font-size--medium) !important;}.has-large-font-size{font-size: var(--wp--preset--font-size--large) !important;}.has-x-large-font-size{font-size: var(--wp--preset--font-size--x-large) !important;}
</style>
<link rel='stylesheet' id='WEDKU_STYLE-css'  href='https://unityinvitation.com/wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.16' type='text/css' media='all' />
<link rel='stylesheet' id='pafe-extension-style-css'  href='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
<link rel='stylesheet' id='wdp_style-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
<style id='wdp_style-inline-css' type='text/css'>


        .wdp-wrapper {

          font-size: 14px

        }

    

        .wdp-wrapper {

          background: #ffffff;

        }

        .wdp-wrapper.wdp-border {

          border: 1px solid #d5deea;

        }



        .wdp-wrapper .wdp-wrap-comments a:link,

        .wdp-wrapper .wdp-wrap-comments a:visited {

          color: #54595f;

        }



        .wdp-wrapper .wdp-wrap-link a.wdp-link {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {

          color: #54595f;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-wrap-form {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {

          border: 1px solid #d5deea;

          background: #FFFFFF;

          color: #44525F;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {

          border-color: #64B6EC;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {

          color: #FFFFFF;

          background: #54595f;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,

        .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {

          background: #a3a3a3;

        }

        .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {

          color: #44525F;

        }



        .wdp-wrapper .wdp-media-btns a > span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-media-btns a > span:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-comment-status {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {

          color: #319342;

        }

        .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {

          color: #C85951;

        }

        .wdp-wrapper .wdp-comment-status.wdp-loading > span {

          color: #54595f;

        }



        .wdp-wrapper ul.wdp-container-comments {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {

          border-bottom: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {

          border-top: 1px solid #d5deea;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {

          color: #54595f !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {

          color: #2a5782 !important;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {

          color: #44525F;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {

          color: #54595f;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {

          color: #2a5782;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link > span:hover {

          color: #3D7DBC;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {

          color: #9DA8B7;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {

          color: #2C9E48;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {

          color: #D13D3D;

        }

        .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {

          color: #c9cfd7;

        }

        .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {

          color: #2a5782;

        }



        .wdp-wrapper .wdp-counter-info {

          color: #9DA8B7;

        }



        .wdp-wrapper .wdp-holder span {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a,

        .wdp-wrapper .wdp-holder a:link,

        .wdp-wrapper .wdp-holder a:visited {

          color: #54595f;

        }

        .wdp-wrapper .wdp-holder a:hover,

        .wdp-wrapper .wdp-holder a:link:hover,

        .wdp-wrapper .wdp-holder a:visited:hover {

          color: #2a5782;

        }

        .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled, .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled, .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {

          color: #9DA8B7;

        }

        

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {

        max-width: 28px;

        max-height: 28px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {

        margin-left: 38px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {

        max-width: 24px;

        max-height: 24px;

    }

    .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {

        max-width: 21px;

        max-height: 21px;

    }

    
</style>
<link rel='stylesheet' id='wdp-centered-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-horizontal-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='wdp-fontello-css-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='exad-main-style-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-css'  href='https://unityinvitation.com/wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='hello-elementor-theme-style-css'  href='https://unityinvitation.com/wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-css'  href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='jet-elements-skin-css'  href='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-1399-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-1399.css?ver=1652056007' type='text/css' media='all' />
<link rel='stylesheet' id='powerpack-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='weddingpress-wdp-css'  href='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-pro-css'  href='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
<link rel='stylesheet' id='uael-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
<link rel='stylesheet' id='jet-tabs-frontend-css'  href='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-27361-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-27361.css?ver=1652060618' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51207-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51207.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-31584-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-31584.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-official-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
<link rel='stylesheet' id='font-awesome-official-v4shim-css'  href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
<style id='font-awesome-official-v4shim-inline-css' type='text/css'>
@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
}

@font-face {
font-family: "FontAwesome";
font-display: block;
src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
		url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
unicode-range: U+F004-F005,U+F007,U+F017,U+F022,U+F024,U+F02E,U+F03E,U+F044,U+F057-F059,U+F06E,U+F070,U+F075,U+F07B-F07C,U+F080,U+F086,U+F089,U+F094,U+F09D,U+F0A0,U+F0A4-F0A7,U+F0C5,U+F0C7-F0C8,U+F0E0,U+F0EB,U+F0F3,U+F0F8,U+F0FE,U+F111,U+F118-F11A,U+F11C,U+F133,U+F144,U+F146,U+F14A,U+F14D-F14E,U+F150-F152,U+F15B-F15C,U+F164-F165,U+F185-F186,U+F191-F192,U+F1AD,U+F1C1-F1C9,U+F1CD,U+F1D8,U+F1E3,U+F1EA,U+F1F6,U+F1F9,U+F20A,U+F247-F249,U+F24D,U+F254-F25B,U+F25D,U+F267,U+F271-F274,U+F279,U+F28B,U+F28D,U+F2B5-F2B6,U+F2B9,U+F2BB,U+F2BD,U+F2C1-F2C2,U+F2D0,U+F2D2,U+F2DC,U+F2ED,U+F328,U+F358-F35B,U+F3A5,U+F3D1,U+F410,U+F4AD;
}
</style>
<link rel='stylesheet' id='google-fonts-1-css'  href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CLora%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.9.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-shared-0-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-brands-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-wedding-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/wedding/css/wedding.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-solid-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-unityinv-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-fa-regular-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
<script type='text/javascript' id='jquery-core-js-extra'>
/* <![CDATA[ */
var pp = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
<link rel="https://api.w.org/" href="https://unityinvitation.com/wp-json/" /><link rel="alternate" type="application/json" href="https://unityinvitation.com/wp-json/wp/v2/posts/27361" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://unityinvitation.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://unityinvitation.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.9.3" />
<link rel='shortlink' href='https://unityinvitation.com/?p=27361' />
<link rel="alternate" type="application/json+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-adat-bali%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://unityinvitation.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Funity-adat-bali%2F&#038;format=xml" />
<link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-32x32.png" sizes="32x32" />
<link rel="icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon" href="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-180x180.png" />
<meta name="msapplication-TileImage" content="https://unityinvitation.com/wp-content/uploads/2021/05/cropped-Unity-Icon-270x270.png" />
<style>.pswp.pafe-lightbox-modal {display: none;}</style>		<style type="text/css" id="wp-custom-css">
			.elementor-section{
	overflow:hidden;
}

.justify-cstm .pp-timeline-card{
	text-align:justify !important;
}		</style>
			<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" /></head>
<body data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-27361 single-format-standard elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-27361">
	<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-dark-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0.49803921568627" /><feFuncG type="table" tableValues="0 0.49803921568627" /><feFuncB type="table" tableValues="0 0.49803921568627" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-grayscale"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-red"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 1" /><feFuncG type="table" tableValues="0 0.27843137254902" /><feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-midnight"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0 0" /><feFuncG type="table" tableValues="0 0.64705882352941" /><feFuncB type="table" tableValues="0 1" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-magenta-yellow"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.78039215686275 1" /><feFuncG type="table" tableValues="0 0.94901960784314" /><feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-purple-green"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" /><feFuncG type="table" tableValues="0 1" /><feFuncB type="table" tableValues="0.44705882352941 0.4" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;" ><defs><filter id="wp-duotone-blue-orange"><feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " /><feComponentTransfer color-interpolation-filters="sRGB" ><feFuncR type="table" tableValues="0.098039215686275 1" /><feFuncG type="table" tableValues="0 0.66274509803922" /><feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" /><feFuncA type="table" tableValues="1 1" /></feComponentTransfer><feComposite in2="SourceGraphic" operator="in" /></filter></defs></svg>		<div data-elementor-type="wp-post" data-elementor-id="27361" class="elementor elementor-27361">
									<section class="pp-bg-effects pp-bg-effects-9d64c18 elementor-section elementor-top-section elementor-element elementor-element-9d64c18 elementor-section-stretched pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="9d64c18" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="#B7881E" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="75" data-part-speed="" data-part-direction="none" data-hover-effect="grab" data-hover-size="" data-id="9d64c18" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;slideshow&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;background_slideshow_gallery&quot;:[{&quot;id&quot;:46518,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Adat-Bali-Adat-Bali-home4.jpg&quot;},{&quot;id&quot;:46519,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Adat-Bali-Adat-Bali-home5.jpg&quot;},{&quot;id&quot;:46520,&quot;url&quot;:&quot;https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/12\/Unity-Adat-Bali-Adat-Bali-home6.jpg&quot;}],&quot;background_slideshow_transition_duration&quot;:1500,&quot;background_slideshow_slide_duration&quot;:2500,&quot;sticky&quot;:&quot;top&quot;,&quot;background_slideshow_loop&quot;:&quot;yes&quot;,&quot;background_slideshow_slide_transition&quot;:&quot;fade&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1d2e86a wdp-sticky-section-no" data-id="1d2e86a" data-element_type="column" id="home" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-c36ff28 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c36ff28" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;bounceIn&quot;,&quot;_animation_delay&quot;:200}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">PAWIWAHAN</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-92092c9 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="92092c9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-default">Wijaya & Ami</h1>		</div>
				</div>
				<div class="elementor-element elementor-element-0e84ed8 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="0e84ed8" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-913b23d animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="913b23d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Payangan</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-fc28bfa animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="fc28bfa" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;,&quot;_animation_delay&quot;:600}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">31 . 12 . 2022</h3>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-2483d06 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2483d06" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f81c64d wdp-sticky-section-no" data-id="f81c64d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3340d88 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3340d88" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="332" src="https://unityinvitation.com/wp-content/uploads/2021/09/Swastiastu-k1-1024x425.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/09/Swastiastu-k1-1024x425.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/09/Swastiastu-k1-300x124.png 300w, https://unityinvitation.com/wp-content/uploads/2021/09/Swastiastu-k1-768x319.png 768w, https://unityinvitation.com/wp-content/uploads/2021/09/Swastiastu-k1.png 1328w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
				<div class="elementor-element elementor-element-5e18f3e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5e18f3e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Om Swastiastu</p>		</div>
				</div>
				<div class="elementor-element elementor-element-5c78599 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5c78599" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Atas Asung Kertha Wara Nugraha Ida Sang Hyang Widhi Wasa/Tuhan Yang Maha Esa kami bermaksud mengundang Bapak/Ibu/Saudara/i pada Upacara Manusa Yadnya Pawiwahan (Pernikahan) Putra dan Putri kami.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-4d9c7c9 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="4d9c7c9" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-bee242c elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="bee242c" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-109016f animated-slow wdp-sticky-section-no" data-id="109016f" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-f519f34 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f519f34" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-68001fe animated-slow wdp-sticky-section-no elementor-invisible" data-id="68001fe" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-2ef58a6 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="2ef58a6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="500" height="750" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-CPP.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-CPP.jpg 500w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-CPP-200x300.jpg 200w" sizes="(max-width: 500px) 100vw, 500px" />															</div>
				</div>
				<div class="elementor-element elementor-element-db9a74e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="db9a74e" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Wijaya</h3>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-48f1fb2 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="48f1fb2" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="couple" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-7fbb20f wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="7fbb20f" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">I Made Jaya Wijaya</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-6dafb78 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="6dafb78" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-ee8a09f wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="ee8a09f" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Anak ketiga dari pasangan</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-4e1ea6b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="4e1ea6b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">I Wayan Wenia 
<br>& <br> 
Ni Nyoman Tunjung</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-a4a0084 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="a4a0084" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Br. Melinggih, <br>Payangan, Gianyar</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-cba6270 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="cba6270" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="#" target="_blank">
						<span class="elementor-screen-only">Instagram</span>
						<i class="fab fa-instagram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-085a158 animated-slow wdp-sticky-section-no" data-id="085a158" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1983ca2 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1983ca2" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">&</h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-88e8265 animated-slow wdp-sticky-section-no" data-id="88e8265" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-60050e4 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="60050e4" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-af6b407 animated-slow wdp-sticky-section-no elementor-invisible" data-id="af6b407" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-801a572 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="801a572" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="500" height="750" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-CPW.jpg" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-CPW.jpg 500w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-CPW-200x300.jpg 200w" sizes="(max-width: 500px) 100vw, 500px" />															</div>
				</div>
				<div class="elementor-element elementor-element-15c7d4d animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="15c7d4d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;,&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Ami</h3>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-223efa7 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="223efa7" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-default">Ni Wayan Indrami Wati</h3>		</div>
				</div>
				<div class="elementor-element elementor-element-e02b58b elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="e02b58b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-b524cae wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="b524cae" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Anak kedua dari pasangan
</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-40a1a0b wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="40a1a0b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">I Nyoman Karba 
<br> & <br> 
Ni Ketut Julinawati</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-5dd6faf wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="5dd6faf" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Br. Payangandesa, <br>Payangan, Gianyar</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-9d53254 e-grid-align-mobile-center elementor-shape-square elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="9d53254" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-c8c25db" href="#" target="_blank">
						<span class="elementor-screen-only">Instagram</span>
						<i class="fab fa-instagram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-9c5f628 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="9c5f628" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="427" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line1-1024x546.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line1-1024x546.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line1-300x160.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line1-768x410.png 768w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line1.png 1505w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-d01e6da elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d01e6da" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f19907b wdp-sticky-section-no" data-id="f19907b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-23ed300 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="23ed300" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Hitung Mundur</p>		</div>
				</div>
				<div class="elementor-element elementor-element-793e135 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="793e135" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Hari Bahagia Kami</p>		</div>
				</div>
				<div class="elementor-element elementor-element-55ad421 uael-countdown-shape-circle uael-countdown-border-none animated-slow uael-countdown-show-seconds-yes uael-countdown-responsive-no wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-uael-countdown" data-id="55ad421" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="uael-countdown.default">
				<div class="elementor-widget-container">
					<div class="uael-countdown-wrapper countdown-active" data-countdown-type="fixed" data-timer-labels="custom" data-animation="" data-timer-days="Hari" data-timer-hours="Jam" data-timer-minutes="Menit" data-timer-seconds="Detik" data-due-date="1672492800" data-expire-action="hide">
			<div class="uael-countdown-items-wrapper" >
											<div class="uael-countdown-days uael-item">
							<span id="days-wrapper-55ad421"class="uael-countdown-item" >
															</span>
															<span id="days-label-wrapper-55ad421" class='uael-countdown-days-label-55ad421 uael-item-label'>
								</span>
													</div>
																	<div class="uael-countdown-hours uael-item">
							<span id="hours-wrapper-55ad421"class="uael-countdown-item" >
															</span>
															<span id="hours-label-wrapper-55ad421" class='uael-countdown-hours-label-55ad421 uael-item-label'>
								</span>
													</div>
																	<div class="uael-countdown-minutes uael-item">
							<span id="minutes-wrapper-55ad421"class="uael-countdown-item" >
															</span>
															<span id="minutes-label-wrapper-55ad421" class='uael-countdown-minutes-label-55ad421 uael-item-label'>
								</span>
													</div>
																	<div class="uael-countdown-seconds uael-item">
							<span id="seconds-wrapper-55ad421"class="uael-countdown-item" >
															</span>
															<span id="seconds-label-wrapper-55ad421" class='uael-countdown-seconds-label-55ad421 uael-item-label'>
								</span>
													</div>
																<div class="uael-expire-message-wrapper">
						<div class='uael-expire-show-message'></div>
					</div>
			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-e38aab9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e38aab9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Ihaiva stam ma vi yaustam, visvam ayur vyasnutam, kridantau putrair naptrbhih, modamanau sve grhe.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-7c5aad9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7c5aad9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">(Rg Veda X.85.42)

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-fdab2e2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="fdab2e2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">"Wahai pasangan suami-isteri, semoga kalian tetap bersatu dan tidak pernah terpisahkan. Semoga kalian mencapai hidup penuh kebahagiaan, tinggal di rumah yang penuh kegembiraan bersama seluruh keturunanmu."</p>		</div>
				</div>
				<div class="elementor-element elementor-element-9f9b8f5 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="9f9b8f5" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-f44a4dc elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f44a4dc" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-ec24a32 wdp-sticky-section-no" data-id="ec24a32" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-eb26535 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="eb26535" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="event" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-7945a07 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7945a07" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Waktu & Tempat</p>		</div>
				</div>
				<div class="elementor-element elementor-element-a5b54e2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="a5b54e2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pernikahan</p>		</div>
				</div>
				<div class="elementor-element elementor-element-0eb11da wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="0eb11da" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pertemuan adalah permulaan, tetap bersama adalah perkembangan, bekerja sama adalah keberhasilan.</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-9d3a29a elementor-section-content-middle animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="9d3a29a" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2fd6ae4 animated-slow wdp-sticky-section-no" data-id="2fd6ae4" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-fe78ab9 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="fe78ab9" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="fontelloicon fontello-iconwedding-photography"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-a6df6a2 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="a6df6a2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pawiwahan</p>		</div>
				</div>
				<div class="elementor-element elementor-element-321d245 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="321d245" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-66842fe animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="66842fe" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Desember</p>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-0c60888 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="0c60888" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-2a920d9 wdp-sticky-section-no" data-id="2a920d9" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-cf5affd wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="cf5affd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Minggu</p>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-58f1437 wdp-sticky-section-no" data-id="58f1437" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-cc7543e wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="cc7543e" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="2000" data-to-value="31" data-from-value="0">0</span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-e25d2a9 wdp-sticky-section-no" data-id="e25d2a9" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-5418aef wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5418aef" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">2022

</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-12e09d4 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="12e09d4" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">08.00 WIB</p>		</div>
				</div>
				<div class="elementor-element elementor-element-18c8a0b animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="18c8a0b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">s/d Selesai</p>		</div>
				</div>
				<div class="elementor-element elementor-element-0fec958 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="0fec958" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-ffce073 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ffce073" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Bertempat di</p>		</div>
				</div>
				<div class="elementor-element elementor-element-e9053f9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e9053f9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Auditorium <br> Graha Widyatama

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-2b7f342 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2b7f342" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</p>		</div>
				</div>
				<div class="elementor-element elementor-element-2fc1bca elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="2fc1bca" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://goo.gl/maps/LhSNdxaXNEUPChSw7" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-location-arrow"></i>			</span>
						<span class="elementor-button-text">Lihat Lokasi</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-a3eb675 animated-slow wdp-sticky-section-no" data-id="a3eb675" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:200}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-2e2031e elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="2e2031e" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="fontelloicon fontello-iconwedding-dinner"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-d9caccc animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d9caccc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Resepsi</p>		</div>
				</div>
				<div class="elementor-element elementor-element-0ec5f6f elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="0ec5f6f" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-783bc49 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="783bc49" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Desember</p>		</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-d57fe8d elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="d57fe8d" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8cf15c7 wdp-sticky-section-no" data-id="8cf15c7" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInRight&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-19a2368 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="19a2368" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Minggu</p>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-29fa9b7 wdp-sticky-section-no" data-id="29fa9b7" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4236c02 wdp-sticky-section-no elementor-widget elementor-widget-counter" data-id="4236c02" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="2000" data-to-value="31" data-from-value="0">0</span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-f269af9 wdp-sticky-section-no" data-id="f269af9" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;fadeInLeft&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-9f202a5 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="9f202a5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">2022

</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-2f764e8 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2f764e8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">14.00 WIB</p>		</div>
				</div>
				<div class="elementor-element elementor-element-74b56b8 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="74b56b8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">s/d Selesai.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-f7c71d4 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="f7c71d4" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-4289569 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4289569" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Bertempat di</p>		</div>
				</div>
				<div class="elementor-element elementor-element-6d3a6e2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6d3a6e2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Auditorium <br> Graha Widyatama

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-e0ad56f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e0ad56f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</p>		</div>
				</div>
				<div class="elementor-element elementor-element-4d4aaa2 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="4d4aaa2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://goo.gl/maps/LhSNdxaXNEUPChSw7" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fas fa-location-arrow"></i>			</span>
						<span class="elementor-button-text">Lihat Lokasi</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-8c9dc05 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="8c9dc05" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cf35a33 wdp-sticky-section-no" data-id="cf35a33" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-df00afe elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="df00afe" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-af90e4b wdp-sticky-section-no" data-id="af90e4b" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0c33469 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="0c33469" data-element_type="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:10,&quot;end&quot;:50}},&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500,&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img src="https://unityinvitation.com/wp-content/uploads/elementor/thumbs/Unity-Adat-Bali-Galeri-4-pi6kftgigcjfa482058inma0dl1apc01ckboca974o.jpg" title="Unity Adat Bali Galeri (4)" alt="Unity Adat Bali Galeri (4)" />															</div>
				</div>
				<div class="elementor-element elementor-element-5ef86a8 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5ef86a8" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-4c58132 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4c58132" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Kami akan <br> Melangsungkan

</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b3984e7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="b3984e7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Live Streaming

</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-fce3606 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="fce3606" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-9c8d53a wdp-sticky-section-no" data-id="9c8d53a" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-6a5d3c7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6a5d3c7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Momen kebahagiaan prosesi Pawiwahan  & Resepsi Pernikahan akan kami tayangkan secara virtual melalui Youtube Live  pada link terlampir.</h2>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0346044 wdp-sticky-section-no" data-id="0346044" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-4e503a5 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="4e503a5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="768" height="172" src="https://unityinvitation.com/wp-content/uploads/2021/04/Logo-Youtube-768x172.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Logo-Youtube-768x172.png 768w, https://unityinvitation.com/wp-content/uploads/2021/04/Logo-Youtube-300x67.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Logo-Youtube-1024x229.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/04/Logo-Youtube.png 1348w" sizes="(max-width: 768px) 100vw, 768px" />															</div>
				</div>
				<div class="elementor-element elementor-element-db218ff elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="db218ff" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://www.youtube.com/channel/UCC3HlQTFyY0fOkiBbyWqtiA" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fab fa-youtube"></i>			</span>
						<span class="elementor-button-text">Join Strreaming</span>
		</span>
					</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-0f95915 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="0f95915" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-920f082 wdp-sticky-section-no" data-id="920f082" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8c02ad9 pp-image-slider-carousel pp-ins-normal pp-ins-hover-normal wdp-sticky-section-no elementor-widget elementor-widget-pp-image-slider" data-id="8c02ad9" data-element_type="widget" data-settings="{&quot;skin&quot;:&quot;carousel&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;infinite_loop&quot;:&quot;yes&quot;}" data-widget_type="pp-image-slider.default">
				<div class="elementor-widget-container">
							<div class="pp-image-slider-container">
			<div class="pp-image-slider-wrap swiper-container-wrap">
				<div class="pp-image-slider-box">
					<div class="pp-image-slider pp-swiper-slider swiper-container" id="pp-image-slider-8c02ad9" data-slider-settings="{&quot;direction&quot;:&quot;horizontal&quot;,&quot;speed&quot;:1500,&quot;effect&quot;:&quot;fade&quot;,&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10,&quot;autoHeight&quot;:true,&quot;loop&quot;:true,&quot;autoplay&quot;:{&quot;delay&quot;:2500},&quot;breakpoints&quot;:{&quot;1025&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;768&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:10},&quot;320&quot;:{&quot;slidesPerView&quot;:1,&quot;slidesPerGroup&quot;:1,&quot;spaceBetween&quot;:0}}}">
						<div class="swiper-wrapper">
										<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-slide1.jpg" alt="Unity Adat Bali slide1" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-slide2.jpg" alt="Unity Adat Bali slide2" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-slide3.jpg" alt="Unity Adat Bali slide3" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
						<div class="pp-image-slider-thumb-item-wrap pp-swiper-slide swiper-slide">
				<div class="pp-image-slider-thumb-item pp-ins-filter-hover">
					<div class="pp-image-slider-thumb-image pp-ins-filter-target"><img src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-slide4.jpg" alt="Unity Adat Bali slide4" /></div><div class="pp-image-slider-thumb-overlay pp-media-overlay"></div>				</div>
			</div>
									</div>
					</div>
									</div>
			</div>
					</div>
					</div>
				</div>
				<div class="elementor-element elementor-element-1471e5b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="1471e5b" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-fe324ec wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="fe324ec" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Sebuah Kisah</p>		</div>
				</div>
				<div class="elementor-element elementor-element-5ce0ee6 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5ce0ee6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Perjalanan Kami</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-47ccddc wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="47ccddc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Perjalanan yang tak terlupakan, dan perjalanan yang mengubah tujuan hidup kami</p>		</div>
				</div>
				<div class="elementor-element elementor-element-006ddc3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="006ddc3" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-46dacee wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="46dacee" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="story" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-10535ca animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="10535ca" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-91b21f0 animated-slow wdp-sticky-section-no" data-id="91b21f0" data-element_type="column" data-settings="{&quot;animation_mobile&quot;:&quot;none&quot;,&quot;animation&quot;:&quot;none&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-690dbd1 animated-slow wdp-sticky-section-no elementor-widget elementor-widget-pp-timeline" data-id="690dbd1" data-element_type="widget" data-settings="{&quot;direction&quot;:&quot;left&quot;,&quot;_animation&quot;:&quot;none&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;_animation_delay&quot;:200,&quot;layout&quot;:&quot;vertical&quot;,&quot;dates&quot;:&quot;yes&quot;,&quot;card_arrow&quot;:&quot;yes&quot;,&quot;direction_tablet&quot;:&quot;left&quot;,&quot;direction_mobile&quot;:&quot;left&quot;}" data-widget_type="pp-timeline.default">
				<div class="elementor-widget-container">
					<div class="pp-timeline-wrapper">
			
			<div class="pp-timeline pp-timeline-vertical pp-timeline-left pp-timeline-tablet-left pp-timeline-mobile-left pp-timeline-dates pp-timeline-arrows-middle" data-timeline-layout="vertical">
									<div class="pp-timeline-connector-wrap">
						<div class="pp-timeline-connector">
							<div class="pp-timeline-connector-inner">
							</div>
						</div>
					</div>
								<div class="pp-timeline-items">
							<div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-3ddf251">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												1 Mei 2018											</div>
																																					<h2 class="pp-timeline-card-title">
										Pertama Bertemu									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								I am timeline item content. Click here to edit this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						1 Mei 2018					</div>
							</div>
											</div>
						<div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-48bb742">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												1 Juni 2019											</div>
																																					<h2 class="pp-timeline-card-title">
										Menjalin Hubungan									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								I am timeline item content. Click here to edit this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						1 Juni 2019					</div>
							</div>
											</div>
						<div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-9e7595d">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												1 Juli 2020											</div>
																																					<h2 class="pp-timeline-card-title">
										Bertunangan									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								I am timeline item content. Click here to edit this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						1 Juli 2020					</div>
							</div>
											</div>
						<div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-255189e">
				<div class="pp-timeline-card-wrapper">
															<div class="pp-timeline-arrow"></div>
										<div class="pp-timeline-card">
																			<div class="pp-timeline-card-title-wrap">
																																						<div class="pp-timeline-card-date">
												1 Desember 2022											</div>
																																					<h2 class="pp-timeline-card-title">
										Menikah									</h2>
															</div>
																			<div class="pp-timeline-card-content">
								I am timeline item content. Click here to edit this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.							</div>
											</div>
										</a>
									</div>

											<div class="pp-timeline-marker-wrapper">
			
			<div class="pp-timeline-marker">
										<span class="pp-icon">
						<i aria-hidden="true" class="fas fa-heart"></i>						</span>
									</div>
		</div>
					<div class="pp-timeline-card-date-wrapper">
									<div class="pp-timeline-card-date">
						1 Desember 2022					</div>
							</div>
											</div>
							</div>
			</div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-f82d42c elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="f82d42c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6c8a528 wdp-sticky-section-no" data-id="6c8a528" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-08f46d3 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="08f46d3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Momen</p>		</div>
				</div>
				<div class="elementor-element elementor-element-48a6f05 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="48a6f05" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Bahagia Kami</p>		</div>
				</div>
				<div class="elementor-element elementor-element-d0037f2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d0037f2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">"Mencintai bukan untuk menyamai, tetapi keikhlasan menerima perbedaan."
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-c89cd4a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="c89cd4a" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-5038f01 wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="5038f01" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="gallery" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-832e9aa elementor-skin-slideshow elementor-aspect-ratio-11 elementor-arrows-yes wdp-sticky-section-no elementor-widget elementor-widget-media-carousel" data-id="832e9aa" data-element_type="widget" data-settings="{&quot;skin&quot;:&quot;slideshow&quot;,&quot;speed&quot;:1500,&quot;space_between&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:3,&quot;sizes&quot;:[]},&quot;space_between_mobile&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:3,&quot;sizes&quot;:[]},&quot;autoplay_speed&quot;:2500,&quot;effect&quot;:&quot;slide&quot;,&quot;show_arrows&quot;:&quot;yes&quot;,&quot;autoplay&quot;:&quot;yes&quot;,&quot;loop&quot;:&quot;yes&quot;,&quot;pause_on_hover&quot;:&quot;yes&quot;,&quot;pause_on_interaction&quot;:&quot;yes&quot;,&quot;space_between_tablet&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]}}" data-widget_type="media-carousel.default">
				<div class="elementor-widget-container">
					<div class="elementor-swiper">
			<div class="elementor-main-swiper swiper-container">
				<div class="swiper-wrapper">
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-1.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MjksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0xLmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-1.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-2.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0yLmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-2.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-3.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0zLmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-3.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-4.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzIsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS00LmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-4.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-5.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzMsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS01LmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-5.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-6.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzQsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS02LmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-6.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-7.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzUsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS03LmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-7.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-8.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzYsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS04LmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-8.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-9.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzcsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS05LmpwZyIsInNsaWRlc2hvdyI6IjgzMmU5YWEifQ%3D%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-9.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-10.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzgsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0xMC5qcGciLCJzbGlkZXNob3ciOiI4MzJlOWFhIn0%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-10.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-11.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1MzksInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0xMS5qcGciLCJzbGlkZXNob3ciOiI4MzJlOWFhIn0%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-11.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-12.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1NDAsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0xMi5qcGciLCJzbGlkZXNob3ciOiI4MzJlOWFhIn0%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-12.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
							<a href="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-13.jpg" data-elementor-open-lightbox="yes" data-elementor-lightbox-slideshow="832e9aa" e-action-hash="#elementor-action%3Aaction%3Dlightbox%26settings%3DeyJpZCI6NDY1NDEsInVybCI6Imh0dHBzOlwvXC91bml0eWludml0YXRpb24uY29tXC93cC1jb250ZW50XC91cGxvYWRzXC8yMDIxXC8xMFwvVW5pdHktQWRhdC1CYWxpLUdhbGVyaS0xMy5qcGciLCJzbGlkZXNob3ciOiI4MzJlOWFhIn0%3D">		<div class="elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-13.jpg)">
					</div>
		</a>						</div>
									</div>
																				<div class="elementor-swiper-button elementor-swiper-button-prev">
							<i aria-hidden="true" class="eicon-chevron-left"></i>							<span class="elementor-screen-only">Previous</span>
						</div>
						<div class="elementor-swiper-button elementor-swiper-button-next">
							<i aria-hidden="true" class="eicon-chevron-right"></i>							<span class="elementor-screen-only">Next</span>
						</div>
												</div>
		</div>
				<div class="elementor-swiper">
			<div class="elementor-thumbnails-swiper swiper-container">
				<div class="swiper-wrapper">
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-1.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-2.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-3.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-4.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-5.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-6.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-7.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-8.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-9.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-10.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-11.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-12.jpg)">
					</div>
		</a>						</div>
											<div class="swiper-slide">
									<div class="elementor-fit-aspect-ratio elementor-carousel-image" style="background-image: url(https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-Galeri-13.jpg)">
					</div>
		</a>						</div>
									</div>
																					</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-d365dbe elementor-aspect-ratio-169 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-video" data-id="d365dbe" data-element_type="widget" data-settings="{&quot;youtube_url&quot;:&quot;https:\/\/youtu.be\/8ak6_O1cxoA&quot;,&quot;yt_privacy&quot;:&quot;yes&quot;,&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;video_type&quot;:&quot;youtube&quot;,&quot;aspect_ratio&quot;:&quot;169&quot;}" data-widget_type="video.default">
				<div class="elementor-widget-container">
					<div class="elementor-wrapper elementor-fit-aspect-ratio elementor-open-inline">
			<div class="elementor-video"></div>		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-dce607d elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="dce607d" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-21613b1 wdp-sticky-section-no" data-id="21613b1" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-757ed71 wdp-sticky-section-no" data-id="757ed71" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3eed5ed wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3eed5ed" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Photo by</p>		</div>
				</div>
				<div class="elementor-element elementor-element-0080ca9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="0080ca9" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="297" src="https://unityinvitation.com/wp-content/uploads/2021/10/logo-daksina-black-1024x380.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/logo-daksina-black-1024x380.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/10/logo-daksina-black-300x111.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/logo-daksina-black-768x285.png 768w, https://unityinvitation.com/wp-content/uploads/2021/10/logo-daksina-black-1536x570.png 1536w, https://unityinvitation.com/wp-content/uploads/2021/10/logo-daksina-black-2048x760.png 2048w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
				<div class="elementor-element elementor-element-a330713 elementor-shape-circle elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-widget elementor-widget-social-icons" data-id="a330713" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/daksinabaliphoto/" target="_blank" rel="noopener">
						<span class="elementor-screen-only">Instagram</span>
						<i class="fab fa-instagram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-77682fa wdp-sticky-section-no" data-id="77682fa" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-8d0e582 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="8d0e582" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-7897460 wdp-sticky-section-no" data-id="7897460" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-e99f150 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="e99f150" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Tanpa mengurangi rasa hormat, <br>acara ini menerapkan</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-481bc9b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="481bc9b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">PROTOKOL KESEHATAN</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-c0abf0c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="c0abf0c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">sesuai dengan peraturan dan <br>rekomendasi pemerintah.</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-54da6bb elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="54da6bb" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-68e263f elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="68e263f" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-df312fc animated-slow wdp-sticky-section-no elementor-invisible" data-id="df312fc" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-028e416 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="028e416" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/medical-mask.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Gunakan Masker</h3><p class="elementor-image-box-description">Wajib menggunakan <br> masker di dalam acara</p></div></div>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-06b2bbb animated-slow wdp-sticky-section-no elementor-invisible" data-id="06b2bbb" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-17eb722 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="17eb722" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/washing-hands.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Cuci Tangan</h3><p class="elementor-image-box-description">Mencuci tangan dan <br> menggunakan Handsanitizier</p></div></div>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-2a17507 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="2a17507" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0801a46 animated-slow wdp-sticky-section-no elementor-invisible" data-id="0801a46" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:300,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-ac0ddd1 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="ac0ddd1" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/physical.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Jaga Jarak </h3><p class="elementor-image-box-description">Menjaga jarak 2M dengan <br> tamu undanga lainnya</p></div></div>		</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e0e9e68 animated-slow wdp-sticky-section-no elementor-invisible" data-id="e0e9e68" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:600,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-df2ae32 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="df2ae32" data-element_type="widget" data-widget_type="image-box.default">
				<div class="elementor-widget-container">
			<div class="elementor-image-box-wrapper"><figure class="elementor-image-box-img"><img width="512" height="512" src="https://unityinvitation.com/wp-content/uploads/2021/05/no-handshake.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" /></figure><div class="elementor-image-box-content"><h3 class="elementor-image-box-title">Tidak berjabat tangan</h3><p class="elementor-image-box-description">Menghindari kontak fisik dengan <br>tamu undangan lainnya</p></div></div>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-dd750b5 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="dd750b5" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-53450b3 wdp-sticky-section-no" data-id="53450b3" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-d721449 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="d721449" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Doa & Ucapan untuk</p>		</div>
				</div>
				<div class="elementor-element elementor-element-cabc359 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="cabc359" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Pasangan Mempelai</p>		</div>
				</div>
				<div class="elementor-element elementor-element-2feef08 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2feef08" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Merupakan suatu kehormatan dan kebahagiaan bagi kami,
apabila Bapak/ Ibu/ Saudara/ i berkenan hadir, untuk 
memberikan do'a restu kepada kedua mempelai.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-65b6e8f wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="65b6e8f" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-14f8d9c wdp-sticky-section-no elementor-widget elementor-widget-menu-anchor" data-id="14f8d9c" data-element_type="widget" data-widget_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="wishes" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-f3c85a8 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="f3c85a8" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-f44596c animated-slow wdp-sticky-section-no elementor-invisible" data-id="f44596c" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-b1c28ef wdp-sticky-section-no elementor-widget elementor-widget-cswdeding-komentar" data-id="b1c28ef" data-element_type="widget" data-widget_type="cswdeding-komentar.default">
				<div class="elementor-widget-container">
			
            <style>
                .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text],.wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea,.wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select,.wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=submit]{height: auto !important;}
                .halooo{background-color:red !important;color:#fff !important;}
                .wdp-wrapper .wdp-wrap-link{text-align: center;padding: 18px;margin-top: 15px;}
                .wdp-wrapper .wdp-wrap-form{border-top:0px;}
                .konfirmasi-kehadiran{display: flex;justify-content: center;flex-direction: row;padding: 10px;margin-top: 10px;}
                .jenis-konfirmasi > i{margin: 0px 5px;}
                .jenis-konfirmasi{padding: 5px 10px;background: red;border-radius: 0.5em;color: #fff;margin:7px;}
            </style>
            <div class='wdp-wrapper wdp-custom' style='overflow: hidden;'>
                <div class='wdp-wrap-link'>
                    <a id='wdp-link-27361' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=27361&amp;comments=2&amp;get=0&amp;order=DESC' title='2 Ucapan'></a>
                    <div class='konfirmasi-kehadiran elementor-screen-only'>
                                    <div class='jenis-konfirmasi cswd-hadir'><i class='fas fa-check'></i> 1 Hadir</div>
                                    <div class='jenis-konfirmasi cswd-tidak-hadir'><i class='fas fa-times'></i> 1 Tidak Hadir</div>
                                </div>
                </div>
                <div id='wdp-wrap-commnent-27361' class='wdp-wrap-comments' style='display:none;'>
                
                    <div id='wdp-wrap-form-27361' class='wdp-wrap-form wdp-clearfix'>
                        <div id='wdp-container-form-27361' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                            <div id='respond-27361' class='respond wdp-clearfix'><form action='https://unityinvitation.com/wp-comments-post.php' method='post' id='commentform-27361'><p class="comment-form-author wdp-field-1"><input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" /><span class="wdp-required">*</span><span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span></p><div class="wdp-wrap-textarea"><textarea id="wdp-textarea-27361" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea><span class="wdp-required">*</span><span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span></div>
        <div class="wdp-wrap-select"><select class="waci_comment wdp-select" name="konfirmasi"><option value="" disabled selected>Konfirmasi Kehadiran</option>></option><option value="Hadir">Hadir</option><option value="Tidak Hadir">Tidak Hadir</option></select><span class="wdp-required">*</span><span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span></div><div class='wdp-wrap-submit wdp-clearfix'><p class='form-submit'><span class="wdp-hide">Do not change these fields following</span><input type="text" class="wdp-hide" name="name" value="username"><input type="text" class="wdp-hide" name="nombre" value=""><input type="text" class="wdp-hide" name="form-wdp" value=""><input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal"><input name='submit' id='submit-27361' value='Kirim' type='submit' /><input type='hidden' name='commentpress' value='true' /><input type='hidden' name='comment_post_ID' value='27361' id='comment_post_ID' />
<input type='hidden' name='comment_parent' id='comment_parent' value='0' />
</p></div></form></div>
                        </div>
                    </div>
                <div id='wdp-comment-status-27361'  class='wdp-comment-status'></div><ul id='wdp-container-comment-27361' class='wdp-container-comments wdp-order-DESC  wdp-has-2-comments wdp-multiple-comments' data-order='DESC'></ul><div class='wdp-holder-27361 wdp-holder'></div>
                </div>
            </div>
        		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-5b1e437 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5b1e437" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-6fe6d83 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="6fe6d83" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="145" src="https://unityinvitation.com/wp-content/uploads/2021/09/Shanti-White1-1024x186.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/09/Shanti-White1-1024x186.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/09/Shanti-White1-300x55.png 300w, https://unityinvitation.com/wp-content/uploads/2021/09/Shanti-White1-768x140.png 768w, https://unityinvitation.com/wp-content/uploads/2021/09/Shanti-White1-1536x280.png 1536w, https://unityinvitation.com/wp-content/uploads/2021/09/Shanti-White1-2048x373.png 2048w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
				<div class="elementor-element elementor-element-5cb6283 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5cb6283" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-6246905 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6246905" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Iha-imāv-indra sam nuda. Cakravākeva dampati.</p>		</div>
				</div>
				<div class="elementor-element elementor-element-cdef805 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="cdef805" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">( Atharvaveda : XIV.2.64 )</p>		</div>
				</div>
				<div class="elementor-element elementor-element-25d53ce wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="25d53ce" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">"Ya Tuhan, karuniailah kepada pasangan ini untuk memiliki cinta kasih yang tulus dalam membina kehidupan berumah tangga."</p>		</div>
				</div>
				<div class="elementor-element elementor-element-31a06d9 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="31a06d9" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-3413b0f wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3413b0f" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="800" height="463" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line2-1024x593.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line2-1024x593.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line2-300x174.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line2-768x445.png 768w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-flowers-line2.png 1186w" sizes="(max-width: 800px) 100vw, 800px" />															</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-top-section elementor-element elementor-element-4784002 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4784002" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-98459a6 wdp-sticky-section-no" data-id="98459a6" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-5e0fc20 wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="5e0fc20" data-element_type="widget" data-widget_type="html.default">
				<div class="elementor-widget-container">
			<script>
document.addEventListener('DOMContentLoaded', function() {
jQuery(function($){
$('.clicktoshow').each(function(i){
$(this).click(function(){ $('.showclick').eq(i).toggle();
$('.clicktoshow');
$('.showclick2').eq(i).hide();
}); });
}); });
</script>
<style>
.clicktoshow{
cursor: pointer;
}
.showclick{
display: none;
}
</style>



<script>
document.addEventListener('DOMContentLoaded', function() {
jQuery(function($){
$('.clicktoshow2').each(function(i){
$(this).click(function(){ $('.showclick2').eq(i).toggle();
$('.clicktoshow2');
}); });
}); });
</script>
<style>
.clicktoshow2{
cursor: pointer;
}
.showclick2{
display: none;
}
</style>		</div>
				</div>
				<div class="elementor-element elementor-element-48e0831 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="48e0831" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Tanda Kasih</p>		</div>
				</div>
				<div class="elementor-element elementor-element-aa9ae17 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="aa9ae17" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami.
</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-ac11c09 elementor-align-center clicktoshow animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="ac11c09" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a class="elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</span>
						<span class="elementor-button-text">Klik di sini</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-6e7fc0d elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="6e7fc0d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_delay&quot;:800}" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Haii..%20Selamat%20menikah%20yaa%20Wijaya%20dan%20Ami,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih.." target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
						<span class="elementor-button-content-wrapper">
							<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="fab fa-whatsapp"></i>			</span>
						<span class="elementor-button-text">Konfirmasi</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-975f70a showclick elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="975f70a" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-9b89e2c wdp-sticky-section-no" data-id="9b89e2c" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8280df1 wdp-sticky-section-no" data-id="8280df1" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-7446d0d animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="7446d0d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="768" height="243" src="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector--768x243.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector--768x243.png 768w, https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector--300x95.png 300w, https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector--1024x324.png 1024w, https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector--1536x487.png 1536w, https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png 1572w" sizes="(max-width: 768px) 100vw, 768px" />															</div>
				</div>
				<div class="elementor-element elementor-element-e8c2ea3 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="e8c2ea3" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n I Made Jaya Wijaya</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">123456&zwj;7891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
				<div class="elementor-element elementor-element-322fb15 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="322fb15" data-element_type="widget" data-widget_type="divider.default">
				<div class="elementor-widget-container">
					<div class="elementor-divider">
			<span class="elementor-divider-separator">
						</span>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-08b89da elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="08b89da" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0faba06 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="0faba06" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ni Wayan Indrami Wati</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, &zwj;Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-85fba10 wdp-sticky-section-no" data-id="85fba10" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				<section class="pp-bg-effects pp-bg-effects-beb1c08 elementor-section elementor-top-section elementor-element elementor-element-beb1c08 pp-bg-effects-yes elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-section-id="beb1c08" data-effect-enable="yes" data-animation-type="snow" data-canvas-opacity="1" data-hide-max-width="none" data-hide-min-width="none" data-part-color="#EEB430" data-line-color="" data-line-h-color="" data-part-opacity="0.1" data-rand-opacity="" data-quantity="25" data-part-size="75" data-part-speed="" data-part-direction="none" data-hover-effect="noeffect" data-hover-size="" data-id="beb1c08" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;pp_background_effects_enable&quot;:&quot;yes&quot;,&quot;effect_hide_tablet&quot;:&quot;label_off&quot;,&quot;effect_hide_mobile&quot;:&quot;label_off&quot;}">
							<div class="elementor-background-overlay"></div>
							<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-63b0fa3 wdp-sticky-section-no" data-id="63b0fa3" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-9c01be8 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="9c01be8" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-c0c2d67 animated-fast wdp-sticky-section-no elementor-invisible" data-id="c0c2d67" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;fadeIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-0d58f0a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="0d58f0a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Terima Kasih</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-235ac1d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="235ac1d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-default">Wijaya & Ami</h1>		</div>
				</div>
				<div class="elementor-element elementor-element-def886e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="def886e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Keluarga Besar</p>		</div>
				</div>
				<div class="elementor-element elementor-element-062dd45 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="062dd45" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">I Wayan Wenia 
& Ni Nyoman Tunjung 
<br>
I Nyoman Karba 
& Ni Ketut Julinawati</p>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-60c952a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="60c952a" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-a95cb49 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="a95cb49" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-f8f96c3 wdp-sticky-section-no" data-id="f8f96c3" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-8d3457b wdp-sticky-section-no" data-id="8d3457b" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-c824a17 wdp-sticky-section-no" data-id="c824a17" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-9fae19b animated-slow wdp-sticky-section-no elementor-invisible" data-id="9fae19b" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-255bb6e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="255bb6e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
				<div class="elementor-widget-container">
																<a href="https://unityinvitation.com/" target="_blank">
							<img width="768" height="589" src="https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-768x589.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-768x589.png 768w, https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite-300x230.png 300w, https://unityinvitation.com/wp-content/uploads/2021/05/LogosWhite.png 779w" sizes="(max-width: 768px) 100vw, 768px" />								</a>
															</div>
				</div>
				<div class="elementor-element elementor-element-a932642 elementor-shape-circle animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="a932642" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="social-icons.default">
				<div class="elementor-widget-container">
					<div class="elementor-social-icons-wrapper elementor-grid">
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-305f5e1" href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Halo%20Kak,%20saya%20mau%20tanya%20Soal%20Undangan%20Digital%20Unity%20nih.%20Bisa%20dibantu?" target="_blank" rel="noopener">
						<span class="elementor-screen-only">Whatsapp</span>
						<i class="fab fa-whatsapp"></i>					</a>
				</span>
							<span class="elementor-grid-item">
					<a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/unity.invitation/" target="_blank" rel="noopener">
						<span class="elementor-screen-only">Instagram</span>
						<i class="fab fa-instagram"></i>					</a>
				</span>
					</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-d39e5cb animated-slow wdp-sticky-section-no elementor-invisible" data-id="d39e5cb" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-2f776ea animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="2f776ea" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
				<div class="elementor-widget-container">
																<a href="https://unityinvitation.com/" target="_blank">
							<img width="1001" height="1001" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white.png 1001w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-150x150.png 150w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Invitation-QR-Code-white-768x768.png 768w" sizes="(max-width: 1001px) 100vw, 1001px" />								</a>
															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-deadd84 wdp-sticky-section-no" data-id="deadd84" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-6e55873 wdp-sticky-section-no" data-id="6e55873" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
				<div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-5f80add wdp-sticky-section-no" data-id="5f80add" data-element_type="column">
			<div class="elementor-widget-wrap">
									</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-5eceb78 elementor-widget__width-initial elementor-fixed elementor-widget-mobile__width-initial elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-cswd-audio" data-id="5eceb78" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="cswd-audio.default">
				<div class="elementor-widget-container">
					
		<script>
			var settingAutoplay = 'yes';
			window.settingAutoplay = settingAutoplay === 'disable' ? false : true;
		</script>

		<div id="audio-container" class="audio-box">			
			<style>
				.play_cswd_audio{
				animation-name: putar;
				animation-duration: 5000ms;
				animation-iteration-count: infinite;
				animation-timing-function: linear; 
				}
				@keyframes putar{
					from {
						transform:rotate(0deg);
					}
					to {
						transform:rotate(360deg);
					}
				}
			</style>
			<audio id="song" loop>
				<source src="https://unityinvitation.com/wp-content/uploads/2022/03/The-Lion-King-Can-You-Feel-The-Love-Tonight-Saxophone-Cover.mp3"
                type="audio/mp3">
			</audio>  

			<div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
				<div class="elementor-icon">
				
				<img src="https://unityinvitation.com/wp-content/uploads/2022/02/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="pause_cswd_audio">
				</div>
			</div> 

			<div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
				<div class="elementor-icon">
				<img src="https://unityinvitation.com/wp-content/uploads/2022/02/Icon-Audi-Music-Unity-New.png" alt="unity-play-audio" style="width:55px;" class="play_cswd_audio">
				</div>
			</div>
			
		</div>
		<!-- <script>
			jQuery("document").ready(function (n) {
				var e = window.settingAutoplay;
				e ? (n("#mute-sound").show(), document.getElementById("song").play()) : n("#unmute-sound").show(),
					n("#audio-container").click(function (u) {
						e ? (n("#mute-sound").hide(), n("#unmute-sound").show(), document.getElementById("song").pause(), (e = !1)) : (n("#unmute-sound").hide(), n("#mute-sound").show(), document.getElementById("song").play(), (e = !0));
					});
			});
		</script> -->
		
				</div>
				</div>
				<div class="elementor-element elementor-element-eaddb37 wdp-sticky-section-no elementor-widget elementor-widget-CSWeding_modal_popup" data-id="eaddb37" data-element_type="widget" data-settings="{&quot;exit_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="CSWeding_modal_popup.default">
				<div class="elementor-widget-container">
			        <style>
            .elementor-image>img {
                display: initial !important;
            }
        </style>
        <div class="modalx animated none" data-sampul='https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-cover1a.jpg'>

            <div class="overlayy"></div>
            <div class="content-modalx">
                <div class="info_modalx">
                                            <div class="elementor-image img"><img src="https://unityinvitation.com/wp-content/uploads/2021/11/Unity-Adat-Bali-cover2.png" title="Unity Adat Bali cover2" alt="Unity Adat Bali cover2" /></div>
                                                                <div class="text_tambahan" >PAWIWAHAN</div>
                                                                <div class="wdp-mempelai" >Wijaya & Ami</div>
                                                                <div class="wdp-dear" >Yth. Bapak/Ibu/Saudara/i</div>
                                        <div class="wdp-name">
                        Nama Tamu                    </div>
                                                                <div class="wdp-text" >Tanpa mengurangi rasa hormat, <br> Kami mengundang  Bapak/Ibu/Saudara/i <br>untuk hadir  di acara Pernikahan Kami.
</div>
                                                                <div class="wdp-button-wrapper">
                            <button class="elementor-button">
                                                                Buka Undangan                            </button>
                        </div>
                                    </div>
            </div>
        </div>


        <script>
            const sampul = jQuery('.modalx').data('sampul');
            jQuery('.modalx').css('background-image', 'url(' + sampul + ')');
            jQuery('body').css('overflow', 'hidden');
            jQuery('.wdp-button-wrapper button').on('click', function() {
                                    jQuery('.modalx').removeClass('none');
                    jQuery('.modalx').addClass('fadeIn reverse');
                    setTimeout(function() {
                        jQuery('.modalx').addClass('removeModals');
                    }, 1600);
                                jQuery('body').css('overflow', 'auto');
                document.getElementById("song").play();
            });
        </script>
		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<footer class="elementor-section elementor-top-section elementor-element elementor-element-70bf3d8 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="70bf3d8" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;,&quot;animation_delay&quot;:2500,&quot;sticky&quot;:&quot;bottom&quot;,&quot;sticky_on&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;],&quot;sticky_offset&quot;:0,&quot;sticky_effects_offset&quot;:0}">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-cfd3f8e wdp-sticky-section-no" data-id="cfd3f8e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
					<div class="elementor-background-overlay"></div>
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-dbaa3ac elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="dbaa3ac" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-e5c95fe animated-slow wdp-sticky-section-no elementor-invisible" data-id="e5c95fe" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:2500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-660d2eb wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="660d2eb" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
																<a href="#home">
							<img width="300" height="300" src="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-menu.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-menu.png 300w, https://unityinvitation.com/wp-content/uploads/2021/10/Unity-Adat-Bali-menu-150x150.png 150w" sizes="(max-width: 300px) 100vw, 300px" />								</a>
															</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-93367c2 animated-slow wdp-sticky-section-no elementor-invisible" data-id="93367c2" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:3000}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-22bc35a elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="22bc35a" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<a class="elementor-icon elementor-animation-grow" href="#couple">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-couple"></i>			</a>
		</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-a42b562 animated-slow wdp-sticky-section-no elementor-invisible" data-id="a42b562" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:3500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3a3a697 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="3a3a697" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<a class="elementor-icon elementor-animation-grow" href="#event">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-event"></i>			</a>
		</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-c61a57d animated-slow wdp-sticky-section-no elementor-invisible" data-id="c61a57d" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:4000}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-1d25554 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="1d25554" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<a class="elementor-icon elementor-animation-grow" href="#story">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-love-story"></i>			</a>
		</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-dd87a68 animated-slow wdp-sticky-section-no elementor-invisible" data-id="dd87a68" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:4500}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-db77527 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="db77527" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<a class="elementor-icon elementor-animation-grow" href="#gallery">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gallery"></i>			</a>
		</div>
				</div>
				</div>
					</div>
		</div>
				<div class="elementor-column elementor-col-16 elementor-inner-column elementor-element elementor-element-a406094 animated-slow wdp-sticky-section-no elementor-invisible" data-id="a406094" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:5000}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-49639e6 elementor-view-stacked elementor-shape-square wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="49639e6" data-element_type="widget" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<a class="elementor-icon elementor-animation-grow" href="#wishes">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-wishes"></i>			</a>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</footer>
							</div>
				<div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
					<div class="elementor-background-overlay"></div>
								<div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami.
</p>		</div>
				</div>
				<div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
				<div class="elementor-widget-container">
					<div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
			<div class="jet-tabs__control-wrapper">
				<div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-BCA.png" alt=""></div></div><div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt=""></div></div><div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208"><div class="jet-tabs__control-inner"><img class="jet-tabs__label-image" src="https://unityinvitation.com/wp-content/uploads/2021/08/Logo-Mandiri.png" alt=""></div></div>			</div>
			<div class="jet-tabs__content-wrapper">
				<div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div><div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">		<div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
				<div class="elementor-widget-container">
															<img width="486" height="486" src="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity.png 486w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, https://unityinvitation.com/wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />															</div>
				</div>
				<div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Ardito Prambanan</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">1234567891011</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		</div>			</div>
		</div>
				</div>
				</div>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>		</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				<section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
					<div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
				<div class="elementor-widget-container">
					<div class="elementor-icon-wrapper">
			<div class="elementor-icon">
			<i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>			</div>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
				<div class="elementor-widget-container">
					
		<div class="elementor-image img"></div>

		<div class="head-title">a/n Bimo Bramantyo</div>
		<div class="elementor-button-wrapper">
						<div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
				
			<a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
				
		<div class="elementor-button-content-wrapper">
						<span class="elementor-button-icon elementor-align-icon-left">
				<i aria-hidden="true" class="far fa-copy"></i>			</span>
						<span class="elementor-button-text">Salin</span>
		</div>
					</a>
			
		</div>

		<style type="text/css">
			.spancontent {
				padding-bottom: 20px;
			}
			.copy-content {
				color: #6EC1E4;
				text-align: center;
			}
			.head-title {
				color: #6EC1E4;
				text-align: center;
			}
		</style>

		<script>
		function copyText(el) {
		var content = jQuery(el).siblings('div.copy-content').html()
		var temp = jQuery("<textarea>");
		jQuery("body").append(temp);
		temp.val(content.replace(/<br ?\/?>/g, "\n")).select();
		document.execCommand("copy");
		temp.remove();

		var text = jQuery(el).html()
		jQuery(el).html(jQuery(el).data('message'))
		var counter = 0;
		var interval = setInterval(function() {
		counter++;
		
		if (counter == 2) {			
			jQuery(el).html(text)
			Interval(interval);
		}
			}, 5000);
		}
		</script>

				</div>
				</div>
					</div>
		</div>
							</div>
		</section>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
				<div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
		<div class="elementor-section-wrap">
					<section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-no">
					<div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
			<div class="elementor-widget-wrap elementor-element-populated">
								<div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
				<div class="elementor-widget-container">
			
				<div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
								<nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}"><ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a></li>
</ul>
</li>
</ul></nav>
															<div class="pp-menu-toggle pp-menu-toggle-on-tablet">
											<div class="pp-hamburger">
							<div class="pp-hamburger-box">
																	<div class="pp-hamburger-inner"></div>
															</div>
						</div>
														</div>
												<nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
												<ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical"><li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572"><a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
<ul class="sub-menu pp-advanced-menu--dropdown">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265"><a href="https://unityinvitation.com/katalog-web/" class="pp-sub-item">Undangan Website</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306"><a href="https://unityinvitation.com/katalog-statik/" class="pp-sub-item">Undangan Statik</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418"><a href="https://unityinvitation.com/katalog-video/" class="pp-sub-item">Undangan Video</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726"><a href="https://unityinvitation.com/filter-instagram/" class="pp-sub-item">Filter Instagram</a></li>
</ul>
</li>
</ul>							</nav>
							</div>
						</div>
				</div>
					</div>
		</div>
							</div>
		</section>
				</div>
		</div>
		<link rel='stylesheet' id='fancybox-css'  href='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.css?ver=2.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-51208-css'  href='https://unityinvitation.com/wp-content/uploads/elementor/css/post-51208.css?ver=1652056008' type='text/css' media='all' />
<link rel='stylesheet' id='e-animations-css'  href='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
<script type='text/javascript' id='WEDKU_SCRP-js-extra'>
/* <![CDATA[ */
var ceper = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.16' id='WEDKU_SCRP-js'></script>
<script type='text/javascript' id='wdp_js_script-js-extra'>
/* <![CDATA[ */
var WDP_WP = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","wdpNonce":"4bda080e1a","jpages":"true","jPagesNum":"5","textCounter":"true","textCounterNum":"500","widthWrap":"","autoLoad":"true","thanksComment":"Terima kasih atas ucapan & doanya!","thanksReplyComment":"Terima kasih atas balasannya!","duplicateComment":"You might have left one of the fields blank, or duplicate comments","insertImage":"Insert image","insertVideo":"Insert video","insertLink":"Insert link","checkVideo":"Check video","accept":"Accept","cancel":"Cancel","reply":"Balas","textWriteComment":"Tulis Ucapan & Doa","classPopularComment":"wdp-popular-comment","textUrlImage":"Url image","textUrlVideo":"Url video youtube or vimeo","textUrlLink":"Url link","textToDisplay":"Text to display","textCharacteresMin":"Minimal 2 karakter","textNavNext":"Selanjutnya","textNavPrev":"Sebelumnya","textMsgDeleteComment":"Do you want delete this comment?","textLoadMore":"Load more","textLikes":"Likes"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/pp-bg-effects.min.js?ver=1.0.0' id='pp-bg-effects-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/particles/particles.min.js?ver=2.0.0' id='particles-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/lib/js-cookie/js_cookie.min.js?ver=1.36.5' id='uael-cookie-lib-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/ultimate-elementor/assets/min-js/uael-countdown.min.js?ver=1.36.5' id='uael-countdown-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1' id='jquery-numerator-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/fancybox/jquery.fancybox.min.js?ver=2.8.2' id='jquery-fancybox-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/jquery-resize/jquery.resize.min.js?ver=0.5.3' id='jquery-resize-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
<script type='text/javascript' id='powerpack-frontend-js-extra'>
/* <![CDATA[ */
var ppLogin = {"empty_username":"Enter a username or email address.","empty_password":"Enter password.","empty_password_1":"Enter a password.","empty_password_2":"Re-enter password.","empty_recaptcha":"Please check the captcha to verify you are not a robot.","email_sent":"A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.","reset_success":"Your password has been reset successfully.","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
var ppRegistration = {"invalid_username":"This username is invalid because it uses illegal characters. Please enter a valid username.","username_exists":"This username is already registered. Please choose another one.","empty_email":"Please type your email address.","invalid_email":"The email address isn\u2019t correct!","email_exists":"The email is already registered, please choose another one.","password":"Password must not contain the character \"\\\\\"","password_length":"Your password should be at least 8 characters long.","password_mismatch":"Password does not match.","invalid_url":"URL seems to be invalid.","recaptcha_php_ver":"reCAPTCHA API requires PHP version 5.3 or above.","recaptcha_missing_key":"Your reCAPTCHA Site or Secret Key is missing!","show_password":"Show password","hide_password":"Hide password","ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/slick/slick.min.js?ver=2.8.2' id='pp-slick-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-timeline.min.js?ver=2.8.2' id='pp-timeline-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/imagesloaded.min.js?ver=4.1.4' id='imagesloaded-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
<script type='text/javascript' id='bdt-uikit-js-extra'>
/* <![CDATA[ */
var element_pack_ajax_login_config = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","language":"en","loadingmessage":"Sending user info, please wait...","unknownerror":"Unknown error, make sure access is correct!"};
var ElementPackConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"268d485f92","data_table":{"language":{"lengthMenu":"Show _MENU_ Entries","info":"Showing _START_ to _END_ of _TOTAL_ entries","search":"Search :","paginate":{"previous":"Previous","next":"Next"}}},"contact_form":{"sending_msg":"Sending message please wait...","captcha_nd":"Invisible captcha not defined!","captcha_nr":"Could not get invisible captcha response!"},"mailchimp":{"subscribing":"Subscribing you please wait..."},"elements_data":{"sections":[],"columns":[],"widgets":[]}};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
<script type='text/javascript' id='elementor-frontend-js-before'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.6.4","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"a11y_improvements":true,"e_import_export":true,"e_hidden_wordpress_widgets":true,"theme_builder_v2":true,"landing-pages":true,"elements-color-picker":true,"favorite-widgets":true,"admin-top-bar":true,"form-submissions":true},"urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"editorPreferences":[]},"kit":{"viewport_tablet":1024,"active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes"},"post":{"id":27361,"title":"Unity%20Adat%20Bali","excerpt":"","featuredImage":"https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/11\/Adat-Bali-cover-invitation.jpg"}};
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
<script type='text/javascript' id='elementor-pro-frontend-js-before'>
var ElementorProFrontendConfig = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","nonce":"f02c3dc1b5","urls":{"assets":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/","rest":"https:\/\/unityinvitation.com\/wp-json\/"},"i18n":{"toc_no_headings_found":"No headings were found on this page."},"shareButtonsNetworks":{"facebook":{"title":"Facebook","has_counter":true},"twitter":{"title":"Twitter"},"linkedin":{"title":"LinkedIn","has_counter":true},"pinterest":{"title":"Pinterest","has_counter":true},"reddit":{"title":"Reddit","has_counter":true},"vk":{"title":"VK","has_counter":true},"odnoklassniki":{"title":"OK","has_counter":true},"tumblr":{"title":"Tumblr"},"digg":{"title":"Digg"},"skype":{"title":"Skype"},"stumbleupon":{"title":"StumbleUpon","has_counter":true},"mix":{"title":"Mix"},"telegram":{"title":"Telegram"},"pocket":{"title":"Pocket","has_counter":true},"xing":{"title":"XING","has_counter":true},"whatsapp":{"title":"WhatsApp"},"email":{"title":"Email"},"print":{"title":"Print"}},"facebook_sdk":{"lang":"en_US","app_id":""},"lottie":{"defaultAnimationUrl":"https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"}};
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
<script type='text/javascript' id='jet-elements-js-extra'>
/* <![CDATA[ */
var jetElements = {"ajaxUrl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template","devMode":"false","messages":{"invalidMail":"Please specify a valid e-mail"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
<script type='text/javascript' id='jet-tabs-frontend-js-extra'>
/* <![CDATA[ */
var JetTabsSettings = {"ajaxurl":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","isMobile":"false","templateApiUrl":"https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template","devMode":"false"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
<script type='text/javascript' id='weddingpress-wdp-js-extra'>
/* <![CDATA[ */
var cevar = {"ajax_url":"https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php","plugin_url":"https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"};
/* ]]> */
</script>
<script type='text/javascript' src='https://unityinvitation.com/wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
<div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="https://unityinvitation.com/wp-admin/admin-ajax.php"></div><div data-pafe-form-builder-tinymce-upload="https://unityinvitation.com/wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>	</body>
</html>

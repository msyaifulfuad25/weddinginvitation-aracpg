<?php
	// Pengantin Wanita
    $bride = 'Farida';
	$bride_firstname = 'Bikry';
	$bride_middlename = 'Faridatur';
	$bride_lastname = 'Rofiqoh';
	$bride_father = 'Bapak Harno';
	$bride_mother = 'Ibu Siti Khoiriyah';
	$bride_child_position = 'Putri Pertama';
	$bride_ig = 'https://instagram.com';

	// Pengantin Pria
	$groom = 'Maftuh';
	$groom_firstname = 'Ahmad';
	$groom_middlename = 'Maftuh';
	$groom_lastname = '';
	$groom_father = 'Bapak Kustipar';
	$groom_mother = 'Ibu Niswatur Rohmah';
	$groom_child_position = 'Putra Kedua';
	$groom_ig = 'https://instagram.com';

	// Akad
	$akad_fullday = 'November 02, 2023';
	$akad_day = "Kamis";
	$akad_date = '02';
	$akad_date_show_duration = '1000';
	$akad_month = 'November';
	$akad_year = '2023';
	$akad_hour = '08:00 WIB';
	$akad_venue = 'Kediaman Mempelai Wanita';
	$akad_venue_address = 'Desa Karangsoko, Kecamatan Trenggalek, Kabupaten Trenggalek';
	$akad_venue_map = 'https://maps.app.goo.gl/cDhZS86D3Paic5py8?g_st=ic';
	
	// Resepsi
	$wedding_fullday = '02 . 11 . 2023';
	$wedding_day = "Kamis";
	$wedding_date = '02';
	$wedding_date_show_duration = '1000';
	$wedding_month = 'November';
	$wedding_year = '2023';
	$wedding_hour = '10:00 - Selesai WIB';
	$wedding_venue = 'Kediaman Mempelai Wanita';
	$wedding_venue_address = 'Desa Karangsoko, Kecamatan Trenggalek, Kabupaten Trenggalek';
	$wedding_venue_map = 'https://maps.app.goo.gl/cDhZS86D3Paic5py8?g_st=ic';
	
	// Unduh Mantu
	$unduhmantu_fullday = '04 . 11 . 2023';
	$unduhmantu_day = "Sabtu";
	$unduhmantu_date = '04';
	$unduhmantu_date_show_duration = '1000';
	$unduhmantu_month = 'November';
	$unduhmantu_year = '2023';
	$unduhmantu_hour = '13:00 - Selesai WIB';
	$unduhmantu_venue = 'Kediaman Mempelai Pria';
	$unduhmantu_venue_address = 'Desa Karangsari, Kecamatan Rejotangan, Kabupaten Tulungagung';
	$unduhmantu_venue_map = 'https://maps.app.goo.gl/J7JTvg9vioBo3cLy8?g_st=ic';
	
	// Countdown
	// $countdown_to_akad = '2022-07-03 08:00';
	$countdown_to_akad = '02/11/2023 08:00:00';
	
	// Gift
	$transfer_gift_user = 'AHMAD MAFTUH';
	$transfer_gift_rekening = '796801011838532'; //Mandiri
	// $transfer_gift_user_2 = 'Games Yudha Siantoro';
	// $transfer_gift_rekening_2 = '0901711681';
	$send_gift_user = 'Ahmad Maftuh';
	$send_gift_address = 'RT 01, RW 05, Dusun Soko, Desa Karangsari, Kecamatan Rejotangan, Kabupaten Tulungagung, Kode pos 66293';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '+6285731581531';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by = 'https://instagram.com/trinetra.photography';
	$ig_photo_by_2 = 'https://instagram.com/finest.corporation';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/2r_ucRezz-g?si=UfqM4BIribhtkRgg';

	// Love Story
	$first_meet_date = '-';
	$first_meet_story = '-';
	$having_a_relationship_date = '-';
	$having_a_relationship_story = '-';
	$engagement_date = '-';
	$engagement_story = '-';
	$married_date = '-';
	$married_story = '-';

	// Backsound
	$backsound = 'backsound/Naif-Karena-Kamu-Cuma-Satu.mp3';

	// Title
	$title = "$bride & $groom";

	// Colors
	$color_1 = '#736864';
	$color_2 = '#a99f9e';
	$color_3 = '#2e3321';
	$color_4 = '#6b7355';
	$color_5 = '#4e361438';


    $bg = "assets/photos/Repost-1.jpeg";
    $img = "assets/photos/Repost-2.jpeg";
?>
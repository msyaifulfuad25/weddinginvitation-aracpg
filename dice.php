<?php
    $dice1 = random_int(1,6);
    $dice2 = random_int(1,6);

    $resultDice = $dice1+$dice2;
    $type = '';

    if ($resultDice < 7) {
        $type = 'Under';
    } else if ($resultDice > 7) {
        $type = 'Over';
    } else {
        $type = 'Is Seven';
    }

    echo 'Dice 1: <b>'.$dice1.'</b><br>';
    echo 'Dice 2: <b>'.$dice2.'</b><br>';
    echo '<br>';
    echo 'Result: <h1 style="margin-top: 0"><b>'.$resultDice.' ['.$type.']</b></h1> ';
    echo '<button onClick="location.reload()">Refresh</button>'
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        ul.countdown {
        list-style: none;
        margin: 75px 0;
        padding: 0;
        display: block;
        text-align: center;
        }
        ul.countdown li {
        display: inline-block;
        }
        ul.countdown li span {
        font-size: 80px;
        font-weight: 300;
        line-height: 80px;
        }
        ul.countdown li.seperator {
        font-size: 80px;
        line-height: 70px;
        vertical-align: top;
        }
        ul.countdown li p {
        color: #a7abb1;
        font-size: 14px;
        }
    </style>
</head>
<body>
    <ul class="countdown">
        <li> 
            <span class="days">00</span>
            <p class="days_ref">Hari</p>
        </li>
        <li class="seperator">.</li>
        <li> 
            <span class="hours">00</span>
            <p class="hours_ref">Jam</p>
        </li>
        <li class="seperator">:</li>
        <li> 
            <span class="minutes">00</span>
            <p class="minutes_ref">Menit</p>
        </li>
        <li class="seperator">:</li>
        <li> 
            <span class="seconds">00</span>
            <p class="seconds_ref">Detik</p>
        </li>
    </ul>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
    <script type="text/javascript" src="jquery.downCount.js"></script> 
    <script class="source" type="text/javascript">
        $('.countdown').downCount({
            date: '06/15/2022 10:00:00'
        });
    </script>
</body>
</html>
<?php
	// Pengantin Wanita
    $bride = 'Laili';
	$bride_firstname = 'Laili';
	$bride_middlename = 'Nurul';
	$bride_lastname = 'Azizah';
	$bride_father = 'Bapak Suyatno, S.Pd';
	$bride_mother = 'Ibu Mariyah (Almh)';
	$bride_child_position = 'Putri Kedua';
	$bride_ig = 'https://instagram.com/laili_na';

	// Pengantin Pria
	$groom = 'Faried';
	$groom_firstname = 'Faried';
	$groom_middlename = 'Riyanto';
	$groom_lastname = '';
	$groom_father = 'Bapak Yatim Mujianto';
	$groom_mother = 'Ibu Martini (Titin)';
	$groom_child_position = 'Putra Kedua';
	$groom_ig = 'https://instagram.com/fariedriyanto';

	// Akad
	$akad_fullday = 'Desember 27, 2023';
	$akad_day = "Rabu";
	$akad_date = '27';
	$akad_date_show_duration = '1000';
	$akad_month = 'Desember';
	$akad_year = '2023';
	$akad_hour = '09:00 WIB';
	$akad_venue = 'Kediaman Mempelai Wanita / Rumah Bapak Suyatno';
	$akad_venue_address = 'Ds. Sumberdadi RT.03 RW.01 Sumbergempol Tulungagung';
	$akad_venue_map = 'https://maps.app.goo.gl/D2X3hynuysbciKYN9';
	
	// Resepsi
	$wedding_fullday = '27 . 12 . 2023';
	$wedding_day = "Rabu";
	$wedding_date = '27';
	$wedding_date_show_duration = '1000';
	$wedding_month = 'Desember';
	$wedding_year = '2023';
	$wedding_hour = '11:00 - Selesai WIB';
	$wedding_venue = 'Kediaman Mempelai Wanita / Rumah Bapak Suyatno';
	$wedding_venue_address = 'Ds. Sumberdadi RT.03 RW.01 Sumbergempol Tulungagung';
	$wedding_venue_map = 'https://maps.app.goo.gl/D2X3hynuysbciKYN9';
	
	// Countdown
	// $countdown_to_akad = '2022-07-03 08:00';
	$countdown_to_akad = '27/12/2023 08:00:00';
	
	// Gift
	$transfer_gift_user = 'Laili Nurul Azizah';
	$transfer_gift_rekening = '659801023796530'; //Mandiri
	$transfer_gift_user_2 = 'Faried Riyanto';
	$transfer_gift_rekening_2 = '0482133001';
	$send_gift_user = 'Laili Nurul Azizah';
	$send_gift_address = 'Ds. Sumberdadi RT.03 RW.01 Sumbergempol Tulungagung';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	// $gift_confirmation_wa = '6285736530097';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by = 'https://instagram.com/trinetra.photography';
	$ig_photo_by_2 = 'https://instagram.com/memorians.picture';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/d2lKCDqSSvI';

	// Love Story
	$first_meet_date = '26 Juli 2013';
	$first_meet_story = 'Awal kenal di bangku SMA tahun 2012 lewat media sosial (facebook) karena beda sekolah';
	$having_a_relationship_date = '26 Juli 2013';
	$having_a_relationship_story = 'Hubungan kami kebanyakan LDR nya sih selama 10 tahun';
	$engagement_date = '1 September 2019';
	$engagement_story = 'Tidak ada planning sama sekali tiba-tiba setelah wisuda elok, h-2 sepakat untuk bertunangan';
	$married_date = '26 Januari 2023';
	$married_story = 'Tidak ada planning sama sekali , setelah ada acara adat tiba-tiba diputuskannya tanggal pernikahan h- 1 bulan';

	// Backsound
	$backsound = 'backsound/Jaz-Bersamamu.mp3';

	// Title
	$title = "$bride & $groom";

	// Colors
	$color_1 = '#736864';
	$color_2 = '#a99f9e';
	$color_3 = '#2e3321';
	$color_4 = '#6b7355';
	$color_5 = '#4e361438';


    $bg = "assets/photos/Repost-1.jpg";
    $img = "assets/photos/Repost-2.jpg";
?>
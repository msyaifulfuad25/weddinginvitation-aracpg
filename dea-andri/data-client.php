<?php
	// Pengantin Wanita
    $bride = 'Dea';
	$bride_firstname = 'Odilia';
	$bride_middlename = 'Dea Vani';
	$bride_lastname = 'Putri Utami';
	$bride_father = 'Bapak V. J Poerwo Pitojo';
	$bride_mother = 'Ibu Almh. M. M Nurhajati';
	$bride_child_position = 'Putri Kelima';
	$bride_ig = 'https://instagram.com/deavaniputri_';

	// Pengantin Pria
	$groom = 'Andri';
	$groom_firstname = 'Andri';
	$groom_middlename = 'Setiawan';
	$groom_lastname = '';
	$groom_father = 'Bapak Rochani';
	$groom_mother = 'Ibu Luky Candrawati';
	$groom_child_position = 'Putra Pertama';
	$groom_ig = 'https://instagram.com/andripanties';

	// Akad
	$akad_fullday = 'Januari 27, 2023';
	$akad_day = "Jum'at";
	$akad_date = '27';
	$akad_date_show_duration = '1000';
	$akad_month = 'Januari';
	$akad_year = '2023';
	$akad_hour = '08:00 WIB';
	$akad_venue = 'Hotel Narita Tulungagung';
	$akad_venue_address = 'Jl. KH Agus Salim No.87-89, Kenayan, Kec. Tulungagung, Kabupaten Tulungagung, Jawa Timur';
	$akad_venue_map = 'https://goo.gl/maps/oYu7SR76RU1ZrNK6A';
	
	// Resepsi
	$wedding_fullday = '27 . 01 . 2023';
	$wedding_day = "Jum'at";
	$wedding_date = '27';
	$wedding_date_show_duration = '1000';
	$wedding_month = 'Januari';
	$wedding_year = '2023';
	$wedding_hour = '13:00 - 16:00 WIB';
	$wedding_venue = 'Hotel Narita Tulungagung';
	$wedding_venue_address = 'Jl. KH Agus Salim No.87-89, Kenayan, Kec. Tulungagung, Kabupaten Tulungagung, Jawa Timur';
	$wedding_venue_map = 'https://goo.gl/maps/oYu7SR76RU1ZrNK6A';
	
	// Countdown
	// $countdown_to_akad = '2022-07-03 08:00';
	$countdown_to_akad = '27/1/2023 08:00:00';
	
	// Gift
	$transfer_gift_user = 'Odilia Dea Vani';
	$transfer_gift_rekening = '1710012114578'; //Mandiri
	// $transfer_gift_user_2 = 'Games Yudha Siantoro';
	// $transfer_gift_rekening_2 = '0901711681';
	$send_gift_user = 'Dea';
	$send_gift_address = 'Jalan Jayeng Kusuma No 9 Rt/Rw 002/008 Dusun Serut Desa Tapan Kecamatan Kedungwaru Kab. Tulungagung';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '6285736530097';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by = 'https://instagram.com/trinetra.photography';
	$ig_photo_by_2 = 'https://instagram.com/finest.corporation';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/d2lKCDqSSvI';

	// Love Story
	$first_meet_date = '26 Juli 2013';
	$first_meet_story = 'Awal kenal di bangku SMA tahun 2012 lewat media sosial (facebook) karena beda sekolah';
	$having_a_relationship_date = '26 Juli 2013';
	$having_a_relationship_story = 'Hubungan kami kebanyakan LDR nya sih selama 10 tahun';
	$engagement_date = '1 September 2019';
	$engagement_story = 'Tidak ada planning sama sekali tiba-tiba setelah wisuda elok, h-2 sepakat untuk bertunangan';
	$married_date = '26 Januari 2023';
	$married_story = 'Tidak ada planning sama sekali , setelah ada acara adat tiba-tiba diputuskannya tanggal pernikahan h- 1 bulan';

	// Backsound
	$backsound = 'backsound/Backsong-Requested.mp3';

	// Title
	$title = "$bride & $groom";

	// Colors
	$color_1 = '#736864';
	$color_2 = '#a99f9e';
	$color_3 = '#2e3321';
	$color_4 = '#6b7355';
	$color_5 = '#4e361438';


    $bg = "assets/photos/Repost-1.jpg";
    $img = "assets/photos/Repost-2.jpg";
?>
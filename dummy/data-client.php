<?php
	// Pengantin Wanita
    $bride = 'Mila';
	$bride_firstname = 'Fitria';
	$bride_middlename = 'Mila';
	$bride_lastname = 'Shofa, S.Pt., M.Pt';
	$bride_father = 'H. Choirul Anam';
	$bride_mother = 'Hj. Farida Hanim';
	$bride_child_position = 'Putri Pertama';
	$bride_ig = '#';

	// Pengantin Pria
	$groom = 'Akbar';
	$groom_firstname = 'H.';
	$groom_middlename = 'Mishbahul';
	$groom_lastname = 'Akbar, S.Pt., M.Pt';
	$groom_father = 'H. Su\'udi Karim (Alm)';
	$groom_mother = 'Hj. Dian Fujihana';
	$groom_child_position = 'Putra Kedua';
	$groom_ig = '#';

	// Akad
	$akad_fullday = 'November 23, 2022';
	$akad_day = 'Rabu';
	$akad_date = '23';
	$akad_date_show_duration = '1000';
	$akad_month = 'November';
	$akad_year = '2022';
	$akad_hour = '19:00 WIB';
	$akad_venue = 'Kediaman Mempelai Wanita';
	$akad_venue_address = 'Ds. Kerjen Rt 02 Rw 01 Srengat Blitar';
	$akad_venue_map = 'https://goo.gl/maps/z7ogiUtBSxaQMp6p8';
	
	// Resepsi
	$wedding_fullday = '24 . 11 . 2022';
	$wedding_day = 'Kamis';
	$wedding_date = '24';
	$wedding_date_show_duration = '1000';
	$wedding_month = 'November';
	$wedding_year = '2022';
	$wedding_hour = '09:00 WIB';
	$wedding_venue = 'Kediaman Mempelai Wanita';
	$wedding_venue_address = 'Ds. Kerjen Rt 02 Rw 01 Srengat Blitar';
	$wedding_venue_map = 'https://goo.gl/maps/z7ogiUtBSxaQMp6p8';
	
	// Countdown
	// $countdown_to_akad = '2022-07-03 08:00';
	$countdown_to_akad = '23/11/2022 08:00:00';
	
	// Gift
	$transfer_gift_user = 'Fahri Kurniawan';
	$transfer_gift_rekening = '3850684331';
	$send_gift_user = 'Fahri Kurniawan';
	$send_gift_address = 'Jl. Pisang Agung II no 7 rt4 rw5, Kel. Pisang Candi, Kec Sukun, Kota Malang';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '6287866767417';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by = 'https://instagram.com/instagram';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/MT71X613n08';

	// Love Story
	$first_meet_date = '11 Januari 2016';
	$first_meet_story = 'Pertama kali kita bertemu di bangku SMA tanpa mengenal satu sama lain. Kita hanya saling sapa.';
	$having_a_relationship_date = '08 Juni 2019';
	$having_a_relationship_story = 'Setelah pertemuan di bangku SMA dan masing-masing dari kita sudah lulus, barulah kita mulai menjalin hubungan yang berawal dari DM Instagram. Hingga kita memutuskan untuk sama-sama dan LDR.';
	$engagement_date = '17 Agustus 2021';
	$engagement_story = 'Komunikasi jarak jauh dan kesibukan kita masing-masing bukan penghalang. Pada Agustus 2020 kedua keluarga saling bertemu dan melaksanakan acara tunangan kami';
	$married_date = '03 Juli 2022';
	$married_story = 'Akhirnya momen spesial pernikahan kami dilaksanakan pada 01 Desember 2022. Ini menjadi tanggal yang kami pilih untuk saling mengikat janji menjadi sebuah keluarga yang setia & saling menyayangi.';

	// Backsound
	$backsound = 'Emilee-I-Love-You-Baby.mp3';

	// Title
	$title = "$groom & $bride";

	// Colors
	$color_1 = '#736864';
	$color_2 = '#a99f9e';
	$color_3 = '#2e3321';
	$color_4 = '#6b7355';
	$color_5 = '#4e361438';
?>
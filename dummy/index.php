<?php
	include "data-sap.php";
	include "data-client.php";

    $url_save_sap = $base_url.$uri_couple.'/save-sap.php';
    $url_get_sap = $base_url.$uri_couple.'/get-sap.php';
	// Untuk Development Product
	$asset_theme = $base_url.'mila-akbar/assets/';
	// Untuk Production
	// $asset_theme = $base_url.'satria-erika/assets/';
	$theme_name = 'Mila & Akbar';
?>

<!DOCTYPE html>
<html lang="en-US" prefix="og: https://ogp.me/ns#">
  <head>
    <meta charset="UTF-8">
    <style type="text/css">
      .wdp-comment-text img {
        max-width: 100% !important;
      }
    </style>
    <!-- Search Engine Optimization by Rank Math - https://s.rankmath.com/home -->
    <title><?= $theme_name ?></title>
    <meta name="description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="robots" content="nofollow, noindex, noimageindex, noarchive, nosnippet" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?= $title ?>" />
    <meta property="og:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta property="og:url" content="<?= $asset ?>silver1/" />
    <meta property="og:site_name" content="<?= $title ?>" />
    <meta property="article:section" content="Undangan" />
    <meta property="og:updated_time" content="2022-05-21T11:46:43+07:00" />
    <meta property="og:image" content="<?= $asset ?>wp-content/uploads/2021/06/silver1-cover-invitation.jpg" />
    <meta property="og:image:secure_url" content="<?= $asset ?>wp-content/uploads/2021/06/silver1-cover-invitation.jpg" />
    <meta property="og:image:width" content="500" />
    <meta property="og:image:height" content="428" />
    <meta property="og:image:alt" content="<?= $title ?>" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?= $title ?>" />
    <meta name="twitter:description" content="Tanpa Mengurangi Rasa Hormat, Kami Mengundang Bapak/Ibu/Saudara/i Untuk Hadir Di Acara Pernikahan Kami." />
    <meta name="twitter:image" content="<?= $asset ?>wp-content/uploads/2021/06/silver1-cover-invitation.jpg" />
    <meta name="twitter:label1" content="Written by" />
    <meta name="twitter:data1" content="admin" />
    <meta name="twitter:label2" content="Time to read" />
    <meta name="twitter:data2" content="2 minutes" />
    <script type="application/ld+json" class="rank-math-schema">
      {
        "@context": "https://schema.org",
        "@graph": [{
          "@type": "BreadcrumbList",
          "@id": "<?= $asset ?>silver1/#breadcrumb",
          "itemListElement": [{
            "@type": "ListItem",
            "position": "1",
            "item": {
              "@id": "https://unityinvitation.com",
              "name": "Home"
            }
          }, {
            "@type": "ListItem",
            "position": "2",
            "item": {
              "@id": "<?= $asset ?>silver1/",
              "name": "<?= $title ?>"
            }
          }]
        }]
      }
    </script>
    <!-- /Rank Math WordPress SEO plugin -->
    <link rel='dns-prefetch' href='//use.fontawesome.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <!-- <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Feed" href="<?= $asset ?>feed/" />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Comments Feed" href="<?= $asset ?>comments/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Unity Invitation &raquo; Unity Silver 1 Comments Feed" href="<?= $asset ?>silver1/feed/" /> -->
    <script type="text/javascript">
      window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/14.0.0\/svg\/",
        "svgExt": ".svg",
        "source": {
          // "concatemoji": "https:\/\/unityinvitation.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=6.0"
        }
      };
      /*! This file is auto-generated */
      ! function(e, a, t) {
        var n, r, o, i = a.createElement("canvas"),
          p = i.getContext && i.getContext("2d");

        function s(e, t) {
          var a = String.fromCharCode,
            e = (p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0), i.toDataURL());
          return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
        }

        function c(e) {
          var t = a.createElement("script");
          t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
        }
        for (o = Array("flag", "emoji"), t.supports = {
            everything: !0,
            everythingExceptFlag: !0
          }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
          if (!p || !p.fillText) return !1;
          switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
            case "flag":
              return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
            case "emoji":
              return !s([129777, 127995, 8205, 129778, 127999], [129777, 127995, 8203, 129778, 127999])
          }
          return !1
        }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
        t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
          t.DOMReady = !0
        }, t.supports.everything || (n = function() {
          t.readyCallback()
        }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
          "complete" === a.readyState && t.readyCallback()
        })), (e = t.source || {}).concatemoji ? c(e.concatemoji) : e.wpemoji && e.twemoji && (c(e.twemoji), c(e.wpemoji)))
      }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 0.07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
    </style>
    <link rel='stylesheet' id='bdt-uikit-css' href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/bdt-uikit.css?ver=3.13.1' type='text/css' media='all' />
    <link rel='stylesheet' id='ep-helper-css' href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/ep-helper.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-block-library-css' href='<?= $asset ?>wp-includes/css/dist/block-library/style.min.css?ver=6.0' type='text/css' media='all' />
    <style id='global-styles-inline-css' type='text/css'>
      body {
        --wp--preset--color--black: #000000;
        --wp--preset--color--cyan-bluish-gray: #abb8c3;
        --wp--preset--color--white: #ffffff;
        --wp--preset--color--pale-pink: #f78da7;
        --wp--preset--color--vivid-red: #cf2e2e;
        --wp--preset--color--luminous-vivid-orange: #ff6900;
        --wp--preset--color--luminous-vivid-amber: #fcb900;
        --wp--preset--color--light-green-cyan: #7bdcb5;
        --wp--preset--color--vivid-green-cyan: #00d084;
        --wp--preset--color--pale-cyan-blue: #8ed1fc;
        --wp--preset--color--vivid-cyan-blue: #0693e3;
        --wp--preset--color--vivid-purple: #9b51e0;
        --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
        --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
        --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
        --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
        --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
        --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
        --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
        --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
        --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
        --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
        --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
        --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
        --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
        --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
        --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
        --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
        --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
        --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
        --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
        --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
        --wp--preset--font-size--small: 13px;
        --wp--preset--font-size--medium: 20px;
        --wp--preset--font-size--large: 36px;
        --wp--preset--font-size--x-large: 42px;
      }

      .has-black-color {
        color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-color {
        color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-color {
        color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-color {
        color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-color {
        color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-color {
        color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-color {
        color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-color {
        color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-color {
        color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-color {
        color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-color {
        color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-color {
        color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-background-color {
        background-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-background-color {
        background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-background-color {
        background-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-background-color {
        background-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-background-color {
        background-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-background-color {
        background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-background-color {
        background-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-background-color {
        background-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-background-color {
        background-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-background-color {
        background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-background-color {
        background-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-black-border-color {
        border-color: var(--wp--preset--color--black) !important;
      }

      .has-cyan-bluish-gray-border-color {
        border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
      }

      .has-white-border-color {
        border-color: var(--wp--preset--color--white) !important;
      }

      .has-pale-pink-border-color {
        border-color: var(--wp--preset--color--pale-pink) !important;
      }

      .has-vivid-red-border-color {
        border-color: var(--wp--preset--color--vivid-red) !important;
      }

      .has-luminous-vivid-orange-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-amber-border-color {
        border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
      }

      .has-light-green-cyan-border-color {
        border-color: var(--wp--preset--color--light-green-cyan) !important;
      }

      .has-vivid-green-cyan-border-color {
        border-color: var(--wp--preset--color--vivid-green-cyan) !important;
      }

      .has-pale-cyan-blue-border-color {
        border-color: var(--wp--preset--color--pale-cyan-blue) !important;
      }

      .has-vivid-cyan-blue-border-color {
        border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
      }

      .has-vivid-purple-border-color {
        border-color: var(--wp--preset--color--vivid-purple) !important;
      }

      .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
        background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
      }

      .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
        background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
      }

      .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
      }

      .has-luminous-vivid-orange-to-vivid-red-gradient-background {
        background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
      }

      .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
        background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
      }

      .has-cool-to-warm-spectrum-gradient-background {
        background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
      }

      .has-blush-light-purple-gradient-background {
        background: var(--wp--preset--gradient--blush-light-purple) !important;
      }

      .has-blush-bordeaux-gradient-background {
        background: var(--wp--preset--gradient--blush-bordeaux) !important;
      }

      .has-luminous-dusk-gradient-background {
        background: var(--wp--preset--gradient--luminous-dusk) !important;
      }

      .has-pale-ocean-gradient-background {
        background: var(--wp--preset--gradient--pale-ocean) !important;
      }

      .has-electric-grass-gradient-background {
        background: var(--wp--preset--gradient--electric-grass) !important;
      }

      .has-midnight-gradient-background {
        background: var(--wp--preset--gradient--midnight) !important;
      }

      .has-small-font-size {
        font-size: var(--wp--preset--font-size--small) !important;
      }

      .has-medium-font-size {
        font-size: var(--wp--preset--font-size--medium) !important;
      }

      .has-large-font-size {
        font-size: var(--wp--preset--font-size--large) !important;
      }

      .has-x-large-font-size {
        font-size: var(--wp--preset--font-size--x-large) !important;
      }
    </style>
    <link rel='stylesheet' id='WEDKU_STYLE-css' href='<?= $asset ?>wp-content/plugins/cswd/assets/wedku.style.css?ver=1.1.18' type='text/css' media='all' />
    <link rel='stylesheet' id='pafe-extension-style-css' href='<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/assets/css/minify/extension.min.css?ver=6.5.8' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp_style-css' href='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//css/wdp_style.css?ver=2.7.6' type='text/css' media='screen' />
    <style id='wdp_style-inline-css' type='text/css'>
      .wdp-wrapper {
        font-size: 14px
      }

      .wdp-wrapper {
        background: #ffffff;
      }

      .wdp-wrapper.wdp-border {
        border: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-comments a:link,
      .wdp-wrapper .wdp-wrap-comments a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link.wdp-icon-link-true .wdpo-comment {
        color: #54595f;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-link a.wdp-link:hover .wdpo-comment {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-wrap-form {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-post-author {
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='text'] {
        border: 1px solid #d5deea;
        background: #FFFFFF;
        color: #44525F;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input.wdp-input:focus,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea:focus {
        border-color: #64B6EC;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit'],
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn {
        color: #FFFFFF;
        background: #54595f;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='submit']:hover,
      .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type='button'].wdp-form-btn:hover {
        background: #a3a3a3;
      }

      .wdp-wrapper .wdp-wrap-form .wdp-container-form .wdp-captcha .wdp-captcha-text {
        color: #44525F;
      }

      .wdp-wrapper .wdp-media-btns a>span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-media-btns a>span:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-comment-status {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-success {
        color: #319342;
      }

      .wdp-wrapper .wdp-comment-status p.wdp-ajax-error {
        color: #C85951;
      }

      .wdp-wrapper .wdp-comment-status.wdp-loading>span {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment {
        border-bottom: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul li.wdp-item-comment {
        border-top: 1px solid #d5deea;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name {
        color: #54595f !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info a.wdp-commenter-name:hover {
        color: #2a5782 !important;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-info .wdp-comment-time {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-text p {
        color: #44525F;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a {
        color: #54595f;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-actions a:hover {
        color: #2a5782;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-link>span:hover {
        color: #3D7DBC;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-positive {
        color: #2C9E48;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdp-rating-negative {
        color: #D13D3D;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content .wdp-comment-rating .wdp-rating-count.wdpo-loading {
        color: #c9cfd7;
      }

      .wdp-wrapper ul.wdp-container-comments a.wdp-load-more-comments:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-counter-info {
        color: #9DA8B7;
      }

      .wdp-wrapper .wdp-holder span {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a,
      .wdp-wrapper .wdp-holder a:link,
      .wdp-wrapper .wdp-holder a:visited {
        color: #54595f;
      }

      .wdp-wrapper .wdp-holder a:hover,
      .wdp-wrapper .wdp-holder a:link:hover,
      .wdp-wrapper .wdp-holder a:visited:hover {
        color: #2a5782;
      }

      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-previous.jp-disabled:hover,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled,
      .wdp-wrapper .wdp-holder a.jp-next.jp-disabled:hover {
        color: #9DA8B7;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-avatar img {
        max-width: 28px;
        max-height: 28px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment .wdp-comment-content {
        margin-left: 38px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul .wdp-comment-avatar img {
        max-width: 24px;
        max-height: 24px;
      }

      .wdp-wrapper ul.wdp-container-comments li.wdp-item-comment ul ul .wdp-comment-avatar img {
        max-width: 21px;
        max-height: 21px;
      }
    </style>
    <link rel='stylesheet' id='wdp-centered-css-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-centered-timeline.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-horizontal-css-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-horizontal-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='wdp-fontello-css-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp-fontello.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='exad-main-style-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/exad-styles.min.css?ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-css' href='<?= $asset ?>wp-content/themes/hello-elementor/style.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='hello-elementor-theme-style-css' href='<?= $asset ?>wp-content/themes/hello-elementor/theme.min.css?ver=2.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-css' href='<?= $asset ?>wp-content/plugins/jet-elements/assets/css/jet-elements.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-elements-skin-css' href='<?= $asset ?>wp-content/plugins/jet-elements/assets/css/jet-elements-skin.css?ver=2.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.15.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-1399-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-1399.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='powerpack-frontend-css' href='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/css/min/frontend.min.css?ver=2.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='weddingpress-wdp-css' href='<?= $asset ?>wp-content/plugins/weddingpress/assets/css/wdp.css?ver=2.8.8' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-pro-css' href='<?= $asset ?>wp-content/plugins/elementor-pro/assets/css/frontend.min.css?ver=3.5.0' type='text/css' media='all' />
    <link rel='stylesheet' id='uael-frontend-css' href='<?= $asset ?>wp-content/plugins/ultimate-elementor/assets/min-css/uael-frontend.min.css?ver=1.36.5' type='text/css' media='all' />
    <link rel='stylesheet' id='jet-tabs-frontend-css' href='<?= $asset ?>wp-content/plugins/jet-tabs/assets/css/jet-tabs-frontend.css?ver=2.1.17' type='text/css' media='all' />
    <!-- <link rel='stylesheet' id='elementor-post-13245-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-13245.css?ver=1655083460' type='text/css' media='all' /> -->
    <style>
      .elementor-13245
        .elementor-element.elementor-element-76b249e6:not(.elementor-motion-effects-element-type-background),
      .elementor-13245
        .elementor-element.elementor-element-76b249e6
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #d2bd9d;
        background-image: url("<?= $asset_theme ?>photos/Cover3.jpg");
        background-position: bottom center;
        background-repeat: no-repeat;
        background-size: cover;
      }
      .elementor-13245 .elementor-element.elementor-element-76b249e6 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-76b249e6
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-76b249e6
        > .elementor-shape-bottom
        .elementor-shape-fill {
        fill: #fbf3e5;
      }
      .elementor-13245
        .elementor-element.elementor-element-796a6c9f
        > .elementor-element-populated {
        border-style: solid;
        border-width: 0px 0px 0px 0px;
        border-color: #ffffffa8;
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        padding: 150px 0px 200px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-796a6c9f
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245 .elementor-element.elementor-element-4e38f214 {
        text-align: center;
      }
      .elementor-13245 .elementor-element.elementor-element-4e38f214 img {
        max-width: 15%;
      }
      .elementor-13245 .elementor-element.elementor-element-38406c99 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-38406c99
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 20px;
        letter-spacing: 5px;
      }
      .elementor-13245 .elementor-element.elementor-element-1e5c4abc {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1e5c4abc
        .elementor-heading-title {
        color: #ffffff;
        font-family: "Playfair Display", Sans-serif;
        font-size: 175px;
      }
      .elementor-13245
        .elementor-element.elementor-element-1e5c4abc
        > .elementor-widget-container {
        margin: -40px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-2048c2fa {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-2048c2fa
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mr De Haviland", Sans-serif;
        font-size: 130px;
        font-weight: 100;
      }
      .elementor-13245
        .elementor-element.elementor-element-2048c2fa
        > .elementor-widget-container {
        margin: -50px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-1e2d690f {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1e2d690f
        .elementor-heading-title {
        color: #552f01;
        font-family: "Cinzel", Sans-serif;
        font-size: 30px;
        font-weight: 500;
        letter-spacing: 7.5px;
      }
      .elementor-13245 .elementor-element.elementor-element-2e923d5b {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-2e923d5b
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 400;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2e923d5b
        > .elementor-widget-container {
        margin: 0px 400px 0px 400px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5b61c068.elementor-view-stacked
        .elementor-icon {
        background-color: #af8e4f69;
      }
      .elementor-13245
        .elementor-element.elementor-element-5b61c068.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-5b61c068.elementor-view-default
        .elementor-icon {
        fill: #af8e4f69;
        color: #af8e4f69;
        border-color: #af8e4f69;
      }
      .elementor-13245 .elementor-element.elementor-element-5b61c068 {
        --icon-box-icon-margin: 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-5b61c068 .elementor-icon {
        font-size: 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5b61c068
        .elementor-icon-box-wrapper {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5b61c068
        .elementor-icon-box-title {
        margin-bottom: 0px;
        color: #af8e4f69;
      }
      .elementor-13245
        .elementor-element.elementor-element-5b61c068
        .elementor-icon-box-title,
      .elementor-13245
        .elementor-element.elementor-element-5b61c068
        .elementor-icon-box-title
        a {
        font-family: "Montserrat", Sans-serif;
        font-size: 15px;
        font-weight: 600;
        line-height: 1.3em;
      }
      .elementor-13245
        .elementor-element.elementor-element-3be7446e:not(.elementor-motion-effects-element-type-background),
      .elementor-13245
        .elementor-element.elementor-element-3be7446e
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #fbf3e5;
      }
      .elementor-13245 .elementor-element.elementor-element-3be7446e {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        padding: 150px 0px 150px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3be7446e
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-41fd2722
        > .elementor-element-populated {
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-e2f8cb9 img {
        width: 10%;
      }
      .elementor-13245
        .elementor-element.elementor-element-e2f8cb9
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-77a41531 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-77a41531
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
        font-weight: bold;
      }
      .elementor-13245
        .elementor-element.elementor-element-77a41531
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-6c9e306f {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-6c9e306f
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mrs Saint Delafield", Sans-serif;
        font-size: 84px;
      }
      .elementor-13245
        .elementor-element.elementor-element-6c9e306f
        > .elementor-widget-container {
        margin: -10px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-6d5516c2 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-6d5516c2
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 20px;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-6d5516c2
        > .elementor-widget-container {
        padding: 0px 250px 0px 250px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7d4fa686
        > .elementor-container {
        max-width: 960px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7d4fa686
        > .elementor-container
        > .elementor-column
        > .elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }
      .elementor-13245 .elementor-element.elementor-element-7d4fa686 {
        margin-top: 0px;
        margin-bottom: 0px;
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7c030c0f
        > .elementor-widget-wrap
        > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7c030c0f
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-7c030c0f
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-7c030c0f
        > .elementor-background-slideshow {
        border-radius: 1500px 1500px 150px 150px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7c030c0f
        > .elementor-element-populated {
        margin: 10px 50px 10px 50px;
        --e-column-margin-right: 50px;
        --e-column-margin-left: 50px;
        padding: 100px 15px 45px 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-6f32ce6 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-6f32ce6
        .elementor-heading-title {
        color: #552f0161;
        font-family: "Mr De Haviland", Sans-serif;
        font-size: 84px;
        font-weight: 500;
      }
      .elementor-13245
        .elementor-element.elementor-element-6f32ce6
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-6fa0c7d7 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-6fa0c7d7
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
        font-weight: 600;
      }
      .elementor-13245
        .elementor-element.elementor-element-6fa0c7d7
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-2ed44bf5 {
        --divider-border-style: solid;
        --divider-color: #00000030;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2ed44bf5
        .elementor-divider-separator {
        width: 75%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-2ed44bf5
        .elementor-divider {
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-147b0d48 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-147b0d48
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.2em;
      }
      .elementor-13245 .elementor-element.elementor-element-5a03a4d7 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5a03a4d7
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 500;
        line-height: 1.2em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5a03a4d7
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-568f7a68 {
        --grid-template-columns: repeat(0, auto);
        --icon-size: 30px;
        --grid-column-gap: 14px;
        --grid-row-gap: 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-568f7a68
        .elementor-widget-container {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-568f7a68
        .elementor-social-icon {
        background-color: rgba(129, 134, 213, 0);
      }
      .elementor-13245
        .elementor-element.elementor-element-568f7a68
        .elementor-social-icon
        i {
        color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-568f7a68
        .elementor-social-icon
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-591daa95
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-591daa95
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-591daa95
        > .elementor-background-slideshow {
        border-radius: 1500px 1500px 150px 150px;
      }
      .elementor-13245
        .elementor-element.elementor-element-591daa95
        > .elementor-element-populated {
        margin: 0px 0px 0px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-1f193a86 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1f193a86
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 55px;
        font-weight: 100;
        line-height: 1.2em;
      }
      .elementor-13245
        .elementor-element.elementor-element-1f193a86
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-31e509f9
        > .elementor-widget-wrap
        > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-31e509f9
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-31e509f9
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-31e509f9
        > .elementor-background-slideshow {
        border-radius: 1500px 1500px 150px 150px;
      }
      .elementor-13245
        .elementor-element.elementor-element-31e509f9
        > .elementor-element-populated {
        margin: 10px 50px 10px 50px;
        --e-column-margin-right: 50px;
        --e-column-margin-left: 50px;
        padding: 100px 15px 45px 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-74bfa98 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-74bfa98
        .elementor-heading-title {
        color: #552f0161;
        font-family: "Mr De Haviland", Sans-serif;
        font-size: 84px;
        font-weight: 500;
      }
      .elementor-13245
        .elementor-element.elementor-element-74bfa98
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-16e0c8da {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-16e0c8da
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
        font-weight: 600;
      }
      .elementor-13245
        .elementor-element.elementor-element-16e0c8da
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-3d42edc9 {
        --divider-border-style: solid;
        --divider-color: #00000030;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3d42edc9
        .elementor-divider-separator {
        width: 75%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-3d42edc9
        .elementor-divider {
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-5594747e {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5594747e
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 14px;
        font-weight: bold;
        line-height: 1.2em;
      }
      .elementor-13245 .elementor-element.elementor-element-76f55830 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-76f55830
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 500;
        line-height: 1.2em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-76f55830
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-19c701fd {
        --grid-template-columns: repeat(0, auto);
        --icon-size: 30px;
        --grid-column-gap: 14px;
        --grid-row-gap: 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-19c701fd
        .elementor-widget-container {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-19c701fd
        .elementor-social-icon {
        background-color: rgba(129, 134, 213, 0);
      }
      .elementor-13245
        .elementor-element.elementor-element-19c701fd
        .elementor-social-icon
        i {
        color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-19c701fd
        .elementor-social-icon
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-3648a29:not(.elementor-motion-effects-element-type-background),
      .elementor-13245
        .elementor-element.elementor-element-3648a29
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-image: url("<?= $asset_theme ?>photos/Cover2.jpg");
        background-position: center center;
        background-size: cover;
      }
      .elementor-13245 .elementor-element.elementor-element-3648a29 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin-top: 0px;
        margin-bottom: 0px;
        padding: 200px 0px 200px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3648a29
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-3648a29
        > .elementor-shape-top
        .elementor-shape-fill {
        fill: #fbf3e5;
      }
      .elementor-13245
        .elementor-element.elementor-element-3648a29
        > .elementor-shape-top
        svg {
        width: calc(200% + 1.3px);
        height: 150px;
        transform: translateX(-50%) rotateY(180deg);
      }
      .elementor-13245
        .elementor-element.elementor-element-3648a29
        > .elementor-shape-bottom
        .elementor-shape-fill {
        fill: #fbf3e5;
      }
      .elementor-13245
        .elementor-element.elementor-element-3648a29
        > .elementor-shape-bottom
        svg {
        width: calc(200% + 1.3px);
        height: 150px;
        transform: translateX(-50%) rotateY(180deg);
      }
      .elementor-13245
        .elementor-element.elementor-element-110926d6
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-110926d6
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-110926d6
        > .elementor-background-slideshow {
        border-radius: 50px 50px 50px 50px;
      }
      .elementor-13245
        .elementor-element.elementor-element-110926d6
        > .elementor-element-populated {
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-1bf9898f {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1bf9898f
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
        font-weight: bold;
      }
      .elementor-13245
        .elementor-element.elementor-element-1bf9898f
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-566e6a9c {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-566e6a9c
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mrs Saint Delafield", Sans-serif;
        font-size: 84px;
      }
      .elementor-13245
        .elementor-element.elementor-element-566e6a9c
        > .elementor-widget-container {
        margin: -10px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-14c24f0f {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-14c24f0f
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 20px;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-14c24f0f
        > .elementor-widget-container {
        padding: 0px 250px 0px 250px;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1.bdt-countdown--label-block
        .bdt-countdown-number {
        margin-bottom: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1.bdt-countdown--label-inline
        .bdt-countdown-number {
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1
        .bdt-countdown-wrapper {
        max-width: 50%;
        margin-left: auto;
        margin-right: auto;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1
        .bdt-countdown-item {
        background-color: #af8e4f;
        border-radius: 50px 50px 50px 50px;
        padding: 30px 0px 30px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1
        .bdt-countdown-number {
        color: var(--e-global-color-secondary);
        font-family: "Playfair Display", Sans-serif;
        font-weight: 100;
        line-height: 1em;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1
        .bdt-countdown-label {
        color: var(--e-global-color-secondary);
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 100;
        line-height: 1em;
      }
      .elementor-13245
        .elementor-element.elementor-element-10bdd0d1
        > .elementor-widget-container {
        margin: 50px 0px 50px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7dd1f3c2
        .elementor-spacer-inner {
        --spacer-size: 100px;
      }
      .elementor-13245 .elementor-element.elementor-element-784e5321 img {
        width: 30%;
      }
      .elementor-13245 .elementor-element.elementor-element-1b9f6237 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1b9f6237
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 22px;
        font-weight: 400;
        font-style: italic;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-1b9f6237
        > .elementor-widget-container {
        margin: 0px 250px 0px 250px;
      }
      .elementor-13245 .elementor-element.elementor-element-341dc986 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-341dc986
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: bold;
      }
      .elementor-13245
        .elementor-element.elementor-element-40c15cd
        > .elementor-container
        > .elementor-column
        > .elementor-widget-wrap {
        align-content: center;
        align-items: center;
      }
      .elementor-bc-flex-widget
        .elementor-13245
        .elementor-element.elementor-element-4a92a9d8.elementor-column
        .elementor-widget-wrap {
        align-items: flex-start;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8.elementor-column.elementor-element[data-element_type="column"]
        > .elementor-widget-wrap.elementor-element-populated {
        align-content: flex-start;
        align-items: flex-start;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-widget-wrap
        > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8:not(.elementor-motion-effects-element-type-background)
        > .elementor-widget-wrap,
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-widget-wrap
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #ffffff54;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-background-slideshow {
        border-radius: 250px 250px 75px 75px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-element-populated {
        box-shadow: 0px 30px 30px -15px rgba(84.99999999999999, 46.99999999999999, 0.9999999999999962, 0.2901960784313726);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 20px 20px 70px 20px;
        --e-column-margin-right: 20px;
        --e-column-margin-left: 20px;
        padding: 130px 25px 75px 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a92a9d8
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245 .elementor-element.elementor-element-5486d470 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5486d470
        .elementor-heading-title {
        color: #552f01;
        font-family: "Cinzel Decorative", Sans-serif;
        font-size: 48px;
        font-weight: bold;
        line-height: 1em;
      }
      .elementor-13245
        .elementor-element.elementor-element-5486d470
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-5c22b998 {
        --divider-border-style: solid;
        --divider-color: #af8e4f7d;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5c22b998
        .elementor-divider-separator {
        width: 75%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-5c22b998
        .elementor-divider {
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-f0f9f94 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-f0f9f94
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
      }
      .elementor-13245 .elementor-element.elementor-element-7173118c {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-7173118c
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7173118c
        > .elementor-widget-container {
        margin: 5px 0px 5px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-3b84784d {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-3b84784d
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
      }
      .elementor-13245 .elementor-element.elementor-element-77c6ac9b {
        --divider-border-style: solid;
        --divider-color: #af8e4f7d;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-77c6ac9b
        .elementor-divider-separator {
        width: 50%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-77c6ac9b
        .elementor-divider {
        text-align: center;
        padding-top: 25px;
        padding-bottom: 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1
        .elementor-icon-wrapper {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1.elementor-view-stacked
        .elementor-icon {
        background-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1.elementor-view-default
        .elementor-icon {
        color: #af8e4f;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1.elementor-view-default
        .elementor-icon
        svg {
        fill: #af8e4f;
      }
      .elementor-13245 .elementor-element.elementor-element-cdc34b1 .elementor-icon {
        font-size: 30px;
      }
      .elementor-13245 .elementor-element.elementor-element-cdc34b1 .elementor-icon i,
      .elementor-13245
        .elementor-element.elementor-element-cdc34b1
        .elementor-icon
        svg {
        transform: rotate(0deg);
      }
      .elementor-13245 .elementor-element.elementor-element-1873c64a {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1873c64a
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 500;
        line-height: 1.3em;
      }
      .elementor-13245
        .elementor-element.elementor-element-1873c64a
        > .elementor-widget-container {
        margin: 5px 0px 10px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-4ed7e2f2 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-4ed7e2f2
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 16px;
        font-weight: 100;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4ed7e2f2
        > .elementor-widget-container {
        margin: 0px 70px 0px 70px;
      }
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button
        .elementor-align-icon-right {
        margin-left: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button
        .elementor-align-icon-left {
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 1.3em;
        background-color: #af8e4f;
        border-style: solid;
        border-width: 2px 2px 2px 2px;
        border-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        box-shadow: 0px 10px 10px -5px rgba(1, 1, 1, 0.35);
        padding: 7px 15px 7px 15px;
      }
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button:focus {
        color: #af8e4f;
        background-color: #02010100;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        .elementor-button:focus
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-1c1758d
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-bc-flex-widget
        .elementor-13245
        .elementor-element.elementor-element-5cb602f9.elementor-column
        .elementor-widget-wrap {
        align-items: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5cb602f9.elementor-column.elementor-element[data-element_type="column"]
        > .elementor-widget-wrap.elementor-element-populated {
        align-content: center;
        align-items: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5cb602f9
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-5cb602f9
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-5cb602f9
        > .elementor-background-slideshow {
        border-radius: 25px 25px 25px 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5cb602f9
        > .elementor-element-populated {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 20px 10px 20px 10px;
        --e-column-margin-right: 10px;
        --e-column-margin-left: 10px;
        padding: 25px 25px 25px 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5cb602f9
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-bc-flex-widget
        .elementor-13245
        .elementor-element.elementor-element-3189c5fc.elementor-column
        .elementor-widget-wrap {
        align-items: flex-start;
      }
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc.elementor-column.elementor-element[data-element_type="column"]
        > .elementor-widget-wrap.elementor-element-populated {
        align-content: flex-start;
        align-items: flex-start;
      }
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-widget-wrap
        > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc:not(.elementor-motion-effects-element-type-background)
        > .elementor-widget-wrap,
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-widget-wrap
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #ffffff54;
      }
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-background-slideshow {
        border-radius: 250px 250px 75px 75px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-element-populated {
        box-shadow: 0px 30px 30px -15px rgba(84.99999999999999, 46.99999999999999, 0.9999999999999962, 0.2901960784313726);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 20px 20px 70px 20px;
        --e-column-margin-right: 20px;
        --e-column-margin-left: 20px;
        padding: 130px 25px 75px 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3189c5fc
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245 .elementor-element.elementor-element-3f9360ae {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-3f9360ae
        .elementor-heading-title {
        color: #552f01;
        font-family: "Cinzel Decorative", Sans-serif;
        font-size: 48px;
        font-weight: bold;
        line-height: 1em;
      }
      .elementor-13245
        .elementor-element.elementor-element-3f9360ae
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-18d1837d {
        --divider-border-style: solid;
        --divider-color: #af8e4f7d;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-18d1837d
        .elementor-divider-separator {
        width: 75%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-18d1837d
        .elementor-divider {
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-2ad3fb03 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-2ad3fb03
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
      }
      .elementor-13245 .elementor-element.elementor-element-38f5188e {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-38f5188e
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
      }
      .elementor-13245
        .elementor-element.elementor-element-38f5188e
        > .elementor-widget-container {
        margin: 5px 0px 5px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-4d13ce5a {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-4d13ce5a
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
      }
      .elementor-13245 .elementor-element.elementor-element-3e7ab8ef {
        --divider-border-style: solid;
        --divider-color: #af8e4f7d;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3e7ab8ef
        .elementor-divider-separator {
        width: 50%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-3e7ab8ef
        .elementor-divider {
        text-align: center;
        padding-top: 25px;
        padding-bottom: 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4351d8b
        .elementor-icon-wrapper {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-4351d8b.elementor-view-stacked
        .elementor-icon {
        background-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-4351d8b.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-4351d8b.elementor-view-default
        .elementor-icon {
        color: #af8e4f;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-4351d8b.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-4351d8b.elementor-view-default
        .elementor-icon
        svg {
        fill: #af8e4f;
      }
      .elementor-13245 .elementor-element.elementor-element-4351d8b .elementor-icon {
        font-size: 30px;
      }
      .elementor-13245 .elementor-element.elementor-element-4351d8b .elementor-icon i,
      .elementor-13245
        .elementor-element.elementor-element-4351d8b
        .elementor-icon
        svg {
        transform: rotate(0deg);
      }
      .elementor-13245 .elementor-element.elementor-element-58490392 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-58490392
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 500;
        line-height: 1.3em;
      }
      .elementor-13245
        .elementor-element.elementor-element-58490392
        > .elementor-widget-container {
        margin: 5px 0px 10px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-2d7ccc1b {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-2d7ccc1b
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 16px;
        font-weight: 100;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2d7ccc1b
        > .elementor-widget-container {
        margin: 0px 70px 0px 70px;
      }
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button
        .elementor-align-icon-right {
        margin-left: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button
        .elementor-align-icon-left {
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 1.3em;
        background-color: #af8e4f;
        border-style: solid;
        border-width: 2px 2px 2px 2px;
        border-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        box-shadow: 0px 10px 10px -5px rgba(1, 1, 1, 0.35);
        padding: 7px 15px 7px 15px;
      }
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button:focus {
        color: #af8e4f;
        background-color: #02010100;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        .elementor-button:focus
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-18a3f64c
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7cc8608b
        .elementor-spacer-inner {
        --spacer-size: 100px;
      }
      .elementor-13245 .elementor-element.elementor-element-38898da {
        margin-top: 20px;
        margin-bottom: 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-60ddc842 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-60ddc842
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
        font-weight: bold;
        line-height: 1.3em;
      }
      .elementor-13245
        .elementor-element.elementor-element-60ddc842
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-27454581 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-27454581
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mrs Saint Delafield", Sans-serif;
        font-size: 84px;
      }
      .elementor-13245
        .elementor-element.elementor-element-27454581
        > .elementor-widget-container {
        margin: -10px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-37d3d335 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-37d3d335
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 500;
        font-style: italic;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-37d3d335
        > .elementor-widget-container {
        margin: 0px 400px 0px 400px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button
        .elementor-align-icon-right {
        margin-left: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button
        .elementor-align-icon-left {
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 1.3em;
        background-color: #af8e4f;
        border-style: solid;
        border-width: 2px 2px 2px 2px;
        border-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        box-shadow: 0px 10px 10px -5px rgba(19, 12, 3.000000000000001, 0.2901960784313726);
        padding: 7px 15px 7px 15px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button:focus {
        color: #af8e4f;
        background-color: #02010100;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        .elementor-button:focus
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-2807d831
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5a3f9b62:not(.elementor-motion-effects-element-type-background),
      .elementor-13245
        .elementor-element.elementor-element-5a3f9b62
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #fbf3e5;
      }
      .elementor-13245 .elementor-element.elementor-element-5a3f9b62 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        padding: 200px 0px 200px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5a3f9b62
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-250132d4
        > .elementor-element-populated {
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-7c2273f8 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-7c2273f8
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
        font-weight: bold;
      }
      .elementor-13245
        .elementor-element.elementor-element-7c2273f8
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-3194c0c {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-3194c0c
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mrs Saint Delafield", Sans-serif;
        font-size: 84px;
      }
      .elementor-13245
        .elementor-element.elementor-element-3194c0c
        > .elementor-widget-container {
        margin: -10px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2e90b7a
        .elementor-spacer-inner {
        --spacer-size: 50px;
      }
      .elementor-13245
        .elementor-element.elementor-element-726abe0
        > .elementor-element-populated {
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline
        .pp-timeline-card {
        padding: 30px 50px 30px 50px;
        background-color: #ffffff3d;
        border-radius: 50px 50px 50px 50px;
        color: #552f01;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-card {
        text-align: left;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 100;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline
        .pp-timeline-arrow {
        color: #ffffff3d;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-card-image {
        margin-bottom: 30px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-card-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 100;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline
        .pp-timeline-item-active
        .pp-timeline-card,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline
        .slick-current
        .pp-timeline-card {
        background-color: #ffffffad;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline
        .pp-timeline-item-active
        .pp-timeline-arrow,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline
        .slick-current
        .pp-timeline-arrow {
        color: #ffffffad;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-marker {
        font-size: 10px;
        width: 30px;
        height: 30px;
        color: #ffffff00;
        background-color: #af8e4f00;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-marker
        img {
        width: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-connector-wrap {
        width: 30px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-navigation:before,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-navigation
        .pp-slider-arrow {
        bottom: calc(30px / 2);
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-marker
        svg {
        fill: #ffffff00;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-item:hover
        .pp-timeline-marker,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-marker-wrapper:hover
        .pp-timeline-marker {
        background-color: #d7bb87;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-item-active
        .pp-timeline-marker,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .slick-current
        .pp-timeline-marker {
        color: var(--e-global-color-secondary);
        background-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-item-active
        .pp-timeline-marker
        svg,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .slick-current
        .pp-timeline-marker
        svg {
        fill: var(--e-global-color-secondary);
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-card-date {
        font-family: "Playfair Display", Sans-serif;
        font-size: 20px;
        line-height: 2em;
        color: #552f0154;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-vertical.pp-timeline-left
        .pp-timeline-marker-wrapper {
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-vertical.pp-timeline-right
        .pp-timeline-marker-wrapper {
        margin-left: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-vertical.pp-timeline-center
        .pp-timeline-marker-wrapper {
        margin-left: 10px;
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-horizontal {
        margin-top: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-navigation
        .pp-timeline-card-date-wrapper {
        margin-bottom: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-connector,
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-navigation:before {
        background-color: #c7b28c00;
      }
      .elementor-13245
        .elementor-element.elementor-element-337e18ca
        .pp-timeline-connector-inner {
        background-color: #cb983663;
      }
      .elementor-13245
        .elementor-element.elementor-element-36e409f8
        .elementor-spacer-inner {
        --spacer-size: 150px;
      }
      .elementor-13245 .elementor-element.elementor-element-4183bbbd {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-4183bbbd
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display SC", Sans-serif;
        font-size: 48px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4183bbbd
        > .elementor-widget-container {
        margin: -10px 0px -10px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-29c50091 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-29c50091
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.3em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-29c50091
        > .elementor-widget-container {
        margin: -10px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-cabbd42
        .elementor-spacer-inner {
        --spacer-size: 50px;
      }
      .elementor-13245 .elementor-element.elementor-element-68b58272 {
        margin-top: 0px;
        margin-bottom: 30px;
        padding: 0px 250px 0px 250px;
      }
      .elementor-13245
        .elementor-element.elementor-element-557f5c82
        .elementor-image-box-wrapper
        .elementor-image-box-img {
        width: 25%;
      }
      .elementor-13245
        .elementor-element.elementor-element-557f5c82
        .elementor-image-box-img
        img {
        transition-duration: 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-557f5c82
        .elementor-image-box-title {
        margin-bottom: 10px;
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 500;
      }
      .elementor-13245
        .elementor-element.elementor-element-557f5c82
        .elementor-image-box-description {
        color: #552f01;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-64d20e01
        .elementor-image-box-wrapper
        .elementor-image-box-img {
        width: 25%;
      }
      .elementor-13245
        .elementor-element.elementor-element-64d20e01
        .elementor-image-box-img
        img {
        transition-duration: 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-64d20e01
        .elementor-image-box-title {
        margin-bottom: 10px;
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 500;
      }
      .elementor-13245
        .elementor-element.elementor-element-64d20e01
        .elementor-image-box-description {
        color: #552f01;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245 .elementor-element.elementor-element-4faee864 {
        margin-top: 30px;
        margin-bottom: 0px;
        padding: 0px 250px 0px 250px;
      }
      .elementor-13245
        .elementor-element.elementor-element-87af7ad
        .elementor-image-box-wrapper
        .elementor-image-box-img {
        width: 25%;
      }
      .elementor-13245
        .elementor-element.elementor-element-87af7ad
        .elementor-image-box-img
        img {
        transition-duration: 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-87af7ad
        .elementor-image-box-title {
        margin-bottom: 10px;
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 500;
      }
      .elementor-13245
        .elementor-element.elementor-element-87af7ad
        .elementor-image-box-description {
        color: #552f01;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-550a4f5
        .elementor-image-box-wrapper
        .elementor-image-box-img {
        width: 25%;
      }
      .elementor-13245
        .elementor-element.elementor-element-550a4f5
        .elementor-image-box-img
        img {
        transition-duration: 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-550a4f5
        .elementor-image-box-title {
        margin-bottom: 10px;
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 500;
      }
      .elementor-13245
        .elementor-element.elementor-element-550a4f5
        .elementor-image-box-description {
        color: #552f01;
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-40d57445:not(.elementor-motion-effects-element-type-background),
      .elementor-13245
        .elementor-element.elementor-element-40d57445
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #ffe3ce;
        background-image: url("<?= $asset_theme ?>photos/Cover2.jpg");
        background-repeat: no-repeat;
        background-size: cover;
      }
      .elementor-13245 .elementor-element.elementor-element-40d57445 {
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        padding: 200px 0px 200px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-40d57445
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-40d57445
        > .elementor-shape-top
        .elementor-shape-fill {
        fill: #fbf3e5;
      }
      .elementor-13245
        .elementor-element.elementor-element-40d57445
        > .elementor-shape-top
        svg {
        height: 70px;
      }
      .elementor-13245
        .elementor-element.elementor-element-e3c5fef
        > .elementor-element-populated {
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-5a9a91ec {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-5a9a91ec
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 36px;
        font-weight: bold;
      }
      .elementor-13245
        .elementor-element.elementor-element-5a9a91ec
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-75feba40 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-75feba40
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mrs Saint Delafield", Sans-serif;
        font-size: 84px;
      }
      .elementor-13245
        .elementor-element.elementor-element-75feba40
        > .elementor-widget-container {
        margin: -10px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-370accba {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-370accba
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 20px;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-370accba
        > .elementor-widget-container {
        padding: 0px 250px 0px 250px;
      }
      .elementor-13245 .elementor-element.elementor-element-ccfca4c,
      .elementor-13245
        .elementor-element.elementor-element-ccfca4c
        > .elementor-background-overlay {
        border-radius: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-ccfca4c {
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5e6a055:not(.elementor-motion-effects-element-type-background)
        > .elementor-widget-wrap,
      .elementor-13245
        .elementor-element.elementor-element-5e6a055
        > .elementor-widget-wrap
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #ffffff75;
      }
      .elementor-13245
        .elementor-element.elementor-element-5e6a055
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-5e6a055
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-5e6a055
        > .elementor-background-slideshow {
        border-radius: 75px 75px 75px 75px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5e6a055
        > .elementor-element-populated {
        box-shadow: 0px 20px 20px -15px rgba(19, 12, 3.000000000000001, 0.17);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 30px 0px 50px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
        padding: 50px 30px 40px 30px;
      }
      .elementor-13245
        .elementor-element.elementor-element-5e6a055
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245 .elementor-element.elementor-element-f0586a1 .wdp-wrapper {
        background-color: #ffffff00;
        border-radius: 50px 50px 50px 50px;
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-komentar-cswd
        input[type="text"],
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-komentar-cswd
        select,
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-komentar-cswd
        textarea {
        font-family: "Playfair Display", Sans-serif;
        border-radius: 10px 10px 10px 10px;
        padding: 15px 15px 15px 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-f0586a1 .wdp-wrap-link a {
        font-family: "Montserrat", Sans-serif;
        font-size: 14px;
        font-weight: 700;
        line-height: 1.3em;
        color: #552f01;
      }
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-submit
        input[type="submit"],
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-submit
        button {
        font-family: "Montserrat", Sans-serif;
        font-weight: 500;
        line-height: 1.3em;
        background-color: #af8e4f;
        border-style: solid;
        border-radius: 13px 13px 13px 13px;
        padding: 7px 25px 7px 25px;
      }
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-submit
        input[type="submit"]:hover,
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        .form-submit
        button:hover {
        color: #af8e4f;
        background-color: #af8e4f00;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-f0586a1
        > .elementor-widget-container {
        margin: -50px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-21870f4a
        .elementor-spacer-inner {
        --spacer-size: 100px;
      }
      .elementor-13245 .elementor-element.elementor-element-76ed6c96 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-76ed6c96
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 22px;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-76ed6c96
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
        padding: 0px 250px 0px 250px;
      }
      .elementor-13245 .elementor-element.elementor-element-65efc767 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-65efc767
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: bold;
        font-style: normal;
        line-height: 1.3em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-65efc767
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7eaa471
        .elementor-spacer-inner {
        --spacer-size: 100px;
      }
      .elementor-13245 .elementor-element.elementor-element-741d8c5 {
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245
        .elementor-element.elementor-element-d8e3645:not(.elementor-motion-effects-element-type-background)
        > .elementor-widget-wrap,
      .elementor-13245
        .elementor-element.elementor-element-d8e3645
        > .elementor-widget-wrap
        > .elementor-motion-effects-container
        > .elementor-motion-effects-layer {
        background-color: #ffffff75;
      }
      .elementor-13245
        .elementor-element.elementor-element-d8e3645
        > .elementor-element-populated,
      .elementor-13245
        .elementor-element.elementor-element-d8e3645
        > .elementor-element-populated
        > .elementor-background-overlay,
      .elementor-13245
        .elementor-element.elementor-element-d8e3645
        > .elementor-background-slideshow {
        border-radius: 75px 75px 75px 75px;
      }
      .elementor-13245
        .elementor-element.elementor-element-d8e3645
        > .elementor-element-populated {
        box-shadow: 0px 20px 20px -15px rgba(19, 12, 3.000000000000001, 0.17);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
        margin: 30px 0px 50px 0px;
        --e-column-margin-right: 0px;
        --e-column-margin-left: 0px;
        padding: 100px 30px 75px 30px;
      }
      .elementor-13245
        .elementor-element.elementor-element-d8e3645
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-3374eef
        .elementor-icon-wrapper {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-3374eef.elementor-view-stacked
        .elementor-icon {
        background-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-3374eef.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-3374eef.elementor-view-default
        .elementor-icon {
        color: #af8e4f;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-3374eef.elementor-view-framed
        .elementor-icon,
      .elementor-13245
        .elementor-element.elementor-element-3374eef.elementor-view-default
        .elementor-icon
        svg {
        fill: #af8e4f;
      }
      .elementor-13245 .elementor-element.elementor-element-3374eef .elementor-icon i,
      .elementor-13245
        .elementor-element.elementor-element-3374eef
        .elementor-icon
        svg {
        transform: rotate(0deg);
      }
      .elementor-13245 .elementor-element.elementor-element-7d14be38 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-7d14be38
        .elementor-heading-title {
        color: #552f01;
        font-family: "Mrs Saint Delafield", Sans-serif;
        font-size: 75px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7d14be38
        > .elementor-widget-container {
        margin: 20px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-7d95a8ba {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-7d95a8ba
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 20px;
        font-weight: 100;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 0.5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-7d95a8ba
        > .elementor-widget-container {
        padding: 0px 100px 0px 100px;
      }
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button
        .elementor-align-icon-right {
        margin-left: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button
        .elementor-align-icon-left {
        margin-right: 10px;
      }
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 14px;
        font-weight: 600;
        line-height: 1.3em;
        fill: var(--e-global-color-secondary);
        color: var(--e-global-color-secondary);
        background-color: #af8e4f;
        border-style: solid;
        border-width: 2px 2px 2px 2px;
        border-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        box-shadow: 0px 10px 15px -5px rgba(19, 12, 3.000000000000001, 0.2901960784313726);
        padding: 10px 20px 10px 20px;
      }
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button:focus {
        color: #af8e4f;
        background-color: #af8e4f00;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        .elementor-button:focus
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-75e7f29d
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-963a4ee
        .elementor-spacer-inner {
        --spacer-size: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-0be8669 {
        margin-top: 20px;
        margin-bottom: 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-b205ac1
        > .elementor-widget-wrap
        > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 5px;
      }
      .elementor-13245
        .elementor-element.elementor-element-b205ac1
        > .elementor-element-populated {
        padding: 0px 150px 50px 150px;
      }
      .elementor-13245 .elementor-element.elementor-element-ea618ee {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-ea618ee
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
        font-weight: normal;
        line-height: 1.3em;
        letter-spacing: 1px;
      }
      .elementor-13245 .elementor-element.elementor-element-2dc2cb6 {
        --divider-border-style: solid;
        --divider-color: #af8e4f52;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-2dc2cb6
        .elementor-divider-separator {
        width: 75%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-2dc2cb6
        .elementor-divider {
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-c4ecd23 img {
        width: 30%;
      }
      .elementor-13245
        .elementor-element.elementor-element-c4ecd23
        > .elementor-widget-container {
        margin: 0px 0px 10px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-c9aa069 .copy-content {
        color: #513e19;
        font-family: "Montserrat", Sans-serif;
        font-size: 30px;
        font-weight: 500;
        line-height: 2em;
      }
      .elementor-13245 .elementor-element.elementor-element-c9aa069 .head-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 600;
      }
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        a.elementor-button,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 1.3em;
        fill: var(--e-global-color-secondary);
        color: var(--e-global-color-secondary);
        background-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        padding: 10px 20px 10px 20px;
      }
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        a.elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        a.elementor-button:focus,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        .elementor-button:focus {
        color: #af8e4f;
        background-color: #af8e4f00;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        a.elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        .elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        a.elementor-button:focus
        svg,
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        .elementor-button:focus
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-c9aa069
        .elementor-button {
        border-style: solid;
        border-width: 2px 2px 2px 2px;
        border-color: #af8e4f;
        box-shadow: 0px 10px 15px -5px rgba(19, 12, 3.000000000000001, 0.2901960784313726);
      }
      .elementor-13245
        .elementor-element.elementor-element-170027b
        .elementor-spacer-inner {
        --spacer-size: 50px;
      }
      .elementor-13245 .elementor-element.elementor-element-dbb37f1 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-dbb37f1
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
        font-weight: normal;
        line-height: 1.3em;
        letter-spacing: 1px;
      }
      .elementor-13245 .elementor-element.elementor-element-13ff743 {
        --divider-border-style: solid;
        --divider-color: #af8e4f52;
        --divider-border-width: 2px;
      }
      .elementor-13245
        .elementor-element.elementor-element-13ff743
        .elementor-divider-separator {
        width: 75%;
        margin: 0 auto;
        margin-center: 0;
      }
      .elementor-13245
        .elementor-element.elementor-element-13ff743
        .elementor-divider {
        text-align: center;
        padding-top: 15px;
        padding-bottom: 15px;
      }
      .elementor-13245 .elementor-element.elementor-element-79fdebf .copy-content {
        color: #513e19;
        font-family: "Playfair Display", Sans-serif;
        font-size: 16px;
        font-weight: 100;
        line-height: 2em;
      }
      .elementor-13245 .elementor-element.elementor-element-79fdebf .head-title {
        color: #af8e4f;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 600;
      }
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        a.elementor-button,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        .elementor-button {
        font-family: "Poppins", Sans-serif;
        font-size: 14px;
        font-weight: 500;
        line-height: 1.3em;
        fill: var(--e-global-color-secondary);
        color: var(--e-global-color-secondary);
        background-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        padding: 10px 20px 10px 20px;
      }
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        a.elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        a.elementor-button:focus,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        .elementor-button:focus {
        color: #af8e4f;
        background-color: #af8e4f00;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        a.elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        .elementor-button:hover
        svg,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        a.elementor-button:focus
        svg,
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        .elementor-button:focus
        svg {
        fill: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        .elementor-button {
        border-style: solid;
        border-width: 2px 2px 2px 2px;
        border-color: #af8e4f;
        box-shadow: 0px 10px 15px -5px rgba(19, 12, 3.000000000000001, 0.2901960784313726);
      }
      .elementor-13245
        .elementor-element.elementor-element-79fdebf
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-40cfc15a
        .elementor-spacer-inner {
        --spacer-size: 100px;
      }
      .elementor-13245 .elementor-element.elementor-element-220faa5e img {
        max-width: 25%;
      }
      .elementor-13245 .elementor-element.elementor-element-28e7f5a9 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-28e7f5a9
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 30px;
        font-weight: bold;
        font-style: normal;
        line-height: 1.3em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-28e7f5a9
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245 .elementor-element.elementor-element-48604da8 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-48604da8
        .elementor-heading-title {
        color: #af8e4f;
        font-family: "Mr De Haviland", Sans-serif;
        font-size: 84px;
        font-weight: 100;
      }
      .elementor-13245
        .elementor-element.elementor-element-48604da8
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-612510b6 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-612510b6
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: bold;
        font-style: normal;
        line-height: 1.3em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-612510b6
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245 .elementor-element.elementor-element-35313fc0 {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-35313fc0
        .elementor-heading-title {
        color: #552f01;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: normal;
        font-style: normal;
        line-height: 1.5em;
        letter-spacing: 1px;
      }
      .elementor-13245
        .elementor-element.elementor-element-35313fc0
        > .elementor-widget-container {
        margin: 0px 0px 0px 0px;
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4d90b3b
        .elementor-spacer-inner {
        --spacer-size: 50px;
      }
      .elementor-13245 .elementor-element.elementor-element-fd9eee1 {
        padding: 0px 0px 0px 0px;
      }
      .elementor-13245
        .elementor-element.elementor-element-b168b68
        > .elementor-element-populated {
        border-style: solid;
        border-width: 0px 1px 0px 0px;
        border-color: var(--e-global-color-primary);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-b168b68
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245 .elementor-element.elementor-element-af16753 img {
        width: 60%;
      }
      .elementor-13245
        .elementor-element.elementor-element-af16753
        > .elementor-widget-container {
        margin: 0px 0px -15px 0px;
      }
      .elementor-13245 .elementor-element.elementor-element-897c052 {
        --grid-template-columns: repeat(0, auto);
        --icon-size: 18px;
      }
      .elementor-13245
        .elementor-element.elementor-element-897c052
        .elementor-widget-container {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-897c052
        .elementor-social-icon {
        background-color: rgba(129, 134, 213, 0);
        --icon-padding: 0.3em;
      }
      .elementor-13245
        .elementor-element.elementor-element-897c052
        .elementor-social-icon
        i {
        color: #223017;
      }
      .elementor-13245
        .elementor-element.elementor-element-897c052
        .elementor-social-icon
        svg {
        fill: #223017;
      }
      .elementor-bc-flex-widget
        .elementor-13245
        .elementor-element.elementor-element-69a01a9.elementor-column
        .elementor-widget-wrap {
        align-items: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-69a01a9.elementor-column.elementor-element[data-element_type="column"]
        > .elementor-widget-wrap.elementor-element-populated {
        align-content: center;
        align-items: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-69a01a9
        > .elementor-widget-wrap
        > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
        margin-bottom: 7px;
      }
      .elementor-13245
        .elementor-element.elementor-element-69a01a9
        > .elementor-element-populated {
        border-style: solid;
        border-width: 0px 0px 0px 1px;
        border-color: var(--e-global-color-primary);
        transition: background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;
      }
      .elementor-13245
        .elementor-element.elementor-element-69a01a9
        > .elementor-element-populated
        > .elementor-background-overlay {
        transition: background 0.3s, border-radius 0.3s, opacity 0.3s;
      }
      .elementor-13245 .elementor-element.elementor-element-1eb8edf img {
        width: 50%;
      }
      .elementor-13245 .elementor-element.elementor-element-1ecbd0d {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-1ecbd0d
        .elementor-heading-title {
        color: var(--e-global-color-primary);
        font-family: "Poppins", Sans-serif;
        font-size: 7px;
        font-weight: normal;
        line-height: 1.3em;
      }
      .elementor-13245 .elementor-element.elementor-element-15c36066 {
        padding: 0px 200px 0px 200px;
      }
      .elementor-13245
        .elementor-element.elementor-element-57d53e91
        .elementor-icon-wrapper {
        text-align: center;
      }
      .elementor-13245
        .elementor-element.elementor-element-57d53e91
        .elementor-icon
        i,
      .elementor-13245
        .elementor-element.elementor-element-57d53e91
        .elementor-icon
        svg {
        transform: rotate(0deg);
      }
      .elementor-13245 .elementor-element.elementor-element-57d53e91 {
        width: 50px;
        max-width: 50px;
        bottom: 100px;
      }
      body:not(.rtl) .elementor-13245 .elementor-element.elementor-element-57d53e91 {
        left: 5px;
      }
      body.rtl .elementor-13245 .elementor-element.elementor-element-57d53e91 {
        right: 5px;
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-dear {
        text-align: center;
        margin-top: 15px;
        font-family: "Playfair Display", Sans-serif;
        font-size: 18px;
        font-weight: 500;
        color: var(--e-global-color-secondary);
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-text {
        text-align: center;
        margin-top: 15px;
        font-family: "Playfair Display", Sans-serif;
        font-size: 16px;
        font-weight: 100;
        line-height: 1.5em;
        letter-spacing: 0.5px;
        color: var(--e-global-color-secondary);
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .overlayy {
        background-color: #000000;
        opacity: 0.5;
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-name {
        margin-top: 15px;
        font-family: "Playfair Display", Sans-serif;
        color: var(--e-global-color-secondary);
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .dwp-sesi {
        margin-top: 15px;
        font-family: "Poppins", Sans-serif;
        font-weight: 500;
        line-height: 1.3em;
        color: #552f01;
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .text_tambahan {
        margin-top: 20px;
        font-family: "Playfair Display", Sans-serif;
        font-size: 24px;
        font-weight: 600;
        letter-spacing: 9px;
        color: var(--e-global-color-secondary);
      }
      .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-mempelai {
        margin-top: -15px;
        margin-bottom: -15px;
        font-family: "Mr De Haviland", Sans-serif;
        font-size: 100px;
        color: var(--e-global-color-secondary);
      }
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        .wdp-button-wrapper {
        margin-top: 40px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        .elementor-image
        img {
        width: 10%;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        a.elementor-button,
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        .elementor-button {
        fill: var(--e-global-color-secondary);
        color: var(--e-global-color-secondary);
        font-family: "Poppins", Sans-serif;
        font-size: 12px;
        font-weight: 500;
        line-height: 1.3em;
        background-color: #af8e4f;
        border-radius: 13px 13px 13px 13px;
        padding: 7px 20px 8px 20px;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        .elementor-button {
        border-style: solid;
        border-width: 1px 1px 1px 1px;
        border-color: #af8e4f;
      }
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        a.elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        .elementor-button:hover,
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        a.elementor-button:focus,
      .elementor-13245
        .elementor-element.elementor-element-4a3a5d04
        .elementor-button:focus {
        color: var(--e-global-color-secondary);
        background-color: #ffffff00;
        border-color: var(--e-global-color-secondary);
      }
      .elementor-widget .tippy-tooltip .tippy-content {
        text-align: center;
      }
      @media (max-width: 1024px) {
        .elementor-13245
          .elementor-element.elementor-element-76b249e6:not(.elementor-motion-effects-element-type-background),
        .elementor-13245
          .elementor-element.elementor-element-76b249e6
          > .elementor-motion-effects-container
          > .elementor-motion-effects-layer {
          background-position: center center;
          background-size: cover;
        }
        .elementor-13245
          .elementor-element.elementor-element-796a6c9f
          > .elementor-element-populated {
          padding: 400px 0px 450px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-4e38f214 img {
          max-width: 35%;
        }
        .elementor-13245
          .elementor-element.elementor-element-1e5c4abc
          .elementor-heading-title {
          font-size: 145px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2048c2fa
          .elementor-heading-title {
          font-size: 120px;
        }
        .elementor-13245
          .elementor-element.elementor-element-41fd2722
          > .elementor-element-populated {
          padding: 50px 50px 75px 50px;
        }
        .elementor-bc-flex-widget
          .elementor-13245
          .elementor-element.elementor-element-7c030c0f.elementor-column
          .elementor-widget-wrap {
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-7c030c0f.elementor-column.elementor-element[data-element_type="column"]
          > .elementor-widget-wrap.elementor-element-populated {
          align-content: center;
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-7c030c0f.elementor-column
          > .elementor-widget-wrap {
          justify-content: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-7c030c0f
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 30px 100px 0px 100px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6f32ce6
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2ed44bf5
          .elementor-divider-separator {
          width: 50%;
        }
        .elementor-bc-flex-widget
          .elementor-13245
          .elementor-element.elementor-element-591daa95.elementor-column
          .elementor-widget-wrap {
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-591daa95.elementor-column.elementor-element[data-element_type="column"]
          > .elementor-widget-wrap.elementor-element-populated {
          align-content: center;
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-591daa95.elementor-column
          > .elementor-widget-wrap {
          justify-content: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-591daa95
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-1f193a86 {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-1f193a86
          .elementor-heading-title {
          font-size: 70px;
        }
        .elementor-bc-flex-widget
          .elementor-13245
          .elementor-element.elementor-element-31e509f9.elementor-column
          .elementor-widget-wrap {
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-31e509f9.elementor-column.elementor-element[data-element_type="column"]
          > .elementor-widget-wrap.elementor-element-populated {
          align-content: center;
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-31e509f9.elementor-column
          > .elementor-widget-wrap {
          justify-content: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-31e509f9
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 30px 100px 0px 100px;
        }
        .elementor-13245
          .elementor-element.elementor-element-74bfa98
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3d42edc9
          .elementor-divider-separator {
          width: 50%;
        }
        .elementor-13245 .elementor-element.elementor-element-3648a29 {
          padding: 75px 0px 100px 0px;
        }
        .elementor-bc-flex-widget
          .elementor-13245
          .elementor-element.elementor-element-110926d6.elementor-column
          .elementor-widget-wrap {
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-110926d6.elementor-column.elementor-element[data-element_type="column"]
          > .elementor-widget-wrap.elementor-element-populated {
          align-content: center;
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-110926d6.elementor-column
          > .elementor-widget-wrap {
          justify-content: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-110926d6
          > .elementor-element-populated {
          margin: 10px 10px 10px 10px;
          --e-column-margin-right: 10px;
          --e-column-margin-left: 10px;
          padding: 35px 50px 35px 50px;
        }
        .elementor-13245 .elementor-element.elementor-element-784e5321 img {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-1b9f6237
          .elementor-heading-title {
          font-size: 17px;
        }
        .elementor-13245 .elementor-element.elementor-element-40c15cd {
          padding: 0px 10px 0px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a92a9d8
          > .elementor-element-populated {
          margin: 10px 10px 10px 10px;
          --e-column-margin-right: 10px;
          --e-column-margin-left: 10px;
          padding: 20px 20px 20px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5cb602f9
          > .elementor-element-populated {
          margin: 10px 10px 10px 10px;
          --e-column-margin-right: 10px;
          --e-column-margin-left: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3189c5fc
          > .elementor-element-populated {
          margin: 10px 10px 10px 10px;
          --e-column-margin-right: 10px;
          --e-column-margin-left: 10px;
          padding: 20px 20px 20px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-250132d4
          > .elementor-element-populated {
          padding: 75px 50px 70px 50px;
        }
        .elementor-13245
          .elementor-element.elementor-element-726abe0
          > .elementor-element-populated {
          padding: 0px 50px 0px 50px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-tablet-left
          .pp-timeline-marker-wrapper {
          margin-right: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-tablet-right
          .pp-timeline-marker-wrapper {
          margin-left: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-tablet-center
          .pp-timeline-marker-wrapper {
          margin-left: 10px;
          margin-right: 10px;
        }
        .elementor-13245 .elementor-element.elementor-element-68b58272 {
          margin-top: 0px;
          margin-bottom: 0px;
          padding: 0px 250px 0px 250px;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 70%;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-title {
          font-size: 14px;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 70%;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-title {
          font-size: 14px;
        }
        .elementor-13245 .elementor-element.elementor-element-4faee864 {
          margin-top: 0px;
          margin-bottom: 0px;
          padding: 0px 250px 0px 250px;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 70%;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-title {
          font-size: 14px;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 70%;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-title {
          font-size: 14px;
        }
        .elementor-13245 .elementor-element.elementor-element-ccfca4c {
          padding: 0px 150px 0px 150px;
        }
        .elementor-13245
          .elementor-element.elementor-element-b205ac1
          > .elementor-element-populated {
          padding: 0px 50px 0px 50px;
        }
        .elementor-13245 .elementor-element.elementor-element-220faa5e img {
          max-width: 35%;
        }
        .elementor-13245
          .elementor-element.elementor-element-48604da8
          .elementor-heading-title {
          font-size: 120px;
        }
      }
      @media (max-width: 767px) {
        .elementor-13245
          .elementor-element.elementor-element-76b249e6:not(.elementor-motion-effects-element-type-background),
        .elementor-13245
          .elementor-element.elementor-element-76b249e6
          > .elementor-motion-effects-container
          > .elementor-motion-effects-layer {
          background-image: url("<?= $asset_theme ?>photos/Cover1.jpg");
          background-position: bottom center;
          background-repeat: no-repeat;
          background-size: cover;
        }
        .elementor-13245 .elementor-element.elementor-element-76b249e6 {
          margin-top: 0px;
          margin-bottom: 0px;
          padding: 150px 30px 200px 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-796a6c9f
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-796a6c9f
          > .elementor-element-populated {
          border-width: 7px 7px 7px 7px;
          padding: 100px 20px 50px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-796a6c9f
          > .elementor-element-populated,
        .elementor-13245
          .elementor-element.elementor-element-796a6c9f
          > .elementor-element-populated
          > .elementor-background-overlay,
        .elementor-13245
          .elementor-element.elementor-element-796a6c9f
          > .elementor-background-slideshow {
          border-radius: 500px 500px 500px 500px;
        }
        .elementor-13245 .elementor-element.elementor-element-4e38f214 img {
          max-width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-4e38f214
          > .elementor-widget-container {
          margin: 0px 0px 20px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-38406c99
          .elementor-heading-title {
          font-size: 16px;
        }
        .elementor-13245 .elementor-element.elementor-element-1e5c4abc {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-1e5c4abc
          .elementor-heading-title {
          font-size: 84px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1e5c4abc
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-2048c2fa {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-2048c2fa
          .elementor-heading-title {
          font-size: 64px;
          line-height: 0.8em;
        }
        .elementor-13245
          .elementor-element.elementor-element-2048c2fa
          > .elementor-widget-container {
          margin: -30px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1e2d690f
          .elementor-heading-title {
          font-size: 20px;
          letter-spacing: 3px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1e2d690f
          > .elementor-widget-container {
          margin: 20px 0px 20px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-2e923d5b {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-2e923d5b
          .elementor-heading-title {
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2e923d5b
          > .elementor-widget-container {
          margin: 0px 10px 0px 10px;
        }
        .elementor-13245 .elementor-element.elementor-element-5b61c068 {
          --icon-box-icon-margin: -5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5b61c068
          .elementor-icon {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5b61c068
          .elementor-icon-box-title {
          margin-bottom: 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5b61c068
          .elementor-icon-box-title,
        .elementor-13245
          .elementor-element.elementor-element-5b61c068
          .elementor-icon-box-title
          a {
          font-size: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5b61c068
          > .elementor-widget-container {
          margin: 20px 0px 20px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-3be7446e {
          padding: 130px 20px 100px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-41fd2722
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-41fd2722
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-e2f8cb9 img {
          width: 25%;
        }
        .elementor-13245
          .elementor-element.elementor-element-e2f8cb9
          > .elementor-widget-container {
          margin: 0px 0px 10px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-77a41531
          .elementor-heading-title {
          font-size: 24px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6c9e306f
          .elementor-heading-title {
          font-size: 58px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6c9e306f
          > .elementor-widget-container {
          margin: -10px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6d5516c2
          .elementor-heading-title {
          font-size: 13px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6d5516c2
          > .elementor-widget-container {
          margin: 0px 20px 0px 20px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-7d4fa686 {
          margin-top: 30px;
          margin-bottom: 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7c030c0f
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7c030c0f
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 30px 0px 30px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6f32ce6
          .elementor-heading-title {
          font-size: 75px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6f32ce6
          > .elementor-widget-container {
          margin: 0px 0px -20px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6fa0c7d7
          .elementor-heading-title {
          font-size: 24px;
        }
        .elementor-13245
          .elementor-element.elementor-element-6fa0c7d7
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2ed44bf5
          .elementor-divider-separator {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-2ed44bf5
          .elementor-divider {
          padding-top: 10px;
          padding-bottom: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-147b0d48
          .elementor-heading-title {
          font-size: 12px;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5a03a4d7
          .elementor-heading-title {
          font-size: 13px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5a03a4d7
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-568f7a68 {
          --icon-size: 30px;
          --grid-column-gap: 5px;
          --grid-row-gap: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-591daa95
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1f193a86
          .elementor-heading-title {
          font-size: 48px;
        }
        .elementor-13245
          .elementor-element.elementor-element-31e509f9
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-31e509f9
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 30px 0px 30px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-74bfa98
          .elementor-heading-title {
          font-size: 75px;
        }
        .elementor-13245
          .elementor-element.elementor-element-74bfa98
          > .elementor-widget-container {
          margin: 0px 0px -20px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-16e0c8da
          .elementor-heading-title {
          font-size: 24px;
        }
        .elementor-13245
          .elementor-element.elementor-element-16e0c8da
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3d42edc9
          .elementor-divider-separator {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-3d42edc9
          .elementor-divider {
          padding-top: 10px;
          padding-bottom: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5594747e
          .elementor-heading-title {
          font-size: 12px;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-76f55830
          .elementor-heading-title {
          font-size: 13px;
        }
        .elementor-13245
          .elementor-element.elementor-element-76f55830
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-19c701fd {
          --icon-size: 30px;
          --grid-column-gap: 5px;
          --grid-row-gap: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3648a29:not(.elementor-motion-effects-element-type-background),
        .elementor-13245
          .elementor-element.elementor-element-3648a29
          > .elementor-motion-effects-container
          > .elementor-motion-effects-layer {
          background-image: url("<?= $asset_theme ?>photos/Cover.jpg");
        }
        .elementor-13245
          .elementor-element.elementor-element-3648a29
          > .elementor-shape-top
          svg {
          width: calc(200% + 1.3px);
          height: 100px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3648a29
          > .elementor-shape-bottom
          svg {
          width: calc(200% + 1.3px);
          height: 100px;
        }
        .elementor-13245 .elementor-element.elementor-element-3648a29 {
          padding: 150px 20px 150px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-110926d6
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1bf9898f
          .elementor-heading-title {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-566e6a9c
          .elementor-heading-title {
          font-size: 42px;
        }
        .elementor-13245
          .elementor-element.elementor-element-566e6a9c
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-14c24f0f
          .elementor-heading-title {
          font-size: 13px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-14c24f0f
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1.bdt-countdown--label-block
          .bdt-countdown-number {
          margin-bottom: 7px;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1.bdt-countdown--label-inline
          .bdt-countdown-number {
          margin-right: 7px;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1
          .bdt-countdown-wrapper {
          max-width: 90%;
          margin-left: auto;
          margin-right: auto;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1
          .bdt-countdown-item {
          border-radius: 50px 50px 50px 50px;
          padding: 15px 10px 20px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1
          .bdt-countdown-number {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1
          .bdt-countdown-label {
          font-size: 12px;
        }
        .elementor-13245
          .elementor-element.elementor-element-10bdd0d1
          > .elementor-widget-container {
          margin: 10px 0px 10px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-784e5321 img {
          width: 60%;
        }
        .elementor-13245
          .elementor-element.elementor-element-1b9f6237
          .elementor-heading-title {
          font-size: 13px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1b9f6237
          > .elementor-widget-container {
          margin: 0px 10px 0px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-341dc986
          .elementor-heading-title {
          font-size: 18px;
        }
        .elementor-13245 .elementor-element.elementor-element-40c15cd {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a92a9d8
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a92a9d8
          > .elementor-element-populated,
        .elementor-13245
          .elementor-element.elementor-element-4a92a9d8
          > .elementor-element-populated
          > .elementor-background-overlay,
        .elementor-13245
          .elementor-element.elementor-element-4a92a9d8
          > .elementor-background-slideshow {
          border-radius: 250px 250px 100px 100px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a92a9d8
          > .elementor-element-populated {
          margin: 50px 30px 50px 30px;
          --e-column-margin-right: 30px;
          --e-column-margin-left: 30px;
          padding: 85px 0px 35px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5486d470
          .elementor-heading-title {
          font-size: 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-f0f9f94
          .elementor-heading-title {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7173118c
          .elementor-heading-title {
          font-size: 22px;
          letter-spacing: -0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7173118c
          > .elementor-widget-container {
          margin: 7px 0px 7px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3b84784d
          .elementor-heading-title {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-77c6ac9b
          .elementor-divider-separator {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-cdc34b1
          .elementor-icon {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1873c64a
          .elementor-heading-title {
          font-size: 18px;
          line-height: 1.3em;
        }
        .elementor-13245
          .elementor-element.elementor-element-1873c64a
          > .elementor-widget-container {
          margin: 5px 40px 10px 40px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4ed7e2f2
          .elementor-heading-title {
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4ed7e2f2
          > .elementor-widget-container {
          margin: 0px 30px 0px 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-1c1758d
          .elementor-button {
          font-size: 12px;
          padding: 7px 20px 7px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5cb602f9
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 50px 30px 50px 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3189c5fc
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3189c5fc
          > .elementor-element-populated,
        .elementor-13245
          .elementor-element.elementor-element-3189c5fc
          > .elementor-element-populated
          > .elementor-background-overlay,
        .elementor-13245
          .elementor-element.elementor-element-3189c5fc
          > .elementor-background-slideshow {
          border-radius: 250px 250px 100px 100px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3189c5fc
          > .elementor-element-populated {
          margin: 50px 30px 75px 30px;
          --e-column-margin-right: 30px;
          --e-column-margin-left: 30px;
          padding: 85px 0px 35px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3f9360ae
          .elementor-heading-title {
          font-size: 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2ad3fb03
          .elementor-heading-title {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-38f5188e
          .elementor-heading-title {
          font-size: 22px;
          letter-spacing: -0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-38f5188e
          > .elementor-widget-container {
          margin: 7px 0px 7px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4d13ce5a
          .elementor-heading-title {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3e7ab8ef
          .elementor-divider-separator {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-4351d8b
          .elementor-icon {
          font-size: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-58490392
          .elementor-heading-title {
          font-size: 18px;
          line-height: 1.3em;
        }
        .elementor-13245
          .elementor-element.elementor-element-58490392
          > .elementor-widget-container {
          margin: 5px 40px 10px 40px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2d7ccc1b
          .elementor-heading-title {
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2d7ccc1b
          > .elementor-widget-container {
          margin: 0px 30px 0px 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-18a3f64c
          .elementor-button {
          font-size: 12px;
          padding: 7px 20px 7px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7cc8608b
          .elementor-spacer-inner {
          --spacer-size: 50px;
        }
        .elementor-13245 .elementor-element.elementor-element-38898da {
          margin-top: -5px;
          margin-bottom: 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4aa75142
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4aa75142
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 0px 0px 20px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-60ddc842
          .elementor-heading-title {
          font-size: 18px;
          line-height: 1.3em;
        }
        .elementor-13245
          .elementor-element.elementor-element-27454581
          .elementor-heading-title {
          font-size: 42px;
        }
        .elementor-13245
          .elementor-element.elementor-element-27454581
          > .elementor-widget-container {
          margin: -10px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-37d3d335
          .elementor-heading-title {
          font-size: 13px;
          line-height: 1.5em;
        }
        .elementor-13245
          .elementor-element.elementor-element-37d3d335
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 50px 10px 50px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2807d831
          .elementor-button {
          font-size: 12px;
          padding: 7px 20px 7px 20px;
        }
        .elementor-13245 .elementor-element.elementor-element-5a3f9b62 {
          padding: 100px 20px 150px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-250132d4
          > .elementor-element-populated {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7c2273f8
          .elementor-heading-title {
          font-size: 18px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3194c0c
          .elementor-heading-title {
          font-size: 42px;
        }
        .elementor-13245
          .elementor-element.elementor-element-3194c0c
          > .elementor-widget-container {
          margin: -10px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2e90b7a
          .elementor-spacer-inner {
          --spacer-size: 15px;
        }
        .elementor-13245 .elementor-element.elementor-element-79b16a98 {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-726abe0
          > .elementor-element-populated {
          padding: 0px 10px 0px 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline
          .pp-timeline-card {
          padding: 30px 30px 30px 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-card {
          text-align: left;
          font-size: 10px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-card-title {
          font-size: 16px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-marker {
          font-size: 8px;
          width: 20px;
          height: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-marker
          img {
          width: 8px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-connector-wrap {
          width: 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-navigation:before,
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-navigation
          .pp-slider-arrow {
          bottom: calc(20px / 2);
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-card-date {
          font-size: 13px;
          line-height: 2em;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-mobile-left
          .pp-timeline-marker-wrapper {
          margin-right: 10px !important;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-mobile-right
          .pp-timeline-marker-wrapper {
          margin-left: 10px !important;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-mobile-center
          .pp-timeline-marker-wrapper {
          margin-left: 10px !important;
          margin-right: 10px !important;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-left
          .pp-timeline-marker-wrapper {
          margin-right: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-right
          .pp-timeline-marker-wrapper {
          margin-left: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical.pp-timeline-center
          .pp-timeline-marker-wrapper {
          margin-left: 10px;
          margin-right: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-horizontal {
          margin-top: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-navigation
          .pp-timeline-card-date-wrapper {
          margin-bottom: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-vertical
          .pp-timeline-connector {
          width: 3px;
        }
        .elementor-13245
          .elementor-element.elementor-element-337e18ca
          .pp-timeline-navigation:before {
          height: 3px;
          transform: translateY(calc(3px / 2));
        }
        .elementor-13245
          .elementor-element.elementor-element-36e409f8
          .elementor-spacer-inner {
          --spacer-size: 100px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4183bbbd
          .elementor-heading-title {
          font-size: 42px;
          line-height: 1em;
        }
        .elementor-13245
          .elementor-element.elementor-element-4183bbbd
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-29c50091
          .elementor-heading-title {
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-29c50091
          > .elementor-widget-container {
          margin: -5px 0px 0px 0px;
          padding: 0px 40px 0px 40px;
        }
        .elementor-13245
          .elementor-element.elementor-element-cabbd42
          .elementor-spacer-inner {
          --spacer-size: 5px;
        }
        .elementor-13245 .elementor-element.elementor-element-68b58272 {
          margin-top: 0px;
          margin-bottom: 20px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-52b7f221 {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-52b7f221
          > .elementor-element-populated {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82.elementor-position-right
          .elementor-image-box-img {
          margin-left: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82.elementor-position-left
          .elementor-image-box-img {
          margin-right: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82.elementor-position-top
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 25%;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-wrapper {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-title {
          margin-bottom: 5px;
          font-size: 12px;
          line-height: 1.2em;
        }
        .elementor-13245
          .elementor-element.elementor-element-557f5c82
          .elementor-image-box-description {
          font-size: 8px;
          letter-spacing: 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-3edabd4 {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-3edabd4
          > .elementor-element-populated {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01.elementor-position-right
          .elementor-image-box-img {
          margin-left: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01.elementor-position-left
          .elementor-image-box-img {
          margin-right: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01.elementor-position-top
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 25%;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-wrapper {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-title {
          margin-bottom: 5px;
          font-size: 12px;
          line-height: 1.2em;
        }
        .elementor-13245
          .elementor-element.elementor-element-64d20e01
          .elementor-image-box-description {
          font-size: 8px;
          letter-spacing: 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-4faee864 {
          margin-top: 20px;
          margin-bottom: 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-6ffa99f7 {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-6ffa99f7
          > .elementor-element-populated {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad.elementor-position-right
          .elementor-image-box-img {
          margin-left: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad.elementor-position-left
          .elementor-image-box-img {
          margin-right: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad.elementor-position-top
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 25%;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-wrapper {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-title {
          margin-bottom: 5px;
          font-size: 12px;
          line-height: 1.2em;
        }
        .elementor-13245
          .elementor-element.elementor-element-87af7ad
          .elementor-image-box-description {
          font-size: 8px;
          letter-spacing: 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-4c4a3dd8 {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-4c4a3dd8
          > .elementor-element-populated {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5.elementor-position-right
          .elementor-image-box-img {
          margin-left: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5.elementor-position-left
          .elementor-image-box-img {
          margin-right: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5.elementor-position-top
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-img {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-wrapper
          .elementor-image-box-img {
          width: 25%;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-wrapper {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-title {
          margin-bottom: 5px;
          font-size: 12px;
          line-height: 1.2em;
        }
        .elementor-13245
          .elementor-element.elementor-element-550a4f5
          .elementor-image-box-description {
          font-size: 8px;
          letter-spacing: 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-40d57445:not(.elementor-motion-effects-element-type-background),
        .elementor-13245
          .elementor-element.elementor-element-40d57445
          > .elementor-motion-effects-container
          > .elementor-motion-effects-layer {
          background-position: center center;
          background-size: cover;
        }
        .elementor-13245
          .elementor-element.elementor-element-40d57445
          > .elementor-shape-top
          svg {
          height: 45px;
        }
        .elementor-13245 .elementor-element.elementor-element-40d57445 {
          padding: 150px 20px 50px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-e3c5fef
          > .elementor-element-populated {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5a9a91ec
          .elementor-heading-title {
          font-size: 20px;
          line-height: 1.3em;
        }
        .elementor-13245
          .elementor-element.elementor-element-75feba40
          .elementor-heading-title {
          font-size: 42px;
        }
        .elementor-13245
          .elementor-element.elementor-element-75feba40
          > .elementor-widget-container {
          margin: -10px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-370accba
          .elementor-heading-title {
          font-size: 13px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-370accba
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-ccfca4c {
          padding: 0px 10px 0px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5e6a055
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5e6a055
          > .elementor-element-populated,
        .elementor-13245
          .elementor-element.elementor-element-5e6a055
          > .elementor-element-populated
          > .elementor-background-overlay,
        .elementor-13245
          .elementor-element.elementor-element-5e6a055
          > .elementor-background-slideshow {
          border-radius: 50px 50px 50px 50px;
        }
        .elementor-13245
          .elementor-element.elementor-element-5e6a055
          > .elementor-element-populated {
          margin: 20px 0px 50px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 0px 10px 40px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          .form-komentar-cswd
          input[type="text"],
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          .form-komentar-cswd
          select,
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          .form-komentar-cswd
          textarea {
          font-size: 12px;
        }
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          .wdp-wrap-link
          a {
          font-size: 14px;
        }
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          .form-submit
          input[type="submit"],
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          .form-submit
          button {
          font-size: 12px;
          border-width: 2px 2px 2px 2px;
        }
        .elementor-13245
          .elementor-element.elementor-element-f0586a1
          > .elementor-widget-container {
          margin: -30px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-21870f4a
          .elementor-spacer-inner {
          --spacer-size: 75px;
        }
        .elementor-13245
          .elementor-element.elementor-element-76ed6c96
          .elementor-heading-title {
          font-size: 13px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-76ed6c96
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-65efc767
          .elementor-heading-title {
          font-size: 14px;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-65efc767
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7eaa471
          .elementor-spacer-inner {
          --spacer-size: 75px;
        }
        .elementor-13245 .elementor-element.elementor-element-741d8c5 {
          padding: 0px 10px 0px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-d8e3645
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-d8e3645
          > .elementor-element-populated,
        .elementor-13245
          .elementor-element.elementor-element-d8e3645
          > .elementor-element-populated
          > .elementor-background-overlay,
        .elementor-13245
          .elementor-element.elementor-element-d8e3645
          > .elementor-background-slideshow {
          border-radius: 75px 75px 75px 75px;
        }
        .elementor-13245
          .elementor-element.elementor-element-d8e3645
          > .elementor-element-populated {
          margin: 20px 0px 50px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 50px 10px 30px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7d14be38
          .elementor-heading-title {
          font-size: 36px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7d14be38
          > .elementor-widget-container {
          margin: 10px 0px 10px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7d95a8ba
          .elementor-heading-title {
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-7d95a8ba
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 10px 0px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-75e7f29d
          .elementor-button {
          font-size: 12px;
          padding: 7px 15px 7px 15px;
        }
        .elementor-13245
          .elementor-element.elementor-element-75e7f29d
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-963a4ee
          .elementor-spacer-inner {
          --spacer-size: 15px;
        }
        .elementor-13245
          .elementor-element.elementor-element-b205ac1
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-b205ac1
          > .elementor-element-populated {
          padding: 0px 10px 20px 10px;
        }
        .elementor-13245
          .elementor-element.elementor-element-ea618ee
          .elementor-heading-title {
          font-size: 24px;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-ea618ee
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2dc2cb6
          .elementor-divider-separator {
          width: 50%;
          margin: 0 auto;
          margin-center: 0;
        }
        .elementor-13245
          .elementor-element.elementor-element-2dc2cb6
          .elementor-divider {
          text-align: center;
          padding-top: 5px;
          padding-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-2dc2cb6
          > .elementor-widget-container {
          margin: 0px 0px 10px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-c4ecd23 img {
          width: 50%;
        }
        .elementor-13245
          .elementor-element.elementor-element-c4ecd23
          > .elementor-widget-container {
          margin: 0px 0px 10px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-c9aa069 .copy-content {
          font-size: 20px;
          line-height: 1em;
        }
        .elementor-13245 .elementor-element.elementor-element-c9aa069 .head-title {
          font-size: 16px;
        }
        .elementor-13245
          .elementor-element.elementor-element-c9aa069
          a.elementor-button,
        .elementor-13245
          .elementor-element.elementor-element-c9aa069
          .elementor-button {
          font-size: 12px;
          padding: 5px 15px 5px 15px;
        }
        .elementor-13245
          .elementor-element.elementor-element-dbb37f1
          .elementor-heading-title {
          font-size: 24px;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-dbb37f1
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-13ff743
          .elementor-divider-separator {
          width: 50%;
          margin: 0 auto;
          margin-center: 0;
        }
        .elementor-13245
          .elementor-element.elementor-element-13ff743
          .elementor-divider {
          text-align: center;
          padding-top: 5px;
          padding-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-13ff743
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-79fdebf .copy-content {
          font-size: 10px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245 .elementor-element.elementor-element-79fdebf .head-title {
          font-size: 16px;
        }
        .elementor-13245
          .elementor-element.elementor-element-79fdebf
          a.elementor-button,
        .elementor-13245
          .elementor-element.elementor-element-79fdebf
          .elementor-button {
          font-size: 12px;
          padding: 5px 15px 5px 15px;
        }
        .elementor-13245
          .elementor-element.elementor-element-79fdebf
          > .elementor-widget-container {
          margin: 0px 30px 0px 30px;
        }
        .elementor-13245
          .elementor-element.elementor-element-40cfc15a
          .elementor-spacer-inner {
          --spacer-size: 75px;
        }
        .elementor-13245 .elementor-element.elementor-element-220faa5e img {
          max-width: 62%;
        }
        .elementor-13245
          .elementor-element.elementor-element-28e7f5a9
          .elementor-heading-title {
          font-size: 18px;
        }
        .elementor-13245
          .elementor-element.elementor-element-28e7f5a9
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 20px 0px 20px;
        }
        .elementor-13245 .elementor-element.elementor-element-48604da8 {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-48604da8
          .elementor-heading-title {
          font-size: 56px;
          line-height: 0.8em;
        }
        .elementor-13245
          .elementor-element.elementor-element-48604da8
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-612510b6
          .elementor-heading-title {
          font-size: 14px;
        }
        .elementor-13245
          .elementor-element.elementor-element-612510b6
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
          padding: 0px 20px 0px 20px;
        }
        .elementor-13245
          .elementor-element.elementor-element-35313fc0
          .elementor-heading-title {
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-35313fc0
          > .elementor-widget-container {
          margin: -15px 0px 0px 0px;
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4d90b3b
          .elementor-spacer-inner {
          --spacer-size: 50px;
        }
        .elementor-13245 .elementor-element.elementor-element-8160aaf {
          width: 15%;
        }
        .elementor-13245 .elementor-element.elementor-element-b168b68 {
          width: 35%;
        }
        .elementor-bc-flex-widget
          .elementor-13245
          .elementor-element.elementor-element-b168b68.elementor-column
          .elementor-widget-wrap {
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-b168b68.elementor-column.elementor-element[data-element_type="column"]
          > .elementor-widget-wrap.elementor-element-populated {
          align-content: center;
          align-items: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-b168b68
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 7px;
        }
        .elementor-13245
          .elementor-element.elementor-element-b168b68
          > .elementor-element-populated {
          margin: 0px 0px 0px 0px;
          --e-column-margin-right: 0px;
          --e-column-margin-left: 0px;
          padding: 10px 0px 10px 15px;
        }
        .elementor-13245 .elementor-element.elementor-element-af16753 img {
          width: 60%;
        }
        .elementor-13245
          .elementor-element.elementor-element-af16753
          > .elementor-widget-container {
          margin: 0px 0px 0px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-897c052 {
          --icon-size: 18px;
        }
        .elementor-13245 .elementor-element.elementor-element-69a01a9 {
          width: 35%;
        }
        .elementor-13245
          .elementor-element.elementor-element-69a01a9
          > .elementor-widget-wrap
          > .elementor-widget:not(.elementor-widget__width-auto):not(.elementor-widget__width-initial):not(:last-child):not(.elementor-absolute) {
          margin-bottom: 5px;
        }
        .elementor-13245
          .elementor-element.elementor-element-69a01a9
          > .elementor-element-populated {
          padding: 10px 15px 10px 0px;
        }
        .elementor-13245 .elementor-element.elementor-element-1eb8edf img {
          width: 60%;
        }
        .elementor-13245 .elementor-element.elementor-element-1ecbd0d {
          text-align: center;
        }
        .elementor-13245
          .elementor-element.elementor-element-1ecbd0d
          .elementor-heading-title {
          font-size: 5px;
          line-height: 1.4em;
        }
        .elementor-13245 .elementor-element.elementor-element-ac9add5 {
          width: 15%;
        }
        .elementor-13245 .elementor-element.elementor-element-15c36066 {
          padding: 0px 0px 0px 0px;
        }
        .elementor-13245
          .elementor-element.elementor-element-57d53e91
          .elementor-icon {
          font-size: 1px;
        }
        .elementor-13245 .elementor-element.elementor-element-57d53e91 {
          width: 35px;
          max-width: 35px;
          bottom: 70px;
        }
        body:not(.rtl)
          .elementor-13245
          .elementor-element.elementor-element-57d53e91 {
          left: 10px;
        }
        body.rtl .elementor-13245 .elementor-element.elementor-element-57d53e91 {
          right: 10px;
        }
        .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-text {
          margin-top: 20px;
          font-size: 12px;
          line-height: 1.5em;
          letter-spacing: 0.5px;
        }
        .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-dear {
          margin-top: 20px;
          font-size: 14px;
          letter-spacing: 1px;
        }
        .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-name {
          margin-top: 20px;
          font-size: 16px;
        }
        .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .dwp-sesi {
          margin-top: 20px;
          font-size: 12px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a3a5d04
          .text_tambahan {
          margin-top: 0px;
          font-size: 15px;
          line-height: 5em;
          letter-spacing: 6px;
        }
        .elementor-13245 .elementor-element.elementor-element-4a3a5d04 .wdp-mempelai {
          margin-top: -30px;
          margin-bottom: -30px;
          font-size: 60px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a3a5d04
          .wdp-button-wrapper {
          margin-top: 40px;
        }
        .elementor-13245
          .elementor-element.elementor-element-4a3a5d04
          .elementor-image
          img {
          width: 35%;
        }
      }
      @media (min-width: 768px) {
        .elementor-13245 .elementor-element.elementor-element-7c030c0f {
          width: 45%;
        }
        .elementor-13245 .elementor-element.elementor-element-591daa95 {
          width: 9.331%;
        }
        .elementor-13245 .elementor-element.elementor-element-31e509f9 {
          width: 45%;
        }
        .elementor-13245 .elementor-element.elementor-element-4a92a9d8 {
          width: 45%;
        }
        .elementor-13245 .elementor-element.elementor-element-5cb602f9 {
          width: 9.332%;
        }
        .elementor-13245 .elementor-element.elementor-element-3189c5fc {
          width: 45%;
        }
      }
      @media (max-width: 1024px) and (min-width: 768px) {
        .elementor-13245 .elementor-element.elementor-element-7c030c0f {
          width: 100%;
        }
        .elementor-13245 .elementor-element.elementor-element-591daa95 {
          width: 100%;
        }
        .elementor-13245 .elementor-element.elementor-element-31e509f9 {
          width: 100%;
        }
        .elementor-13245 .elementor-element.elementor-element-4a92a9d8 {
          width: 33.33%;
        }
        .elementor-13245 .elementor-element.elementor-element-5cb602f9 {
          width: 33.33%;
        }
        .elementor-13245 .elementor-element.elementor-element-3189c5fc {
          width: 33.33%;
        }
      }
    </style>

    
    <link rel='stylesheet' id='elementor-post-51207-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-51207.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-31584-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-31584.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-css' href='https://use.fontawesome.com/releases/v5.15.3/css/all.css' type='text/css' media='all' integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous" />
    <link rel='stylesheet' id='loftloader-lite-animation-css' href='<?= $asset ?>wp-content/plugins/loftloader/assets/css/loftloader.min.css?ver=2022022501' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-official-v4shim-css' href='https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css' type='text/css' media='all' integrity="sha384-C2B+KlPW+WkR0Ld9loR1x3cXp7asA0iGVodhCoJ4hwrWm/d9qKS59BGisq+2Y0/D" crossorigin="anonymous" />
    <style id='font-awesome-official-v4shim-inline-css' type='text/css'>
      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-brands-400.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-solid-900.svg#fontawesome") format("svg");
      }

      @font-face {
        font-family: "FontAwesome";
        font-display: block;
        src: url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff2") format("woff2"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.woff") format("woff"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.ttf") format("truetype"),
          url("https://use.fontawesome.com/releases/v5.15.3/webfonts/fa-regular-400.svg#fontawesome") format("svg");
        unicode-range: U+F004-F005, U+F007, U+F017, U+F022, U+F024, U+F02E, U+F03E, U+F044, U+F057-F059, U+F06E, U+F070, U+F075, U+F07B-F07C, U+F080, U+F086, U+F089, U+F094, U+F09D, U+F0A0, U+F0A4-F0A7, U+F0C5, U+F0C7-F0C8, U+F0E0, U+F0EB, U+F0F3, U+F0F8, U+F0FE, U+F111, U+F118-F11A, U+F11C, U+F133, U+F144, U+F146, U+F14A, U+F14D-F14E, U+F150-F152, U+F15B-F15C, U+F164-F165, U+F185-F186, U+F191-F192, U+F1AD, U+F1C1-F1C9, U+F1CD, U+F1D8, U+F1E3, U+F1EA, U+F1F6, U+F1F9, U+F20A, U+F247-F249, U+F24D, U+F254-F25B, U+F25D, U+F267, U+F271-F274, U+F279, U+F28B, U+F28D, U+F2B5-F2B6, U+F2B9, U+F2BB, U+F2BD, U+F2C1-F2C2, U+F2D0, U+F2D2, U+F2DC, U+F2ED, U+F328, U+F358-F35B, U+F3A5, U+F3D1, U+F410, U+F4AD;
      }
    </style>
    <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Averia+Serif+Libre%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontserrat%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRaleway%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRufina%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CDM+Serif+Display%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCrimson+Pro%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CAlegreya%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCourgette%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CEuphoria+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMa+Shan+Zheng%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMolle%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMr+De+Haviland%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMrs+Saint+Delafield%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CParisienne%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRouge+Script%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CZhi+Mang+Xing%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CMontez%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CCinzel+Decorative%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPlayfair+Display+SC%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=6.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-shared-0-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-unityinv-css' href='<?= $asset ?>wp-content/uploads/elementor/custom-icons/unityinv/style.css?ver=1.0.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-regular-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/font-awesome/css/regular.min.css?ver=5.15.3' type='text/css' media='all' />
    <script type='text/javascript' id='jquery-core-js-extra'>
      /* 
					<![CDATA[ */
      var pp = {
        // "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>plugins/jquery/js/jquery-3.3.1.js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/jquery.min.js?ver=3.6.0' id='jquery-core-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/jquery-migrate.min.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/assets/js/minify/extension.min.js?ver=6.5.8' id='pafe-extension-js'></script>
    <link rel="https://api.w.org/" href="<?= $asset ?>wp-json/" />
    <link rel="alternate" type="application/json" href="<?= $asset ?>wp-json/wp/v2/posts/13245" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?= $asset ?>xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?= $asset ?>wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.0" />
    <link rel='shortlink' href='<?= $asset ?>?p=13245' />
    <link rel="alternate" type="application/json+oembed" href="<?= $asset ?>wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Fsilver1%2F" />
    <link rel="alternate" type="text/xml+oembed" href="<?= $asset ?>wp-json/oembed/1.0/embed?url=https%3A%2F%2Funityinvitation.com%2Fsilver1%2F&#038;format=xml" />
    <link rel="icon" href="<?= $asset_theme ?>icons/icon1.png" sizes="32x32" />
    <link rel="icon" href="<?= $asset_theme ?>icons/icon1.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?= $asset_theme ?>icons/icon1.png" />
    <meta name="msapplication-TileImage" content="<?= $asset_theme ?>icons/icon1.png" />
    <style>
      .pswp.pafe-lightbox-modal {
        display: none;
      }
    </style>
    <style id="loftloader-lite-custom-bg-color">
      #loftloader-wrapper .loader-section {
        background: #ffffff;
      }
    </style>
    <style id="loftloader-lite-custom-bg-opacity">
      #loftloader-wrapper .loader-section {
        opacity: 1;
      }
    </style>
    <style id="loftloader-lite-custom-loader">
      #loftloader-wrapper.pl-imgloading #loader {
        width: 70px;
      }

      #loftloader-wrapper.pl-imgloading #loader span {
        background-size: cover;
        background-image: url(<?= $asset_theme ?>logos/logo1.png);
      }
    </style>
    <style type="text/css" id="wp-custom-css">
      .elementor-section {
        overflow: hidden;
      }

      .justify-cstm .pp-timeline-card {
        text-align: justify !important;
      }
    </style>

    <!-- Repost -->
    <link type="text/css" src="https://lycon.co.id/template/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <link type="text/css" src="https://lycon.co.id/template/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link type="text/css" src="https://lycon.co.id/template/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link type="text/css" src="https://lycon.co.id/template/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link type="text/css" src="https://lycon.co.id/template/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover" />
  </head>
  <body onload="onLoad()" data-rsssl=1 class="post-template post-template-elementor_canvas single single-post postid-13245 single-format-standard loftloader-lite-enabled elementor-default elementor-template-canvas elementor-kit-1399 elementor-page elementor-page-13245">
    <div id="loftloader-wrapper" class="pl-imgloading" data-show-close-time="15000" data-max-load-time="10000">
      <div class="loader-inner">
        <div id="loader">
          <div class="imgloading-container">
            <span style="background-image: url(<?= $asset_theme ?>logos/logo1.png);"></span>
          </div>
          <img width="70" height="43" data-no-lazy="1" class="skip-lazy" alt="loader image" src="<?= $asset_theme ?>logos/logo1.png">
        </div>
      </div>
      <div class="loader-section section-fade"></div>
      <div class="loader-close-button" style="display: none;">
        <span class="screen-reader-text">Close</span>
      </div>
    </div>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-dark-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0.49803921568627" />
            <feFuncG type="table" tableValues="0 0.49803921568627" />
            <feFuncB type="table" tableValues="0 0.49803921568627" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-grayscale">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.54901960784314 0.98823529411765" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.71764705882353 0.25490196078431" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-red">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 1" />
            <feFuncG type="table" tableValues="0 0.27843137254902" />
            <feFuncB type="table" tableValues="0.5921568627451 0.27843137254902" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-midnight">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0 0" />
            <feFuncG type="table" tableValues="0 0.64705882352941" />
            <feFuncB type="table" tableValues="0 1" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-magenta-yellow">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.78039215686275 1" />
            <feFuncG type="table" tableValues="0 0.94901960784314" />
            <feFuncB type="table" tableValues="0.35294117647059 0.47058823529412" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-purple-green">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.65098039215686 0.40392156862745" />
            <feFuncG type="table" tableValues="0 1" />
            <feFuncB type="table" tableValues="0.44705882352941 0.4" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 0 0" width="0" height="0" focusable="false" role="none" style="visibility: hidden; position: absolute; left: -9999px; overflow: hidden;">
      <defs>
        <filter id="wp-duotone-blue-orange">
          <feColorMatrix color-interpolation-filters="sRGB" type="matrix" values=" .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 .299 .587 .114 0 0 " />
          <feComponentTransfer color-interpolation-filters="sRGB">
            <feFuncR type="table" tableValues="0.098039215686275 1" />
            <feFuncG type="table" tableValues="0 0.66274509803922" />
            <feFuncB type="table" tableValues="0.84705882352941 0.41960784313725" />
            <feFuncA type="table" tableValues="1 1" />
          </feComponentTransfer>
          <feComposite in2="SourceGraphic" operator="in" />
        </filter>
      </defs>
    </svg>
    <div data-elementor-type="wp-post" data-elementor-id="13245" class="elementor elementor-13245">
      <section class="elementor-section elementor-top-section elementor-element elementor-element-76b249e6 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="76b249e6" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_bottom&quot;:&quot;waves&quot;}">
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
	c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
	c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-796a6c9f wdp-sticky-section-no" data-id="796a6c9f" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-4e38f214 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="4e38f214" data-element_type="widget" data-widget_type="image.default">
                <div class="elementor-widget-container">
                  <img width="351" height="86" src="<?= $asset_theme ?>otherimages/image1.png" class="attachment-full size-full" alt="" loading="lazy" srcset="<?= $asset_theme ?>otherimages/image1.png 351w, <?= $asset_theme ?>otherimages/image1.png 300w" sizes="(max-width: 351px) 100vw, 351px" />
                </div>
              </div>
              <div class="elementor-element elementor-element-38406c99 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="38406c99" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h1 class="elementor-heading-title elementor-size-default">PERNIKAHAN</h1>
                </div>
              </div>
              <div class="elementor-element elementor-element-1e5c4abc wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1e5c4abc" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default"><?= substr($bride, 0, 1) ?>&<?= substr($groom, 0, 1) ?></h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-2048c2fa wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2048c2fa" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default"><?= $bride ?> <?= $groom ?></h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-1e2d690f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1e2d690f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default"><?= $wedding_fullday ?></h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-2e923d5b animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2e923d5b" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;,&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Maha Suci Allah Subhanahu wa Ta'ala yang telah menciptakan makhluk-Nya berpasang-pasangan. Ya Allah, perkenankanlah dan Ridhoilah Pernikahan Kami.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-5b61c068 elementor-view-default elementor-mobile-position-top elementor-vertical-align-top wdp-sticky-section-no elementor-widget elementor-widget-icon-box" data-id="5b61c068" data-element_type="widget" data-widget_type="icon-box.default">
                <div class="elementor-widget-container">
                  <div class="elementor-icon-box-wrapper">
                    <div class="elementor-icon-box-icon">
                      <span class="elementor-icon elementor-animation-">
                        <i aria-hidden="true" class="fas fa-angle-double-up"></i>
                      </span>
                    </div>
                    <div class="elementor-icon-box-content">
                      <h3 class="elementor-icon-box-title">
                        <span> Swipe Up </span>
                      </h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-3be7446e elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="3be7446e" data-element_type="section" id="couple" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-41fd2722 wdp-sticky-section-no" data-id="41fd2722" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-e2f8cb9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="e2f8cb9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:55}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                <div class="elementor-widget-container">
                  <img width="300" height="212" src="<?= $asset_theme ?>otherimages/image2.png" class="attachment-medium size-medium" alt="" loading="lazy" srcset="<?= $asset_theme ?>otherimages/image2.png 300w, <?= $asset_theme ?>otherimages/image2.png 1024w, <?= $asset_theme ?>otherimages/image2.png 768w, <?= $asset_theme ?>otherimages/image2.png 1536w, <?= $asset_theme ?>otherimages/image2.png 2048w" sizes="(max-width: 300px) 100vw, 300px" />
                </div>
              </div>
              <div class="elementor-element elementor-element-77a41531 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="77a41531" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pasangan</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-6c9e306f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6c9e306f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-6d5516c2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6d5516c2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Pernikahan adalah ibadah, dan setiap ibadah bermuara pada cinta-Nya sebagai tujuan. Sudah sewajarnya setiap upaya meraih cinta-Nya dilakukan dengan sukacita.</p>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-7d4fa686 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7d4fa686" data-element_type="section">
                <div class="elementor-container elementor-column-gap-no">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-7c030c0f animated-slow wdp-sticky-section-no elementor-invisible" data-id="7c030c0f" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInRight&quot;,&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation_tablet&quot;:&quot;fadeInUp&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-6f32ce6 animated-fast wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6f32ce6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default"><?= $bride ?></h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-6fa0c7d7 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="6fa0c7d7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default"><?= $bride_firstname ?> <?= $bride_middlename ?> <?= $bride_lastname ?></h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2ed44bf5 elementor-widget-divider--view-line wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-divider" data-id="2ed44bf5" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-147b0d48 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="147b0d48" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default"><?= $bride_child_position ?></h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5a03a4d7 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5a03a4d7" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">Bapak <?= $bride_father ?> <br>& <br> Ibu <?= $bride_mother ?> </h3>
                        </div>
                      </div>
                      <!-- <div class="elementor-element elementor-element-568f7a68 elementor-shape-circle animated-fast elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="568f7a68" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="<?= $bride_ig ?>" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div> -->
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-591daa95 animated-slow wdp-sticky-section-no elementor-invisible" data-id="591daa95" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;zoomIn&quot;,&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation_tablet&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1f193a86 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1f193a86" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">&</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-31e509f9 animated-slow wdp-sticky-section-no elementor-invisible" data-id="31e509f9" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInLeft&quot;,&quot;animation_mobile&quot;:&quot;zoomIn&quot;,&quot;animation_tablet&quot;:&quot;fadeInUp&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-74bfa98 animated-fast wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="74bfa98" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default"><?= $groom ?></h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-16e0c8da animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="16e0c8da" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default"><?= $groom_firstname ?> <?= $groom_middlename ?> <?= $groom_lastname ?></h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3d42edc9 elementor-widget-divider--view-line wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-divider" data-id="3d42edc9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5594747e animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5594747e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default"><?= $groom_child_position ?></h3>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-76f55830 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="76f55830" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h3 class="elementor-heading-title elementor-size-default">Bapak <?= $groom_father ?> <br>& <br> Ibu <?= $groom_mother ?> </h3>
                        </div>
                      </div>
                      <!-- <div class="elementor-element elementor-element-19c701fd elementor-shape-circle animated-fast elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="19c701fd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="<?= $groom_ig ?>" target="_blank">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div> -->
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
      <section class="elementor-section elementor-top-section elementor-element elementor-element-3648a29 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="3648a29" data-element_type="section" id="date" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;background_background&quot;:&quot;classic&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;shape_divider_top&quot;:&quot;waves&quot;,&quot;shape_divider_bottom&quot;:&quot;waves&quot;}">
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
	c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
	c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z" />
          </svg>
        </div>
        <div class="elementor-shape elementor-shape-bottom" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
	c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
	c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-110926d6 animated-slow wdp-sticky-section-no" data-id="110926d6" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;none&quot;,&quot;animation_delay&quot;:0,&quot;animation_mobile&quot;:&quot;none&quot;}">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-1bf9898f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1bf9898f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Hitung Mundur </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-566e6a9c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="566e6a9c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Hari Bahagia Kami</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-14c24f0f wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="14c24f0f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Untuk mengikuti Sunnah Rasul-Mu dalam rangka membentuk keluarga yang sakinah, mawaddah, dan warahmah, maka izinkanlah kami melangsungkan Pernikahan.</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-10bdd0d1 bdt-countdown--align-center bdt-countdown--label-block wdp-sticky-section-no elementor-widget elementor-widget-bdt-countdown" data-id="10bdd0d1" data-element_type_="widget" data-settings="{&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:54}},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="bdt-countdown.default">
                <div class="elementor-widget-container">
                  <div class="bdt-countdown-wrapper bdt-countdown-skin-default" data-settings="{&quot;id&quot;:&quot;#bdt-countdown-10bdd0d1&quot;,&quot;msgId&quot;:&quot;#bdt-countdown-msg-10bdd0d1&quot;,&quot;adminAjaxUrl&quot;:&quot;https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php&quot;,&quot;endActionType&quot;:&quot;none&quot;,&quot;redirectUrl&quot;:null,&quot;redirectDelay&quot;:1000,&quot;finalTime&quot;:&quot;2022-12-30T17:00:00+00:00&quot;,&quot;wpCurrentTime&quot;:1655305500,&quot;endTime&quot;:1672419600,&quot;loopHours&quot;:false,&quot;isLogged&quot;:false,&quot;couponTrickyId&quot;:&quot;bdt-sf-10bdd0d1&quot;,&quot;triggerId&quot;:false}">
                    <div id="bdt-countdown-10bdd0d1-timer" class="bdt-grid bdt-grid-small bdt-child-width-1-4 bdt-child-width-1-2@s bdt-child-width-1-4@m countdown" data-bdt-countdown_="date: 2022-12-30T17:00:00+00:00" data-bdt-grid="" style="x">
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-days-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-days bdt-text-center">
                            <span class="days">00</span>
                          </span>
                          <span class="bdt-countdown-label bdt-text-center">Hari</span>
                        </div>
                      </div>
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-hours-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-hours bdt-text-center">
                            <span class="hours">00</span>
                          </span>
                          <span class="bdt-countdown-label bdt-text-center">Jam</span>
                        </div>
                      </div>
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-minutes-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-minutes bdt-text-center">
                            <span class="minutes">00</span>
                          </span>
                          <span class="bdt-countdown-label bdt-text-center">Menit</span>
                        </div>
                      </div>
                      <div class="bdt-countdown-item-wrapper">
                        <div class="bdt-countdown-item bdt-seconds-wrapper">
                          <span class="bdt-countdown-number bdt-countdown-seconds bdt-text-center">
                            <span class="seconds">00</span>
                          </span>
                          <span class="bdt-countdown-label bdt-text-center">Detik</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-7dd1f3c2 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="7dd1f3c2" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-784e5321 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="784e5321" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:53}},&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:55}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="image.default">
                <div class="elementor-widget-container">
                  <img width="768" height="268" src="<?= $asset_theme ?>otherimages/image3.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset_theme ?>otherimages/image3.png 768w, <?= $asset_theme ?>otherimages/image3.png 300w, <?= $asset_theme ?>otherimages/image3.png 1024w, <?= $asset_theme ?>otherimages/image3.png 1536w, <?= $asset_theme ?>otherimages/image3.png 2001w" sizes="(max-width: 768px) 100vw, 768px" />
                </div>
              </div>
              <div class="elementor-element elementor-element-1b9f6237 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1b9f6237" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:50}},&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:50}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default"><?= $verse_value ?></p>
                </div>
              </div>
              <div class="elementor-element elementor-element-341dc986 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="341dc986" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:50}},&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:20,&quot;end&quot;:50}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default"><?= $verse_name ?></p>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-40c15cd elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="40c15cd" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4a92a9d8 animated-slow wdp-sticky-section-no" data-id="4a92a9d8" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:52}},&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:56}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-5486d470 wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="5486d470" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Akad</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-5c22b998 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="5c22b998" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-f0f9f94 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f0f9f94" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_day ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7173118c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7173118c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_date ?> <?= $akad_month ?> <?= $akad_year ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3b84784d wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3b84784d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_hour ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-77c6ac9b elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="77c6ac9b" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-cdc34b1 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="cdc34b1" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1873c64a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="1873c64a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_venue ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4ed7e2f2 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4ed7e2f2" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $akad_venue_address ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1c1758d elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="1c1758d" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="<?= $akad_venue_map ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-5cb602f9 animated-slow wdp-sticky-section-no elementor-invisible" data-id="5cb602f9" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-3189c5fc animated-slow wdp-sticky-section-no" data-id="3189c5fc" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;none&quot;,&quot;animation_mobile&quot;:&quot;none&quot;,&quot;motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:52}},&quot;motion_fx_opacity_effect&quot;:&quot;yes&quot;,&quot;motion_fx_opacity_range&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:56}},&quot;motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:4,&quot;sizes&quot;:[]},&quot;motion_fx_opacity_direction&quot;:&quot;out-in&quot;,&quot;motion_fx_opacity_level&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:10,&quot;sizes&quot;:[]},&quot;motion_fx_devices&quot;:[&quot;desktop&quot;,&quot;tablet&quot;,&quot;mobile&quot;]}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-3f9360ae wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="3f9360ae" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Resepsi</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-18d1837d elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="18d1837d" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2ad3fb03 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2ad3fb03" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_day ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-38f5188e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="38f5188e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_date ?> <?= $wedding_month ?> <?= $wedding_year ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4d13ce5a wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4d13ce5a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_hour ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-3e7ab8ef elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="3e7ab8ef" data-element_type="widget" data-widget_type="divider.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-divider">
                            <span class="elementor-divider-separator"></span>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4351d8b elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="4351d8b" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="fas fa-map-marker-alt"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-58490392 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="58490392" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_venue ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2d7ccc1b wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="2d7ccc1b" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default"><?= $wedding_venue_address ?></p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-18a3f64c elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="18a3f64c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="<?= $wedding_venue_map ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fas fa-location-arrow"></i>
                                </span>
                                <span class="elementor-button-text">Lihat Lokasi</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-7cc8608b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="7cc8608b" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <!-- <section class="elementor-section elementor-inner-section elementor-element elementor-element-38898da elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="38898da" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_mobile&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-4aa75142 wdp-sticky-section-no" data-id="4aa75142" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-60ddc842 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="60ddc842" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Kami Akan <br> Melangsungkan </p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-27454581 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="27454581" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Live Streaming</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-37d3d335 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="37d3d335" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Momen kebahagiaan prosesi Pernikahan akan kami tayangkan secara virtual melalui Youtube Live. </h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-2807d831 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-button" data-id="2807d831" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a href="<?= $youtube_streaming ?>" target="_blank" class="elementor-button-link elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="fab fa-youtube"></i>
                                </span>
                                <span class="elementor-button-text">Join Streaming</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section> -->
            </div>
          </div>
        </div>
      </section>
      <!-- <section class="elementor-section elementor-top-section elementor-element elementor-element-5a3f9b62 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="5a3f9b62" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-250132d4 wdp-sticky-section-no" data-id="250132d4" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-7c2273f8 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7c2273f8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Sebuah Kisah</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-3194c0c wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="3194c0c" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Perjalanan Kami</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-2e90b7a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="2e90b7a" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-79b16a98 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="79b16a98" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-726abe0 wdp-sticky-section-no" data-id="726abe0" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-337e18ca justify-cstm wdp-sticky-section-no elementor-widget elementor-widget-pp-timeline" data-id="337e18ca" data-element_type="widget" data-settings="{&quot;direction&quot;:&quot;left&quot;,&quot;layout&quot;:&quot;vertical&quot;,&quot;dates&quot;:&quot;yes&quot;,&quot;card_arrow&quot;:&quot;yes&quot;,&quot;direction_tablet&quot;:&quot;left&quot;,&quot;direction_mobile&quot;:&quot;left&quot;}" data-widget_type="pp-timeline.default">
                        <div class="elementor-widget-container">
                          <div class="pp-timeline-wrapper">
                            <div class="pp-timeline pp-timeline-vertical pp-timeline-left pp-timeline-tablet-left pp-timeline-mobile-left pp-timeline-dates pp-timeline-arrows-middle" data-timeline-layout="vertical">
                              <div class="pp-timeline-connector-wrap">
                                <div class="pp-timeline-connector">
                                  <div class="pp-timeline-connector-inner"></div>
                                </div>
                              </div>
                              <div class="pp-timeline-items">
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-711f6c4">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $first_meet_date ?></div>
                                        <h1 class="pp-timeline-card-title"> Pertama Bertemu </h1>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $first_meet_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $first_meet_date ?></div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-ced78a2">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $having_a_relationship_date ?></div>
                                        <h1 class="pp-timeline-card-title"> Menjalin Hubungan </h1>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $having_a_relationship_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $having_a_relationship_date ?></div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-left elementor-repeater-item-6e6e67e">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $engagement_date ?></div>
                                        <h1 class="pp-timeline-card-title"> Bertunangan </h1>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $engagement_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $engagement_date ?></div>
                                  </div>
                                </div>
                                <div class="pp-timeline-item pp-timeline-item-right elementor-repeater-item-b117de4">
                                  <div class="pp-timeline-card-wrapper">
                                    <div class="pp-timeline-arrow"></div>
                                    <div class="pp-timeline-card">
                                      <div class="pp-timeline-card-title-wrap">
                                        <div class="pp-timeline-card-date"><?= $married_date ?></div>
                                        <h1 class="pp-timeline-card-title"> Menikah </h1>
                                      </div>
                                      <div class="pp-timeline-card-content">
                                        <p><?= $married_story ?></p>
                                      </div>
                                    </div>
                                    </a>
                                  </div>
                                  <div class="pp-timeline-marker-wrapper">
                                    <div class="pp-timeline-marker">
                                      <span class="pp-icon">
                                        <i aria-hidden="true" class="fas fa-heart"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div class="pp-timeline-card-date-wrapper">
                                    <div class="pp-timeline-card-date"><?= $married_date ?></div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-36e409f8 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="36e409f8" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-4183bbbd wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="4183bbbd" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">PROTOKOL KESEHATAN</h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-29c50091 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="29c50091" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default">Tanpa mengurangi rasa hormat <br> acara ini menerapkan Protokol Kesehatan sesuai dengan peraturan dan rekomendasi pemerintah. </h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-cabbd42 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="cabbd42" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-68b58272 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="68b58272" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-52b7f221 animated-slow wdp-sticky-section-no elementor-invisible" data-id="52b7f221" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:200,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-557f5c82 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="557f5c82" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/medical-mask.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Gunakan Masker</h3>
                              <p class="elementor-image-box-description">Wajib menggunakan <br> masker di dalam acara </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-3edabd4 animated-slow wdp-sticky-section-no elementor-invisible" data-id="3edabd4" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:100,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-64d20e01 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="64d20e01" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/washing-hands.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Cuci Tangan</h3>
                              <p class="elementor-image-box-description">Mencuci tangan dan <br> menggunakan Handsanitizier </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-4faee864 elementor-section-full_width elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="4faee864" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-6ffa99f7 animated-slow wdp-sticky-section-no elementor-invisible" data-id="6ffa99f7" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:300,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-87af7ad elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="87af7ad" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/physical.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Jaga Jarak </h3>
                              <p class="elementor-image-box-description">Menjaga jarak 2M dengan <br> tamu undanga lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-4c4a3dd8 animated-slow wdp-sticky-section-no elementor-invisible" data-id="4c4a3dd8" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;,&quot;animation_delay&quot;:600,&quot;animation_mobile&quot;:&quot;zoomIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-550a4f5 elementor-vertical-align-middle elementor-position-top wdp-sticky-section-no elementor-widget elementor-widget-image-box" data-id="550a4f5" data-element_type="widget" data-widget_type="image-box.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-image-box-wrapper">
                            <figure class="elementor-image-box-img">
                              <img width="512" height="512" src="<?= $asset ?>wp-content/uploads/2021/05/no-handshake.png" class="elementor-animation-grow attachment-full size-full" alt="" loading="lazy" />
                            </figure>
                            <div class="elementor-image-box-content">
                              <h3 class="elementor-image-box-title">Tidak Berjabat Tangan</h3>
                              <p class="elementor-image-box-description">Menghindari kontak fisik dengan <br>tamu undangan lainnya </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section> -->
      <section class="elementor-section elementor-top-section elementor-element elementor-element-40d57445 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="40d57445" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;shape_divider_top&quot;:&quot;waves&quot;}">
        <div class="elementor-shape elementor-shape-top" data-negative="false">
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
            <path class="elementor-shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
	c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
	c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z" />
          </svg>
        </div>
        <div class="elementor-container elementor-column-gap-default">
          <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-e3c5fef wdp-sticky-section-no" data-id="e3c5fef" data-element_type="column">
            <div class="elementor-widget-wrap elementor-element-populated">
              <div class="elementor-element elementor-element-5a9a91ec wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="5a9a91ec" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Ucapan untuk</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-75feba40 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="75feba40" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Kedua Mempelai</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-370accba wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="370accba" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Kesan mendalam akan terukir dihati kami, apabila Bapak/Ibu/Saudara/i berkenan hadir untuk memberikan do’a restu kepada kedua mempelai. Atas kehadiran dan do’a restu Bapak/Ibu/Saudara/i kami ucapkan banyak terima kasih. </p>
                </div>
              </div>
              <section class="elementor-section elementor-inner-section elementor-element elementor-element-ccfca4c elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ccfca4c" data-element_type="section" data-settings="{&quot;animation_mobile&quot;:&quot;fadeIn&quot;,&quot;animation&quot;:&quot;none&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-5e6a055 wdp-sticky-section-no" data-id="5e6a055" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-f0586a1 wdp-sticky-section-no elementor-widget elementor-widget-cswdeding-komentar" data-id="f0586a1" data-element_type="widget" data-widget_type="cswdeding-komentar.default">
                        <div class="elementor-widget-container">
                          <style>
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=text],
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form textarea.wdp-textarea,
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form select.wdp-select,
                            .wdp-wrapper .wdp-wrap-form .wdp-container-form input[type=submit] {
                              height: auto !important;
                            }

                            .halooo {
                              background-color: red !important;
                              color: #fff !important;
                            }

                            .wdp-wrapper .wdp-wrap-link {
                              text-align: center;
                              padding: 18px;
                              margin-top: 15px;
                            }

                            .wdp-wrapper .wdp-wrap-form {
                              border-top: 0px;
                            }

                            .konfirmasi-kehadiran {
                              display: flex;
                              justify-content: center;
                              flex-direction: row;
                              padding: 10px;
                              margin-top: 10px;
                            }

                            .jenis-konfirmasi>i {
                              margin: 0px 5px;
                            }

                            .jenis-konfirmasi {
                              padding: 5px 10px;
                              background: red;
                              border-radius: 0.5em;
                              color: #fff;
                              margin: 7px;
                            }
                          </style>
                          <div class='wdp-wrapper wdp-custom' style='overflow: hidden;'>
                            <div class='wdp-wrap-link'>
                              <a id='wdp-link-13245' class='wdp-link wdp-icon-link wdp-icon-link-true auto-load-true' href='?post_id=13245&amp;comments=4&amp;get=0&amp;order=DESC' title='4 Ucapan'></a>
                              <div class='konfirmasi-kehadiran elementor-screen-only'>
                                <div class='jenis-konfirmasi cswd-hadir'>
                                  <i class='fas fa-check'></i> 1 Hadir
                                </div>
                                <div class='jenis-konfirmasi cswd-tidak-hadir'>
                                  <i class='fas fa-times'></i> 3 Tidak Hadir
                                </div>
                              </div>
                            </div>
                            <!-- <div id='wdp-wrap-commnent-13245' class='wdp-wrap-comments' style='display:none;'>
                              <div id='wdp-wrap-form-13245' class='wdp-wrap-form wdp-clearfix'>
                                <div id='wdp-container-form-13245' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                                  <div id='respond-13245' class='respond wdp-clearfix'>
                                    <form action='<?= $asset ?>wp-comments-post.php' method='post' id='commentform-13245'>
                                      <p class="comment-form-author wdp-field-1">
                                        <input id="author" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" />
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span>
                                      </p>
                                      <div class="wdp-wrap-textarea">
                                        <textarea id="wdp-textarea-13245" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                      </div>
                                      <div class="wdp-wrap-select">
                                        <select class="waci_comment wdp-select" name="konfirmasi">
                                          <option value="" disabled selected>Konfirmasi Kehadiran</option>> </option>
                                          <option value="Hadir">Hadir</option>
                                          <option value="Tidak Hadir">Tidak Hadir</option>
                                        </select>
                                        <span class="wdp-required">*</span>
                                        <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                      </div>
                                      <div class='wdp-wrap-submit wdp-clearfix'>
                                        <p class='form-submit'>
                                          <span class="wdp-hide">Do not change these fields following</span>
                                          <input type="text" class="wdp-hide" name="name" value="username">
                                          <input type="text" class="wdp-hide" name="nombre" value="">
                                          <input type="text" class="wdp-hide" name="form-wdp" value="">
                                          <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                          <input name='submit' id='submit-13245' value='Kirim' type='submit' />
                                          <input type='hidden' name='commentpress' value='true' />
                                          <input type='hidden' name='comment_post_ID' value='13245' id='comment_post_ID' />
                                          <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                        </p>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <div id='wdp-comment-status-13245' class='wdp-comment-status'></div>
                              <ul id='wdp-container-comment-13245' class='wdp-container-comments wdp-order-DESC  wdp-has-4-comments wdp-multiple-comments' data-order='DESC'></ul>
                              <div class='wdp-holder-13245 wdp-holder'></div>
                            </div> -->
                            <div id='wdp-wrap-commnent-13245_' class='wdp-wrap-comments' style='display:block;'>
                                <div id='wdp-wrap-form-13245' class='wdp-wrap-form wdp-clearfix'>
                                    <div id='wdp-container-form-13245' class='wdp-container-form form-komentar-cswd wdp-no-login'>
                                        <div id='respond-13245' class='respond wdp-clearfix'>
                                          <form action='https://unityinvitation.com/save-sap.php' method='post' id='commentform-13245'>
                                            <p class="comment-form-author wdp-field-1">
                                              <input id="name" name="author" type="text" aria-required="true" class="wdp-input" placeholder="Nama Anda" />
                                              <span class="wdp-required">*</span>
                                              <span class="wdp-error-info wdp-error-info-name">Mohon maaf! Khusus untuk tamu undangan</span>
                                            </p>
                                            <div class="wdp-wrap-textarea">
                                              <textarea id="description" class="waci_comment wdp-textarea autosize-textarea" name="comment" aria-required="true" placeholder="Berikan Ucapan & Doa untuk Kedua Mempelai" rows="3"></textarea>
                                              <span class="wdp-required">*</span>
                                              <span class="wdp-error-info wdp-error-info-text">Minimal 2 karakter.</span>
                                            </div>
                                            <div class="wdp-wrap-select">
                                              <select class="waci_comment wdp-select" id="confirmation_of_attendance" name="konfirmasi">
                                                <option value="" disabled selected>Konfirmasi Kehadiran</option>></option>
                                                <option value="Hadir">Hadir</option>
                                                <option value="Tidak Hadir">Tidak Hadir</option>
                                              </select>
                                              <span class="wdp-required">*</span>
                                              <span class="wdp-error-info wdp-error-info-confirm">Silahkan pilih konfirmasi kehadiran</span>
                                            </div>
                                            <div class='wdp-wrap-submit wdp-clearfix'>
                                              <p class='form-submit'>
                                                <span class="wdp-hide">Do not change these fields following</span>
                                                <input type="text" class="wdp-hide" name="name" value="username">
                                                <input type="text" class="wdp-hide" name="nombre" value="">
                                                <input type="text" class="wdp-hide" name="form-wdp" value="">
                                                <input type="button" class="wdp-form-btn wdp-cancel-btn" value="Batal">
                                                <input name='submit' value="Kirim" id='submit-13245' type='button' onclick="saveSAP()" style="background-color: #AF8E4F; color: #ffffff; border-color: #AF8E4F; border-radius: 12px; padding: 5px 25px 5px 25px; font-size: 14px;" onMouseOver="this.style.background='#ffffff';this.style.color='#AF8E4F';" onMouseOut="this.style.background='#AF8E4F';this.style.color='#ffffff';">
                                                <input type='hidden' name='commentpress' value='true' />
                                                <input type='hidden' name='comment_post_ID' value='13245' id='comment_post_ID' />
                                                <input type='hidden' name='comment_parent' id='comment_parent' value='0' />
                                              </p>
                                            </div>
                                          </form>
                                        </div>
                                    </div>
                                </div>
                                <div id='wdp-comment-status-13245'  class='wdp-comment-status' style="display: none">
                                  <p class="wdp-ajax-success">Terima kasih atas ucapan &amp; doanya!</p>
                                </div>
                                <ul id="wdp-container-comment-13245" class="wdp-container-comments wdp-order-DESC  wdp-has-6-comments wdp-multiple-comments" data-order="DESC" style="display: block;">
                                  <?php foreach ($data_sap as $k => $v) { ?>
                                    <li class="comment even thread-even depth-1 wdp-item-comment animated fadeIn" id="wdp-item-comment-14904" data-likes="0">
                                    <div id="wdp-comment-14904" class="wdp-comment wdp-clearfix">
                                      <div class="wdp-comment-avatar">
                                        <img src="https://unityinvitation.com/wp-content/uploads/2022/03/Icon-Unity-Comment.png">
                                      </div><!--.wdp-comment-avatar-->
                                      <div class="wdp-comment-content">
                                        <div class="wdp-comment-info">
                                          <a class="wdp-commenter-name" title="<?= $v['name'] ?>"><?= $v['name'] ?></a>
                                          <span class="wdp-post-author"><i class="fas fa-check-circle"></i> <?= $v['confirmation_of_attendance'] ?></span>
                                          <br>
                                          <span class="wdp-comment-time">
                                            <i class="far fa-clock"></i>
                                            <?= $v['human_created_at'] ?>
                                          </span>
                                        </div><!--.wdp-comment-info-->
                                        <div class="wdp-comment-text">
                                          <p><?= $v['description'] ?></p>
                                        </div><!--.wdp-comment-text-->
                                        <div class="wdp-comment-actions" style="display: block;">
                                        </div><!--.wdp-comment-actions-->
                                      </div><!--.wdp-comment-content-->
                                    </div><!--.wdp-comment-->
                                    <!--</li>-->
                                    </li><!-- #comment-## -->
                                  <?php } ?>
                                </ul>
                                <div id="page" class="wdp-holder-13245 wdp-holder" style="display: block;">
                                  <a class="jp-previous">← Sebelumnya</a>
                                  <a class="">1</a>
                                  <!-- <span class="jp-hidden">...</span> -->
                                  <a class="jp-current">2</a>
                                  <a class="jp-next jp-disabled">Selanjutnya →</a>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="elementor-element elementor-element-21870f4a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="21870f4a" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-76ed6c96 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="76ed6c96" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">"Semoga Allah memberkahimu dan memberkahi apa yang menjadi tanggung jawabmu, serta menyatukan kalian berdua dalam kebaikan."</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-65efc767 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="65efc767" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">(HR. Ahmad, at-Tirmidzi, an-Nasa'i, <br> Abu Dawud, dan Ibnu Majah) </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-7eaa471 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="7eaa471" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <!-- <section class="elementor-section elementor-inner-section elementor-element elementor-element-741d8c5 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="741d8c5" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-d8e3645 wdp-sticky-section-no" data-id="d8e3645" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-3374eef elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="3374eef" data-element_type="widget" data-widget_type="icon.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-icon-wrapper">
                            <div class="elementor-icon">
                              <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7d14be38 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7d14be38" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <p class="elementor-heading-title elementor-size-default">Hadiah Pernikahan</p>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-7d95a8ba wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="7d95a8ba" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">Doa restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah. Tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentu semakin melengkapi kebahagiaan kami.</h2>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-596323ce wdp-sticky-section-no elementor-widget elementor-widget-html" data-id="596323ce" data-element_type="widget" data-widget_type="html.default">
                        <div class="elementor-widget-container">
                          <script>
                            document.addEventListener('DOMContentLoaded', function() {
                              jQuery(function($) {
                                $('.clicktoshow').each(function(i) {
                                  $(this).click(function() {
                                    $('.showclick').eq(i).toggle();
                                    $('.clicktoshow');
                                    $('.showclick2').eq(i).hide();
                                  });
                                });
                              });
                            });
                          </script>
                          <style>
                            .clicktoshow {
                              cursor: pointer;
                            }

                            .showclick {
                              display: none;
                            }
                          </style>
                          <script>
                            document.addEventListener('DOMContentLoaded', function() {
                              jQuery(function($) {
                                $('.clicktoshow2').each(function(i) {
                                  $(this).click(function() {
                                    $('.showclick2').eq(i).toggle();
                                    $('.clicktoshow2');
                                  });
                                });
                              });
                            });
                          </script>
                          <style>
                            .clicktoshow2 {
                              cursor: pointer;
                            }

                            .showclick2 {
                              display: none;
                            }
                          </style>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-75e7f29d elementor-align-center clicktoshow wdp-sticky-section-no elementor-widget elementor-widget-button" data-id="75e7f29d" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;zoomIn&quot;}" data-widget_type="button.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-button-wrapper">
                            <a class="elementor-button elementor-size-sm elementor-animation-grow" role="button">
                              <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-left">
                                  <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                                </span>
                                <span class="elementor-button-text">Kirim Hadiah</span>
                              </span>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-963a4ee wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="963a4ee" data-element_type="widget" data-widget_type="spacer.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-spacer">
                            <div class="elementor-spacer-inner"></div>
                          </div>
                        </div>
                      </div>
                      <section class="elementor-section elementor-inner-section elementor-element elementor-element-0be8669 showclick animated-slow elementor-hidden-desktop elementor-hidden-tablet elementor-hidden-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="0be8669" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                          <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-b205ac1 wdp-sticky-section-no" data-id="b205ac1" data-element_type="column">
                            <div class="elementor-widget-wrap elementor-element-populated">
                              <div class="elementor-element elementor-element-ea618ee wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="ea618ee" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Amplop</p>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-2dc2cb6 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="2dc2cb6" data-element_type="widget" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-divider">
                                    <span class="elementor-divider-separator"></span>
                                  </div>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-c4ecd23 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="c4ecd23" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
                                <div class="elementor-widget-container">
                                  <img width="768" height="243" src="<?= $asset_theme ?>banks/bank1.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset_theme ?>banks/bank1.png 768w, <?= $asset_theme ?>banks/bank1.png 300w, <?= $asset_theme ?>banks/bank1.png 1024w, <?= $asset_theme ?>banks/bank1.png 1536w, <?= $asset_theme ?>banks/bank1.png 1572w" sizes="(max-width: 768px) 100vw, 768px" />
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-c9aa069 elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="c9aa069" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-image img"></div>
                                  <div class="head-title">a/n <?= $transfer_gift_user ?></div>
                                  <div class="elementor-button-wrapper">
                                    <div class="copy-content spancontent"><?= $transfer_gift_rekening ?></div>
                                    <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                      <div class="elementor-button-content-wrapper">
                                        <span class="elementor-button-icon elementor-align-icon-left">
                                          <i aria-hidden="true" class="far fa-copy"></i>
                                        </span>
                                        <span class="elementor-button-text">Salin</span>
                                      </div>
                                    </a>
                                  </div>
                                  <style type="text/css">
                                    .spancontent {
                                      padding-bottom: 20px;
                                    }

                                    .copy-content {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }

                                    .head-title {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }
                                  </style>
                                  <script>
                                    function copyText(el) {
                                      var content = jQuery(el).siblings('div.copy-content').html()
                                      var temp = jQuery(" < textarea > ");
                                        jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                        var text = jQuery(el).html()
                                        jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                        var interval = setInterval(function() {
                                          counter++;
                                          if (counter == 2) {
                                            jQuery(el).html(text)
                                            Interval(interval);
                                          }
                                        }, 5000);
                                      }
                                  </script>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-170027b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="170027b" data-element_type="widget" data-widget_type="spacer.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-spacer">
                                    <div class="elementor-spacer-inner"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-dbb37f1 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="dbb37f1" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                                <div class="elementor-widget-container">
                                  <p class="elementor-heading-title elementor-size-default">Kado</p>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-13ff743 elementor-widget-divider--view-line wdp-sticky-section-no elementor-widget elementor-widget-divider" data-id="13ff743" data-element_type="widget" data-widget_type="divider.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-divider">
                                    <span class="elementor-divider-separator"></span>
                                  </div>
                                </div>
                              </div>
                              <div class="elementor-element elementor-element-79fdebf elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="79fdebf" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="weddingpress-copy-text.default">
                                <div class="elementor-widget-container">
                                  <div class="elementor-image img"></div>
                                  <div class="head-title">a/n <?= $send_gift_user ?></div>
                                  <div class="elementor-button-wrapper">
                                    <div class="copy-content spancontent"><?= $send_gift_address ?></div>
                                    <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                      <div class="elementor-button-content-wrapper">
                                        <span class="elementor-button-icon elementor-align-icon-left">
                                          <i aria-hidden="true" class="far fa-copy"></i>
                                        </span>
                                        <span class="elementor-button-text">Salin</span>
                                      </div>
                                    </a>
                                  </div>
                                  <style type="text/css">
                                    .spancontent {
                                      padding-bottom: 20px;
                                    }

                                    .copy-content {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }

                                    .head-title {
                                      color: #6EC1E4;
                                      text-align: center;
                                    }
                                  </style>
                                  <script>
                                    function copyText(el) {
                                      var content = jQuery(el).siblings('div.copy-content').html()
                                      var temp = jQuery(" < textarea > ");
                                        jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                        var text = jQuery(el).html()
                                        jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                        var interval = setInterval(function() {
                                          counter++;
                                          if (counter == 2) {
                                            jQuery(el).html(text)
                                            Interval(interval);
                                          }
                                        }, 5000);
                                      }
                                  </script>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </section> -->
              <div class="elementor-element elementor-element-40cfc15a wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="40cfc15a" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>
              <div class="elementor-element elementor-element-220faa5e wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="220faa5e" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="image.default">
                <div class="elementor-widget-container">
                  <img width="351" height="86" src="<?= $asset_theme ?>otherimages/image1.png" class="attachment-full size-full" alt="" loading="lazy" srcset="<?= $asset_theme ?>otherimages/image1.png 351w, <?= $asset_theme ?>otherimages/image1.png 300w" sizes="(max-width: 351px) 100vw, 351px" />
                </div>
              </div>
              <div class="elementor-element elementor-element-28e7f5a9 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="28e7f5a9" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Terima Kasih</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-48604da8 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="48604da8" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInDown&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <h2 class="elementor-heading-title elementor-size-default"><?= $bride ?> & <?= $groom ?></h2>
                </div>
              </div>
              <div class="elementor-element elementor-element-612510b6 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="612510b6" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Keluarga Besar</p>
                </div>
              </div>
              <div class="elementor-element elementor-element-35313fc0 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="35313fc0" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">Bapak <?= $bride_father ?> & Ibu <?= $bride_mother ?> <br> Bapak <?= $groom_father ?> & Ibu <?= $groom_mother ?> </p>
                </div>
              </div>
              <div class="elementor-element elementor-element-4d90b3b wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="4d90b3b" data-element_type="widget" data-widget_type="spacer.default">
                <div class="elementor-widget-container">
                  <div class="elementor-spacer">
                    <div class="elementor-spacer-inner"></div>
                  </div>
                </div>
              </div>


              <br><br><br><br><br><br>
              <div class="elementor-element elementor-element-35313fc0 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="adb4295" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                <div class="elementor-widget-container">
                  <p class="elementor-heading-title elementor-size-default">
                    Made With ♥ by <a href="<?= $base_url ?>">Ara Invitation</a>
                  </p>
                </div>
              </div>


              <!-- <section class="elementor-section elementor-inner-section elementor-element elementor-element-fd9eee1 animated-slow elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no elementor-invisible" data-id="fd9eee1" data-element_type="section" data-settings="{&quot;animation&quot;:&quot;fadeInUp&quot;}">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-8eeec31 wdp-sticky-section-no" data-id="8eeec31" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-728eb80 wdp-sticky-section-no" data-id="728eb80" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-8160aaf wdp-sticky-section-no" data-id="8160aaf" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-b168b68 animated-slow wdp-sticky-section-no elementor-invisible" data-id="b168b68" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-af16753 animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="af16753" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:100}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="<?= $asset ?>" target="_blank">
                            <img width="768" height="589" src="<?= $asset ?>wp-content/uploads/2021/05/Logos-768x589.png" class="attachment-medium_large size-medium_large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/05/Logos-768x589.png 768w, <?= $asset ?>wp-content/uploads/2021/05/Logos-300x230.png 300w, <?= $asset ?>wp-content/uploads/2021/05/Logos.png 779w" sizes="(max-width: 768px) 100vw, 768px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-897c052 elementor-shape-circle animated-slow elementor-grid-0 e-grid-align-center wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-social-icons" data-id="897c052" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:300}" data-widget_type="social-icons.default">
                        <div class="elementor-widget-container">
                          <div class="elementor-social-icons-wrapper elementor-grid">
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-whatsapp elementor-animation-grow elementor-repeater-item-305f5e1" href="https://api.whatsapp.com/send?phone=6283100551000&#038;text=Halo%20Kak,%20saya%20mau%20tanya%20Soal%20Undangan%20Digital%20Unity%20nih.%20Bisa%20dibantu?" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Whatsapp</span>
                                <i class="fab fa-whatsapp"></i>
                              </a>
                            </span>
                            <span class="elementor-grid-item">
                              <a class="elementor-icon elementor-social-icon elementor-social-icon-instagram elementor-animation-grow elementor-repeater-item-00c4ce7" href="https://www.instagram.com/unity.invitation/" target="_blank" rel="noopener">
                                <span class="elementor-screen-only">Instagram</span>
                                <i class="fab fa-instagram"></i>
                              </a>
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-69a01a9 animated-slow wdp-sticky-section-no elementor-invisible" data-id="69a01a9" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeIn&quot;}">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-1eb8edf animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-image" data-id="1eb8edf" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_delay&quot;:500}" data-widget_type="image.default">
                        <div class="elementor-widget-container">
                          <a href="<?= $asset ?>" target="_blank">
                            <img width="1001" height="1001" src="<?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-green.png" class="attachment-full size-full" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-green.png 1001w, <?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-green-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-green-150x150.png 150w, <?= $asset ?>wp-content/uploads/2021/10/Unity-Invitation-QR-Code-green-768x768.png 768w" sizes="(max-width: 1001px) 100vw, 1001px" />
                          </a>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-1ecbd0d wdp-sticky-section-no elementor-widget elementor-widget-heading" data-id="1ecbd0d" data-element_type="widget" data-widget_type="heading.default">
                        <div class="elementor-widget-container">
                          <h2 class="elementor-heading-title elementor-size-default">© 2021 Unity Invitation, <br>All Rights Reserved </h2>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-ac9add5 wdp-sticky-section-no" data-id="ac9add5" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-25c0112 wdp-sticky-section-no" data-id="25c0112" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                  <div class="elementor-column elementor-col-12 elementor-inner-column elementor-element elementor-element-5d4b5ba wdp-sticky-section-no" data-id="5d4b5ba" data-element_type="column">
                    <div class="elementor-widget-wrap"></div>
                  </div>
                </div>
              </section> -->


              <section class="elementor-section elementor-inner-section elementor-element elementor-element-15c36066 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="15c36066" data-element_type="section">
                <div class="elementor-container elementor-column-gap-default">
                  <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-2258fa9e wdp-sticky-section-no" data-id="2258fa9e" data-element_type="column">
                    <div class="elementor-widget-wrap elementor-element-populated">
                      <div class="elementor-element elementor-element-57d53e91 elementor-widget__width-initial elementor-fixed elementor-widget-mobile__width-initial elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-cswd-audio" data-id="57d53e91" data-element_type="widget" data-settings="{&quot;_position&quot;:&quot;fixed&quot;}" data-widget_type="cswd-audio.default">
                        <div class="elementor-widget-container">
                          <script>
                            var settingAutoplay = 'yes';
                            window.settingAutoplay = settingAutoplay === 'disable' ? false : true;
                          </script>
                          <div id="audio-container" class="audio-box">
                            <style>
                              .play_cswd_audio {
                                animation-name: putar;
                                animation-duration: 5000ms;
                                animation-iteration-count: infinite;
                                animation-timing-function: linear;
                              }

                              @keyframes putar {
                                from {
                                  transform: rotate(0deg);
                                }

                                to {
                                  transform: rotate(360deg);
                                }
                              }
                            </style>
                            <audio id="song_" loop>
                              <source src="<?= $asset_theme ?>backsound/The-Bygone-DaysPorco-Rosso.mp3" type="audio/mp3">
                            </audio>
                            <div class="elementor-icon-wrapper" id="unmute-sound" style="display: none;">
                              <div class="elementor-icon">
                                <img src="<?= $asset_theme ?>icons/icon-music-1.png" alt="unity-play-audio" style="width:55px;" class="pause_cswd_audio">
                              </div>
                            </div>
                            <div class="elementor-icon-wrapper" id="mute-sound" style="display: none;">
                              <div class="elementor-icon">
                                <img src="<?= $asset_theme ?>icons/icon-music-1.png" alt="unity-play-audio" style="width:55px;" class="play_cswd_audio">
                              </div>
                            </div>
                          </div>
                          <script>
                            // jQuery("document").ready(function (n) {
                            //   var e = window.settingAutoplay;
                            //   e ? (n("#mute-sound").show(), document.getElementById("song").play()) : n("#unmute-sound").show(),
                            //     n("#audio-container").click(function (u) {
                            //       e ? (n("#mute-sound").hide(), n("#unmute-sound").show(), document.getElementById("song").pause(), (e = !1)) : (n("#unmute-sound").hide(), n("#mute-sound").show(), document.getElementById("song").play(), (e = !0));
                            //     });
                            // });

                            function onLoad() {
                              // Disable Button
                              // document.getElementById("open-invitation").disabled = false;
                              // setTimeout(function() {
                              // 	document.getElementById("open-invitation").innerHTML = '3';
                              // 	setTimeout(function() {
                              // 		document.getElementById("open-invitation").innerHTML = '2';
                              // 		setTimeout(function() {
                              // 			document.getElementById("open-invitation").innerHTML = '1';
                              // 			setTimeout(function() {
                              // 				document.getElementById("open-invitation").innerHTML = 'Buka Undangan';
                              // 			}, 500);
                              // 		}, 500);
                              // 	}, 500);
                              // }, 500);

                              // Setting Click
                              document.getElementById("unmute-sound").onclick = function() {
                                document.getElementById("unmute-sound").style.display = 'none';
                                document.getElementById("mute-sound").style.display = 'block';
                                document.getElementById("song_").play();
                              };
                              document.getElementById("mute-sound").onclick = function() {
                                document.getElementById("mute-sound").style.display = 'none';
                                document.getElementById("unmute-sound").style.display = 'block';
                                document.getElementById("song_").pause();
                              };
                              
                            }
                          </script>
                        </div>
                      </div>
                      <div class="elementor-element elementor-element-4a3a5d04 wdp-sticky-section-no elementor-widget elementor-widget-CSWeding_modal_popup" data-id="4a3a5d04" data-element_type="widget" data-settings="{&quot;exit_animation&quot;:&quot;fadeIn&quot;}" data-widget_type="CSWeding_modal_popup.default">
                        <div class="elementor-widget-container">
                          <style>
                            .elementor-image>img {
                              display: initial !important;
                            }
                          </style>
                          <div class="modalx animated none" data-sampul='<?= $asset_theme ?>photos/Cover1.jpg'>
                            <div class="overlayy"></div>
                            <div class="content-modalx">
                              <div class="info_modalx">
                                <div class="elementor-image img"></div>
                                <div class="text_tambahan">PERNIKAHAN</div>
                                <div class="wdp-mempelai"><?= $bride ?> & <?= $groom ?></div>
                                <div class="wdp-dear">Yth. Bapak/Ibu/Saudara/i</div>
                                <div class="wdp-name"><?= $to ?></div>
                                <div class="wdp-text">Tanpa mengurangi rasa hormat, <br> Kami mengundang Bapak/Ibu/Saudara/i <br>untuk hadir di acara pernikahan kami. </div>
                                <div class="wdp-button-wrapper">
                                  <button class="elementor-button"> Buka Undangan </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <script>
                            const sampul = jQuery('.modalx').data('sampul');
                            jQuery('.modalx').css('background-image', 'url(' + sampul + ')');
                            jQuery('body').css('overflow', 'hidden');
                            jQuery('.wdp-button-wrapper button').on('click', function() {
                              jQuery('.modalx').removeClass('none');
                              jQuery('.modalx').addClass('fadeIn reverse');
                              setTimeout(function() {
                                jQuery('.modalx').addClass('removeModals');
                              }, 1600);
                              jQuery('body').css('overflow', 'auto');
                              document.getElementById("song_").play();
                            });
                          </script>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!-- <div data-elementor-type="popup" data-elementor-id="51207" class="elementor elementor-51207 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;fadeInUp&quot;,&quot;exit_animation&quot;:&quot;fadeInDown&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:2,&quot;sizes&quot;:[]},&quot;entrance_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;exit_animation_mobile&quot;:&quot;slideInRight&quot;,&quot;prevent_scroll&quot;:&quot;yes&quot;,&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-7c3c6a3 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="7c3c6a3" data-element_type="section">
          <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-84e6c78 wdp-sticky-section-no" data-id="84e6c78" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-widget-wrap elementor-element-populated">
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-038bcbf elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="038bcbf" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-6ca425d wdp-sticky-section-no" data-id="6ca425d" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-element elementor-element-5516c67 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="5516c67" data-element_type="widget" data-widget_type="spacer.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-spacer">
                              <div class="elementor-spacer-inner"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="elementor-element elementor-element-f3fa899 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="f3fa899" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;zoomIn&quot;}" data-widget_type="heading.default">
                  <div class="elementor-widget-container">
                    <p class="elementor-heading-title elementor-size-default">Do'a restu keluarga, sahabat, serta rekan-rekan semua di pernikahan kami sudah sangat cukup sebagai hadiah, tetapi jika memberi merupakan tanda kasih, kami dengan senang hati menerimanya dan tentunya semakin melengkapi kebahagiaan kami. </p>
                  </div>
                </div>
                <div class="elementor-element elementor-element-b98e0e3 wdp-sticky-section-no elementor-widget elementor-widget-spacer" data-id="b98e0e3" data-element_type="widget" data-widget_type="spacer.default">
                  <div class="elementor-widget-container">
                    <div class="elementor-spacer">
                      <div class="elementor-spacer-inner"></div>
                    </div>
                  </div>
                </div>
                <div class="elementor-element elementor-element-0da7688 wdp-sticky-section-no elementor-widget elementor-widget-jet-tabs" data-id="0da7688" data-element_type="widget" data-settings="{&quot;tabs_position&quot;:&quot;top&quot;}" data-widget_type="jet-tabs.default">
                  <div class="elementor-widget-container">
                    <div class="jet-tabs jet-tabs-position-top jet-tabs-fall-perspective-effect " data-settings="{&quot;activeIndex&quot;:0,&quot;event&quot;:&quot;click&quot;,&quot;autoSwitch&quot;:false,&quot;autoSwitchDelay&quot;:3000,&quot;ajaxTemplate&quot;:false,&quot;tabsPosition&quot;:&quot;top&quot;,&quot;switchScrolling&quot;:false}" role="tablist">
                      <div class="jet-tabs__control-wrapper">
                        <div id="jet-tabs-control-1431" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor active-tab" data-tab="1" tabindex="1431" role="tab" aria-controls="jet-tabs-content-1431" aria-expanded="true" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="<?= $asset ?>wp-content/uploads/2021/08/Logo-BCA.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1432" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="2" tabindex="1432" role="tab" aria-controls="jet-tabs-content-1432" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="<?= $asset ?>wp-content/uploads/2021/07/Logo-BNI-Bank-Negara-Indonesia-46-Vector-.png" alt="">
                          </div>
                        </div>
                        <div id="jet-tabs-control-1433" class="jet-tabs__control jet-tabs__control-icon-left elementor-menu-anchor " data-tab="3" tabindex="1433" role="tab" aria-controls="jet-tabs-content-1433" aria-expanded="false" data-template-id="51208">
                          <div class="jet-tabs__control-inner">
                            <img class="jet-tabs__label-image" src="<?= $asset ?>wp-content/uploads/2021/08/Logo-Mandiri.png" alt="">
                          </div>
                        </div>
                      </div>
                      <div class="jet-tabs__content-wrapper">
                        <div id="jet-tabs-content-1431" class="jet-tabs__content active-content" data-tab="1" role="tabpanel" aria-hidden="false" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png 486w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1432" class="jet-tabs__content " data-tab="2" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png 486w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                        <div id="jet-tabs-content-1433" class="jet-tabs__content " data-tab="3" role="tabpanel" aria-hidden="true" data-template-id="51208">
                          <div data-elementor-type="section" data-elementor-id="51208" class="elementor elementor-51208 elementor-location-popup">
                            <div class="elementor-section-wrap">
                              <section class="elementor-section elementor-top-section elementor-element elementor-element-08fd163 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="08fd163" data-element_type="section">
                                <div class="elementor-container elementor-column-gap-default">
                                  <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1565c5d wdp-sticky-section-no" data-id="1565c5d" data-element_type="column">
                                    <div class="elementor-widget-wrap elementor-element-populated">
                                      <div class="elementor-element elementor-element-3f4d4e9 wdp-sticky-section-no elementor-widget elementor-widget-image" data-id="3f4d4e9" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeIn&quot;}" data-widget_type="image.default">
                                        <div class="elementor-widget-container">
                                          <img width="486" height="486" src="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png" class="attachment-large size-large" alt="" loading="lazy" srcset="<?= $asset ?>wp-content/uploads/2021/04/Dana-Unity.png 486w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-300x300.png 300w, <?= $asset ?>wp-content/uploads/2021/04/Dana-Unity-150x150.png 150w" sizes="(max-width: 486px) 100vw, 486px" />
                                        </div>
                                      </div>
                                      <div class="elementor-element elementor-element-3541e1a elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="3541e1a" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                                        <div class="elementor-widget-container">
                                          <div class="elementor-image img"></div>
                                          <div class="head-title">a/n Ardito Prambanan</div>
                                          <div class="elementor-button-wrapper">
                                            <div class="copy-content spancontent">1234567891011</div>
                                            <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                              <div class="elementor-button-content-wrapper">
                                                <span class="elementor-button-icon elementor-align-icon-left">
                                                  <i aria-hidden="true" class="far fa-copy"></i>
                                                </span>
                                                <span class="elementor-button-text">Salin</span>
                                              </div>
                                            </a>
                                          </div>
                                          <style type="text/css">
                                            .spancontent {
                                              padding-bottom: 20px;
                                            }

                                            .copy-content {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }

                                            .head-title {
                                              color: #6EC1E4;
                                              text-align: center;
                                            }
                                          </style>
                                          <script>
                                            function copyText(el) {
                                              var content = jQuery(el).siblings('div.copy-content').html()
                                              var temp = jQuery(" < textarea > ");
                                                jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                                var text = jQuery(el).html()
                                                jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                                var interval = setInterval(function() {
                                                  counter++;
                                                  if (counter == 2) {
                                                    jQuery(el).html(text)
                                                    Interval(interval);
                                                  }
                                                }, 5000);
                                              }
                                          </script>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </section>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-ef95a0b elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="ef95a0b" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-3889ddb wdp-sticky-section-no" data-id="3889ddb" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-56feb50 wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-heading" data-id="56feb50" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeInUp&quot;}" data-widget_type="heading.default">
                          <div class="elementor-widget-container">
                            <h2 class="elementor-heading-title elementor-size-default">Atau kirim hadiah fisik ke</h2>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="elementor-section elementor-inner-section elementor-element elementor-element-711e283 elementor-section-boxed elementor-section-height-default elementor-section-height-default wdp-sticky-section-no" data-id="711e283" data-element_type="section">
                  <div class="elementor-container elementor-column-gap-default">
                    <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fda62bb wdp-sticky-section-no" data-id="fda62bb" data-element_type="column">
                      <div class="elementor-widget-wrap elementor-element-populated">
                        <div class="elementor-element elementor-element-8014281 elementor-view-default wdp-sticky-section-no elementor-widget elementor-widget-icon" data-id="8014281" data-element_type="widget" data-settings="{&quot;_animation_mobile&quot;:&quot;fadeInUp&quot;}" data-widget_type="icon.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-icon-wrapper">
                              <div class="elementor-icon">
                                <i aria-hidden="true" class="iconunityinv icon-unityinvunity-gift"></i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-element elementor-element-856da5f elementor-align-center animated-slow wdp-sticky-section-no elementor-invisible elementor-widget elementor-widget-weddingpress-copy-text" data-id="856da5f" data-element_type="widget" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;,&quot;_animation_mobile&quot;:&quot;none&quot;}" data-widget_type="weddingpress-copy-text.default">
                          <div class="elementor-widget-container">
                            <div class="elementor-image img"></div>
                            <div class="head-title">a/n Bimo Bramantyo</div>
                            <div class="elementor-button-wrapper">
                              <div class="copy-content spancontent">Jl. Profesor DR. HR Boenyamin No.708, Sumampir Wetan, Pabuaran, Kec. Purwokerto Utara, Kabupaten Banyumas, Jawa Tengah 53122</div>
                              <a style="cursor:pointer;" onclick="copyText(this)" data-message="berhasil disalin" class="elementor-button" role="button">
                                <div class="elementor-button-content-wrapper">
                                  <span class="elementor-button-icon elementor-align-icon-left">
                                    <i aria-hidden="true" class="far fa-copy"></i>
                                  </span>
                                  <span class="elementor-button-text">Salin</span>
                                </div>
                              </a>
                            </div>
                            <style type="text/css">
                              .spancontent {
                                padding-bottom: 20px;
                              }

                              .copy-content {
                                color: #6EC1E4;
                                text-align: center;
                              }

                              .head-title {
                                color: #6EC1E4;
                                text-align: center;
                              }
                            </style>
                            <script>
                              function copyText(el) {
                                var content = jQuery(el).siblings('div.copy-content').html()
                                var temp = jQuery(" < textarea > ");
                                  jQuery("body").append(temp); temp.val(content.replace(/ < br ? \/?>/g, "\n")).select(); document.execCommand("copy"); temp.remove();
                                  var text = jQuery(el).html()
                                  jQuery(el).html(jQuery(el).data('message')) var counter = 0;
                                  var interval = setInterval(function() {
                                    counter++;
                                    if (counter == 2) {
                                      jQuery(el).html(text)
                                      Interval(interval);
                                    }
                                  }, 5000);
                                }
                            </script>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    <div data-elementor-type="popup" data-elementor-id="31584" class="elementor elementor-31584 elementor-location-popup" data-elementor-settings="{&quot;entrance_animation&quot;:&quot;slideInLeft&quot;,&quot;exit_animation&quot;:&quot;slideInRight&quot;,&quot;entrance_animation_duration&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:1.5,&quot;sizes&quot;:[]},&quot;triggers&quot;:[],&quot;timing&quot;:[]}">
      <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-6e0c221 elementor-section-full_width elementor-section-height-min-height elementor-section-height-default elementor-section-items-middle wdp-sticky-section-no" data-id="6e0c221" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
          <div class="elementor-container elementor-column-gap-no">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f275f5f wdp-sticky-section-no" data-id="f275f5f" data-element_type="column">
              <div class="elementor-widget-wrap elementor-element-populated">
                <div class="elementor-element elementor-element-59c300e pp-advanced-menu--stretch wdp-sticky-section-no elementor-widget elementor-widget-pp-advanced-menu" data-id="59c300e" data-element_type="widget" data-settings="{&quot;layout&quot;:&quot;vertical&quot;,&quot;show_submenu_on&quot;:&quot;click&quot;,&quot;full_width&quot;:&quot;stretch&quot;,&quot;expanded_submenu&quot;:&quot;no&quot;,&quot;submenu_icon&quot;:{&quot;value&quot;:&quot;&lt;i class=\&quot;fas fa-caret-down\&quot;&gt;&lt;\/i&gt;&quot;,&quot;library&quot;:&quot;fa-solid&quot;},&quot;menu_type&quot;:&quot;default&quot;,&quot;toggle&quot;:&quot;icon&quot;,&quot;toggle_icon_type&quot;:&quot;hamburger&quot;}" data-widget_type="pp-advanced-menu.default">
                  <div class="elementor-widget-container">
                    <div class="pp-advanced-menu-main-wrapper pp-advanced-menu__align-center pp-advanced-menu--dropdown-tablet pp-advanced-menu--type-default pp-advanced-menu__text-align-center pp-advanced-menu--toggle pp-advanced-menu--icon">
                      <nav id="pp-menu-59c300e" class="pp-advanced-menu--main pp-advanced-menu__container pp-advanced-menu--layout-vertical pp--pointer-none e--animation-fade" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="<?= $asset ?>katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="<?= $asset ?>katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="<?= $asset ?>katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="<?= $asset ?>filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                      <div class="pp-menu-toggle pp-menu-toggle-on-tablet">
                        <div class="pp-hamburger">
                          <div class="pp-hamburger-box">
                            <div class="pp-hamburger-inner"></div>
                          </div>
                        </div>
                      </div>
                      <nav class="pp-advanced-menu--dropdown pp-menu-style-toggle pp-advanced-menu__container pp-menu-59c300e pp-menu-default" data-settings="{&quot;menu_id&quot;:&quot;59c300e&quot;,&quot;breakpoint&quot;:&quot;tablet&quot;,&quot;full_width&quot;:true}">
                        <ul id="menu-primary-menu-1" class="pp-advanced-menu sm-vertical">
                          <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10572">
                            <a href="#" class="pp-menu-item pp-menu-item-anchor">Produk</a>
                            <ul class="sub-menu pp-advanced-menu--dropdown">
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34265">
                                <a href="<?= $asset ?>katalog-web/" class="pp-sub-item">Undangan Website</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34306">
                                <a href="<?= $asset ?>katalog-statik/" class="pp-sub-item">Undangan Statik</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-34418">
                                <a href="<?= $asset ?>katalog-video/" class="pp-sub-item">Undangan Video</a>
                              </li>
                              <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52726">
                                <a href="<?= $asset ?>filter-instagram/" class="pp-sub-item">Filter Instagram</a>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </nav>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div> -->


    <!-- Repost -->
    <div class="modal fade" id="modal-preview">
      <div class="modal-dialog modal-xl">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">
              Post on Story Instagram
            </h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <!-- <div class="mx-auto bg-info" id="capture" style="height: 500px; width: 300px; background: url({{url('assets/img/regar-ervan.jpg')}});">
                Cover Wedding
              </div> -->

              <div style="background-image: url({{url('assets/img/regar-ervan_background.jpg')}});background-size:cover; width: 300px; height: 500px; border: 0px;" id="capture">
                
                <div style="background: rgba(0,0,0,0.5); width: 300px; height: 500px">
                  <div class="white-text text-center">
                    <div class="card-block">


                      <img src="{{url('assets/img/regar-ervan_photo.png')}}" style="height: 150px; margin-top: 40px; left:75px;">
                      
                      <h3 style="width: 300px; top: 200px; left:100px; margin-top: 15px; margin-bottom: 0; color: white; font-family: Times New Roman">Regar & Ervan</h3>

                      <small style="color: white;">30 . 10 . 2022</small>

                      <div style="width: 300px; margin-top: 30px; bottom: 80px">
                        <section>
                          <div class="container">
                            <div class="row d-flex align-items-center">
                              <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
                              <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10">
                                <div class="bg-white p-3" style="border-left: .25rem solid #a34e78; border-radius: 0 5px 5px 0">
                                  <blockquote class="blockquote p-0 m-0" style="border-left: 0">
                                    <p style="font-size: 10px" class="text-left">
                                    Semoga Lancar sampai hari H
                                    </p>
                                  </blockquote>
                                  <figcaption class="blockquote-footer mt-2 mb-0 font-italic text-left" style="font-size: 10px">
                                    Mas Lembu
                                  </figcaption>
                                </div>
                              </div>
                              <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
                            </div>
                          </div>
                        </section>

                        <p class="text-center" style="width: 300px; font-size: 8px; font-family: Times New Roman; color: #bcbcbc">arainvitation.online</p>
                        
                      </div>


                    </div>
                  </div>
                </div>

                <!-- <img src="{{url('assets/img/regar-ervan.jpg')}}" height="500px" width="300px" style="border: 2px solid black"/> -->
              </div>
            </div>
          </div>
          <div class="modal-footer text-right">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" id="btnPost" onclick="post()">Post</button>
          </div>
        </div>
      </div>
    </div>


    <link rel='stylesheet' id='ep-countdown-css' href='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/css/ep-countdown.css?ver=6.0.10' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-51208-css' href='<?= $asset ?>wp-content/uploads/elementor/css/post-51208.css?ver=1655050137' type='text/css' media='all' />
    <link rel='stylesheet' id='e-animations-css' href='<?= $asset ?>wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.6.4' type='text/css' media='all' />
    <script type='text/javascript' id='WEDKU_SCRP-js-extra'>
      /* 
																																								<![CDATA[ */
      var ceper = {
        // "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/cswd/assets/cswp.script.js?ver=1.1.18' id='WEDKU_SCRP-js'></script>
    <script type='text/javascript' id='wdp_js_script-js-extra'>
      /* 
																																								<![CDATA[ */
      var WDP_WP = {
        // "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "wdpNonce": "da95284813",
        "jpages": "true",
        "jPagesNum": "5",
        "textCounter": "true",
        "textCounterNum": "500",
        "widthWrap": "",
        "autoLoad": "true",
        "thanksComment": "Terima kasih atas ucapan & doanya!",
        "thanksReplyComment": "Terima kasih atas balasannya!",
        "duplicateComment": "You might have left one of the fields blank, or duplicate comments",
        "insertImage": "Insert image",
        "insertVideo": "Insert video",
        "insertLink": "Insert link",
        "checkVideo": "Check video",
        "accept": "Accept",
        "cancel": "Cancel",
        "reply": "Balas",
        "textWriteComment": "Tulis Ucapan & Doa",
        "classPopularComment": "wdp-popular-comment",
        "textUrlImage": "Url image",
        "textUrlVideo": "Url video youtube or vimeo",
        "textUrlLink": "Url link",
        "textToDisplay": "Text to display",
        "textCharacteresMin": "Minimal 2 karakter",
        "textNavNext": "Selanjutnya",
        "textNavPrev": "Sebelumnya",
        "textMsgDeleteComment": "Do you want delete this comment?",
        "textLoadMore": "Load more",
        "textLikes": "Likes"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/wdp_script.js?ver=2.7.6' id='wdp_js_script-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.jPages.min.js?ver=0.7' id='wdp_jPages-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.textareaCounter.js?ver=2.0' id='wdp_textCounter-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/jquery.placeholder.min.js?ver=2.0.7' id='wdp_placeholder-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/addons/comment-kit//js/libs/autosize.min.js?ver=1.14' id='wdp_autosize-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp-swiper.min.js' id='wdp-swiper-js-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/qr-code.js' id='weddingpress-qr-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp-horizontal.js' id='wdp-horizontal-js-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/exad-scripts.min.js?ver=2.8.8' id='exad-main-script-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/loftloader/assets/js/loftloader.min.js?ver=2022022501' id='loftloader-lite-front-main-js'></script>
    <script type='text/javascript' id='bdt-uikit-js-extra'>
      /* 
																																								<![CDATA[ */
      var element_pack_ajax_login_config = {
        // "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "language": "en",
        "loadingmessage": "Sending user info, please wait...",
        "unknownerror": "Unknown error, make sure access is correct!"
      };
      var ElementPackConfig = {
        // "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "nonce": "040d1b5c31",
        "data_table": {
          "language": {
            "lengthMenu": "Show _MENU_ Entries",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "search": "Search :",
            "paginate": {
              "previous": "Previous",
              "next": "Next"
            }
          }
        },
        "contact_form": {
          "sending_msg": "Sending message please wait...",
          "captcha_nd": "Invisible captcha not defined!",
          "captcha_nr": "Could not get invisible captcha response!"
        },
        "mailchimp": {
          "subscribing": "Subscribing you please wait..."
        },
        "elements_data": {
          "sections": [],
          "columns": [],
          "widgets": []
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/bdt-uikit.min.js?ver=3.13.1' id='bdt-uikit-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.6.4' id='elementor-webpack-runtime-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.6.4' id='elementor-frontend-modules-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-includes/js/jquery/ui/core.min.js?ver=1.13.1' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.6.4' id='share-link-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.9.0' id='elementor-dialog-js'></script>
    <script type='text/javascript' id='elementor-frontend-js-before'>
      var elementorFrontendConfig = {
        "environmentMode": {
          "edit": false,
          "wpPreview": false,
          "isScriptDebug": false
        },
        "i18n": {
          "shareOnFacebook": "Share on Facebook",
          "shareOnTwitter": "Share on Twitter",
          "pinIt": "Pin it",
          "download": "Download",
          "downloadImage": "Download image",
          "fullscreen": "Fullscreen",
          "zoom": "Zoom",
          "share": "Share",
          "playVideo": "Play Video",
          "previous": "Previous",
          "next": "Next",
          "close": "Close"
        },
        "is_rtl": false,
        "breakpoints": {
          "xs": 0,
          "sm": 480,
          "md": 768,
          "lg": 1025,
          "xl": 1440,
          "xxl": 1600
        },
        "responsive": {
          "breakpoints": {
            "mobile": {
              "label": "Mobile",
              "value": 767,
              "default_value": 767,
              "direction": "max",
              "is_enabled": true
            },
            "mobile_extra": {
              "label": "Mobile Extra",
              "value": 880,
              "default_value": 880,
              "direction": "max",
              "is_enabled": false
            },
            "tablet": {
              "label": "Tablet",
              "value": 1024,
              "default_value": 1024,
              "direction": "max",
              "is_enabled": true
            },
            "tablet_extra": {
              "label": "Tablet Extra",
              "value": 1200,
              "default_value": 1200,
              "direction": "max",
              "is_enabled": false
            },
            "laptop": {
              "label": "Laptop",
              "value": 1366,
              "default_value": 1366,
              "direction": "max",
              "is_enabled": false
            },
            "widescreen": {
              "label": "Widescreen",
              "value": 2400,
              "default_value": 2400,
              "direction": "min",
              "is_enabled": false
            }
          }
        },
        "version": "3.6.4",
        "is_static": false,
        "experimentalFeatures": {
          "e_dom_optimization": true,
          "a11y_improvements": true,
          "e_import_export": true,
          "e_hidden_wordpress_widgets": true,
          "theme_builder_v2": true,
          "landing-pages": true,
          "elements-color-picker": true,
          "favorite-widgets": true,
          "admin-top-bar": true,
          "form-submissions": true
        },
        "urls": {
          // "assets": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor\/assets\/"
        },
        "settings": {
          "page": [],
          "editorPreferences": []
        },
        "kit": {
          "viewport_tablet": 1024,
          "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
          "global_image_lightbox": "yes"
        },
        "post": {
          "id": 13245,
          "title": "Unity%20Silver%201",
          "excerpt": "",
          // "featuredImage": "https:\/\/unityinvitation.com\/wp-content\/uploads\/2021\/06\/silver1-cover-invitation.jpg"
        }
      };
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.6.4' id='elementor-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/modules/ep-countdown.min.js?ver=6.0.10' id='ep-countdown-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/slick/slick.min.js?ver=2.8.2' id='pp-slick-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend-timeline.min.js?ver=2.8.2' id='pp-timeline-js'></script>
    <script type='text/javascript' id='powerpack-frontend-js-extra'>
      /* 
																																								<![CDATA[ */
      var ppLogin = {
        "empty_username": "Enter a username or email address.",
        "empty_password": "Enter password.",
        "empty_password_1": "Enter a password.",
        "empty_password_2": "Re-enter password.",
        "empty_recaptcha": "Please check the captcha to verify you are not a robot.",
        "email_sent": "A password reset email has been sent to the email address for your account, but may take several minutes to show up in your inbox. Please wait at least 10 minutes before attempting another reset.",
        "reset_success": "Your password has been reset successfully.",
        // "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      var ppRegistration = {
        "invalid_username": "This username is invalid because it uses illegal characters. Please enter a valid username.",
        "username_exists": "This username is already registered. Please choose another one.",
        "empty_email": "Please type your email address.",
        "invalid_email": "The email address isn\u2019t correct!",
        "email_exists": "The email is already registered, please choose another one.",
        "password": "Password must not contain the character \"\\\\\"",
        "password_length": "Your password should be at least 8 characters long.",
        "password_mismatch": "Password does not match.",
        "invalid_url": "URL seems to be invalid.",
        "recaptcha_php_ver": "reCAPTCHA API requires PHP version 5.3 or above.",
        "recaptcha_missing_key": "Your reCAPTCHA Site or Secret Key is missing!",
        "show_password": "Show password",
        "hide_password": "Hide password",
        // "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend.min.js?ver=2.8.2' id='powerpack-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/lib/smartmenu/jquery-smartmenu.js?ver=1.1.1' id='jquery-smartmenu-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/powerpack-elements/assets/js/min/frontend-advanced-menu.min.js?ver=2.8.2' id='pp-advanced-menu-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/bdthemes-element-pack/assets/js/common/helper.min.js?ver=6.0.10' id='element-pack-helper-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/webpack-pro.runtime.min.js?ver=3.5.0' id='elementor-pro-webpack-runtime-js'></script>
    <script type='text/javascript' id='elementor-pro-frontend-js-before'>
      var ElementorProFrontendConfig = {
        // "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "nonce": "21391c188c",
        "urls": {
          // "assets": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/assets\/",
          // "rest": "https:\/\/unityinvitation.com\/wp-json\/"
        },
        "i18n": {
          "toc_no_headings_found": "No headings were found on this page."
        },
        "shareButtonsNetworks": {
          "facebook": {
            "title": "Facebook",
            "has_counter": true
          },
          "twitter": {
            "title": "Twitter"
          },
          "linkedin": {
            "title": "LinkedIn",
            "has_counter": true
          },
          "pinterest": {
            "title": "Pinterest",
            "has_counter": true
          },
          "reddit": {
            "title": "Reddit",
            "has_counter": true
          },
          "vk": {
            "title": "VK",
            "has_counter": true
          },
          "odnoklassniki": {
            "title": "OK",
            "has_counter": true
          },
          "tumblr": {
            "title": "Tumblr"
          },
          "digg": {
            "title": "Digg"
          },
          "skype": {
            "title": "Skype"
          },
          "stumbleupon": {
            "title": "StumbleUpon",
            "has_counter": true
          },
          "mix": {
            "title": "Mix"
          },
          "telegram": {
            "title": "Telegram"
          },
          "pocket": {
            "title": "Pocket",
            "has_counter": true
          },
          "xing": {
            "title": "XING",
            "has_counter": true
          },
          "whatsapp": {
            "title": "WhatsApp"
          },
          "email": {
            "title": "Email"
          },
          "print": {
            "title": "Print"
          }
        },
        "facebook_sdk": {
          "lang": "en_US",
          "app_id": ""
        },
        "lottie": {
          // "defaultAnimationUrl": "https:\/\/unityinvitation.com\/wp-content\/plugins\/elementor-pro\/modules\/lottie\/assets\/animations\/default.json"
        }
      };
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/frontend.min.js?ver=3.5.0' id='elementor-pro-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/js/preloaded-elements-handlers.min.js?ver=3.5.0' id='pro-preloaded-elements-handlers-js'></script>
    <script type='text/javascript' id='jet-elements-js-extra'>
      /* 
																																								<![CDATA[ */
      var jetElements = {
        // "ajaxUrl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "isMobile": "false",
        // "templateApiUrl": "https:\/\/unityinvitation.com\/wp-json\/jet-elements-api\/v1\/elementor-template",
        "devMode": "false",
        "messages": {
          "invalidMail": "Please specify a valid e-mail"
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/jet-elements/assets/js/jet-elements.min.js?ver=2.6.4' id='jet-elements-js'></script>
    <script type='text/javascript' id='jet-tabs-frontend-js-extra'>
      /* 
																																								<![CDATA[ */
      var JetTabsSettings = {
        // "ajaxurl": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        "isMobile": "false",
        // "templateApiUrl": "https:\/\/unityinvitation.com\/wp-json\/jet-tabs-api\/v1\/elementor-template",
        "devMode": "false"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/jet-tabs/assets/js/jet-tabs-frontend.min.js?ver=2.1.17' id='jet-tabs-frontend-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.6.4' id='preloaded-modules-js'></script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/elementor-pro/assets/lib/sticky/jquery.sticky.min.js?ver=3.5.0' id='e-sticky-js'></script>
    <script type='text/javascript' id='weddingpress-wdp-js-extra'>
      /* 
																																								<![CDATA[ */
      var cevar = {
        // "ajax_url": "https:\/\/unityinvitation.com\/wp-admin\/admin-ajax.php",
        // "plugin_url": "https:\/\/unityinvitation.com\/wp-content\/plugins\/weddingpress\/"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='<?= $asset ?>wp-content/plugins/weddingpress/assets/js/wdp.min.js?ver=2.8.8' id='weddingpress-wdp-js'></script>
    

    <!-- COUNTDOWN -->
    <script type="text/javascript" src="<?= $asset ?>custom/plugins/downCount/jquery.downCount.js"></script> 
    <script class="source" type="text/javascript">
      $('.countdown').downCount({
        // Bulan/Tanggal/Tahun Jam/Menit/Detik
        date: '11/23/2022 19:00:00',
        offset: +7
      });
    </script>
    <script>
      function openInvitation() {
        document.getElementById("unmute-sound").style.display = 'none';
        document.getElementById("mute-sound").style.display = 'block';
      }

      function saveSAP() {
        let name = $('#name').val();
        let description = $('#description').val();
        let confirmation_of_attendance = $('#confirmation_of_attendance').val();

        if (name == '') { return false }
        if (description == '') { return false }
        
            $.ajax({
                type: 'POST',
                url: '<?= $url_save_sap ?>',
                data: {
                    'uri_couple': '<?= $uri_couple ?>',
                    'name': name,
                    'description': description,
                    'confirmation_of_attendance': confirmation_of_attendance,
                },
                success: function(response) {
            $('#name').val('');
            $('#description').val('');
            $('#confirmation_of_attendance').val('').change();
            
            $('#wdp-comment-status-13245').css('display', 'block');
            setTimeout(() => {
              $('#wdp-comment-status-13245').css('display', 'none');
            }, 3000);

            getSAP({page: 1});
          }
        });
      }

      function getSAP(params) {
            $.ajax({
                type: 'POST',
                url: '<?= $url_get_sap ?>',
                data: {
                    'uri_couple': '<?= $uri_couple ?>',
            'page': params.page
                },
                success: function(response) {
            let res = JSON.parse(response);
            let sap = '';
            let page = res.page.length > 1 ? `<a class="jp-previous ${params.page == 1 ? 'jp-disabled' : ''}" ${params.page == 1 ? '' : `onclick="getSAP({page: ${params.page-1}})"`}>← Sebelumnya</a>` : '';

            $.each(res.sap, function(k, v) {
              if (v.page == params.page) {
                sap += `<li class="comment even thread-even depth-1 wdp-item-comment animated fadeIn" id="wdp-item-comment-14904" data-likes="0">
                  <div id="wdp-comment-14904" class="wdp-comment wdp-clearfix">
                    <div class="wdp-comment-avatar">
                      <img src="https://unityinvitation.com/wp-content/uploads/2022/03/Icon-Unity-Comment.png">
                    </div><!--.wdp-comment-avatar-->
                    <div class="wdp-comment-content">
                      <div class="wdp-comment-info">
                        <a class="wdp-commenter-name" title="${v.name}">${v.name}</a>
                        <span class="wdp-post-author"><i class="fas fa-check-circle"></i> ${v.confirmation_of_attendance}</span>
                        <br>
                        <span class="wdp-comment-time">
                          <i class="far fa-clock"></i>
                          ${v.human_created_at}
                        </span>

                        <button class="btn btn-secondary" onclick="preview()">Preview</button>
                      </div><!--.wdp-comment-info-->
                      <div class="wdp-comment-text">
                        <p>${v.description}</p>
                      </div><!--.wdp-comment-text-->
                      <div class="wdp-comment-actions" style="display: block;">
                      </div><!--.wdp-comment-actions-->
                    </div><!--.wdp-comment-content-->
                  </div><!--.wdp-comment-->
                  <!--</li>-->
                  </li><!-- #comment-## -->`
              }
            });

            if (res.page.length > 1) {
              $.each(res.page, function(k, v) {
                page += `<a class="" ${params.page == v ? '' : `onclick="getSAP({page: ${v}})"`}>${params.page == v ? `<b><u>${v}</u></b>` : v}</a>`;
              })
            }
            page += res.page.length > 1 ? `<a class="jp-next ${params.page == res.page.length ? 'jp-disabled' : ''}" ${params.page == res.page.length ? '' : `onclick="getSAP({page: ${params.page+1}})"`}>Selanjutnya →</a>` : '';
            
            $('#wdp-container-comment-13245').html(sap);
            $('#page').html(page);
          }
        });
      }

      function checkDarkmode() {
        let isDarkMode = window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches;
        if (isDarkMode) {
          Swal.fire("Warning", "Mohon maaf, undangan tidak support Darkmode, karena akan berpengaruh pada tampilan undangan. Silahkan ubah ke Lightmode. Terimakasih :)", "warning");
        } 
      }

      $(document).ready(function() {
        checkDarkmode();
        getSAP({page: 1});
      })
    </script>

    <!-- Sweetalert -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.all.min.js"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@10.10.1/dist/sweetalert2.min.css'>
    <style>
      .swal2-container {
        display: -webkit-box;
        display: flex;
        position: fixed;
        z-index: 300000;
      }
    </style>


    <div class="pafe-break-point" data-pafe-break-point-md="768" data-pafe-break-point-lg="1025" data-pafe-ajax-url="<?= $asset ?>wp-admin/admin-ajax.php"></div>
    <div data-pafe-form-builder-tinymce-upload="<?= $asset ?>wp-content/plugins/piotnet-addons-for-elementor-pro/inc/tinymce/tinymce-upload.php"></div>


    <!-- Repost -->
    <script type="text/javascript" src="https://lycon.co.id/template/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript" src="https://lycon.co.id/template/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
    <script type="text/javascript" src="https://lycon.co.id/template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://lycon.co.id/template/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://lycon.co.id/template/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script>
      function preview() {
        $('#modal-preview').modal('show')
      }

      function post() {
        $('#btnPost').text('Capturing...')
    
        const captureElement = document.querySelector('#capture')
        html2canvas(captureElement)
          .then(canvas => {
            canvas.style.display = 'none'
            document.body.appendChild(canvas)
            return canvas
          })
          .then(canvas => {
            const image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
            const a = document.createElement('a')
            a.setAttribute('download', 'my-image.png')
            a.setAttribute('href', image)
            a.click()
            canvas.remove()

            $('#btnPost').text('Captured')
          })
      }
    </script>
  </body>
</html>
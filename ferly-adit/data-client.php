<?php
	// Pengantin Wanita
    $bride = 'Ferly';
	$bride_firstname = 'Ferly';
	$bride_middlename = 'Dhia Mauldy';
	$bride_lastname = 'Khairunnisa';
	$bride_father = 'Bapak Rifangi';
	$bride_mother = 'Ibu Siti Mariana';
	$bride_child_position = 'Putri Pertama';
	$bride_ig = 'https://instagram.com/ferlydmk';

	// Pengantin Pria
	$groom = 'Adit';
	$groom_firstname = 'Aditya';
	$groom_middlename = 'Putra';
	$groom_lastname = 'Pratama';
	$groom_father = 'Bapak Achmad Hariyanto';
	$groom_mother = 'Ibu Ruliati';
	$groom_child_position = 'Putra Pertama';
	$groom_ig = 'https://instagram.com/aditheism';

	// Akad
	$akad_fullday = 'Desember 24, 2023';
	$akad_day = "Minggu";
	$akad_date = '24';
	$akad_date_show_duration = '1000';
	$akad_month = 'Desember';
	$akad_year = '2023';
	$akad_hour = '08:00 WIB';
	$akad_venue = 'Kediaman Mempelai Wanita';
	$akad_venue_address = 'Desa Winong RT 001/ RW 001 no. 43, Kec. Kedungwaru, Kab. Tulungagung, Jawa Timur (Patokan depan balai desa winong)';
	$akad_venue_map = 'https://maps.app.goo.gl/Vh5iLqCqFQxo1SeH9';
	
	// Resepsi
	$wedding_fullday = '24 . 12 . 2023';
	$wedding_day = "Minggu";
	$wedding_date = '24';
	$wedding_date_show_duration = '1000';
	$wedding_month = 'Desember';
	$wedding_year = '2023';
	$wedding_hour = '13:00 - 16:00 WIB';
	$wedding_venue = 'Kediaman Mempelai Wanita';
	$wedding_venue_address = 'Desa Winong RT 001/ RW 001 no. 43, Kec. Kedungwaru, Kab. Tulungagung, Jawa Timur (Patokan depan balai desa winong)';
	$wedding_venue_map = 'https://maps.app.goo.gl/Vh5iLqCqFQxo1SeH9';
	
	// Countdown
	// $countdown_to_akad = '2022-07-03 08:00';
	$countdown_to_akad = '27/1/2023 08:00:00';
	
	// Gift
	$transfer_gift_user = '-';
	$transfer_gift_rekening = '-'; //Mandiri
	// $transfer_gift_user_2 = 'Games Yudha Siantoro';
	// $transfer_gift_rekening_2 = '0901711681';
	$send_gift_user = 'Dea';
	$send_gift_address = 'Desa Winong RT 001/ RW 001 no. 43, Kec. Kedungwaru, Kab. Tulungagung, Jawa Timur';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '081556564007';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by = 'https://instagram.com/trinetra.photography';
	$ig_photo_by_2 = 'https://instagram.com/finest.corporation';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/2r_ucRezz-g?si=UfqM4BIribhtkRgg';

	// Love Story
	$first_meet_date = '-';
	$first_meet_story = '-';
	$having_a_relationship_date = '-';
	$having_a_relationship_story = '-';
	$engagement_date = '-';
	$engagement_story = '-';
	$married_date = '24 Desember 2023';
	$married_story = 'Awal cerita dimulai di tahun 2008 saat masih di bangku SD, kami bertemu di sebuah bimbingan belajar di dekat rumah kami. Ya, kami adalah tetangga. Tetapi tidak akrab karena memang berbeda sekolah, jadi jarang sekali bertemu dan berbincang.
	<br><br>Setahun kemudian, pertemanan kami berlanjut di jenjang SMP. Tidak disangka, kami satu sekolah. Intensitas pertemuan membuat kami akrab. Dari mulai berangkat hingga pulang sekolah bersama naik angkutan umum, lalu dilanjutkan diskusi soal pelajaran sekolah melalui pesan singkat. Itu membuat kami semakin dekat.
	<br><br>Akhirnya tak terasa, kami lulus SMP di tahun 2012 dan memulai kehidupan remaja kami masing-masing karena sudah berbeda SMA. Bahkan berbeda kota. Disini kami mengira pertemanan ini sudah berakhir karena memang sudah tidak lagi ada kontak. Namun saat lulus SMA, kami saling menanyakan kabar masing-masing dan rencana akan melanjutkan kuliah di kota mana.
	<br><br>Dari titik ini keajaiban kisah cinta kami dimulai. Tahun 2015 menjadi titik awal kami menjalin hubungan lebih dari sekedar teman karena tidak sengaja kuliah di kota yang sama. Yang paling ajaib adalah hingga tahun 2023 (sekarang), kami masih bersama dengan hubungan yang lebih dekat lagi bahkan sampai di tempatkan di kantor yang sama dimana kami bekerja.
	<br><br>Di tahun kedelapan ini, kami tidak mau menolak takdir yang diberikan oleh Tuhan YME untuk masa depan kami. Kami percaya jika selama ini sudah cukup untuk kami diberikan waktu mengenal satu sama lain dengan segala kelebihan dan kekurangan yang ada. Kami memutuskan untuk melakukan ibadah terlama umat manusia yaitu menikah. Karena kami yakin, Tuhan YME sudah menakdirkan kami untuk bersama hingga akhir hayat. ';

	// Backsound
	$backsound = 'backsound/Cinta-Sejati.mp3';

	// Title
	$title = "$bride & $groom";

	// Colors
	$color_1 = '#800000';
	$color_2 = '#ffeecb';
	$color_3 = '#2e3321';
	$color_4 = '#6b7355';
	$color_5 = '#4e361438';


    $bg = "assets/photos/Repost-1.jpg";
    $img = "assets/photos/Repost-2.jpg";
?>
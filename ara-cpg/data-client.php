<?php
	// Pengantin Wanita
    $bride = 'Ratri';
	$bride_firstname = 'Nurillia';
	$bride_middlename = 'Ainia';
	$bride_lastname = 'Ratri';
	$bride_father = 'Wiwit Tri Subekti';
	$bride_mother = 'Winarsih';
	$bride_child_position = 'Putri Pertama';
	$bride_ig = 'ar.rtr';

	// Pengantin Pria
	$groom = 'Syaiful';
	$groom_firstname = 'Muhammad';
	$groom_middlename = 'Syaiful';
	$groom_lastname = 'Fuad';
	$groom_father = 'Imam Kateni';
	$groom_mother = 'Tarwiyatin';
	$groom_child_position = 'Putra Kedua';
	$groom_ig = 'syaifulfu';

	// Akad
	$akad_fullday = 'July 24, 2022';
	$akad_day = 'Minggu';
	$akad_date = '24';
	$akad_month = 'Juli';
	$akad_year = '2022';
	$akad_hour = '07:30 WIB';
	$akad_venue = 'Kediaman Mempelai Pria';
	$akad_venue_address = 'Dsn. Gondangsari RT.04 RW.02 Ds. Jabalsari, Kec. Sumbergempol, Kab. Tulungagung';
	$akad_venue_map = 'https://goo.gl/maps/MZ3vaBFaFR1fnHp79';
	
	// Resepsi
	$wedding_fullday = '24 . 07 . 2022';
	$wedding_day = 'Minggu';
	$wedding_date = '24';
	$wedding_month = 'Juli';
	$wedding_year = '2022';
	$wedding_hour = '13:00 WIB';
	$wedding_venue = 'Kediaman Mempelai Pria';
	$wedding_venue_address = 'Dsn. Gondangsari RT.04 RW.02 Ds. Jabalsari, Kec. Sumbergempol, Kab. Tulungagung';
	$wedding_venue_map = 'https://goo.gl/maps/MZ3vaBFaFR1fnHp79';
	
	// Countdown
	$countdown_to_akad = '2022-07-24 07:30';
	
	// Gift
	$transfer_gift_user = 'Muhammad Syaiful Fuad';
	$transfer_gift_rekening = '0895658391';
	$transfer_gift_user2 = 'Nurillia Ainia Ratri';
	$transfer_gift_rekening2 = '0481773163';
	$send_gift_user = 'Muhammad Syaiful Fuad';
	$send_gift_address = 'Dsn. Gondangsari RT.04 RW.02 Ds. Jabalsari, Kec. Sumbergempol, Kab. Tulungagung';
	$text_gift_confirm = "Haii..%20Selamat%20menikah%20yaa%20$bride%20dan%20$groom,%20oiya%20aku%20mau%20konfirmasi%20Amplop%20Digital%20nih..";
	$gift_confirmation_wa = '6285772567156';
	
	// Tamu
	$to = @$_GET['to'] ? $_GET['to'] : 'Nama Tamu';

	// Ayat
	$verse_name = '- Q.S. Ar-Rum : 21 -';
	$verse_value = '" Dan di antara tanda-tanda kekuasaan-Nya diciptakan-Nya untukmu pasangan hidup dari jenismu sendiri supaya kamu dapat ketenangan hati dan dijadikannya kasih sayang di antara kamu. Sesungguhnya yang demikian menjadi tanda-tanda kebesaran-Nya bagi orang-orang yang berpikir. "';
	
	// Photo by
	$ig_photo_by1 = 'trinetra.photography';
	$ig_photo_by2 = 'loisenses';
	$title_photo_by1 = 'Trinetra';
	$title_photo_by2 = 'Ara';

	// Youtube
	$youtube_streaming = 'https://www.youtube.com/channel/UCGjqq2SKLBNMCrG3YyTQIdw';
	$youtube_prewedding = 'https://youtu.be/W4d-zI77Rd0';

	// Love Story
	$first_meet_date = '1 Mei 2018';
	$first_meet_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
	$having_a_relationship_date = '1 Juni 2019';
	$having_a_relationship_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
	$engagement_date = '1 Juli 2020';
	$engagement_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
	$married_date = '1 Desember 2022';
	$married_story = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';

	// Backsound
	$backsound = 'Coldplay-Fix-You.mp3';

	// Title
	$title = "$groom & $bride";

	// Particle
	// $particle_color = '#D9A378';
	$particle_color = '#B59FB9';

	// Asset Theme
	$asset_theme = $base_url.'ara-cpg/assets/';

	// Backgrounds
	// Awal Belakang
	$background_1 = $asset_theme.'photos/Asset-Platinum-4-cover1.jpg';
	// Awal Kecil
	$background_2 = $asset_theme.'photos/Asset-Platinum-4-cover2.png';

	// Colors
	// $color_1 = '#B8743F';
	$color_1 = '#6E7D72';
	// $color_2 = '#673610';
	$color_2 = '#4A5950';
	// $color_3 = '#9C5620';
	$color_3 = '#6E7D72';
	// $color_4 = '#753809';
	$color_4 = '#738168';
	// $color_5 = '#4D2608';
	$color_5 = '#889173';




    $bg = "assets/photos/Asset-Platinum-4-cover1.jpg";
    $img = "assets/photos/Asset-Platinum-4-cover2.png";
?>
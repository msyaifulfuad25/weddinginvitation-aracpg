<?php

    $saying = $_GET['saying'];
    $by = $_GET['by'];
    
	include "data-sap.php";
	include "data-client.php";
	
    $bg = $base_url.$uri_couple.'/'.rawurlencode($_GET['bg']);
    $img = $base_url.$uri_couple.'/'.rawurlencode($_GET['img']);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $title ?></title>

  <link rel="stylesheet" href="../public/adminlte/adminlte.min.css">
  <link rel="stylesheet" href="../public/adminlte/fontawesome-free-6.2.1-web/css/all.min.css">


    <link rel="icon" href="<?= $asset ?>images/favicon.png" sizes="32x32" />
    <link rel="icon" href="<?= $asset ?>images/favicon.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="<?= $asset ?>images/favicon.png" />
    <meta name="msapplication-TileImage" content="<?= $asset ?>images/favicon.png" />
</head>
<body>
<div class="wrapper">
    <div class="row text-center">
        <div class="col-md-12">
            <div class="mt-2" style="margin: auto; background-image: url(<?=$bg?>);background-size:cover; width: 300px; height: 500px; border: 0px;" id="capture">
            
                <div style="background: rgba(0,0,0,0.5); width: 300px; height: 500px">
                    <div class="white-text text-center">
                        <div class="card-block">
    
    
                            <img src="<?=$img?>" style="height: 150px; margin-top: 40px; left:75px;">
                            
                            <h3 style="width: 300px; top: 200px; left:100px; margin-top: 15px; margin-bottom: 0; color: white; font-family: Times New Roman"><?=$bride?> & <?=$groom?></h3>
    
                            <small style="color: white;"><?=$wedding_fullday?></small>
    
                            <div style="width: 300px; margin-top: 30px; bottom: 80px">
                                <section>
                                    <div class="container">
                                    <div class="row d-flex align-items-center">
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
                                        <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10 col-10">
                                        <div class="bg-white p-3" style="border-left: .25rem solid <?= '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT) ?>; border-radius: 0 5px 5px 0">
                                            <blockquote class="blockquote p-0 m-0" style="border-left: 0">
                                            <p style="font-size: 10px" class="text-left">
                                            <?=$saying?>
                                            </p>
                                            </blockquote>
                                            <figcaption class="blockquote-footer mt-2 mb-0 font-italic text-left" style="font-size: 10px">
                                            <?=$by?>
                                            </figcaption>
                                        </div>
                                        </div>
                                        <div class="col-xl-1 col-lg-1 col-md-1 col-sm-1 col-1"></div>
                                    </div>
                                    </div>
                                </section>
        
                                <p class="text-center" style="width: 300px; font-size: 8px; font-family: Times New Roman; color: #bcbcbc">arainvitation.online</p>
                            
                            </div>
    
    
                        </div>
                    </div>
                </div>

            </div>

            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
            <button type="button" class="btn btn-success mt-2" id="btnPost" onclick="post(`${'<?=$bride?>'} & ${'<?=$groom?>'}`)"><i class="fa fa-download"></i> Download and share to your story :)</button>
        </div>
    </div>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
    <script src="../public/adminlte/jquery.min.js"></script>
    <script>
            
            $(document).ready(() => {
              getData()
            })
            
            function getData() {
              $('#table-applicant').DataTable().destroy();
              mytable = $('#table-applicant').DataTable({
                'pageLength': 10,
            'searching': true,
            'processing': true,
            'serverSide': true,
            'bInfo': true,
            'bFilter': false,
            'bLengthChange': false,
            'order': [[1, 'ASC']],
            'columns': [
              { mData: 'no', searchable: false, orderable: false, class: 'text-right' },
              { mData: 'username' },
              { mData: 'action', orderable: false },
            ],
            'ajax': {
              'url': '/api/user',
              'type': 'GET',
              'dataType':'json',
              'data': {
                '_token': $('meta[name="csrf-token"]').attr('content'),
              },
            }
          })
        }
    </script>

    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    <script>
        $(document).ready(function() {
            // post(`${"<?=$bride?>"} & ${"<?=$groom?>"}`)
        })

      function preview() {
        $('#modal-preview').modal('show')
      }

      function post(pictureName="Ara Invitation") {
        $('#btnPost').text('Downloading...')
    
        const captureElement = document.querySelector('#capture')
        html2canvas(captureElement)
          .then(canvas => {
            canvas.style.display = 'none'
            document.body.appendChild(canvas)
            return canvas
          })
          .then(canvas => {
            const image = canvas.toDataURL('image/png').replace('image/png', 'image/octet-stream')
            const a = document.createElement('a')
            a.setAttribute('download', pictureName+'.jpg')
            a.setAttribute('href', image)
            a.click()
            canvas.remove()

            $('#btnPost').text('Downloaded')
          })
      }
    </script>
</body>
</html>